﻿namespace OrderDespatcher
{
    partial class frmWeightCheck
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWeightCheck));
            this.gpbAuthorisation = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAuthoriseLogin = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAuthorisePassword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnAuthorise = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAmend = new System.Windows.Forms.Button();
            this.gpbAuthorisation.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpbAuthorisation
            // 
            this.gpbAuthorisation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpbAuthorisation.Controls.Add(this.btnAuthorise);
            this.gpbAuthorisation.Controls.Add(this.label3);
            this.gpbAuthorisation.Controls.Add(this.txtAuthorisePassword);
            this.gpbAuthorisation.Controls.Add(this.label2);
            this.gpbAuthorisation.Controls.Add(this.txtAuthoriseLogin);
            this.gpbAuthorisation.Controls.Add(this.label1);
            this.gpbAuthorisation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbAuthorisation.Location = new System.Drawing.Point(16, 115);
            this.gpbAuthorisation.Name = "gpbAuthorisation";
            this.gpbAuthorisation.Size = new System.Drawing.Size(507, 125);
            this.gpbAuthorisation.TabIndex = 0;
            this.gpbAuthorisation.TabStop = false;
            this.gpbAuthorisation.Text = "Authorisation";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(422, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Please get a Warehouse Manager or any other authorised person to authorise this o" +
    "rder.";
            // 
            // txtAuthoriseLogin
            // 
            this.txtAuthoriseLogin.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtAuthoriseLogin.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAuthoriseLogin.Location = new System.Drawing.Point(142, 49);
            this.txtAuthoriseLogin.Name = "txtAuthoriseLogin";
            this.txtAuthoriseLogin.Size = new System.Drawing.Size(188, 20);
            this.txtAuthoriseLogin.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(100, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Login:";
            // 
            // txtAuthorisePassword
            // 
            this.txtAuthorisePassword.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtAuthorisePassword.Location = new System.Drawing.Point(142, 76);
            this.txtAuthorisePassword.Name = "txtAuthorisePassword";
            this.txtAuthorisePassword.Size = new System.Drawing.Size(188, 20);
            this.txtAuthorisePassword.TabIndex = 3;
            this.txtAuthorisePassword.UseSystemPasswordChar = true;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(80, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Password:";
            // 
            // btnAuthorise
            // 
            this.btnAuthorise.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnAuthorise.Location = new System.Drawing.Point(357, 76);
            this.btnAuthorise.Name = "btnAuthorise";
            this.btnAuthorise.Size = new System.Drawing.Size(75, 23);
            this.btnAuthorise.TabIndex = 5;
            this.btnAuthorise.Text = "Authorise";
            this.btnAuthorise.UseVisualStyleBackColor = true;
            this.btnAuthorise.Click += new System.EventHandler(this.btnAuthorise_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(507, 67);
            this.label4.TabIndex = 1;
            this.label4.Text = resources.GetString("label4.Text");
            // 
            // btnAmend
            // 
            this.btnAmend.Location = new System.Drawing.Point(205, 86);
            this.btnAmend.Name = "btnAmend";
            this.btnAmend.Size = new System.Drawing.Size(97, 23);
            this.btnAmend.TabIndex = 2;
            this.btnAmend.Text = "OK";
            this.btnAmend.UseVisualStyleBackColor = true;
            this.btnAmend.Click += new System.EventHandler(this.btnAmend_Click);
            // 
            // frmWeightCheck
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(532, 252);
            this.Controls.Add(this.btnAmend);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.gpbAuthorisation);
            this.Name = "frmWeightCheck";
            this.Text = "Please Check Weight!";
            this.gpbAuthorisation.ResumeLayout(false);
            this.gpbAuthorisation.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpbAuthorisation;
        private System.Windows.Forms.Button btnAuthorise;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtAuthorisePassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAuthoriseLogin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnAmend;
    }
}