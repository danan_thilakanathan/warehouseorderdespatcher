﻿namespace OrderDespatcher.Courier
{
    partial class frmOrderProcessed
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblArea = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.txtInstructions = new System.Windows.Forms.TextBox();
            this.btnPrintLabel = new System.Windows.Forms.Button();
            this.btnPrintPackingSlip = new System.Windows.Forms.Button();
            this.btnPrintDG = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDespatchID = new System.Windows.Forms.TextBox();
            this.btnPrinterSettings = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Done!";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(47, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(228, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Please Move Order To";
            // 
            // lblArea
            // 
            this.lblArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblArea.AutoSize = true;
            this.lblArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArea.Location = new System.Drawing.Point(74, 123);
            this.lblArea.Name = "lblArea";
            this.lblArea.Size = new System.Drawing.Size(266, 42);
            this.lblArea.TabIndex = 2;
            this.lblArea.Text = "Despatch Area";
            // 
            // btnOk
            // 
            this.btnOk.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Location = new System.Drawing.Point(132, 399);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(118, 48);
            this.btnOk.TabIndex = 3;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // txtInstructions
            // 
            this.txtInstructions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInstructions.Location = new System.Drawing.Point(35, 281);
            this.txtInstructions.Multiline = true;
            this.txtInstructions.Name = "txtInstructions";
            this.txtInstructions.Size = new System.Drawing.Size(310, 100);
            this.txtInstructions.TabIndex = 4;
            // 
            // btnPrintLabel
            // 
            this.btnPrintLabel.Location = new System.Drawing.Point(35, 202);
            this.btnPrintLabel.Name = "btnPrintLabel";
            this.btnPrintLabel.Size = new System.Drawing.Size(95, 45);
            this.btnPrintLabel.TabIndex = 5;
            this.btnPrintLabel.Text = "Print Label";
            this.btnPrintLabel.UseVisualStyleBackColor = true;
            this.btnPrintLabel.Click += new System.EventHandler(this.btnPrintLabel_Click);
            // 
            // btnPrintPackingSlip
            // 
            this.btnPrintPackingSlip.Location = new System.Drawing.Point(136, 202);
            this.btnPrintPackingSlip.Name = "btnPrintPackingSlip";
            this.btnPrintPackingSlip.Size = new System.Drawing.Size(105, 45);
            this.btnPrintPackingSlip.TabIndex = 6;
            this.btnPrintPackingSlip.Text = "Print Packing Slip";
            this.btnPrintPackingSlip.UseVisualStyleBackColor = true;
            this.btnPrintPackingSlip.Click += new System.EventHandler(this.btnPrintPackingSlip_Click);
            // 
            // btnPrintDG
            // 
            this.btnPrintDG.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrintDG.Location = new System.Drawing.Point(247, 202);
            this.btnPrintDG.Name = "btnPrintDG";
            this.btnPrintDG.Size = new System.Drawing.Size(98, 45);
            this.btnPrintDG.TabIndex = 7;
            this.btnPrintDG.Text = "Print DG Form";
            this.btnPrintDG.UseVisualStyleBackColor = true;
            this.btnPrintDG.Click += new System.EventHandler(this.btnPrintDG_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(190, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "Despatch ID:";
            // 
            // txtDespatchID
            // 
            this.txtDespatchID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDespatchID.Location = new System.Drawing.Point(274, 10);
            this.txtDespatchID.Name = "txtDespatchID";
            this.txtDespatchID.ReadOnly = true;
            this.txtDespatchID.Size = new System.Drawing.Size(100, 20);
            this.txtDespatchID.TabIndex = 9;
            // 
            // btnPrinterSettings
            // 
            this.btnPrinterSettings.Location = new System.Drawing.Point(247, 252);
            this.btnPrinterSettings.Name = "btnPrinterSettings";
            this.btnPrinterSettings.Size = new System.Drawing.Size(98, 23);
            this.btnPrinterSettings.TabIndex = 10;
            this.btnPrinterSettings.Text = "Printer Settings";
            this.btnPrinterSettings.UseVisualStyleBackColor = true;
            this.btnPrinterSettings.Click += new System.EventHandler(this.btnPrinterSettings_Click);
            // 
            // frmOrderProcessed
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 458);
            this.Controls.Add(this.btnPrinterSettings);
            this.Controls.Add(this.txtDespatchID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnPrintDG);
            this.Controls.Add(this.btnPrintPackingSlip);
            this.Controls.Add(this.btnPrintLabel);
            this.Controls.Add(this.txtInstructions);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.lblArea);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmOrderProcessed";
            this.Text = "Order Processed";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblArea;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TextBox txtInstructions;
        private System.Windows.Forms.Button btnPrintLabel;
        private System.Windows.Forms.Button btnPrintPackingSlip;
        private System.Windows.Forms.Button btnPrintDG;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDespatchID;
        private System.Windows.Forms.Button btnPrinterSettings;
    }
}