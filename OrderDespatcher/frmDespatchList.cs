﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OrderDespatcher
{
    public partial class frmDespatchList : Form
    {
        public string despatchID = "-1";

        public frmDespatchList()
        {
            InitializeComponent();
        }

        public frmDespatchList(string orderNo)
        {
            InitializeComponent();

            dgrPreviousDespatch.DataSource = OrderDespatchService.GetAllDespatches(orderNo.Trim());
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            if (dgrPreviousDespatch.SelectedRows.Count == 1)
                this.despatchID = dgrPreviousDespatch.SelectedRows[0].Cells["despatchID"].Value.ToString();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
