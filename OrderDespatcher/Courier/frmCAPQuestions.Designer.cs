﻿namespace OrderDespatcher.Courier
{
    partial class frmCAPQuestions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxQuestion1 = new System.Windows.Forms.GroupBox();
            this.cboCustomerCloseToday = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbxQuestion2 = new System.Windows.Forms.GroupBox();
            this.rdoNo = new System.Windows.Forms.RadioButton();
            this.rdoYes = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.gbxQuestion3 = new System.Windows.Forms.GroupBox();
            this.dtpNextAvailableDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbxQuestion4 = new System.Windows.Forms.GroupBox();
            this.cboCustomerCloseAdvanceTime = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.gbxQuestion1.SuspendLayout();
            this.gbxQuestion2.SuspendLayout();
            this.gbxQuestion3.SuspendLayout();
            this.gbxQuestion4.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxQuestion1
            // 
            this.gbxQuestion1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxQuestion1.Controls.Add(this.cboCustomerCloseToday);
            this.gbxQuestion1.Controls.Add(this.label1);
            this.gbxQuestion1.Location = new System.Drawing.Point(12, 41);
            this.gbxQuestion1.Name = "gbxQuestion1";
            this.gbxQuestion1.Size = new System.Drawing.Size(520, 116);
            this.gbxQuestion1.TabIndex = 0;
            this.gbxQuestion1.TabStop = false;
            this.gbxQuestion1.Text = "Question #1:";
            // 
            // cboCustomerCloseToday
            // 
            this.cboCustomerCloseToday.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboCustomerCloseToday.FormattingEnabled = true;
            this.cboCustomerCloseToday.Location = new System.Drawing.Point(337, 68);
            this.cboCustomerCloseToday.Name = "cboCustomerCloseToday";
            this.cboCustomerCloseToday.Size = new System.Drawing.Size(168, 21);
            this.cboCustomerCloseToday.TabIndex = 2;
            this.cboCustomerCloseToday.SelectedIndexChanged += new System.EventHandler(this.cboCustomerCloseToday_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(423, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "What time does this customer close today?";
            // 
            // gbxQuestion2
            // 
            this.gbxQuestion2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxQuestion2.Controls.Add(this.rdoNo);
            this.gbxQuestion2.Controls.Add(this.rdoYes);
            this.gbxQuestion2.Controls.Add(this.label2);
            this.gbxQuestion2.Location = new System.Drawing.Point(15, 163);
            this.gbxQuestion2.Name = "gbxQuestion2";
            this.gbxQuestion2.Size = new System.Drawing.Size(517, 119);
            this.gbxQuestion2.TabIndex = 0;
            this.gbxQuestion2.TabStop = false;
            this.gbxQuestion2.Text = "Question #2:";
            // 
            // rdoNo
            // 
            this.rdoNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rdoNo.AutoSize = true;
            this.rdoNo.Checked = true;
            this.rdoNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdoNo.Location = new System.Drawing.Point(455, 75);
            this.rdoNo.Name = "rdoNo";
            this.rdoNo.Size = new System.Drawing.Size(47, 24);
            this.rdoNo.TabIndex = 3;
            this.rdoNo.TabStop = true;
            this.rdoNo.Text = "No";
            this.rdoNo.UseVisualStyleBackColor = true;
            this.rdoNo.CheckedChanged += new System.EventHandler(this.rdoNo_CheckedChanged);
            // 
            // rdoYes
            // 
            this.rdoYes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rdoYes.AutoSize = true;
            this.rdoYes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdoYes.Location = new System.Drawing.Point(334, 75);
            this.rdoYes.Name = "rdoYes";
            this.rdoYes.Size = new System.Drawing.Size(55, 24);
            this.rdoYes.TabIndex = 2;
            this.rdoYes.Text = "Yes";
            this.rdoYes.UseVisualStyleBackColor = true;
            this.rdoYes.CheckedChanged += new System.EventHandler(this.rdoYes_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(490, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "Is the Order Urgent (Needs to be sent out today) ?";
            // 
            // gbxQuestion3
            // 
            this.gbxQuestion3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxQuestion3.Controls.Add(this.dtpNextAvailableDate);
            this.gbxQuestion3.Controls.Add(this.label3);
            this.gbxQuestion3.Location = new System.Drawing.Point(15, 288);
            this.gbxQuestion3.Name = "gbxQuestion3";
            this.gbxQuestion3.Size = new System.Drawing.Size(517, 146);
            this.gbxQuestion3.TabIndex = 0;
            this.gbxQuestion3.TabStop = false;
            this.gbxQuestion3.Text = "Question #3:";
            this.gbxQuestion3.Visible = false;
            // 
            // dtpNextAvailableDate
            // 
            this.dtpNextAvailableDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpNextAvailableDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNextAvailableDate.Location = new System.Drawing.Point(279, 95);
            this.dtpNextAvailableDate.Name = "dtpNextAvailableDate";
            this.dtpNextAvailableDate.Size = new System.Drawing.Size(229, 20);
            this.dtpNextAvailableDate.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(437, 50);
            this.label3.TabIndex = 0;
            this.label3.Text = "When is the next earliest available date and \r\ntime to book the order?";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(57, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(436, 20);
            this.label4.TabIndex = 1;
            this.label4.Text = "Please refer to the DD for answers (or call OE staff if unsure)";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmit.Location = new System.Drawing.Point(136, 581);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(133, 56);
            this.btnSubmit.TabIndex = 2;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Visible = false;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(275, 581);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(133, 56);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gbxQuestion4
            // 
            this.gbxQuestion4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxQuestion4.Controls.Add(this.cboCustomerCloseAdvanceTime);
            this.gbxQuestion4.Controls.Add(this.label5);
            this.gbxQuestion4.Location = new System.Drawing.Point(15, 440);
            this.gbxQuestion4.Name = "gbxQuestion4";
            this.gbxQuestion4.Size = new System.Drawing.Size(517, 135);
            this.gbxQuestion4.TabIndex = 0;
            this.gbxQuestion4.TabStop = false;
            this.gbxQuestion4.Text = "Question #4:";
            this.gbxQuestion4.Visible = false;
            // 
            // cboCustomerCloseAdvanceTime
            // 
            this.cboCustomerCloseAdvanceTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboCustomerCloseAdvanceTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerCloseAdvanceTime.FormattingEnabled = true;
            this.cboCustomerCloseAdvanceTime.Location = new System.Drawing.Point(330, 73);
            this.cboCustomerCloseAdvanceTime.Name = "cboCustomerCloseAdvanceTime";
            this.cboCustomerCloseAdvanceTime.Size = new System.Drawing.Size(168, 21);
            this.cboCustomerCloseAdvanceTime.TabIndex = 2;
            this.cboCustomerCloseAdvanceTime.SelectedIndexChanged += new System.EventHandler(this.cboCustomerCloseToday_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(426, 25);
            this.label5.TabIndex = 0;
            this.label5.Text = "What time will the customer close that day?";
            // 
            // frmCAPQuestions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 649);
            this.Controls.Add(this.gbxQuestion2);
            this.Controls.Add(this.gbxQuestion4);
            this.Controls.Add(this.gbxQuestion3);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.gbxQuestion1);
            this.Name = "frmCAPQuestions";
            this.Text = "Capital Booking Questions";
            this.gbxQuestion1.ResumeLayout(false);
            this.gbxQuestion1.PerformLayout();
            this.gbxQuestion2.ResumeLayout(false);
            this.gbxQuestion2.PerformLayout();
            this.gbxQuestion3.ResumeLayout(false);
            this.gbxQuestion3.PerformLayout();
            this.gbxQuestion4.ResumeLayout(false);
            this.gbxQuestion4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxQuestion1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbxQuestion2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gbxQuestion3;
        private System.Windows.Forms.DateTimePicker dtpNextAvailableDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.RadioButton rdoNo;
        private System.Windows.Forms.RadioButton rdoYes;
        private System.Windows.Forms.ComboBox cboCustomerCloseToday;
        private System.Windows.Forms.GroupBox gbxQuestion4;
        private System.Windows.Forms.ComboBox cboCustomerCloseAdvanceTime;
        private System.Windows.Forms.Label label5;
    }
}