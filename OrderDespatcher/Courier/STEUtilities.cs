﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using Livingstone.Library;
using JsonPrettyPrinterPlus;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;


namespace OrderDespatcher.Courier
{
    class STEUtilities
    {

        public string Company { get; set; }
        public string Warehouse { get; set; }
        public string Courier { get; set; }
        public string API_KEY { get; set; }
        public string API_PWD { get; set; }
        public string API_HOST { get; set; }
        public string API_SCHEME { get; set; }
        public int API_PORT { get; set; }
        public string API_BASE_URL { get; set; }
        public string ACCOUNT_NO { get; set; }


        private BackgroundWorker BackgroundWorker = new BackgroundWorker();


        public STEUtilities()
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            ServicePointManager.DefaultConnectionLimit = 9999;
        }

        public STEUtilities(string api_key, string api_pwd, string api_host, string account_no, string api_scheme, int api_port, string api_base_url): this()
        {
            this.API_KEY = api_key;
            this.API_PWD = api_pwd;
            this.API_HOST = api_host;
            this.API_SCHEME = api_scheme;
            this.API_PORT = api_port;
            this.API_BASE_URL = api_base_url;
            this.ACCOUNT_NO = account_no;
        }


        public STEUtilities(string company, string warehouse, string courier)
        {
            UpdateAPICredentials(company, warehouse, courier);

        }

        private void UpdateAPICredentials(string company, string warehouse, string courier)
        {
            object[,] xargs = { 
                { "@company", company }, 
                { "@warehouse", warehouse }, 
                { "@courier", courier } 
            };

            DataTable cred_dt = DataSource.GetTableFrom(@"
                select * from tblWhAusPostDespatch_Settings 
                where Company = @company and Warehouse = @warehouse and Courier = @courier
            ", "NetCRMAU", xargs);

            if (cred_dt != null && cred_dt.Rows.Count > 0)
            {
                this.Company = company;
                this.Warehouse = warehouse;
                this.Courier = courier;
                this.API_KEY = cred_dt.Rows[0]["API_KEY"].ToString().Trim();
                this.API_PWD = cred_dt.Rows[0]["API_PWD"].ToString().Trim();
                this.API_HOST = cred_dt.Rows[0]["API_HOST"].ToString().Trim();
                this.API_SCHEME = cred_dt.Rows[0]["API_SCHEME"].ToString().Trim();
                this.API_PORT = Int32.Parse(cred_dt.Rows[0]["API_PORT"].ToString().Trim());
                this.API_BASE_URL = cred_dt.Rows[0]["API_BASE_URL"].ToString().Trim();
                this.ACCOUNT_NO = cred_dt.Rows[0]["ACCOUNT_NO"].ToString().Trim();
            }
        }


        /****** RESPONSE OBJECTS ***********/

        //CREATE SHIPMENT OBJECTS

        public class CS_TrackingDetails
        {
            public string article_id { get; set; }
            public string consignment_id { get; set; }
        }

        public class CS_ItemSummary
        {
            public double total_cost { get; set; }
            public double total_gst { get; set; }
            public string status { get; set; }
        }

        public class CS_Item
        {
            public string item_id { get; set; }
            public string item_reference { get; set; }
            public CS_TrackingDetails tracking_details { get; set; }
            public string product_id { get; set; }
            public CS_ItemSummary item_summary { get; set; }
        }

        public class CS_TrackingSummary
        {
            public int Initiated { get; set; }
        }

        public class CS_ShipmentSummary
        {
            public double total_cost { get; set; }
            public double total_gst { get; set; }
            public string status { get; set; }
            public int number_of_items { get; set; }
            public CS_TrackingSummary tracking_summary { get; set; }
        }

        public class CS_Shipment
        {
            public string shipment_id { get; set; }
            public string shipment_reference { get; set; }
            public DateTime shipment_creation_date { get; set; }
            public string customer_reference_1 { get; set; }
            public string customer_reference_2 { get; set; }
            public List<string> sender_references { get; set; }
            public bool email_tracking_enabled { get; set; }
            public List<CS_Item> items { get; set; }
            public CS_ShipmentSummary shipment_summary { get; set; }
        }

        public class CreateShipmentsResponse
        {
            public List<CS_Shipment> shipments { get; set; }
        }



        //CREATE LABEL OBJECTS


        public class CL_Item
        {
            public string item_id { get; set; }
        }

        public class CL_Shipment
        {
            public string shipment_id { get; set; }
            public List<CL_Item> items { get; set; }
        }

        public class CL_Label
        {
            public string request_id { get; set; }
            public string status { get; set; }
            public string request_date { get; set; }
            public List<CL_Shipment> shipments { get; set; }
            public List<string> shipment_ids { get; set; }
        }

        public class CreateLabelsResponse
        {
            public string message { get; set; }
            public string code { get; set; }
            public List<CL_Label> labels { get; set; }
        }



        //GET LABEL OBJECTS

        public class GL_Shipment
        {
            public string shipment_id { get; set; }
        }

        public class GL_Label
        {
            public string __invalid_name__request_id { get; set; }
            public string url { get; set; }
            public string status { get; set; }
            public string request_date { get; set; }
            public List<GL_Shipment> shipments { get; set; }
            public string url_creation_date { get; set; }
            public List<string> shipment_ids { get; set; }
        }

        public class GetLabelsResponse
        {
            public List<GL_Label> labels { get; set; }
        }



        //CREATE ORDER OBJECTS

        public class CO_TrackingSummary
        {
            public int Initiated { get; set; }
        }

        public class CO_OrderSummary
        {
            public double total_cost { get; set; }
            public double total_gst { get; set; }
            public string status { get; set; }
            public CO_TrackingSummary tracking_summary { get; set; }
            public int number_of_shipments { get; set; }
        }

        public class CO_TrackingSummary2
        {
            public int Sealed { get; set; }
        }

        public class CO_ShipmentSummary
        {
            public double total_cost { get; set; }
            public double total_gst { get; set; }
            public string status { get; set; }
            public CO_TrackingSummary2 tracking_summary { get; set; }
            public int number_of_items { get; set; }
        }

        public class CO_Shipment
        {
            public string shipment_id { get; set; }
            public bool email_tracking_enabled { get; set; }
            public CO_ShipmentSummary shipment_summary { get; set; }
        }

        public class CO_Order
        {
            public string order_id { get; set; }
            public string order_reference { get; set; }
            public DateTime order_creation_date { get; set; }
            public CO_OrderSummary order_summary { get; set; }
            public List<CO_Shipment> shipments { get; set; }
            public string payment_method { get; set; }
        }

        public class CreateOrderResponse
        {
            public CO_Order order { get; set; }
        }


        //GET ORDER OBJECTS
        //none


        /************* REQUEST OBJECTS ****************/

        /// <summary>
        /// Holds an Australia address for communicating with the API.
        /// </summary>
        [DataContract]
        public class Address
        {
            [DataMember]
            public string name { get; set; }
            [DataMember]
            protected List<string> lines { get; set; }
            [DataMember]
            public string suburb { get; set; }
            [DataMember]
            public string state { get; set; }
            [DataMember]
            public string postcode { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public string phone { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public List<string> delivery_instructions { get; set; }

            /// <summary>
            /// Constructor; prepares the address for use.
            /// </summary>
            public Address()
            {
                lines = new List<string>();
                delivery_instructions = new List<string>();
            }

            /// <summary>
            /// Adds a line to the address.
            /// </summary>
            /// <param name="s_line">The line to add.</param>
            public void addLine(string s_line)
            {
                // Restrict the number of lines that can be added
                if (lines.Count > 3) return;

                if (s_line.Length > 40) s_line = s_line.Substring(0, 40);

                lines.Add(s_line); // Restrict line length to 40 characters
            }

            public void addDeliveryInstruction(string deliveryInstruction)
            {
                if (deliveryInstruction.Length > 75) deliveryInstruction = deliveryInstruction.Substring(0, 75);

                for (int i = 0; i < deliveryInstruction.Length; i += 25)
                {
                    if ((i + 25) < deliveryInstruction.Length)
                        delivery_instructions.Add(deliveryInstruction.Substring(i, 25));
                    else
                        delivery_instructions.Add(deliveryInstruction.Substring(i));
                }
            }

        };



        /// <summary>
        /// Holds an item to be included in a shipment.
        /// </summary>
        [DataContract]
        public class Item
        {
            [DataMember]
            public string item_reference { get; set; }
            [DataMember]
            public string product_id { get; set; }
            [DataMember]
            public string length { get; set; }
            [DataMember]
            public string width { get; set; }
            [DataMember]
            public string height { get; set; }
            [DataMember]
            public string weight { get; set; }
            [DataMember]
            public Boolean authority_to_leave { get; set; }
            [DataMember]
            public string packaging_type { get; set; }
            [DataMember]
            public bool contains_dangerous_goods { get; set; }

            /// <summary>
            /// Constructor; set defaults for the object
            /// </summary>
            public Item()
            {
                // Set the product id based on the results from the GetAccounts call
                product_id = "EXP";
                authority_to_leave = false;
            }

            public Item(string packType)
            {
                packaging_type = packType;
            }
        };


        /// <summary>
        /// Holds a DG to be included in a shipment.
        /// </summary>
        [DataContract]
        public class DangerousGoods
        {
            [DataMember]
            public string un_number { get; set; }
            [DataMember]
            public string technical_name { get; set; }
            [DataMember]
            public string net_weight { get; set; }
            [DataMember]
            public string class_division { get; set; }
            [DataMember]
            public string outer_packaging_type { get; set; }
            [DataMember]
            public string outer_packaging_quantity { get; set; }

            /// <summary>
            /// Constructor; set defaults for the object
            /// </summary>
            public DangerousGoods()
            {

            }

        };

        /// <summary>
        /// Holds the data for a Create Shipments call.
        /// </summary>
        [DataContract]
        public class Shipment
        {
            [DataMember]
            string shipment_reference { get; set; }
            [DataMember]
            string customer_reference_1 { get; set; }
            [DataMember]
            bool consolidate { get; set; }
            [DataMember]
            Address from { get; set; }
            [DataMember]
            Address to { get; set; }
            [DataMember]
            public List<Item> items { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public DangerousGoods dangerous_goods { get; set; }


            /// <summary>
            /// Constructor; initializes the object.
            /// </summary>
            /// <param name="from_addr">The address to ship from.</param>
            /// <param name="to_addr">The address to ship to.</param>
            public Shipment(string shipment_reference, string customer_reference1, Address from_addr, Address to_addr)
            {
                this.shipment_reference = shipment_reference;
                customer_reference_1 = customer_reference1;
                from = from_addr;
                to = to_addr;
                this.consolidate = false;
            }

            /// <summary>
            /// Sets the list of items for the shipment.
            /// </summary>
            /// <param name="item_list">The list of items.</param>
            public void setItemList(List<Item> item_list)
            {
                items = item_list;
            }
            // TODO create an "addItem" method
        };

        /// <summary>
        /// Performs a "Create Shipments" API call.
        /// </summary>
        [DataContract]
        public class CreateShipments
        {
            /// <summary>
            /// The list of shipments to create.
            /// </summary>
            [DataMember]
            public List<Shipment> shipments;

            /// <summary>
            /// Constructor; initializes the object ready for use.
            /// </summary>
            public CreateShipments()
            {
                shipments = new List<Shipment>();
            }

            /// <summary>
            /// Sets the list of shipments to create.
            /// </summary>
            /// <param name="shipment_list"></param>
            public void setShipments(List<Shipment> shipment_list)
            {
                shipments = shipment_list;
            }


            /// <summary>
            /// Adds a shipment.
            /// </summary>
            /// <param name="shipment"></param>
            public void addShipment(Shipment shipment)
            {
                shipments.Add(shipment);
            }

        };




        /// <summary>
        /// Holds the data for a Create Labels call.
        /// </summary>
        [DataContract]
        public class Group
        {
            [DataMember]
            string group { get; set; }
            [DataMember]
            string layout { get; set; }
            [DataMember]
            bool branded { get; set; }
            [DataMember]
            int left_offset { get; set; }
            [DataMember]
            int top_offset { get; set; }

            /// <summary>
            /// Constructor; initializes the object.
            /// </summary>
            /// <param name="from_addr">The address to ship from.</param>
            /// <param name="to_addr">The address to ship to.</param>
            public Group(string groupName)
            {
                //shipment_reference = Guid.NewGuid().ToString();
                group = groupName;
                layout = "A6-1pp";
                branded = true;
                left_offset = 10;
                top_offset = 10;
            }

        };


        /// <summary>
        /// Holds the data for a Create Labels call.
        /// </summary>
        [DataContract]
        public class Preference
        {
            [DataMember]
            string type { get; set; }
            [DataMember]
            public List<Group> groups { get; set; }

            /// <summary>
            /// Constructor; initializes the object.
            /// </summary>
            /// <param name="from_addr">The address to ship from.</param>
            /// <param name="to_addr">The address to ship to.</param>
            public Preference()
            {
                type = "PRINT";
                groups = new List<Group>();
            }


            /// <summary>
            /// Sets the list of groups to create.
            /// </summary>
            /// <param name="shipment_list"></param>
            public void setGroup(List<Group> groupList)
            {
                groups = groupList;
            }

            /// <summary>
            /// Adds a group.
            /// </summary>
            /// <param name="shipment"></param>
            public void addGroup(Group group)
            {
                groups.Add(group);
            }

        };


        /// <summary>
        /// Holds the data for a Create Labels call.
        /// </summary>
        [DataContract]
        public class ShipmentItem
        {
            [DataMember]
            string shipment_id { get; set; }


            public ShipmentItem(string shipmentID)
            {
                shipment_id = shipmentID;
            }

        };

        /// <summary>
        /// Performs a "Create Labels" API call.
        /// </summary>
        [DataContract]
        public class CreateLabels
        {
            /// <summary>
            /// The list of preferences.
            /// </summary>
            [DataMember]
            public List<Preference> preferences;

            /// <summary>
            /// The list of shipment items.
            /// </summary>
            [DataMember]
            public List<ShipmentItem> shipments;

            /// <summary>
            /// Constructor; initializes the object ready for use.
            /// </summary>
            public CreateLabels()
            {
                preferences = new List<Preference>();
                shipments = new List<ShipmentItem>();
            }


            /// <summary>
            /// Sets the list of preferences to create.
            /// </summary>
            /// <param name="shipment_list"></param>
            public void setPreferences(List<Preference> preferenceList)
            {
                preferences = preferenceList;
            }

            /// <summary>
            /// Adds a preference.
            /// </summary>
            /// <param name="shipment"></param>
            public void addPreference(Preference preference)
            {
                preferences.Add(preference);
            }


            /// <summary>
            /// Sets the list of shipments to create.
            /// </summary>
            /// <param name="shipment_list"></param>
            public void setShipments(List<ShipmentItem> shipment_list)
            {
                shipments = shipment_list;
            }

            /// <summary>
            /// Adds a shipment.
            /// </summary>
            /// <param name="shipment"></param>
            public void addShipment(ShipmentItem shipment)
            {
                shipments.Add(shipment);
            }


        };



        /// <summary>
        /// Performs a "Create Labels" API call.
        /// </summary>
        [DataContract]
        public class CreateOrder
        {
            [DataMember]
            string order_reference { get; set; }
            [DataMember]
            string payment_method { get; set; }
            [DataMember]
            public List<ShipmentItem> shipments;

            /// <summary>
            /// Constructor; initializes the object ready for use.
            /// </summary>
            public CreateOrder(string orderRef)
            {
                order_reference = orderRef;
                payment_method = "CHARGE_TO_ACCOUNT";
                shipments = new List<ShipmentItem>();
            }


            /// <summary>
            /// Sets the list of shipments to create.
            /// </summary>
            /// <param name="shipment_list"></param>
            public void setShipments(List<ShipmentItem> shipment_list)
            {
                shipments = shipment_list;
            }

            /// <summary>
            /// Adds a shipment.
            /// </summary>
            /// <param name="shipment"></param>
            public void addShipment(ShipmentItem shipment)
            {
                shipments.Add(shipment);
            }


        };



        public class Context
        {
            public string shipment_id { get; set; }
        }

        public class Error
        {
            public string code { get; set; }
            public string name { get; set; }
            public string message { get; set; }
            public string field { get; set; }
            public Context context { get; set; }
        }

        public class AusPostError
        {
            public List<Error> errors { get; set; }
        }


        //HELPERS

        /// <summary>
        /// Generates test data for a "Create Labels" API call.
        /// </summary>
        /// <returns>CreateLabels object that can execute the API call</returns>
        public CreateLabels getShipmentLabels(string shipmentID)
        {

            Group group = new Group("Express Post");
            Preference preference = new Preference();
            preference.addGroup(group);
            List<Preference> preferences = new List<Preference>();
            preferences.Add(preference);

            ShipmentItem package = new ShipmentItem(shipmentID);
            List<ShipmentItem> packages = new List<ShipmentItem>();
            packages.Add(package);

            CreateLabels crlabel = new CreateLabels();

            crlabel.setPreferences(preferences);
            crlabel.setShipments(packages);

            return crlabel;
        }

        private string validateAddress(string delName, string delAddr1, string delAddr2, string delAddr3, string delSuburb, string delState, string delPostcode, string delCountry)
        {
            if (string.IsNullOrEmpty(delName) || delName.Length > 40)
            {
                MessageBox.Show("Delivery Name is mandatory and should only be upto 40 characters. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (delAddr1.Length > 40 || delAddr2.Length > 40 || delAddr3.Length > 40)
            {
                MessageBox.Show("Delivery Address lines should only be a maximum of 40 characters. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delAddr1) && string.IsNullOrEmpty(delAddr2) && string.IsNullOrEmpty(delAddr3))
            {
                MessageBox.Show("Delivery Address lines are mandatory. Please add the delivery address and try again.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delSuburb) || delSuburb.Length > 30)
            {
                MessageBox.Show("Delivery Suburb is mandatory and should only be upto 40 characters. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delPostcode) || delPostcode.Length > 10)
            {
                MessageBox.Show("Delivery Postcode should be upto 10 characters only. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delState) || delState.Length > 3)
            {
                MessageBox.Show("Delivery State should be upto 3 characters only. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delCountry) || delCountry.Length > 2)
            {
                MessageBox.Show("Delivery Country should be upto 2 characters only. Please fix first.");
                return "[NEED TO FIX]";
            }

            return "OK";
        }



        /**************HELPER FUNCTIONS************************/



        //Generates a connote number and prints connote label for this order
        public string GenerateConnoteNumber(string cusNo, string exactOrderNo, string despatchID, string userName, ref string userMessage)
        {

            //Get DESPATCH Details
            DataTable orderDetails = DataSource.GetTableFrom(@"
                    select *, convert(VARCHAR, orderDate, 112) as formattedOrderDate, convert(VARCHAR, etaDate, 112) as shippingDate 
                    from tblWhOrderDespatch
                    where orderNo = @orderNo and id = @despatchID   
                    order by ts desc
            ", "NetCRMAU", new object[,] { { "@orderNo", exactOrderNo.Trim() }, { "@despatchID", despatchID.Trim() } });

            DataTable packingDetails = DataSource.GetTableFrom(@"
                    select * from tblWhPackingDetails where orderNo = @orderNo and despatchID = @despatchID order by ts desc
            ", "NetCRMAU", new object[,] { { "@orderNo", exactOrderNo.Trim() }, { "@despatchID", despatchID.Trim() } });

            DataTable itemDetails = DataSource.GetTableFrom(@"
                    select * from tblWhPackedItems where orderNo = @orderNo and despatchID = @despatchID order by ts desc
            ", "NetCRMAU", new object[,] { { "@orderNo", exactOrderNo.Trim() }, { "@despatchID", despatchID.Trim() } });

            DataTable dangerousItemDeclaration = DataSource.GetTableFrom(@"
                    select di.despatchID, di.orderNo, di.itemNo, di.weight, di.qty, dg.* 
                    from tblWhPackingDetailsDangerousItems di
                    left join tblDangerousGoods dg
	                    on (di.itemNo = dg.Item_no)
                    where despatchID = @despatchID and orderNo = @orderNo
            ", "NetCRMAU", new object[,] { { "@orderNo", exactOrderNo.Trim() }, { "@despatchID", despatchID.Trim() } });

            //Check Connected
            bool Connected = ConnectCourier();

            if (Connected)
            {

                /****************** Create a Shipment **************/
                CreateShipments crship = new CreateShipments();
                getShipmentTestData();

                Address from = new Address();
                Address to = new Address();

                Shipment shipment = new Shipment(exactOrderNo, despatchID, from, to);
                List<Shipment> shipment_list = new List<Shipment>();

                if (orderDetails.Rows[0]["warehouse"].ToString() == "P" && cusNo.Substring(0, 2) != "U-")
                {
                    from.name = "LIVINGSTONE INTERNATIONAL";
                    from.addLine("1B/5-35 YARRUNGA ST");
                    from.suburb = "PRESTONS";
                    from.postcode = "2170";
                    from.state = "NSW";
                }
                else if (orderDetails.Rows[0]["warehouse"].ToString() == "R" && cusNo.Substring(0, 2) != "U-")
                {
                    from.name = "LIVINGSTONE INTERNATIONAL";
                    from.addLine("106-116 EPSOM ROAD");
                    from.suburb = "ROSEBERY";
                    from.postcode = "2018";
                    from.state = "NSW";
                }
                else if (orderDetails.Rows[0]["warehouse"].ToString() == "T" || cusNo.Substring(0, 2) == "U-")
                {
                    if (cusNo.Substring(0, 2) == "U-") from.name = "UNIVERSAL CHOICE";
                    else from.name = "LIVINGSTONE INTERNATIONAL";

                    from.addLine("127-139 TURRELLA ST");
                    from.suburb = "TURRELLA";
                    from.postcode = "2205";
                    from.state = "NSW";
                }
                else if (orderDetails.Rows[0]["warehouse"].ToString() == "V" && cusNo.Substring(0, 2) != "U-")
                {
                    from.name = "LIVINGSTONE INTERNATIONAL";
                    from.addLine("7 HILLWIN ST");
                    from.suburb = "RESERVOIR";
                    from.postcode = "3073";
                    from.state = "VIC";
                }
                else
                {
                    from.name = "LIVINGSTONE INTERNATIONAL";
                    from.addLine("1B/5-35 YARRUNGA ST");
                    from.suburb = "PRESTONS";
                    from.postcode = "2170";
                    from.state = "NSW";
                }

                //Address Validation
                string delName = orderDetails.Rows[0]["delName"].ToString();
                string delAddr1 = orderDetails.Rows[0]["delAddr1"].ToString();
                string delAddr2 = orderDetails.Rows[0]["delAddr2"].ToString();
                string delAddr3 = orderDetails.Rows[0]["delAddr3"].ToString();
                string delSuburb = orderDetails.Rows[0]["delSuburb"].ToString();
                string delState = orderDetails.Rows[0]["delState"].ToString();
                string delPostcode = orderDetails.Rows[0]["delPostcode"].ToString();
                string delCountry = orderDetails.Rows[0]["country"].ToString();

                string validAddress = validateAddress(delName, delAddr1, delAddr2, delAddr3, delSuburb, delState, delPostcode, delCountry);
                if (validAddress == "[NEED TO FIX]") return "[NEED TO FIX]";

                delName = delName.Replace(@"/", "");
                if (delName.Length > 40) delName = delName.Substring(0, 40);


                //Add Delivery Address
                to.name = delName.Trim();

                if (!string.IsNullOrEmpty(delAddr1))
                    to.addLine(delAddr1);
                if (!string.IsNullOrEmpty(delAddr2))
                    to.addLine(delAddr2);
                if (!string.IsNullOrEmpty(delAddr3))
                    to.addLine(delAddr3);

                to.suburb = delSuburb.Trim();
                to.postcode = delPostcode.Trim();
                to.state = delState.Trim();


                /********* Add items (CTN, PAL) to shipment ***********/
                List<Item> item_list = new List<Item>();

                bool shipmentContainsDG = false;

                foreach (DataRow dr in packingDetails.Rows)
                {
                    Item item = new Item();

                    //Item reference is of form orderNo_despatchID_id   (e.g, 3212447_5_34)
                    item.item_reference = dr["orderNo"].ToString().Trim() + "_" + dr["despatchID"].ToString().Trim() + "_" + dr["id"].ToString().Trim();
                    item.weight = Math.Round(double.Parse(dr["weight"].ToString().Trim()), 3).ToString();
                    item.height = Math.Round(double.Parse(dr["height"].ToString().Trim()), 1).ToString();
                    item.width = Math.Round(double.Parse(dr["width"].ToString().Trim()), 1).ToString();
                    item.length = Math.Round(double.Parse(dr["length"].ToString().Trim()), 1).ToString();
                    item.contains_dangerous_goods = (dr["isDG"].ToString().Trim() == "True" ? true : false);

                    string packingType = "CTN";
                    if (dr["type"].ToString().Trim() == "CARTON")
                    {
                        packingType = "CTN";
                    }
                    else if (dr["type"].ToString().Trim() == "PALLET")
                    {
                        packingType = "PAL";
                    }
                    else packingType = "CTN";

                    item.packaging_type = packingType;

                    if (item.contains_dangerous_goods)
                        shipmentContainsDG = true;

                    item_list.Add(item);

                    int qty = Int32.Parse(dr["qty"].ToString().Trim());
                    if (qty > 1)
                    {
                        for (int i = 1; i < qty; i++)
                        {
                            item_list.Add(item);
                        }
                    }
                }


                //Handle DG
                if (shipmentContainsDG && !string.IsNullOrEmpty(orderDetails.Rows[0]["connoteNo"].ToString().Trim()))
                {
                    if (dangerousItemDeclaration != null && dangerousItemDeclaration.Rows.Count > 0)
                    {

                        DangerousGoods dangerousGoods = new DangerousGoods();

                        //Get the first row DG
                        dangerousGoods.un_number = dangerousItemDeclaration.Rows[0]["UN_no"].ToString().Trim();
                        dangerousGoods.technical_name = dangerousItemDeclaration.Rows[0]["Proper_Shipping_name"].ToString().Trim();
                        dangerousGoods.net_weight = Math.Round(double.Parse(dangerousItemDeclaration.Rows[0]["weight"].ToString().Trim()), 5).ToString();
                        dangerousGoods.class_division = dangerousItemDeclaration.Rows[0]["Class"].ToString().Trim();
                        dangerousGoods.outer_packaging_type = dangerousItemDeclaration.Rows[0]["Packaging_Method"].ToString().Trim();
                        dangerousGoods.outer_packaging_quantity = dangerousItemDeclaration.Rows[0]["qty"].ToString().Trim();

                        shipment.dangerous_goods = dangerousGoods;
                    }


                }

                /******************** Create Shipment (Despatch Entry) and Extract Data from JSON *************************/
                shipment.setItemList(item_list);
                shipment_list.Add(shipment);
                crship.setShipments(shipment_list);


                //Check if Connote Already Exists. If so, allow option to update
                if (!string.IsNullOrEmpty(orderDetails.Rows[0]["connoteNo"].ToString().Trim()))
                {
                    if (MessageBox.Show("Connote Already Exists. Update this Connote?", "Connote Already Exists!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        try
                        {
                            UpdateShipment(orderDetails.Rows[0]["connoteNo"].ToString().Trim(), crship);

                            MessageBox.Show("Connote Updated!");

                        }
                        catch { }

                        return "[NEED TO FIX]";
                    }
                }


                //Create Shipment Request
                CreateShipmentsResponse csresp;
                try
                {
                    csresp = CreateShipment(crship);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Could not create STE shipment entry. Please check your inputs and try again.");
                    return "[NEED TO FIX]";
                }
                string shipmentID = csresp.shipments[0].shipment_id;
                string connoteNo = csresp.shipments[0].shipment_id;
                double totalCost = csresp.shipments[0].shipment_summary.total_cost;
                double totalGST = csresp.shipments[0].shipment_summary.total_gst;

                /******* Get connoteNo and estimated Cost and store to DB with despatchID reference (mark as OrderNotCreated) ****/
                //Create new shipment entry in table
                object[,] xargs = {
                    { "@orderNo", exactOrderNo.Trim() },
                    { "@despatchID", despatchID.Trim() },
                    { "@shipmentID", shipmentID },
                    { "@connoteNo", connoteNo },
                    { "@totalCost", totalCost.ToString() },
                    { "@totalGST", totalGST.ToString() },
                    { "@createdBy", userName }
                };

                DataTable dt = DataSource.GetTableFrom(@"
                    insert into tblWhAusPostDespatch (courier, despatchID, exactOrderNo, shipmentID, connoteNo, totalCost, totalGST, createdBy)
                    values ('STE', @despatchID, @orderNo, @shipmentID, @connoteNo, @totalCost, @totalGST, @createdBy)

                    select @@identity
                ", "NetCRMAU", xargs);

                string tableID = "";
                if (dt != null && dt.Rows.Count > 0)
                    tableID = dt.Rows[0][0].ToString().Trim();

                //                //Update connote no in Order Despatch table
                //                DataSource.ExecuteNonQuery(@"
                //                    update tblWhOrderDespatch
                //                    set connoteNo = @connoteNo
                //                    where id = @despatchID and orderNo = @orderNo
                //                ", "NetCRMAU", xargs);


                /*************************** Create a label from Shipment (Despatch Entry) *********************************/
                CreateLabels crlabel = getShipmentLabels(shipmentID);
                CreateLabelsResponse clresp = CreateCourierLabels(crlabel);
                string requestID = clresp.labels[0].request_id;

                //Store requestID to database with despatchID reference
                DataSource.ExecuteNonQuery(@"
                    update tblWhAusPostDespatch
                    set requestID = @requestID
                    where id = @tableID
                ", "NetCRMAU", new object[,] { { "@requestID", requestID }, { "@tableID", tableID } });


                /*************************** Get a label for the Shipment (Despatch Entry) *********************************/
                string status = "PENDING";
                string url = "";

                while (status != "AVAILABLE" && status != "ERROR")
                {
                    GetLabelsResponse glresp = GetCourierLabels(requestID);
                    url = glresp.labels[0].url;
                    status = glresp.labels[0].status;

                    MessageBox.Show("Message: " + clresp.message + " Status: " + status + " RequestID: " + requestID);
                }

                MessageBox.Show("URL: " + url);

                //Store url to despatchID in database
                DataSource.ExecuteNonQuery(@"
                    update tblWhAusPostDespatch
                    set labelURL = @url
                    where id = @tableID
                ", "NetCRMAU", new object[,] { { "@url", url }, { "@tableID", tableID } });


                string filePath = (string)DataSource.GetValue("select top 1 labelFilePath from tblWhOrderDespatch_Settings", "NetCRMAU");

                string labelFilePath = filePath + @"Label_STE_" + connoteNo.Trim() + ".pdf";

                using (WebClient webClient = new WebClient())
                {
                    webClient.DownloadFile(url, labelFilePath);
                }

                //Print the downloaded label
                PrintService printerService = new PrintService();
                printerService.PrintFile(labelFilePath);

                //PrintService.PrintFile(labelFilePath, "Kyocera-Up");

                return connoteNo;

            }
            else
            {
                MessageBox.Show("Could Not Connect to STE!");
                return "[NEED TO FIX]";
            }
        }


        public string GenerateCourierManifest()
        {
            //Check with user which shipmentIDs they need to despatch
            frmSTEDespatch steDespatch = new frmSTEDespatch(this.Courier, ACCOUNT_NO, this.Warehouse, this.Company);
            steDespatch.ShowDialog();

            if (steDespatch.DialogResult == DialogResult.OK)
            {
                List<ShipmentItem> shipments = new List<ShipmentItem>();

                DataTable shipmentList = steDespatch.shipmentList;

                foreach (DataRow dr in shipmentList.Rows)
                {
                    if (dr["includeInManifest"].ToString().Trim() == "True")
                    {
                        ShipmentItem shipment = new ShipmentItem(dr["shipmentID"].ToString().Trim());
                        shipments.Add(shipment);
                    }

                }


                //Create order from list of shipments
                CreateOrder crorder = new CreateOrder(this.Courier + ACCOUNT_NO + DateTime.Now.ToString("yyyyMMdd"));
                crorder.setShipments(shipments);

                try
                {

                    //Create Order
                    CreateOrderResponse coresp = CreateOrderFromShipments(crorder, this.Courier, System.DirectoryServices.AccountManagement.UserPrincipal.Current.DisplayName);
                    string order_id = coresp.order.order_id;


                    //Print manifest
                    string filePath = (string)DataSource.GetValue("select top 1 manifestFilePath from tblWhOrderDespatch_Settings", "NetCRMAU");

                    string fileName = filePath + @"ORD_" + order_id + ".pdf";
                    string result = GetOrderSummary(order_id, fileName);
                    MessageBox.Show(result);

                    //Print order using PrintService
                    PrintService printService = new PrintService();
                    printService.PrintFile(fileName);

                    object[,] xargs1 = {
                            { "@orderID", order_id.Trim() }
                        };

                    DataSource.ExecuteNonQuery(@"
                            update tblWhAusPostDespatchManifest
                            set isPrinted = 1
                            where orderID = @orderID
                        ", "NetCRMAU", xargs1);

                    //Delete file after printed? (Not for now)

                    MessageBox.Show("Manifest Generated and Printed to the selected A4 Printer");

                }
                catch
                {
                    MessageBox.Show("Error: Could not generate manifest. Please contact IT or inform OE to generate manifest manually.");
                }


            }
            else
            {
                return "";
            }

            return "Done!";
        }


        public void PrintManifest()
        {
             //Select order ID
            frmSTEOrders steOrder = new frmSTEOrders(ACCOUNT_NO.Trim());
            steOrder.ShowDialog();

            if (steOrder.DialogResult == DialogResult.OK)
            {
                string orderID = steOrder.selectedOrderID;


                //Print manifest

                string filePath = (string)DataSource.GetValue("select top 1 manifestFilePath from tblWhOrderDespatch_Settings", "NetCRMAU");

                string fileName = filePath + @"ORD_" + this.Courier + "_" + orderID + ".pdf";
                string result = GetOrderSummary(orderID, fileName);
                //MessageBox.Show(result);

                //Print order using PrintService
                PrintService printService = new PrintService();
                printService.PrintFile(fileName);

                object[,] xargs1 = {
                            { "@orderID", orderID.Trim() }
                        };

                DataSource.ExecuteNonQuery(@"
                            update tblWhAusPostDespatchManifest
                            set isPrinted = 1
                            where orderID = @orderID
                        ", "NetCRMAU", xargs1);

                //Delete file after printed?
            }
        }


        public bool UpdateConnoteNo(string despatchID, string connoteNo)
        {
            object[,] xargs = {
                { "@despatchID", despatchID.Trim() },
                { "@connoteNo", connoteNo.Trim() }
            };

            try
            {
                DataTable dt = DataSource.GetTableFrom(@"
                    update tblWhAusPostDespatch
                    set connoteNo = @connoteNo
                    where despatchID = @despatchID 

                    update tblWhOrderDespatch
                    set connoteNo = @connoteNo
                    where id = @despatchID
                ", "NetCRMAU", xargs);

                return true;
            }
            catch
            {
                return false;
            }
        }


        public void PrintLabel(string despatchID, string courier)
        {
            //Store requestID to database with despatchID reference
            DataTable dt = DataSource.GetTableFrom(@"
                    select requestID, connoteNo
                    from tblWhAusPostDespatch
                    where despatchID = @despatchID
                ", "NetCRMAU", new object[,] { { "@despatchID", despatchID.Trim() }});

            if (dt != null && dt.Rows.Count > 0)
            {
                string requestID = dt.Rows[0][0].ToString().Trim();
                string connoteNo = dt.Rows[0][1].ToString().Trim();

                /*************************** Get a label for the Shipment (Despatch Entry) *********************************/
                string status = "PENDING";
                string url = "";

                STEUtilities.GetLabelsResponse glresp = GetCourierLabels(requestID);
                url = glresp.labels[0].url;
                status = glresp.labels[0].status;

                if (status != "AVAILABLE")
                {
                    MessageBox.Show("Could not print. Status is " + status);
                    return;
                }

                //Store url to despatchID in database
                DataSource.ExecuteNonQuery(@"
                    update tblWhAusPostDespatch
                    set labelURL = @url
                    where despatchID = @despatchID
                ", "NetCRMAU", new object[,] { { "@url", url } , { "@despatchID", despatchID.Trim() } });


                string filePath = (string)DataSource.GetValue("select top 1 labelFilePath from tblWhOrderDespatch_Settings", "NetCRMAU");

                string labelFilePath = filePath + @"Label_" + courier + "_" + connoteNo.Trim() + ".pdf";

                using (WebClient webClient = new WebClient())
                {
                    webClient.DownloadFile(url, labelFilePath);
                }

                //Print the downloaded label
                PrintService printerService = new PrintService();
                printerService.PrintLabel(labelFilePath);

                //Delete file once printed
                if (File.Exists(labelFilePath))
                    File.Delete(labelFilePath);

            }
            else
            {
                MessageBox.Show("Something went wrong. Data not stored correctly in the database.");
            }


        }


        public void PrintLabel(string despatchID, string courier, string connoteNo)
        {
            //Store requestID to database with despatchID reference
            DataTable dt = DataSource.GetTableFrom(@"
                    select requestID
                    from tblWhAusPostDespatch
                    where despatchID = @despatchID and connoteNo = @connoteNo
                ", "NetCRMAU", new object[,] { { "@despatchID", despatchID.Trim() }, {"@connoteNo", connoteNo.Trim()} });

            if (dt != null && dt.Rows.Count == 1)
            {
                string requestID = dt.Rows[0][0].ToString().Trim();

                /*************************** Get a label for the Shipment (Despatch Entry) *********************************/
                string status = "PENDING";
                string url = "";

                STEUtilities.GetLabelsResponse glresp = GetCourierLabels(requestID);
                url = glresp.labels[0].url;
                status = glresp.labels[0].status;

                if (status != "AVAIALBLE")
                {
                    MessageBox.Show("Could not print. Status is " + status);
                    return;
                }

                //Store url to despatchID in database
                DataSource.ExecuteNonQuery(@"
                    update tblWhAusPostDespatch
                    set labelURL = @url
                    where despatchID = @despatchID
                ", "NetCRMAU", new object[,] { { "@despatchID", despatchID.Trim() } });


                string filePath = (string)DataSource.GetValue("select top 1 labelFilePath from tblWhOrderDespatch_Settings", "NetCRMAU");

                string labelFilePath = filePath + @"Label_" + courier + "_" + connoteNo.Trim() + ".pdf";

                using (WebClient webClient = new WebClient())
                {
                    webClient.DownloadFile(url, labelFilePath);
                }

                //Print the downloaded label
                PrintService printerService = new PrintService();
                printerService.PrintLabel(labelFilePath);

                if (File.Exists(labelFilePath))
                    File.Delete(labelFilePath);

            }
            else
            {
                MessageBox.Show("Something went wrong. Data not stored correctly in the database.");
            }


        }


        /************ MAIN API FUNCTIONS ********************/

        //GET
        public bool ConnectCourier()
        {

            string s_action = "accounts";
            string s_url = buildURL(s_action + "/" + ACCOUNT_NO);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(s_url);

            // Setup the main header lines for the request.
            request.ContentType = "application/json";
            request.Headers.Add("account-number", ACCOUNT_NO);
            request.Host = API_HOST;
            request.Accept = "*/*";

            string responseFromServer;
            string result = completeRequest(request, out responseFromServer);


            if (result.Contains("Unauthorized"))
                return false;

            return true;
        }


        //POST
        public CreateShipmentsResponse CreateShipment(CreateShipments crship)
        {
            string s_action = "shipments";
            string s_url = buildURL(s_action);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(s_url);

            // Setup the main header lines for the request.
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers.Add("account-number", ACCOUNT_NO);
            request.Host = API_HOST;
            request.Accept = "*/*";

            // Supply the request data to the server, including serializing the data into JSON.
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                MemoryStream ms = new MemoryStream();
                DataContractJsonSerializer dcser = new DataContractJsonSerializer(typeof(CreateShipments));

                dcser.WriteObject(ms, crship);

                ms.Position = 0;
                StreamReader sr = new StreamReader(ms);

                //System.Diagnostics.Debug.WriteLine("JSON:");
                //System.Diagnostics.Debug.WriteLine(sr.ReadToEnd().PrettyPrintJson());

                streamWriter.Write(sr.ReadToEnd());
                streamWriter.Flush();
                streamWriter.Close();
            }

            string responseFromServer;
            string result = completeRequest(request, out responseFromServer);

            if (responseFromServer != "Success")
            {
                try
                {
                    AusPostError AusPostResponse = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<AusPostError>(responseFromServer);
                    MessageBox.Show(AusPostResponse.errors[0].code + " message-" + AusPostResponse.errors[0].message + " name-" + AusPostResponse.errors[0].name);
                }
                catch { }
            }



            //MessageBox.Show(responseFromServer);

            
            
            
            
            
    
     CreateShipmentsResponse CSResponse = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<CreateShipmentsResponse>(result);

            try
            {
                //Store to DB
                object[,] xargs = {
                  { "@shipmentID", CSResponse.shipments[0].shipment_id},
                  { "@response", result.ToString() }
                };

                DataSource.ExecuteNonQuery(@"
                    insert into tblWhAusPostDespatch_data (shipmentID, createShipmentResponse)
                    values (@shipmentID, LEFT(@response,3000))
                ", "NetCRMAU", xargs);
            }
            catch { }

            return CSResponse;
        }


        //PUT
        public void UpdateShipment(string shipmentID, CreateShipments crship)
        {
            string s_action = "shipments";
            string s_url = buildURL(s_action) + @"/" + shipmentID;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(s_url);

            // Setup the main header lines for the request.
            request.Method = "PUT";
            request.ContentType = "application/json";
            request.Headers.Add("account-number", ACCOUNT_NO);
            request.Host = API_HOST;
            request.Accept = "*/*";

            // Supply the request data to the server, including serializing the data into JSON.
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                MemoryStream ms = new MemoryStream();
                DataContractJsonSerializer dcser = new DataContractJsonSerializer(typeof(CreateShipments));

                dcser.WriteObject(ms, crship);

                ms.Position = 0;
                StreamReader sr = new StreamReader(ms);

                //System.Diagnostics.Debug.WriteLine("JSON:");
                //System.Diagnostics.Debug.WriteLine(sr.ReadToEnd().PrettyPrintJson());

                streamWriter.Write(sr.ReadToEnd());
                streamWriter.Flush();
                streamWriter.Close();
            }

            string responseFromServer;
            string result = completeRequest(request, out responseFromServer);

        }


        //DELETE
        public bool DeleteShipment(string shipmentID)
        {
            string s_action = "shipments";
            string s_url = buildURL(s_action) + @"/" + shipmentID;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(s_url);

            // Setup the main header lines for the request.
            request.Method = "DELETE";
            request.ContentType = "application/json";
            request.Headers.Add("account-number", ACCOUNT_NO);
            request.Host = API_HOST;
            request.Accept = "*/*";

            string responseFromServer;
            string result = completeRequest(request, out responseFromServer);

            if (!string.IsNullOrEmpty(result))
                MessageBox.Show(result);
            else 
                return true;

            return false;

        }


        ////POST
        public CreateLabelsResponse CreateCourierLabels(CreateLabels crlabels)
        {
            string s_action = "labels";
            string s_url = buildURL(s_action);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(s_url);

            // Setup the main header lines for the request.
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers.Add("account-number", ACCOUNT_NO);
            request.Host = API_HOST;
            request.Accept = "*/*";

            // Supply the request data to the server, including serializing the data into JSON.
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                MemoryStream ms = new MemoryStream();
                DataContractJsonSerializer dcser = new DataContractJsonSerializer(typeof(CreateLabels));

                dcser.WriteObject(ms, crlabels);

                ms.Position = 0;
                StreamReader sr = new StreamReader(ms);

                //System.Diagnostics.Debug.WriteLine("JSON:");
                //System.Diagnostics.Debug.WriteLine(sr.ReadToEnd().PrettyPrintJson());

                streamWriter.Write(sr.ReadToEnd());
                streamWriter.Flush();
                streamWriter.Close();
            }

            string responseFromServer;
            string result = completeRequest(request, out responseFromServer);

            CreateLabelsResponse CLResponse = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<CreateLabelsResponse>(result);

            try
            {
                //Store to DB
                object[,] xargs = {
                  { "@shipmentID", CLResponse.labels[0].shipment_ids[0].ToString()},
                  { "@response", result }
                };

                DataSource.ExecuteNonQuery(@"
                    update tblWhAusPostDespatch_data
                    set createLabelsResponse = LEFT(@response,3000)
                    where shipmentID = @shipmentID
                ", "NetCRMAU", xargs);
            }
            catch { }


            return CLResponse;
        }


        ////GET
        public GetLabelsResponse GetCourierLabels(string requestID)
        {
            string s_action = "labels";
            string s_url = buildURL(s_action + "/" + requestID);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(s_url);

            // Setup the main header lines for the request.
            request.Method = "GET";
            request.ContentType = "application/json";
            request.Headers.Add("account-number", ACCOUNT_NO);
            request.Host = API_HOST;
            request.Accept = "*/*";

            string responseFromServer;
            string result = completeRequest(request, out responseFromServer);

            GetLabelsResponse GLResponse = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<GetLabelsResponse>(result);

            try
            {
                //Store to DB
                object[,] xargs = {
                  { "@shipmentID", GLResponse.labels[0].shipment_ids[0].ToString()},
                  { "@response", result }
                };

                DataSource.ExecuteNonQuery(@"
                    update tblWhAusPostDespatch_data
                    set getLabelsResponse = LEFT(@response,3000)
                    where shipmentID = @shipmentID
                ", "NetCRMAU", xargs);
            }
            catch { }

            return GLResponse;
        }


        /// <summary>
        /// Create an order from a list of shipments
        /// </summary>
        /// <param name="crorder"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public CreateOrderResponse CreateOrderFromShipments(CreateOrder crorder, string courier, string userName)
        {
            string s_action = "orders";
            string s_url = buildURL(s_action);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(s_url);

            // Setup the main header lines for the request.
            request.Method = "PUT";
            request.ContentType = "application/json";
            request.Headers.Add("account-number", ACCOUNT_NO);
            request.Host = API_HOST;
            request.Accept = "*/*";

            // Supply the request data to the server, including serializing the data into JSON.
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                MemoryStream ms = new MemoryStream();
                DataContractJsonSerializer dcser = new DataContractJsonSerializer(typeof(CreateOrder));

                dcser.WriteObject(ms, crorder);

                ms.Position = 0;
                StreamReader sr = new StreamReader(ms);

                //System.Diagnostics.Debug.WriteLine("JSON:");
                //System.Diagnostics.Debug.WriteLine(sr.ReadToEnd().PrettyPrintJson());

                streamWriter.Write(sr.ReadToEnd());
                streamWriter.Flush();
                streamWriter.Close();
            }

            string responseFromServer;
            string result = completeRequest(request, out responseFromServer);

            if (responseFromServer != "Success")
            {
                try
                {
                    AusPostError AusPostResponse = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<AusPostError>(responseFromServer);
                    MessageBox.Show(AusPostResponse.errors[0].code + " message-" + AusPostResponse.errors[0].message + " name-" + AusPostResponse.errors[0].name + " shipid-" + AusPostResponse.errors[0].context.shipment_id);

                    DataTable errorList = new DataTable();
                    errorList.Columns.Add("despatchID");
                    errorList.Columns.Add("connoteNo");
                    errorList.Columns.Add("exactOrderNo");
                    errorList.Columns.Add("shipmentID");
                    errorList.Columns.Add("name");
                    errorList.Columns.Add("message");

                    foreach (Error error in AusPostResponse.errors) 
                    {
                        string shipmentID = error.context.shipment_id;
                        string name = error.name;
                        string message = error.message;

                        string connoteNo = "";
                        string exactOrderNo = "";
                        string despatchID = "";
                        DataTable dt = DataSource.GetTableFrom("select connoteNo, exactOrderNo, despatchID from tblWhAusPostDespatch where shipmentID = @shipmentID", "NetCRMAU", new object[,]{{"@shipmentID", shipmentID }});

                        if (dt != null && dt.Rows.Count > 0) {
                            connoteNo = dt.Rows[0]["connoteNo"].ToString().Trim();
                            exactOrderNo = dt.Rows[0]["exactOrderNo"].ToString().Trim();
                            despatchID = dt.Rows[0]["despatchID"].ToString().Trim();
                        }

                        errorList.Rows.Add(despatchID, connoteNo, exactOrderNo, shipmentID, name, message);

                    }

                    frmAUPSTEError errorPopup = new frmAUPSTEError("Manifest Generation Error!", errorList);
                    errorPopup.ShowDialog();
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error! Please contact IT immediately.");
                }
                return null;


            }

            CreateOrderResponse COResponse = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<CreateOrderResponse>(result);

            try
            {
                //Store to DB
                object[,] xargs = {
                  { "@orderID", COResponse.order.order_id},
                  { "@courier", courier},
                  { "@account", ACCOUNT_NO.Trim()},
                  { "@totalCost", COResponse.order.order_summary.total_cost.ToString() },
                  { "@totalGST", COResponse.order.order_summary.total_gst.ToString() },
                  { "@orderCreationDate", COResponse.order.order_creation_date },
                  { "@orderReference", COResponse.order.order_reference },
                  { "@generatedBy", userName},
                  { "@response", result }
                };

                DataSource.ExecuteNonQuery(@"
                    insert into tblWhAusPostDespatchManifest (orderID, account, courier, totalCost, totalGST, orderCreationDate, orderReference, generatedBy)
                    values (@orderID, @account, @courier, @totalCost, @totalGST, @orderCreationDate, @orderReference, @generatedBy)
                ", "NetCRMAU", xargs);

                //Save orderID to each line in database as well as update main table status
                foreach (CO_Shipment shipment in COResponse.order.shipments)
                {
                    string shipmentID = shipment.shipment_id.Trim();

                    //Update lines table with new order ID
                    //Store to DB
                    object[,] xargs1 = {
                            { "@orderID", COResponse.order.order_id },
                            { "@courier", courier},
                            { "@shipmentID", shipmentID.Trim() }
                        };

                    DataSource.ExecuteNonQuery(@"
                            declare @despatchID varchar(20)

                            update tblWhAusPostDespatch
                            set orderID = @orderID
                            where shipmentID = @shipmentID and courier = @courier

                            select @despatchID = despatchID from tblWhAusPostDespatch where shipmentID = @shipmentID and courier = @courier

                            update tblWhOrderDespatch
                            set despatchStatus = 'Manifested'
                            where id = @despatchID
                        ", "NetCRMAU", xargs1);
                }
                    
            }
            catch { }

            return COResponse;

        }




        public string GetOrderSummary(string order_id, string filePath)
        {
            string s_action = "accounts";
            string s_url = buildURL(s_action + "/" + ACCOUNT_NO + "/orders/" + order_id + "/summary");

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(s_url);

            // Setup the main header lines for the request.
            request.Method = "GET";
            request.ContentType = "application/pdf";
            request.Headers.Add("account-number", ACCOUNT_NO);
            request.Host = API_HOST;
            request.Accept = "*/*";

            completePDFRequest(request, filePath);

            //MessageBox.Show(result);

            return "Done!";

        }


        /// <summary>
        /// Complete an HTTP request and process the response from the server.
        /// </summary>
        /// <param name="bw"></param>
        /// <param name="args"></param>
        /// <param name="request"></param>
        private string completeRequest(HttpWebRequest request, out string responseFromServer)
        {
            string s_result;

            setBasicAuthHeader(request, API_KEY, API_PWD);

            // Get the response from the server
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                var reader = new StreamReader(response.GetResponseStream());
                s_result = reader.ReadToEnd();
                response.Close();

                responseFromServer = "Success";

                // Return a result that is the JSON response beautified                
                return s_result.PrettyPrintJson();
            }
            catch (WebException ex)
            {
                responseFromServer = (new StreamReader(ex.Response.GetResponseStream()).ReadToEnd()).PrettyPrintJson();

                return ex.ToString();
            }

        }




        /// <summary>
        /// Complete an HTTP request and process the response from the server.
        /// </summary>
        /// <param name="bw"></param>
        /// <param name="args"></param>
        /// <param name="request"></param>
        private bool completePDFRequest(HttpWebRequest request, string filePath)
        {

            setBasicAuthHeader(request, API_KEY, API_PWD);

            // Get the response from the server
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                //SaveToOutput(response.GetResponseStream(), @"\\livdoc9\mydocs\IT\Danan\OrderTest\testing3.pdf")
                using (var writeStream = File.OpenWrite(filePath))
                {
                    response.GetResponseStream().CopyTo(writeStream);
                    writeStream.Close();
                }


                response.Close();



                return true;
            }
            catch (WebException ex)
            {
                return false;
            }

        }

        /// <summary>
        /// Sets the HTTP authorization header for an API call.
        /// Adapted from here: http://blog.kowalczyk.info/article/at3/Forcing-basic-http-authentication-for-HttpWebReq.html
        /// </summary>
        /// <param name="req"></param>
        /// <param name="userName"></param>
        /// <param name="userPassword"></param>
        public void setBasicAuthHeader(WebRequest req, string userName, string userPassword)
        {
            string authInfo = userName + ":" + userPassword;
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            req.Headers["Authorization"] = "Basic " + authInfo;
        }


        /// <summary>
        /// Builds the full URL for an API call
        /// </summary>
        /// <param name="s_path">The path component specifying the API call.</param>
        /// <returns>The URL in a string.</returns>
        private string buildURL(string s_path)
        {
            return API_SCHEME + API_HOST + ":" + API_PORT + API_BASE_URL + s_path;
        }


        /// <summary>
        /// Generats test data for a "Create Shipments" API call.
        /// </summary>
        /// <returns>CreateShipments object that can execute the API call</returns>
        public CreateShipments getShipmentTestData()
        {
            Address from = new Address();
            Address to = new Address();
            List<Item> item_list = new List<Item>();
            Item item = new Item();
            CreateShipments crship = new CreateShipments();
            Shipment shipment = new Shipment("12345", "12345", from, to);
            List<Shipment> shipment_list = new List<Shipment>();

            from.name = "Jack Smith";
            from.addLine("106 Epsom Road");
            from.suburb = "Rosebery";
            from.postcode = "2018";
            from.state = "NSW";

            to.name = "John Jones";
            to.addLine("5 John Street");
            to.suburb = "Lidcombe";
            to.postcode = "2141";
            to.state = "NSW";
            to.phone = "0391234567";

            item.item_reference = "TETSUNG";
            item.weight = "10";
            item.height = item.width = item.length = "10";
            item.packaging_type = "CTN";
            item_list.Add(item);

            item = new Item();
            item.item_reference = "TETSING2";
            item.weight = "11";
            item.height = item.width = item.length = "12";
            item.packaging_type = "PAL"; //CTN
            item_list.Add(item);


            shipment.setItemList(item_list);
            shipment_list.Add(shipment);
            crship.setShipments(shipment_list);

            return crship;
        }


        /// <summary>
        /// Generates test data for a "Create Labels" API call.
        /// </summary>
        /// <returns>CreateLabels object that can execute the API call</returns>
        public CreateLabels getShipmentTestLabels(string shipmentID)
        {

            Group group = new Group("Express Post");
            Preference preference = new Preference();
            preference.addGroup(group);
            List<Preference> preferences = new List<Preference>();
            preferences.Add(preference);

            ShipmentItem package = new ShipmentItem(shipmentID);
            List<ShipmentItem> packages = new List<ShipmentItem>();
            packages.Add(package);

            CreateLabels crlabel = new CreateLabels();

            crlabel.setPreferences(preferences);
            crlabel.setShipments(packages);

            return crlabel;
        }


        /// <summary>
        /// Generates test data for a "Create Labels" API call.
        /// </summary>
        /// <returns>CreateLabels object that can execute the API call</returns>
        public CreateOrder createOrderTest(string shipmentID, string orderReference)
        {
            ShipmentItem shipment = new ShipmentItem(shipmentID);
            List<ShipmentItem> shipments = new List<ShipmentItem>();
            shipments.Add(shipment);

            CreateOrder crorder = new CreateOrder(orderReference);

            crorder.setShipments(shipments);

            return crorder;
        }



    }
}
