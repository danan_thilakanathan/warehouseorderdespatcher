﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using Livingstone.Library;
using JsonPrettyPrinterPlus;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace OrderDespatcher.Courier
{
    public class StarTrackCourier : CourierService
    {

        private bool connected = false;

        public bool Connected
        {
            get { return connected; }
            set { connected = value; }
        }

        public StarTrackCourier()
        {
            //STEUtilities aup = new STEUtilities(API_KEY, API_PWD, API_HOST, ACCOUNT_NO, API_SCHEME, API_PORT, API_BASE_URL);

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            ServicePointManager.DefaultConnectionLimit = 9999;
        }

        //Test Connection
        public void CheckConnected()
        {
            STEUtilities ste = new STEUtilities("LIVINGSTONE", "R", "STE");
            if (ste.ConnectCourier())
            {
                MessageBox.Show("Connected with LIVINGSTONE STE Courier!");
            }
            else MessageBox.Show("Could not connect with LIVINGSTONE STE Courier.");

            STEUtilities u_ste = new STEUtilities("UNIVERSAL", "R", "STE");
            if (u_ste.ConnectCourier())
            {
                MessageBox.Show("Connected with UNIVERSAL STE Courier!");
            }
            else MessageBox.Show("Could not connect with UNIVERSAL STE Courier.");

        }



        private string validateAddress(string delName, string delAddr1, string delAddr2, string delAddr3, string delSuburb, string delState, string delPostcode, string delCountry)
        {
            if (string.IsNullOrEmpty(delName) || delName.Length > 40)
            {
                MessageBox.Show("Delivery Name is mandatory and should only be upto 40 characters. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (delAddr1.Length > 40 || delAddr2.Length > 40 || delAddr3.Length > 40)
            {
                MessageBox.Show("Delivery Address lines should only be a maximum of 40 characters. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delAddr1) && string.IsNullOrEmpty(delAddr2) && string.IsNullOrEmpty(delAddr3))
            {
                MessageBox.Show("Delivery Address lines are mandatory. Please add the delivery address and try again.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delSuburb) || delSuburb.Length > 30)
            {
                MessageBox.Show("Delivery Suburb is mandatory and should only be upto 40 characters. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delPostcode) || delPostcode.Length > 10)
            {
                MessageBox.Show("Delivery Postcode should be upto 10 characters only. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delState) || delState.Length > 3)
            {
                MessageBox.Show("Delivery State should be upto 3 characters only. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delCountry) || delCountry.Length > 2)
            {
                MessageBox.Show("Delivery Country should be upto 2 characters only. Please fix first.");
                return "[NEED TO FIX]";
            }

            return "OK";
        }




        //**********    IMPLEMENTING FUNCTIONS    ***************/


        public override string GenerateConnote(string despatchID, string cusNo, string exactNo, string exactOrderNo, DataTable otherDetails, string userName, ref string userMessage)
        {

            frmSTEProcessOrder stepo = new frmSTEProcessOrder(despatchID, cusNo, exactNo, exactOrderNo, userName, "STE",
                                    OrderDespatchService.GetWarehouseOfPacker(userName), ref userMessage);
            stepo.ShowDialog();

            return stepo.returnConnoteNo;

        }

        public override void PrintConnote(string despatchID, string connoteNo, int noOfLabels, ref string userMessage)
        {
            object[,] xargs = {
                { "@despatchID", despatchID.Trim() },
                { "@connoteNo", connoteNo.Trim() }
            };

            DataTable dt = DataSource.GetTableFrom(@"
                select case when cusNo like 'U-%' then 'UNIVERSAL' else 'LIVINGSTONE' end as company, courier, createdBy
                from tblWhAusPostDespatch
                where (despatchID = @despatchID or @despatchID = '') and connoteNo = @connoteNo
            ", "NetCRMAU", xargs);

            if (dt != null && dt.Rows.Count > 0)
            {
                STEUtilities ste = new STEUtilities(dt.Rows[0]["company"].ToString().Trim(), OrderDespatchService.GetWarehouseOfPacker(dt.Rows[0]["createdBy"].ToString().Trim()), dt.Rows[0]["courier"].ToString().Trim());
                ste.PrintLabel(despatchID, dt.Rows[0]["courier"].ToString().Trim());
            }
            

        }



        public bool DeleteConnote(string despatchID, string connoteNo)
        {


            object[,] xargs = {
                { "@despatchID", despatchID.Trim() },
                { "@connoteNo", connoteNo.Trim() }
            };

            DataTable dt = DataSource.GetTableFrom(@"
                select cusNo, shipmentID, createdBy
                from tblWhAusPostDespatch
                where despatchID = @despatchID and connoteNo = @connoteNo
            ", "NetCRMAU", xargs);

            string shipmentID = "-1";
            if (dt != null && dt.Rows.Count > 0)
            {
                string company = "LIVINGSTONE";
                if (dt.Rows[0]["cusNo"].ToString().Trim().Substring(0, 2) == "U-")
                    company = "UNIVERSAL";

                STEUtilities ste = new STEUtilities(company, OrderDespatchService.GetWarehouseOfPacker(dt.Rows[0]["createdBy"].ToString().Trim()), "STE");
                shipmentID = dt.Rows[0]["shipmentID"].ToString().Trim();
                if (ste.DeleteShipment(shipmentID))
                {
                    ste.UpdateConnoteNo(despatchID, "");
                    return true;
                }

                
            }

            return false;
        }

        public override string GenerateManifest(string userName)
        {
            frmSTEManifestSelector manifestSelect = new frmSTEManifestSelector(userName);
            manifestSelect.ShowDialog();

            if (manifestSelect.DialogResult == DialogResult.OK)
            {
                string company = manifestSelect.company;
                string warehouse = manifestSelect.warehouse;

                STEUtilities ste = new STEUtilities(company, warehouse, "STE");
                ste.GenerateCourierManifest();

            }
            return "Done!";

        }

        public override string PrintManifest(string userName)
        {
            frmSTEManifestSelector manifestSelect = new frmSTEManifestSelector(userName);
            manifestSelect.ShowDialog();

            if (manifestSelect.DialogResult == DialogResult.OK)
            {
                string company = manifestSelect.company;
                string warehouse = manifestSelect.warehouse;

                STEUtilities ste = new STEUtilities(company, warehouse, "STE");
                ste.PrintManifest();


            }


            return "Done!";
        }
    }
}
