﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using Livingstone.Library;

namespace OrderDespatcher.Courier
{
    public class CouriersPleaseCourier : CourierService
    {
        public override string GenerateConnote(string despatchID, string cusNo, string exactNo, string exactOrderNo, DataTable otherDetails, string userName, ref string userMessage)
        {
            string connoteNo = "";

            //Get DESPATCH Details
            DataTable orderDetails = DataSource.GetTableFrom(@"
                    select *, convert(VARCHAR, orderDate, 112) as formattedOrderDate, convert(VARCHAR, etaDate, 112) as shippingDate 
                    from tblWhOrderDespatch
                    where id = @despatchID   
                    order by ts desc
            ", "NetCRMAU", new object[,] { { "@despatchID", despatchID.Trim() } });

            DataTable packingDetails = DataSource.GetTableFrom(@"
                    select * from tblWhPackingDetails where despatchID = @despatchID order by ts desc
            ", "NetCRMAU", new object[,] {  { "@despatchID", despatchID.Trim() } });

            DataTable itemDetails = DataSource.GetTableFrom(@"
                    select * from tblWhPackedItems where despatchID = @despatchID order by ts desc
            ", "NetCRMAU", new object[,] {{ "@despatchID", despatchID.Trim() } });

            DataTable dangerousItemDeclaration = DataSource.GetTableFrom(@"
                    select * from tblWhPackingDetailsDangerousItems where despatchID = @despatchID order by ts desc
            ", "NetCRMAU", new object[,] { { "@despatchID", despatchID.Trim() } });

            string primaryOrderNo = (string)DataSource.GetValue(@"select orderNo from tblWhOrderDespatch_Orders where isPrimary = 1 and despatchID = @despatchID",
                "NetCRMAU", new object[,] { { "@despatchID", despatchID.Trim() } });


            //Check that a connote number exists in every line of the packing details table, if not, instruct to scan in connote barcode per carton line
            List<string> connotes = new List<string>();
            foreach (DataRow dr in packingDetails.Rows)
            {
                if (string.IsNullOrEmpty(dr["connoteNo"].ToString().Trim()))
                {
                    MessageBox.Show("Please scan the connoteNo per carton line.");
                    return "[NEED TO FIX]";
                }


                connotes.Add(dr["connoteNo"].ToString().Trim());

            }

            //Sort the list of COP connotes
            connotes.Sort();

            bool first = true;
            foreach (string connote in connotes)
            {
                if (first)
                {
                    connoteNo = connote;
                    first = false;
                }
                else
                    connoteNo += "," + connote.Substring(connote.Length - 4);
            }

            //Check that there is no Dangerous Goods
            foreach (DataRow dr in itemDetails.Rows)
            {
                string itemNo = dr["itemNo"].ToString().Trim();

                int qtyPicked = 0;

                if (Int32.TryParse(dr["qtyPicked"].ToString(), out qtyPicked))
                {
                    if (qtyPicked > 0)
                    {
                        string isDangerousGood = (string)DataSource.GetValue(@"select 1 from tblDangerousGoods where Item_no = @itemNo", "NetCRMAU", new object[,] { { "@itemNo", itemNo } });
                        if (isDangerousGood == "1")
                        {
                            if (MessageBox.Show("Upon double checking, it seems there are dangerous goods in this order. Process anyway?", "WARNING: Order contains dangerous goods items", MessageBoxButtons.YesNo) == DialogResult.No);
                            {
                                return "[NEED TO FIX]";
                            }
                        }
                    }

                }

            }

            

            //Check the packing type is CARTON
            foreach (DataRow dr in packingDetails.Rows)
            {
                string packingType = dr["type"].ToString().Trim();

                if (packingType != "CARTON")
                {
                    MessageBox.Show("CouriersPlease only accepts packing type CARTON.");
                    return "[NEED TO FIX]";
                }

            }
            
            //Check that there is a maximum of 3 labels
            if (Int32.Parse(orderDetails.Rows[0]["totalCartons"].ToString()) > 3)
            {
                MessageBox.Show("Only a maximum of 3 cartons can be used for CouriersPlease.");
                return "[NEED TO FIX]";
            }
            userMessage = "Make sure the label sticker is attached to each carton separately before despatching.";

            PrintService printService = new PrintService();
            printService.PrintShippingLabel(primaryOrderNo, "COP", Int32.Parse(orderDetails.Rows[0]["totalCartons"].ToString()));
            

            return connoteNo;
        }

        public override void PrintConnote(string despatchID, string connoteNo, int noOfLabels, ref string userMessage)
        {
            if (connoteNo != "[NEED TO FIX]")
            {
                userMessage = "Make sure the label sticker is attached to each carton separately.";
                return;
            }
            else
            {
                string primaryOrderNo = (string)DataSource.GetValue(@"select orderNo from tblWhOrderDespatch_Orders where isPrimary = 1 and despatchID = @despatchID",
                    "NetCRMAU", new object[,] { { "@despatchID", despatchID.Trim() } });


                PrintService printService = new PrintService();
                printService.PrintShippingLabel(primaryOrderNo, "COP", noOfLabels);
        
                userMessage = "Make sure the label sticker is attached to each carton separately. Let OE process this one as normal.";

            }
        }

        public override string GenerateManifest(string userName)
        {
            throw new NotImplementedException();
        }

        public override string PrintManifest(string userName)
        {
            throw new NotImplementedException();
        }
    }
}
