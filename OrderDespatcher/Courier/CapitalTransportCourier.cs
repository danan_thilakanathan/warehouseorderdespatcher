﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using Livingstone.Library;
using JsonPrettyPrinterPlus;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using CromulentBisgetti.ContainerPacking;
using CromulentBisgetti.ContainerPacking.Entities;
using CromulentBisgetti.ContainerPacking.Algorithms;
using System.Data.SqlClient;

namespace OrderDespatcher.Courier
{
    public class CapitalTransportCourier : CourierService
    {

        /************ Helper Objects ***********************/

        [DataContract]
        public class UserCredentials
        {
            [DataMember]
            public string Username { get; set; }
            [DataMember]
            public string SharedKey { get; set; }

            public UserCredentials(string username, string sharedKey)
            {
                this.Username = username;
                this.SharedKey = sharedKey;
            }
        }

        [DataContract]
        public class BookingItem
        {
            [DataMember]
            public string Description { get; set; }
            [DataMember]
            public string Barcode { get; set; }
            [DataMember]
            public int Quantity { get; set; }
            [DataMember]
            public int Weight { get; set; }
            [DataMember]
            public double Volume { get; set; }
        }

        [DataContract]
        public class BookingContactInformation
        {
            [DataMember]
            public string Name { get; set; }
            [DataMember]
            public List<string> PhoneNumbers { get; set; }

            public BookingContactInformation()
            {
                this.Name = "";
                this.PhoneNumbers = new List<string>();
            }
        }

        [DataContract]
        public class CapitalTransportBooking
        {
            [DataMember]
            public UserCredentials UserCredentials { get; set; }
            [DataMember]
            public int RequestType { get; set; }
            [DataMember]
            public string AccountCode { get; set; }
            [DataMember]
            public string ServiceCode { get; set; }
            [DataMember]
            public string FromSuburb { get; set; }
            [DataMember]
            public string FromPostcode { get; set; }
            [DataMember]
            public string FromDetail1 { get; set; }
            [DataMember]
            public string FromDetail2 { get; set; }
            [DataMember]
            public string FromDetail3 { get; set; }
            [DataMember]
            public string FromDetail4 { get; set; }
            [DataMember]
            public string FromDetail5 { get; set; }
            [DataMember]
            public string ToSuburb { get; set; }
            [DataMember]
            public string ToPostcode { get; set; }
            [DataMember]
            public string ToDetail1 { get; set; }
            [DataMember]
            public string ToDetail2 { get; set; }
            [DataMember]
            public string ToDetail3 { get; set; }
            [DataMember]
            public string ToDetail4 { get; set; }
            [DataMember]
            public string ToDetail5 { get; set; }
            [DataMember]
            public int State { get; set; }
            [DataMember]
            public string Reference1 { get; set; }
            [DataMember]
            public string Reference2 { get; set; }
            [DataMember]
            public string ConsignmentNumber { get; set; }
            [DataMember(EmitDefaultValue = false)]
            public string AdvanceDateTime { get; set; }
            [DataMember]
            public string TotalItems { get; set; }
            [DataMember]
            public string TotalWeight { get; set; }
            [DataMember]
            public string TotalVolume { get; set; }
            [DataMember]
            public string Caller { get; set; }
            [DataMember]
            public string ExtraPuInformation { get; set; }
            [DataMember]
            public string ExtraDelInformation { get; set; }
            [DataMember]
            public List<BookingItem> BookingItems { get; set; }
            [DataMember]
            public BookingContactInformation BookingContactInformation { get; set; }

            public CapitalTransportBooking()
            {
                this.FromDetail1 = "";
                this.FromDetail2 = "";
                this.FromDetail3 = "";
                this.FromDetail4 = "";
                this.FromDetail5 = "";
                this.ToDetail1 = "";
                this.ToDetail2 = "";
                this.ToDetail3 = "";
                this.ToDetail4 = "";
                this.ToDetail5 = "";
                this.ExtraDelInformation = "";
                this.ConsignmentNumber = "";
                this.BookingItems = new List<BookingItem>();
            }

            public void setAdvanceDate(int year, int month, int day, int hour, int minutes, int seconds)
            {
                string advanceDate = year.ToString() + "-" + month.ToString().PadLeft(2, '0') + "-" + day.ToString().PadLeft(2, '0')
                            + "T" + hour.ToString().PadLeft(2, '0') + ":" + minutes.ToString().PadLeft(2, '0') + ":" + seconds.ToString().PadLeft(2, '0');

                this.AdvanceDateTime = advanceDate;
            }
        }


        public class CapitalBookingResponse
        {
            public string StatusDescription { get; set; }
            public string JobNumber { get; set; }
            public string Reference1 { get; set; }
            public string Reference2 { get; set; }
            public int StatusCode { get; set; }
            public int State { get; set; }
            public string JobPriceExGst { get; set; }
            public string Gst { get; set; }
            public string JobTotalPrice { get; set; }
        }


        public void testRequest()
        {
            CapitalTransportBooking booking = new CapitalTransportBooking();
            UserCredentials credential = new UserCredentials("livingstone", "42umdyh5xgPAPnEQ");
            booking.UserCredentials = credential;
            booking.RequestType = 1;
            booking.AccountCode = "LIVSYD";
            booking.ServiceCode = "S2T";
            booking.FromSuburb = "Rosebery";
            booking.FromPostcode = "2018";
            booking.FromDetail1 = "106-116 Epsom Road";
            booking.ToSuburb = "Rosebery";
            booking.ToPostcode = "2018";
            booking.ToDetail1 = "45 Epsom Road";
            booking.ToDetail2 = "Test Booking Only";
            booking.State = 1;
            booking.Reference1 = "OrderNo1";
            booking.Reference2 = "DespatchNo1";
            booking.ConsignmentNumber = "test consign number";
            //booking.AdvanceDateTime = "2017-10-08T15:05:00";
            booking.setAdvanceDate(2017, 10, 8, 15, 5, 0);
            booking.TotalItems = "5";
            booking.TotalWeight = "45";
            booking.TotalVolume = "0.2";
            booking.Caller = "Test Caller";
            booking.ExtraPuInformation = "Test Extra PU Details";
            booking.ExtraDelInformation = "Test Extra DEL Details";

            BookingItem bookingItem = new BookingItem();
            bookingItem.Description = "Pallet of TV";
            bookingItem.Barcode = "TV123456";
            bookingItem.Quantity = 3;
            bookingItem.Weight = 30;
            bookingItem.Volume = 0.1;
            booking.BookingItems.Add(bookingItem);

            bookingItem = new BookingItem();
            bookingItem.Description = "Pallet of BBQ";
            bookingItem.Barcode = "BBQ123";
            bookingItem.Quantity = 2;
            bookingItem.Weight = 15;
            bookingItem.Volume = 0.1;
            booking.BookingItems.Add(bookingItem);

            BookingContactInformation contactInfo = new BookingContactInformation();
            contactInfo.Name = "Alan Turing";
            contactInfo.PhoneNumbers.Add("0433826612");
            contactInfo.PhoneNumbers.Add("0433826123");

            booking.BookingContactInformation = contactInfo;

            PostBooking(booking);



        }


        public void testCargoLoading()
        {
            List<CromulentBisgetti.ContainerPacking.Entities.Container> containers = new List<CromulentBisgetti.ContainerPacking.Entities.Container>();
            containers.Add(new CromulentBisgetti.ContainerPacking.Entities.Container(1, 200, 100, 100));

            List<Item> itemsToPack = new List<Item>();
            //itemsToPack.Add(new Item(1, 100, 100, 110, 1));
            //itemsToPack.Add(new Item(2, 100, 100, 50, 1));
            //itemsToPack.Add(new Item(3, 100, 100, 50, 1));

            itemsToPack.Add(new Item(1, 100, 100, 50, 1));
            itemsToPack.Add(new Item(2, 100, 100, 52, 1));
            itemsToPack.Add(new Item(3, 100, 100, 20, 1));
            itemsToPack.Add(new Item(4, 100, 100, 20, 1));
            itemsToPack.Add(new Item(5, 100, 100, 25, 1));
            itemsToPack.Add(new Item(6, 100, 100, 22, 1));



            List<int> algorithms = new List<int>();
            algorithms.Add((int)AlgorithmType.EB_AFIT);

            List<ContainerPackingResult> result = PackingService.Pack(containers, itemsToPack, algorithms);
            AlgorithmPackingResult actualResult =  result[0].AlgorithmPackingResults[0];

            string packed = "";
            foreach(Item i in actualResult.PackedItems) {
                packed += i.ID + ",";
            }

            string unpacked = "";
            foreach(Item i in actualResult.UnpackedItems) {
                unpacked += i.ID + ",";
            }

            MessageBox.Show("Packed Items: " + actualResult.PackedItems.Count + " -- Unpacked Items: " + actualResult.UnpackedItems.Count);
        }

        private CapitalBookingResponse PostBooking(CapitalTransportBooking booking)
        {
            string s_url = "https://ilogixdms.capitaltransport.com.au/api/Booking";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(s_url);

            // Setup the main header lines for the request.
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Accept = "*/*";

            // Supply the request data to the server, including serializing the data into JSON.
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                MemoryStream ms = new MemoryStream();
                DataContractJsonSerializer dcser = new DataContractJsonSerializer(typeof(CapitalTransportBooking));

                dcser.WriteObject(ms, booking);

                ms.Position = 0;
                StreamReader sr = new StreamReader(ms);

                //System.Diagnostics.Debug.WriteLine("JSON:");
                //System.Diagnostics.Debug.WriteLine(sr.ReadToEnd().PrettyPrintJson());

                streamWriter.Write(sr.ReadToEnd());
                streamWriter.Flush();
                streamWriter.Close();
            }

            string result = completeRequest(request);

            try
            {
                CapitalBookingResponse CLBResponse = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<CapitalBookingResponse>(result);
                return CLBResponse;
            }
            catch (Exception e)
            {
                return null;
            }


        }


        /// <summary>
        /// Complete an HTTP request and process the response from the server.
        /// </summary>
        /// <param name="bw"></param>
        /// <param name="args"></param>
        /// <param name="request"></param>
        private string completeRequest(HttpWebRequest request)
        {
            string s_result;

            // Get the response from the server
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                var reader = new StreamReader(response.GetResponseStream());
                s_result = reader.ReadToEnd();
                response.Close();

                // Return a result that is the JSON response beautified                
                return s_result.PrettyPrintJson();
            }
            catch (WebException ex)
            {
                return ex.ToString();
            }

        }

        private string validateAddress(string delName, string delAddr1, string delAddr2, string delAddr3, string delSuburb, string delState, string delPostcode, string delCountry)
        {
            //if (string.IsNullOrEmpty(delName) || delName.Length > 40)
            //{
            //    MessageBox.Show("Delivery Name is mandatory and should only be upto 40 characters. Please fix first.");
            //    return "[NEED TO FIX]";
            //}
            //if (delAddr1.Length > 40 || delAddr2.Length > 40 || delAddr3.Length > 40)
            //{
            //    MessageBox.Show("Delivery Address lines should only be a maximum of 40 characters. Please fix first.");
            //    return "[NEED TO FIX]";
            //}
            if (string.IsNullOrEmpty(delAddr1) && string.IsNullOrEmpty(delAddr2) && string.IsNullOrEmpty(delAddr3))
            {
                MessageBox.Show("Delivery Address lines are mandatory. Please add the delivery address and try again.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delSuburb) || delSuburb.Length > 30)
            {
                MessageBox.Show("Delivery Suburb is mandatory and should only be upto 40 characters. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delPostcode) || delPostcode.Length > 10)
            {
                MessageBox.Show("Delivery Postcode should be upto 10 characters only. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delState) || delState.Length > 3)
            {
                MessageBox.Show("Delivery State should be upto 3 characters only. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delCountry) || delCountry.Length > 2)
            {
                MessageBox.Show("Delivery Country should be upto 2 characters only. Please fix first.");
                return "[NEED TO FIX]";
            }

            return "OK";
        }

        //Check that a list of cartons/pallets with its own dimensions can all fit inside a container with specified dimensions
        private bool verifyPackingDetailsCanFitInContainer(DataTable packingDetails, decimal containerLength, decimal containerWidth, decimal containerHeight)
        {
            //Make sure there is a bit of buffer for the cartons
            containerLength = containerLength * (decimal)0.9;
            containerWidth = containerWidth * (decimal)0.9;
            containerHeight = containerHeight * (decimal)0.9;

            List<CromulentBisgetti.ContainerPacking.Entities.Container> containers = new List<CromulentBisgetti.ContainerPacking.Entities.Container>();
            containers.Add(new CromulentBisgetti.ContainerPacking.Entities.Container(1, containerLength, containerWidth, containerHeight));

            List<Item> itemsToPack = new List<Item>();

            int i = 1;
            foreach (DataRow packing in packingDetails.Rows)
            {
                decimal packingLength = 0;
                decimal packingWidth = 0;
                decimal packingHeight = 0;
                int qty = 0;

                if (!decimal.TryParse(packing["length"].ToString(), out packingLength))
                {
                    return false;
                }
                if (!decimal.TryParse(packing["width"].ToString(), out packingWidth))
                {
                    return false;
                }
                if (!decimal.TryParse(packing["height"].ToString(), out packingHeight))
                {
                    return false;
                }
                if (!Int32.TryParse(packing["qty"].ToString(), out qty))
                {
                    return false;
                }

                itemsToPack.Add(new Item(i, packingLength, packingWidth, packingHeight, qty));

            }



            List<int> algorithms = new List<int>();
            algorithms.Add((int)AlgorithmType.EB_AFIT);

            List<ContainerPackingResult> result = PackingService.Pack(containers, itemsToPack, algorithms);
            AlgorithmPackingResult actualResult = result[0].AlgorithmPackingResults[0];

            if (actualResult.UnpackedItems.Count > 0)
                return false;

            return true;
        }


        //Check that a list of cartons/pallets with its own dimensions can all fit inside specified dimensions
        private bool verifyPackingDetailsCanFitPerItem(DataTable packingDetails, decimal containerLength, decimal containerWidth, decimal containerHeight)
        {
            //Make sure there is a bit of buffer for the cartons
            //containerLength = containerLength * (decimal)0.9;
            //containerWidth = containerWidth * (decimal)0.9;
            //containerHeight = containerHeight * (decimal)0.9;


            foreach (DataRow packing in packingDetails.Rows)
            {
                decimal packingLength = 0;
                decimal packingWidth = 0;
                decimal packingHeight = 0;

                if (!decimal.TryParse(packing["length"].ToString(), out packingLength))
                {
                    return false;
                }
                if (!decimal.TryParse(packing["width"].ToString(), out packingWidth))
                {
                    return false;
                }
                if (!decimal.TryParse(packing["height"].ToString(), out packingHeight))
                {
                    return false;
                }

                if (packingLength > containerLength) return false;
                if (packingWidth > containerWidth) return false;
                if (packingHeight > containerHeight) return false;


            }

            return true;
        }

        private bool verifyMaxCartonWeight(DataTable packingDetails, double maxWeight)
        {
            bool eachCartonWithinMaxWeight = true;

            foreach (DataRow packing in packingDetails.Rows)
            {
                double lineWeight = 0;
                if (double.TryParse(packing["weight"].ToString(), out lineWeight))
                {
                    if (lineWeight > maxWeight)
                    {
                        eachCartonWithinMaxWeight = false;
                        break;
                    }

                }
                else
                {
                    eachCartonWithinMaxWeight = false;
                    break;
                }
            }

            return eachCartonWithinMaxWeight;
        }

        private bool checkPalletFitsTruck(DataTable packingDetails, int noOfPallets, double truckLength, double truckWidth, double truckHeight)
        {

            double individualPalletCBMTruck = 0.9 * (((truckLength * truckWidth * truckHeight) / 1000000) / noOfPallets);

            foreach (DataRow packing in packingDetails.Rows)
            {
                if (packing["type"].ToString().Trim() == "PALLET")
                {
                    int qty = Int32.Parse(packing["qty"].ToString());
                    for (int i = 0; i < qty; ++i)
                    {
                        double length = double.Parse(packing["length"].ToString());
                        double width = double.Parse(packing["width"].ToString());
                        double height = double.Parse(packing["height"].ToString());
                        double weight = double.Parse(packing["weight"].ToString());

                        double palletCBM = (qty * length * width * height) / 1000000;

                        if (palletCBM <= individualPalletCBMTruck)
                        {
                            if (height > 0.9 * truckHeight)
                            {
                                return false;
                            }
                        }
                    }



                }
            }

            return true;
        }

        private string GetServiceCode(DataTable packingDetails, int noOfCartons, int noOfPallets, double totalWeight)
        {

            
            if (noOfPallets == 0 && noOfCartons > 0)
            {
                //CARTON ORDERS

                if (totalWeight <= 25 && verifyPackingDetailsCanFitInContainer(packingDetails, 50, 50, 50))
                {
                    return "SC";
                }
                else if (totalWeight <= 125 && verifyPackingDetailsCanFitPerItem(packingDetails, 50, 50, 50) && verifyMaxCartonWeight(packingDetails, 25)) 
                {
                    return "SSW";
                }
                else if (totalWeight <= 500 && verifyPackingDetailsCanFitInContainer(packingDetails, 120, 150, 100/*1metre high for van*/))
                {
                    return "SHV";
                }
                else if (totalWeight <= 500 && verifyPackingDetailsCanFitInContainer(packingDetails, 120, 150, 150))
                {
                    return "SHT";
                }
                else if (totalWeight <= 1000 && verifyPackingDetailsCanFitInContainer(packingDetails, 240, 150, 100/*1metre high for van*/))
                {
                    return "S1V";
                }
                else if (totalWeight <= 1000 && verifyPackingDetailsCanFitInContainer(packingDetails, 240, 150, 200))
                {
                    return "S1T";
                } 
            }
            else if (noOfPallets == 1 && totalWeight <= 500 && checkPalletFitsTruck(packingDetails, noOfPallets, 120, 150, 150))
            {
                return "SHT";
            }
            else if (noOfPallets <= 2 && totalWeight <= 1000 && checkPalletFitsTruck(packingDetails, noOfPallets, 240, 150, 200))
            {
                return "S1T";
            }
            else if (noOfPallets <= 3 && totalWeight <= 2000 && checkPalletFitsTruck(packingDetails, noOfPallets, 360, 200, 200))
            {
                return "SF2L";
            }
            else if (noOfPallets <= 6 && totalWeight <= 4000 && checkPalletFitsTruck(packingDetails, noOfPallets, 420, 240, 200))
            {
                return "SF4L";
            }
            else if (noOfPallets <= 8 && totalWeight <= 6000 && checkPalletFitsTruck(packingDetails, noOfPallets, 540, 240, 200))
            {
                return "SF6L";
            }
            else if (noOfPallets <= 10 && totalWeight <= 8000 && checkPalletFitsTruck(packingDetails, noOfPallets, 600, 240, 200))
            {
                return "SF8L";
            }
            else if (noOfPallets <= 12 && totalWeight <= 8000 && checkPalletFitsTruck(packingDetails, noOfPallets, 720, 240, 200))
            {
                return "SF12L";
            }
            else if (noOfPallets <= 14 && totalWeight <= 8000 && checkPalletFitsTruck(packingDetails, noOfPallets, 840, 240, 200))
            {
                return "SF14L";
            }
            else if (noOfPallets <= 22 && totalWeight <= 8000 && checkPalletFitsTruck(packingDetails, noOfPallets, 1320, 240, 200))
            {
                return "SF20L";
            }

            return "[ERROR]";
        }

        public override string GenerateConnote(string despatchID, string cusNo, string exactNo, string exactOrderNo, DataTable otherDetails, string userName, ref string userMessage)
        {
            try
            {
                SqlConnection con = DataSource.getConn("NetCRMAU");

                //Get DESPATCH Details
                DataTable orderDetails = DataSource.GetTableFrom(@"
                        select *, convert(VARCHAR, orderDate, 112) as formattedOrderDate, convert(VARCHAR, etaDate, 112) as shippingDate 
                        from tblWhOrderDespatch
                        where id = @despatchID   
                        order by ts desc
                ", con, new object[,] { { "@despatchID", despatchID.Trim() } });

                DataTable packingDetails = DataSource.GetTableFrom(@"
                        select * from tblWhPackingDetails where despatchID = @despatchID order by ts desc
                ", con, new object[,] {{ "@despatchID", despatchID.Trim() } });

                DataTable itemDetails = DataSource.GetTableFrom(@"
                        select * from tblWhPackedItems where despatchID = @despatchID order by ts desc
                ", con, new object[,] { { "@despatchID", despatchID.Trim() } });

                DataTable dangerousItemDeclaration = DataSource.GetTableFrom(@"
                        select di.despatchID, di.orderNo, di.itemNo, di.weight, di.qty, dg.* 
                        from tblWhPackingDetailsDangerousItems di
                        left join tblDangerousGoods dg
	                        on (di.itemNo = dg.Item_no)
                        where despatchID = @despatchID
                ", con, new object[,] { { "@despatchID", despatchID.Trim() } });

                string primaryOrderNo = (string)DataSource.GetValue(@"select orderNo from tblWhOrderDespatch_Orders where isPrimary = 1 and despatchID = @despatchID",
                    con, new object[,] { { "@despatchID", despatchID.Trim() } });

                //Should not create job automatically for Dangerous Goods as per requirement of Capital Transport. Need to phone them manually.
                //Despatch Contains Dangerous Goods Items
                if (dangerousItemDeclaration != null && dangerousItemDeclaration.Rows.Count > 0)
                {
                    MessageBox.Show("This despatch contains Dangerous Goods. Capital Transport does not allow DG items to be done automatically. We need to phone them to book the job. This despatch will now be processed by the OE team.");
                    return "[SEND TO OE]";
                }

                string connoteNo = "[NEED TO FIX]";

                //Create a Booking Job
                CapitalTransportBooking booking = new CapitalTransportBooking();
                UserCredentials credential = new UserCredentials("livingstone", "42umdyh5xgPAPnEQ");
                booking.UserCredentials = credential;
                booking.RequestType = 1;
                booking.AccountCode = "LIVSYD";
                //booking.ServiceCode = "S2T"; //GetServiceCode(DataTable packingDetails)
                booking.ServiceCode = GetServiceCode(packingDetails, Int32.Parse(orderDetails.Rows[0]["noOfCartons"].ToString()), Int32.Parse(orderDetails.Rows[0]["noOfPallets"].ToString()), double.Parse(orderDetails.Rows[0]["totalWeight"].ToString()));

                if (booking.ServiceCode == "[ERROR]")
                {
                    MessageBox.Show("Could not find suitable service code. Please check the dimensions are correct. If you keep seeing this error, send it to OE to process this one.");
                    return "[NEED TO FIX]";
                }

                MessageBox.Show("Service Code Chosen for this order: " + booking.ServiceCode);

                //Store booking service code in freefield value of Order Despatch table
                OrderDespatchService.UpdateFreefield(despatchID.Trim(), 1, booking.ServiceCode);

                //Setup FROM address
                string warehouseFrom = OrderDespatchService.GetWarehouseOfPacker(userName);

                if (warehouseFrom == "P")
                {
                    if (cusNo.Substring(0, 2) == "U-") booking.FromDetail1 = "UNIVERSAL CHOICE(P)";
                    else booking.FromDetail1 = "LIVINGSTONE (PRESTONS)";

                    booking.FromDetail2 = "1B/5-35 YARRUNGA ST";
                    booking.FromDetail3 = "*CLS 3:30PM*";
                    booking.FromSuburb = "PRESTONS";
                    booking.FromPostcode = "2170";
                }
                if (warehouseFrom == "R")
                {
                    if (cusNo.Substring(0, 2) == "U-") booking.FromDetail1 = "UNIVERSAL CHOICE(R)";
                    else booking.FromDetail1 = "LIVINGSTONE (ROSEBERY)";

                    booking.FromDetail2 = "106-116 EPSOM RD";
                    booking.FromDetail3 = "*CLS 3:30PM*";
                    booking.FromSuburb = "ROSEBERY";
                    booking.FromPostcode = "2018";
                }
                else if (warehouseFrom == "T")
                {
                    if (cusNo.Substring(0, 2) == "U-") booking.FromDetail1 = "UNIVERSAL CHOICE(T)";
                    else booking.FromDetail1 = "LIVINGSTONE (TURRELLA)";

                    booking.FromDetail2 = "127-139 TURRELLA ST";
                    booking.FromDetail3 = "*CLS 3:30PM*";
                    booking.FromSuburb = "TURRELLA";
                    booking.FromPostcode = "2205";
                }
                else if (warehouseFrom == "V")
                {
                    if (cusNo.Substring(0, 2) == "U-") booking.FromDetail1 = "UNIVERSAL CHOICE(V)";
                    else booking.FromDetail1 = "LIVINGSTONE (VICTORIA)";

                    booking.FromDetail2 = "7 HILLWIN ST";
                    booking.FromSuburb = "RESERVOIR";
                    booking.FromPostcode = "3073";
                }
                else
                {
                    booking.FromDetail1 = "LIVINGSTONE INTERNATIONAL";
                    booking.FromDetail2 = "1B/5-35 YARRUNGA ST";
                    booking.FromSuburb = "PRESTONS";
                    booking.FromPostcode = "2170";
                }


                booking.State = /*NSW*/2; 
                booking.Reference1 = primaryOrderNo.Trim() + "-" + despatchID.Trim();
                booking.Reference2 = despatchID.Trim();
                //booking.ConsignmentNumber = "test consign number";
                //booking.AdvanceDateTime = "2017-10-08T15:05:00";
            
                /**Ask user questions to check to see whether an advance date needs to be set for booking ***/

                frmCAPQuestions capQuestions = new frmCAPQuestions();
                capQuestions.ShowDialog();

                DateTime customerClosingTime = DateTime.Parse("16:00 PM");
                if (capQuestions.DialogResult == DialogResult.OK)
                {
                    bool moreThan4Hours = capQuestions.moreThan4Hours;
                    bool isUrgent = capQuestions.isUrgent;
                    bool needAdvanceBooking = capQuestions.setAdvanceBooking;
                    DateTime advanceBookingDate = capQuestions.advanceBookingDate;

                    customerClosingTime = DateTime.Parse(capQuestions.closingTime);


                    //If more than 4 hours --> Process as Standard
                    //If less than 4 hours and is urgent --> Push to OE to process manually and book the job manually
                    //If less than 4 hours and is NOT urgent --> Set advance booking date

                    if (!moreThan4Hours)
                    {
                        if (isUrgent)
                        {
                            if (MessageBox.Show("Are you sure? This will send the order to HOLD area where OE will book the courier manually (as EXPRESS or VIP service needed here). Please confirm if you want to go ahead with this.", "Need to send to OE for Authorisation!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            {
                                //Urgent and less than 4 hours --> Push to OE to process manually
                                return "[SEND TO OE]";
                            }
                            else
                            {
                                return "[NEED TO FIX]";
                            }
                        }
                        else
                        {
                            //Not urgent and less than 4 hours --> Set advance booking date
                            booking.AdvanceDateTime = advanceBookingDate.ToString("yyyy-MM-ddTHH:mm:ss");

                            if (advanceBookingDate < DateTime.Now)
                            {
                                return "[SEND TO OE]";
                            }
                            if ((advanceBookingDate - DateTime.Now).TotalMinutes < 240)
                            {
                                return "[SEND TO OE]";
                            }
                            if (!capQuestions.advanceMoreThan4Hours)
                            {
                                return "[SEND TO OE]";
                            }

                        }
                    }
            
            
                }
                else
                {
                    MessageBox.Show("NOTE: Questions need to be answered in order to process CAP orders.");
                    return "[NEED TO FIX]";
                }

                //TO Address Validation and Setup TO address
                string delName = orderDetails.Rows[0]["delName"].ToString().Trim();
                string delAddr1 = orderDetails.Rows[0]["delAddr1"].ToString().Trim();
                string delAddr2 = orderDetails.Rows[0]["delAddr2"].ToString().Trim();
                string delAddr3 = orderDetails.Rows[0]["delAddr3"].ToString().Trim();
                string delSuburb = orderDetails.Rows[0]["delSuburb"].ToString().Trim();
                string delState = orderDetails.Rows[0]["delState"].ToString().Trim();
                string delPostcode = orderDetails.Rows[0]["delPostcode"].ToString().Trim();
                string delCountry = orderDetails.Rows[0]["country"].ToString().Trim();

                string validAddress = validateAddress(delName, delAddr1, delAddr2, delAddr3, delSuburb, delState, delPostcode, delCountry);
                if (validAddress == "[NEED TO FIX]")
                    return "[NEED TO FIX]";

                delName = delName.Replace(@"/", "");
                if (delName.Length > 30) delName = delName.Substring(0, 30);

                booking.ToSuburb = delSuburb;
                booking.ToPostcode = delPostcode;
                booking.ToDetail1 = delName;



                string fullAddress = (!string.IsNullOrEmpty(delAddr1) ? delAddr1 : "") + (!string.IsNullOrEmpty(delAddr2) ? " " + delAddr2 : "") + (!string.IsNullOrEmpty(delAddr3) ? " " + delAddr3 : "");

                fullAddress = fullAddress.Replace(",", " ").Replace("*", " ").Replace("$", " ").Replace("%", " ");

                if (fullAddress.Length > 68)
                {
                    MessageBox.Show("Delivery Address Too Long for CAP. Maximum 68 characters for the 3 delivery lines. Please fix or send to OE.");
                    return "[NEED TO FIX]";
                }

                if (fullAddress.Length <= 30)
                {
                    booking.ToDetail2 = fullAddress;
                    booking.ToDetail3 = "DELIVER BY " + customerClosingTime.ToString("hh:mm tt");
                }
                else if (delAddr1.Length <= 30 && (delAddr2 + delAddr3).Length <= 30)
                {
                    booking.ToDetail2 = (!string.IsNullOrEmpty(delAddr2) ? delAddr1 : "");
                    booking.ToDetail3 = (!string.IsNullOrEmpty(delAddr2) ? " " + delAddr2 : "") + (!string.IsNullOrEmpty(delAddr3) ? " " + delAddr3 : "");
                    booking.ExtraDelInformation = "DELIVER BY " + customerClosingTime.ToString("hh:mm tt");
                }
                else
                {
                    fullAddress += " DELIVER BY " + customerClosingTime.ToString("hh:mm tt");
                    booking.ToDetail2 = fullAddress.Substring(0, 30);

                    string remainingAddress1 = fullAddress.Substring(30);

                    if (remainingAddress1.Length > 30) 
                    {
                        booking.ToDetail3 = remainingAddress1.Substring(0, 30);

                        string remainingAddress2 = remainingAddress1.Substring(30);
                        if (remainingAddress2.Length > 30)
                        {
                            booking.ExtraDelInformation = remainingAddress2.Substring(0, 30);
                        }
                        else
                            booking.ExtraDelInformation = remainingAddress2.Substring(0, remainingAddress2.Length);
                    }
                    else 
                    {
                        booking.ToDetail3 = remainingAddress1.Substring(0, remainingAddress1.Length);
                    }

                }


                MessageBox.Show("booking delivery\nDEL line1: " + booking.ToDetail1 + "\nDEL line2: " + booking.ToDetail2 + "\nDEL line3: " + booking.ToDetail3 + "\nExtraDeliveryInfo: " + (string.IsNullOrEmpty(booking.ExtraDelInformation) ? "" : booking.ExtraDelInformation));

                //booking.setAdvanceDate(2017, 10, 8, 15, 5, 0);

                /*************************************************************************************************/

                int totalLabels = Int32.Parse(orderDetails.Rows[0]["noOfCartons"].ToString()) + Int32.Parse(orderDetails.Rows[0]["noOfPallets"].ToString());
                string totalWeight = orderDetails.Rows[0]["totalWeight"].ToString();
                string totalVolume = orderDetails.Rows[0]["totalDimensions"].ToString();

                booking.TotalItems = totalLabels.ToString();
                booking.TotalWeight = totalWeight;
                booking.TotalVolume = totalVolume;

                booking.Caller = userName;

                if (cusNo.Substring(0, 2) == "U-" || exactNo.Substring(0,2) == "60")
                    booking.ExtraPuInformation = "(UNIVERSAL SIGN)*ENCLOSE VEHIC";
                else
                    booking.ExtraPuInformation = "*ENCLOSED VEHICLE ONLY*";


                foreach (DataRow packing in packingDetails.Rows)
                {
                    BookingItem bookingItem = new BookingItem();
                    bookingItem.Description = packing["type"].ToString().Trim();
                    bookingItem.Barcode = "";
                    bookingItem.Quantity = Int32.Parse(packing["qty"].ToString());
                    bookingItem.Weight = Int32.Parse(Math.Round(double.Parse(packing["weight"].ToString()), 0).ToString());
                    bookingItem.Volume = (double.Parse(packing["length"].ToString()) * double.Parse(packing["width"].ToString()) * double.Parse(packing["height"].ToString())) / 1000000;
                    booking.BookingItems.Add(bookingItem);
                }

                BookingContactInformation contactInfo = new BookingContactInformation();
                contactInfo.Name = "Rowena Wong";
                contactInfo.PhoneNumbers.Add("0283447382");

                booking.BookingContactInformation = contactInfo;

                CapitalBookingResponse bookingResponse = PostBooking(booking);

                if (bookingResponse == null)
                {
                    MessageBox.Show("Capital Transport booking internal error. Please contact IT to fix.");
                    return "[ERROR]";
                }

                //MessageBox.Show(bookingResponse.StatusDescription);

                if (bookingResponse.StatusCode == 1)
                {
                    //Set return connoteNo
                    connoteNo = bookingResponse.JobNumber;

                    //Store estimated cost
                    object[,] xargs = {
                        { "@orderNo", primaryOrderNo.Trim() },
                        { "@despatchID", despatchID.Trim() },
                        { "@totalCost", bookingResponse.JobTotalPrice  },
                        { "@totalGST", double.Parse(bookingResponse.JobTotalPrice) - double.Parse(bookingResponse.JobPriceExGst) },
                        { "@totalCostExclGST", bookingResponse.JobPriceExGst }
                    };

                    //Update estimated Cost in Order Despatch table
                    DataSource.ExecuteNonQuery(@"
                                        update tblWhOrderDespatch
                                        set estimatedFreightCost = @totalCost, estimatedFreightGST = @totalGST, estimatedFreightCostExclGST = @totalCostExclGST
                                        where id = @despatchID
                                    ", con, xargs);

                    //Print label
                    PrintService ps = new PrintService();
                    ps.PrintCAPLabel(primaryOrderNo, totalLabels, connoteNo);

                    //MessageBox.Show("CAP label printed");
                    con.Close();
                    con.Dispose();

                    return connoteNo;
                }
                else
                    return "[ERROR]";


            }
            catch
            {
                return "[ERROR]";
            }



        }

        public override void PrintConnote(string despatchID, string connoteNo, int noOfLabels, ref string userMessage)
        {
            string primaryOrderNo = (string)DataSource.GetValue(@"select orderNo from tblWhOrderDespatch_Orders where isPrimary = 1 and despatchID = @despatchID",
                "NetCRMAU", new object[,] { { "@despatchID", despatchID.Trim() } });

            PrintService ps = new PrintService();
            ps.PrintCAPLabel(primaryOrderNo, noOfLabels, connoteNo);
        }

        public override string GenerateManifest(string userName)
        {
            MessageBox.Show("CAP does not need manifest.");
            return "";
        }

        public override string PrintManifest(string userName)
        {
            MessageBox.Show("CAP does not need manifest.");
            return "";
        }
    }
}
