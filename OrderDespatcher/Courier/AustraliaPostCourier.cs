﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using Livingstone.Library;
using JsonPrettyPrinterPlus;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace OrderDespatcher.Courier
{
    public class AustraliaPostCourier : CourierService
    {

        private bool connected = false;

        public bool Connected
        {
            get { return connected; }
            set { connected = value; }
        }





        public AustraliaPostCourier()
        {


            //STEUtilities aup = new STEUtilities("LIVINGSTONE", "R", "AUP");


            /**TESTING ONLY ***

            //Check Connected
            Connected = aup.ConnectCourier();

            if (Connected)
                MessageBox.Show("Connected With Courier Successfully!");
            else
                MessageBox.Show("Could Not Connect!");

            //Create Shipment
            STEUtilities.CreateShipments crship = aup.getShipmentTestData();
            STEUtilities.CreateShipmentsResponse csresp = aup.CreateShipment(crship);
            MessageBox.Show("Ship ID:" + csresp.shipments[0].shipment_id + " Connote:" + csresp.shipments[0].items[0].tracking_details.consignment_id + " Est Cost: " + csresp.shipments[0].shipment_summary.total_cost);
            string shipmentID = csresp.shipments[0].shipment_id;

            

            //Create Labels 
            STEUtilities.CreateLabels crlabel = aup.getShipmentTestLabels(shipmentID);
            STEUtilities.CreateLabelsResponse clresp = aup.CreateCourierLabels(crlabel);
            MessageBox.Show("Message: " + clresp.message + " Status: " + clresp.labels[0].status + " RequestID: " + clresp.labels[0].request_id);
            string requestID = clresp.labels[0].request_id;

            //Get Labels
            STEUtilities.GetLabelsResponse glresp = aup.GetCourierLabels(requestID);
            MessageBox.Show("URL: " + glresp.labels[0].url + " Status: " + glresp.labels[0].status + " ShipID: " + glresp.labels[0].shipment_ids[0].ToString());
            string url = glresp.labels[0].url;



            //Create Order From Shipments
            STEUtilities.CreateOrder crorder = aup.createOrderTest(shipmentID, "LIV_OrderNo_DespatchNo_id");
            STEUtilities.CreateOrderResponse coresp = aup.CreateOrderFromShipments(crorder);
            MessageBox.Show("OrderID: " + coresp.order.order_id  + " OrderRef: " + coresp.order.order_reference + " Total Cost: " + coresp.order.order_summary.total_cost.ToString() );
            string order_id = coresp.order.order_id;

            //Get Order Summary
            string result = aup.GetOrderSummary(order_id, @"\\livdoc9\mydocs\IT\Danan\OrderTest\testing3.pdf");
            MessageBox.Show(result);

            **/

        }


        private string validateAddress(string delName, string delAddr1, string delAddr2, string delAddr3, string delSuburb, string delState, string delPostcode, string delCountry)
        {
            if (string.IsNullOrEmpty(delName) || delName.Length > 40)
            {
                MessageBox.Show("Delivery Name is mandatory and should only be upto 40 characters. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (delAddr1.Length > 40 || delAddr2.Length > 40 || delAddr3.Length > 40)
            {
                MessageBox.Show("Delivery Address lines should only be a maximum of 40 characters. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delAddr1) && string.IsNullOrEmpty(delAddr2) && string.IsNullOrEmpty(delAddr3))
            {
                MessageBox.Show("Delivery Address lines are mandatory. Please add the delivery address and try again.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delSuburb) || delSuburb.Length > 30)
            {
                MessageBox.Show("Delivery Suburb is mandatory and should only be upto 40 characters. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delPostcode) || delPostcode.Length > 10)
            {
                MessageBox.Show("Delivery Postcode should be upto 10 characters only. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delState) || delState.Length > 3)
            {
                MessageBox.Show("Delivery State should be upto 3 characters only. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delCountry) || delCountry.Length > 2)
            {
                MessageBox.Show("Delivery Country should be upto 2 characters only. Please fix first.");
                return "[NEED TO FIX]";
            }

            return "OK";
        }



        public override string GenerateConnote(string despatchID, string cusNo, string exactNo, string exactOrderNo, System.Data.DataTable otherDetails, string userName, ref string userMessage)
        {

            frmSTEProcessOrder stepo = new frmSTEProcessOrder(despatchID, cusNo, exactNo, exactOrderNo, userName, "AUP",
                                    OrderDespatchService.GetWarehouseOfPacker(userName), ref userMessage);
            stepo.ShowDialog();

            if (stepo.DialogResult == DialogResult.Cancel)
            {
                return "[NEED TO FIX]";
            }

            return stepo.returnConnoteNo;
               

        }

        public override void PrintConnote(string despatchID, string connoteNo, int noOfLabels, ref string userMessage)
        {
            object[,] xargs = {
                { "@despatchID", despatchID.Trim() },
                { "@connoteNo", connoteNo.Trim() }
            };

            DataTable dt = DataSource.GetTableFrom(@"
                select case when cusNo like 'U-%' then 'UNIVERSAL' else 'LIVINGSTONE' end as company, courier, createdBy
                from tblWhAusPostDespatch
                where (despatchID = @despatchID or @despatchID = '') and connoteNo = @connoteNo
            ", "NetCRMAU", xargs);

            if (dt != null && dt.Rows.Count > 0)
            {
                STEUtilities aup = new STEUtilities(dt.Rows[0]["company"].ToString().Trim(), OrderDespatchService.GetWarehouseOfPacker(dt.Rows[0]["createdBy"].ToString().Trim()), dt.Rows[0]["courier"].ToString().Trim());
                aup.PrintLabel(despatchID, dt.Rows[0]["courier"].ToString().Trim());
            }
        }


        public bool DeleteConnote(string despatchID, string connoteNo)
        {


            object[,] xargs = {
                { "@despatchID", despatchID.Trim() },
                { "@connoteNo", connoteNo.Trim() }
            };

            DataTable dt = DataSource.GetTableFrom(@"
                select cusNo, shipmentID, createdBy
                from tblWhAusPostDespatch
                where despatchID = @despatchID and connoteNo = @connoteNo
            ", "NetCRMAU", xargs);

            string shipmentID = "-1";
            if (dt != null && dt.Rows.Count > 0)
            {
                string company = "LIVINGSTONE";
                if (dt.Rows[0]["cusNo"].ToString().Trim().Substring(0, 2) == "U-")
                    company = "UNIVERSAL";

                STEUtilities ste = new STEUtilities(company, OrderDespatchService.GetWarehouseOfPacker(dt.Rows[0]["createdBy"].ToString().Trim()), "AUP");
                shipmentID = dt.Rows[0]["shipmentID"].ToString().Trim();
                if (ste.DeleteShipment(shipmentID))
                {
                    ste.UpdateConnoteNo(despatchID, "");
                    return true;
                }


            }

            return false;
        }



        public override string GenerateManifest(string userName)
        {
            frmSTEManifestSelector manifestSelect = new frmSTEManifestSelector(userName);
            manifestSelect.ShowDialog();

            if (manifestSelect.DialogResult == DialogResult.OK)
            {
                string company = manifestSelect.company;
                string warehouse = manifestSelect.warehouse;

                STEUtilities aup = new STEUtilities(company, warehouse, "AUP");
                aup.GenerateCourierManifest();

            }
            return "Done!";
        }


        public override string PrintManifest(string userName)
        {
            frmSTEManifestSelector manifestSelect = new frmSTEManifestSelector(userName);
            manifestSelect.ShowDialog();

            if (manifestSelect.DialogResult == DialogResult.OK)
            {
                string company = manifestSelect.company;
                string warehouse = manifestSelect.warehouse;

                STEUtilities aup = new STEUtilities(company, warehouse, "AUP");
                aup.PrintManifest();


            }

            return "Done!";
        }

    }
}
