﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OrderDespatcher.Courier
{
    public partial class frmSTEManifestSelector : Form
    {
        private string username;
        public string company { get; set; }
        public string warehouse { get; set; }

        public frmSTEManifestSelector()
        {
            InitializeComponent();
        }

        public frmSTEManifestSelector(string username)
        {
            InitializeComponent();
            this.username = username;

            cboWarehouse.Text = OrderDespatchService.GetWarehouseOfPacker(username);
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.company = cboCompany.Text.ToUpper().Trim();
            this.warehouse = cboWarehouse.Text.ToUpper().Trim();

            this.Close();

        }
    }
}
