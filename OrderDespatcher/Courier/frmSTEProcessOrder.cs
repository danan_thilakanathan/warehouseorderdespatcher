﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using System.Threading;
using Livingstone.Library;

namespace OrderDespatcher.Courier
{
    public partial class frmSTEProcessOrder : Form
    {
        private string exactOrderNo { get; set; }
        private string despatchID { get; set; }
        private string cusNo { get; set; }
        private string courier { get; set; }
        private string userName { get; set; }

        private string pgrMessage { get; set; }

        public string returnConnoteNo { get; set; }

        //private bool closePending;
        //private AutoResetEvent _workCompleted = new AutoResetEvent(false);

        private STEUtilities aup { get; set; }

        public frmSTEProcessOrder()
        {
            InitializeComponent();
        }

        public frmSTEProcessOrder(string despatchID, string cusNo, string exactOrderNo, string userName, string courier,
            string API_KEY, string API_PWD, string API_HOST, string ACCOUNT_NO, string API_SCHEME, int API_PORT, string API_BASE_URL, ref string userMessage)
        {
            InitializeComponent();


            this.exactOrderNo = exactOrderNo;
            this.despatchID = despatchID;
            this.cusNo = cusNo;
            this.courier = courier;
            this.userName = userName;

            this.returnConnoteNo = "[NEED TO FIX]";

            //SETUP
            this.aup = new STEUtilities(API_KEY, API_PWD, API_HOST, ACCOUNT_NO, API_SCHEME, API_PORT, API_BASE_URL);

            bgwProcessOrder.RunWorkerAsync();
            //_workCompleted.WaitOne();

        }

        public frmSTEProcessOrder(string despatchID, string cusNo, string exactNo, string exactOrderNo, string userName, string courier,
    string warehouse, ref string userMessage)
        {
            InitializeComponent();


            this.exactOrderNo = exactOrderNo;
            this.despatchID = despatchID;
            this.cusNo = cusNo;
            this.courier = courier;
            this.userName = userName;

            this.returnConnoteNo = "[NEED TO FIX]";

            //SETUP
            string company = "LIVINGSTONE";
            if (exactNo.StartsWith("60"))
            {
                company = "UNIVERSAL";
            }

            this.aup = new STEUtilities(company, warehouse, courier);

            bgwProcessOrder.RunWorkerAsync();
            //_workCompleted.WaitOne();

        }

        

        public void addLine(string text)
        {
            txtProgress.AppendText(text);
            txtProgress.AppendText(Environment.NewLine);
        }


        private string validateAddress(string delName, string delAddr1, string delAddr2, string delAddr3, string delSuburb, string delState, string delPostcode, string delCountry)
        {
            if (string.IsNullOrEmpty(delName) || delName.Length > 40)
            {
                MessageBox.Show("Delivery Name is mandatory and should only be upto 40 characters. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (delAddr1.Length > 40 || delAddr2.Length > 40 || delAddr3.Length > 40)
            {
                MessageBox.Show("Delivery Address lines should only be a maximum of 40 characters. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delAddr1) && string.IsNullOrEmpty(delAddr2) && string.IsNullOrEmpty(delAddr3))
            {
                MessageBox.Show("Delivery Address lines are mandatory. Please add the delivery address and try again.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delSuburb) || delSuburb.Length > 30)
            {
                MessageBox.Show("Delivery Suburb is mandatory and should only be upto 40 characters. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delPostcode) || delPostcode.Length > 10)
            {
                MessageBox.Show("Delivery Postcode should be upto 10 characters only. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delState) || delState.Length > 3)
            {
                MessageBox.Show("Delivery State should be upto 3 characters only. Please fix first.");
                return "[NEED TO FIX]";
            }
            if (string.IsNullOrEmpty(delCountry) || delCountry.Length > 2)
            {
                MessageBox.Show("Delivery Country should be upto 2 characters only. Please fix first.");
                return "[NEED TO FIX]";
            }

            return "OK";
        }




        private void bgwProcessOrder_DoWork(object sender, DoWorkEventArgs e)
        {
            int progress = 0;

            //Get DESPATCH Details
            DataTable orderDetails = DataSource.GetTableFrom(@"
                    select *, convert(VARCHAR, orderDate, 112) as formattedOrderDate, convert(VARCHAR, etaDate, 112) as shippingDate 
                    from tblWhOrderDespatch
                    where orderNo = @orderNo and id = @despatchID   
                    order by ts desc
            ", "NetCRMAU", new object[,] { { "@orderNo", exactOrderNo.Trim() }, { "@despatchID", despatchID.Trim() } });

            DataTable packingDetails = DataSource.GetTableFrom(@"
                    select * from tblWhPackingDetails where orderNo = @orderNo and despatchID = @despatchID order by ts desc
            ", "NetCRMAU", new object[,] { { "@orderNo", exactOrderNo.Trim() }, { "@despatchID", despatchID.Trim() } });

            DataTable itemDetails = DataSource.GetTableFrom(@"
                    select * from tblWhPackedItems where orderNo = @orderNo and despatchID = @despatchID order by ts desc
            ", "NetCRMAU", new object[,] { { "@orderNo", exactOrderNo.Trim() }, { "@despatchID", despatchID.Trim() } });

            DataTable dangerousItemDeclaration = DataSource.GetTableFrom(@"
                    select di.despatchID, di.orderNo, di.itemNo, di.weight, di.qty, dg.* 
                    from tblWhPackingDetailsDangerousItems di
                    left join tblDangerousGoods dg
	                    on (di.itemNo = dg.Item_no)
                    where despatchID = @despatchID and orderNo = @orderNo
            ", "NetCRMAU", new object[,] { { "@orderNo", exactOrderNo.Trim() }, { "@despatchID", despatchID.Trim() } });

            string primaryOrderNo = (string)DataSource.GetValue(@"select orderNo from tblWhOrderDespatch_Orders where isPrimary = 1 and despatchID = @despatchID",
                "NetCRMAU", new object[,] { { "@despatchID", despatchID.Trim() } });

            progress += 10;
            pgrMessage = "Connecting with " + courier;
            bgwProcessOrder.ReportProgress(progress);
            if (bgwProcessOrder.CancellationPending)
            {
                // Indicate that the task was canceled.
                e.Cancel = true;
                return;
            }

            //Check Connected
            bool Connected = aup.ConnectCourier();

            int trialNo = 0;
            while (trialNo < 5 && !Connected)
            {
                ++trialNo;
                
                Connected = aup.ConnectCourier();

                if (Connected) break;
                else
                {
                    progress += 1;
                    bgwProcessOrder.ReportProgress(progress);
                    System.Threading.Thread.Sleep(1000);
                    if (bgwProcessOrder.CancellationPending)
                    {
                        // Indicate that the task was canceled.
                        e.Cancel = true;
                        return;
                    }
                }

            }

            if (Connected)
            {
                progress = 15;
                pgrMessage = "Creating despatch entry";
                bgwProcessOrder.ReportProgress(progress);
                if (bgwProcessOrder.CancellationPending)
                {
                    // Indicate that the task was canceled.
                    e.Cancel = true;
                    return;
                }

                /****************** Create a Shipment **************/
                STEUtilities.CreateShipments crship = new STEUtilities.CreateShipments();
                //aup.getShipmentTestData();

                STEUtilities.Address from = new STEUtilities.Address();
                STEUtilities.Address to = new STEUtilities.Address();

                STEUtilities.Shipment shipment = new STEUtilities.Shipment(primaryOrderNo, primaryOrderNo + "-" + despatchID, from, to);
                List<STEUtilities.Shipment> shipment_list = new List<STEUtilities.Shipment>();

                if (orderDetails.Rows[0]["warehouse"].ToString() == "P" && cusNo.Substring(0, 2) != "U-")
                {
                    from.name = "LIVINGSTONE INTERNATIONAL";
                    from.addLine("1B/5-35 YARRUNGA ST");
                    from.suburb = "PRESTONS";
                    from.postcode = "2170";
                    from.state = "NSW";
                }
                else if (orderDetails.Rows[0]["warehouse"].ToString() == "R" && cusNo.Substring(0, 2) != "U-")
                {
                    from.name = "LIVINGSTONE INTERNATIONAL";
                    from.addLine("106-116 EPSOM ROAD");
                    from.suburb = "ROSEBERY";
                    from.postcode = "2018";
                    from.state = "NSW";
                }
                else if (orderDetails.Rows[0]["warehouse"].ToString() == "T" || cusNo.Substring(0, 2) == "U-")
                {
                    if (cusNo.Substring(0, 2) == "U-") from.name = "UNIVERSAL CHOICE";
                    else from.name = "LIVINGSTONE INTERNATIONAL";

                    from.addLine("127-139 TURRELLA ST");
                    from.suburb = "TURRELLA";
                    from.postcode = "2205";
                    from.state = "NSW";
                }
                else if (orderDetails.Rows[0]["warehouse"].ToString() == "V" && cusNo.Substring(0, 2) != "U-")
                {
                    from.name = "LIVINGSTONE INTERNATIONAL";
                    from.addLine("7 HILLWIN ST");
                    from.suburb = "RESERVOIR";
                    from.postcode = "3073";
                    from.state = "VIC";
                }
                else
                {
                    from.name = "LIVINGSTONE INTERNATIONAL";
                    from.addLine("1B/5-35 YARRUNGA ST");
                    from.suburb = "PRESTONS";
                    from.postcode = "2170";
                    from.state = "NSW";
                }

                //Address Validation
                string delName = orderDetails.Rows[0]["delName"].ToString();
                string delAddr1 = orderDetails.Rows[0]["delAddr1"].ToString();
                string delAddr2 = orderDetails.Rows[0]["delAddr2"].ToString();
                string delAddr3 = orderDetails.Rows[0]["delAddr3"].ToString();
                string delSuburb = orderDetails.Rows[0]["delSuburb"].ToString();
                string delState = orderDetails.Rows[0]["delState"].ToString();
                string delPostcode = orderDetails.Rows[0]["delPostcode"].ToString();
                string delCountry = orderDetails.Rows[0]["country"].ToString();

                string validAddress = validateAddress(delName, delAddr1, delAddr2, delAddr3, delSuburb, delState, delPostcode, delCountry);
                if (validAddress == "[NEED TO FIX]")
                {
                    this.returnConnoteNo = "[NEED TO FIX]";
                    return;
                }

                delName = delName.Replace(@"/", "");
                if (delName.Length > 40) delName = delName.Substring(0, 40);


                //Add Delivery Address
                to.name = delName.Trim();

                if (!string.IsNullOrEmpty(delAddr1))
                    to.addLine(delAddr1);
                if (!string.IsNullOrEmpty(delAddr2))
                    to.addLine(delAddr2);
                if (!string.IsNullOrEmpty(delAddr3))
                    to.addLine(delAddr3);

                to.suburb = delSuburb.Trim();
                to.postcode = delPostcode.Trim();
                to.state = delState.Trim();

                to.addDeliveryInstruction(orderDetails.Rows[0]["specialInstructions"].ToString().Trim() + " " + orderDetails.Rows[0]["specialInstructions2"].ToString().Trim());


                /********* Add items (CTN, PAL) to shipment ***********/
                List<STEUtilities.Item> item_list = new List<STEUtilities.Item>();

                bool shipmentContainsDG = false;

                foreach (DataRow dr in packingDetails.Rows)
                {
                    STEUtilities.Item item = new STEUtilities.Item();

                    //Item reference is of form orderNo_despatchID_id   (e.g, 3212447_5_34)
                    item.item_reference = dr["orderNo"].ToString().Trim() + "_" + dr["despatchID"].ToString().Trim() + "_" + dr["id"].ToString().Trim();
                    item.weight = Math.Round(double.Parse(dr["weight"].ToString().Trim()), 3).ToString();
                    item.height = Math.Round(double.Parse(dr["height"].ToString().Trim()), 1).ToString();
                    item.width = Math.Round(double.Parse(dr["width"].ToString().Trim()), 1).ToString();
                    item.length = Math.Round(double.Parse(dr["length"].ToString().Trim()), 1).ToString();
                    item.contains_dangerous_goods = (dr["isDG"].ToString().Trim() == "True" ? true : false);

                    string packingType = "CTN";
                    if (dr["type"].ToString().Trim() == "CARTON")
                    {
                        packingType = "CTN";
                    }
                    else if (dr["type"].ToString().Trim() == "PALLET")
                    {
                        packingType = "PAL";
                    }
                    else packingType = "CTN";

                    item.packaging_type = packingType;

                    if (item.contains_dangerous_goods)
                        shipmentContainsDG = true;

                    item_list.Add(item);

                    int qty = Int32.Parse(dr["qty"].ToString().Trim());
                    if (qty > 1)
                    {
                        for (int i = 1; i < qty; i++)
                        {
                            item_list.Add(item);
                        }
                    }
                }


                //Handle DG
                if (shipmentContainsDG)
                {
                    if (dangerousItemDeclaration != null && dangerousItemDeclaration.Rows.Count > 0 && string.IsNullOrEmpty(orderDetails.Rows[0]["connoteNo"].ToString()))
                    {

                        STEUtilities.DangerousGoods dangerousGoods = new STEUtilities.DangerousGoods();

                        //Get the first row DG
                        dangerousGoods.un_number = dangerousItemDeclaration.Rows[0]["UN_no"].ToString().Trim();
                        dangerousGoods.technical_name = dangerousItemDeclaration.Rows[0]["Proper_Shipping_name"].ToString().Trim();

                        double netWeight = Math.Round(double.Parse(dangerousItemDeclaration.Rows[0]["weight"].ToString().Trim()), 0);
                        if (netWeight < 1) netWeight = 1;

                        dangerousGoods.net_weight = netWeight.ToString();
                        dangerousGoods.class_division = dangerousItemDeclaration.Rows[0]["Class"].ToString().Trim();
                        dangerousGoods.outer_packaging_type = dangerousItemDeclaration.Rows[0]["Packaging_Method"].ToString().Trim();
                        dangerousGoods.outer_packaging_quantity = dangerousItemDeclaration.Rows[0]["qty"].ToString().Trim();

                        shipment.dangerous_goods = dangerousGoods;
                    }


                }

                /******************** Create Shipment (Despatch Entry) and Extract Data from JSON *************************/
                shipment.setItemList(item_list);
                shipment_list.Add(shipment);
                crship.setShipments(shipment_list);


                //Check if Connote Already Exists. If so, allow option to update
                if (!string.IsNullOrEmpty(orderDetails.Rows[0]["connoteNo"].ToString().Trim()))
                {
                    if (MessageBox.Show("Connote Already Exists. Update this Connote?", "Connote Already Exists!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        try
                        {
                            //aup.UpdateShipment(orderDetails.Rows[0]["connoteNo"].ToString().Trim(), crship);

                            MessageBox.Show("Connote Updating not supported at this time. Please delete the connote and try again.");


                        }
                        catch { }
                    
                        this.returnConnoteNo = "[NEED TO FIX]";
                        return;
                    }
                }

                //Testing thread --Should remove below line
                //System.Threading.Thread.Sleep(8000);

                if (bgwProcessOrder.CancellationPending)
                {
                    // Indicate that the task was canceled.
                    e.Cancel = true;
                    return;
                }


                //Create Shipment Request
                STEUtilities.CreateShipmentsResponse csresp;
                try
                {
                    csresp = aup.CreateShipment(crship);
                }
                catch (Exception ex)
                {
                    //MessageBox.Show("Could not create STE shipment entry. Please check your inputs and try again. " + ex.Message);
                    this.returnConnoteNo = "[NEED TO FIX]";
                    return;
                }
                string shipmentID = csresp.shipments[0].shipment_id;
                string connoteNo = csresp.shipments[0].items[0].tracking_details.consignment_id;
                double totalCost = csresp.shipments[0].shipment_summary.total_cost;
                double totalGST = csresp.shipments[0].shipment_summary.total_gst;
                double totalCostExclGST = totalCost - totalGST;

                /******* Get connoteNo and estimated Cost and store to DB with despatchID reference (mark as OrderNotCreated) ****/


                //Update connote no to main table (want to update it here so that if the print label fails, can come back later and try to print it again).
                OrderDespatchService.UpdateConnoteNo(despatchID.Trim(), connoteNo);
                this.returnConnoteNo = connoteNo;


                //Create new shipment entry in table
                object[,] xargs = {
                    { "@orderNo", primaryOrderNo.Trim() },
                    { "@despatchID", despatchID.Trim() },
                    { "@courier", courier.Trim() },
                    { "@cusNo", cusNo.Trim() },
                    { "@shipmentID", shipmentID },
                    { "@connoteNo", connoteNo },
                    { "@totalCost", totalCost.ToString() },
                    { "@totalGST", totalGST.ToString() },
                    { "@totalCostExclGST", totalCostExclGST.ToString() },
                    { "@warehouse", this.aup.Warehouse },
                    { "@createdBy", userName }
                };

                DataTable dt = DataSource.GetTableFrom(@"
                    insert into tblWhAusPostDespatch (courier, despatchID, exactOrderNo, cusNo, shipmentID, connoteNo, totalCost, totalGST, totalCostExclGST, warehouse, createdBy)
                    values (@courier, @despatchID, @orderNo, @cusNo, @shipmentID, @connoteNo, @totalCost, @totalGST, @totalCostExclGST, @warehouse, @createdBy)

                    select @@identity
                ", "NetCRMAU", xargs);

                string tableID = "";
                if (dt != null && dt.Rows.Count > 0)
                    tableID = dt.Rows[0][0].ToString().Trim();

                //Update estimated Cost in Order Despatch table
                DataSource.ExecuteNonQuery(@"
                                    update tblWhOrderDespatch
                                    set estimatedFreightCost = @totalCost, estimatedFreightGST = @totalGST, estimatedFreightCostExclGST = @totalCostExclGST
                                    where id = @despatchID
                                ", "NetCRMAU", xargs);


                progress += 25;
                pgrMessage = "Created despatch entry with connote " + connoteNo  +". Now generating labels...";
                bgwProcessOrder.ReportProgress(progress);


                /*************************** Create a label from Shipment (Despatch Entry) *********************************/
                STEUtilities.CreateLabels crlabel = aup.getShipmentLabels(shipmentID);
                STEUtilities.CreateLabelsResponse clresp = aup.CreateCourierLabels(crlabel);
                string requestID = clresp.labels[0].request_id;

                //Store requestID to database with despatchID reference
                DataSource.ExecuteNonQuery(@"
                    update tblWhAusPostDespatch
                    set requestID = @requestID
                    where id = @tableID
                ", "NetCRMAU", new object[,] { { "@requestID", requestID }, { "@tableID", tableID } });


                /*************************** Get a label for the Shipment (Despatch Entry) *********************************/
                string status = "PENDING";
                string url = "";

                int trialLabel = 0;
                while (status != "AVAILABLE" && status != "ERROR" && trialLabel < 10)
                {

                    if (bgwProcessOrder.CancellationPending)
                    {
                        // Indicate that the task was canceled.
                        e.Cancel = true;
                        return;
                    }

                    STEUtilities.GetLabelsResponse glresp = aup.GetCourierLabels(requestID);
                    url = glresp.labels[0].url;
                    status = glresp.labels[0].status;

                    //MessageBox.Show("Message: " + clresp.message + " Status: " + status + " RequestID: " + requestID);

                    if (status != "AVAILABLE") System.Threading.Thread.Sleep(2000);
                    trialLabel++;

                    progress += 2;
                    pgrMessage = "Retrying: " + trialLabel + "/10";
                    bgwProcessOrder.ReportProgress(progress);
                }

                if (status != "AVAILABLE")
                {
                    MessageBox.Show("Please contact IT immediately. Cannot connect with STE/AUP label printing.");
                    return;
                }

                progress = 70;
                pgrMessage = "Label now available";
                bgwProcessOrder.ReportProgress(progress);

                //MessageBox.Show("URL: " + url);

                //Store url to despatchID in database
                DataSource.ExecuteNonQuery(@"
                    update tblWhAusPostDespatch
                    set labelURL = @url
                    where id = @tableID
                ", "NetCRMAU", new object[,] { { "@url", url }, { "@tableID", tableID } });


                string filePath = (string)DataSource.GetValue("select top 1 labelFilePath from tblWhOrderDespatch_Settings", "NetCRMAU");

                string labelFilePath = filePath + @"Label_" + courier + "_" + connoteNo.Trim() + ".pdf";

                using (WebClient webClient = new WebClient())
                {
                    webClient.DownloadFile(url, labelFilePath);
                }

                progress += 15;
                pgrMessage = "Printing label";
                bgwProcessOrder.ReportProgress(progress);

                //Print the downloaded label
                PrintService printerService = new PrintService();
                printerService.PrintLabel(labelFilePath);

                //Delete the file once done with
                if (File.Exists(labelFilePath))
                    File.Delete(labelFilePath);

                progress += 15;
                pgrMessage = "Printed and done!";
                bgwProcessOrder.ReportProgress(progress);


                this.returnConnoteNo = connoteNo;

                return;

            }
            else
            {
                MessageBox.Show("Could Not Connect to STE/AUP!");
                this.returnConnoteNo = "[NEED TO FIX]";
                return;
            }

        }

        private void bgwProcessOrder_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pgrProgress.Value = e.ProgressPercentage;
            addLine(pgrMessage);
        }

        private void bgwProcessOrder_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (this.DialogResult != DialogResult.Cancel)
                this.DialogResult = DialogResult.OK;
            //_workCompleted.Set();
            this.Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (bgwProcessOrder.IsBusy)
            {
                bgwProcessOrder.CancelAsync();
            }
            //this.DialogResult = DialogResult.Cancel;
            //this.Close();
        }

        private void frmSTEProcessOrder_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (bgwProcessOrder.IsBusy)
            {
                bgwProcessOrder.CancelAsync();
            }
            this.DialogResult = DialogResult.Cancel;
            //e.Cancel = true;
            //this.Close();
        }



    }
}
