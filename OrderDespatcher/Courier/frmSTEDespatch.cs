﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Livingstone.Library;

namespace OrderDespatcher.Courier
{
    public partial class frmSTEDespatch : Form
    {

        public DataTable shipmentList { get; set; }

        public frmSTEDespatch()
        {
            InitializeComponent();
        }

        public frmSTEDespatch(string courier, DataTable dt)
        {
            InitializeComponent();

            txtCourier.Text = courier;
            dgrShipments.DataSource = dt;
        }

        public frmSTEDespatch(string courier, string account, DataTable dt)
        {
            InitializeComponent();

            txtCourier.Text = courier;
            txtAccountNo.Text = account;
            dgrShipments.DataSource = dt;
        }

        public frmSTEDespatch(string courier, string account, string warehouse, string company)
        {
            InitializeComponent();

            txtCourier.Text = courier;
            txtAccountNo.Text = account;
            txtCompany.Text = company;
            txtWarehouse.Text = warehouse;

            initialiseList();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void gtnGenerate_Click(object sender, EventArgs e)
        {
            this.shipmentList = (DataTable)dgrShipments.DataSource;

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void initialiseList()
        {
            string customerTypeCondition = "cusNo not in (select cus_no from arcusfil_sql where exact_no like '60%')";
            if (txtCompany.Text.Trim() == "UNIVERSAL") customerTypeCondition = "cusNo in (select cus_no from arcusfil_sql where exact_no like '60%')";

            //Get list of shipmentIDs to despatch where there is no order id and has a label url and connote
            DataTable dt = DataSource.GetTableFrom(@"
	            select cast(1 as bit) includeInManifest, courier, cusNo, connoteNo, exactOrderNo, despatchID, warehouse, totalCostExclGST, totalGST, totalCost, shipmentID, requestID, labelURL, createdBy, ts
	            from tblWhAusPostDespatch
	            where " + customerTypeCondition + @" and orderID is null and labelURL is not null and connoteNo is not null and connoteNo <> '' and courier = @courier and deleted = 0 and warehouse = @warehouse
                order by despatchID desc
            ", "NetCRMAU", new object[,] { { "@courier", txtCourier.Text.Trim() }, { "@warehouse", txtWarehouse.Text.Trim() } });

            dgrShipments.DataSource = dt;
        }

        private void btnDeleteShipment_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dr in dgrShipments.SelectedRows)
            {

                STEUtilities aup = new STEUtilities(txtCompany.Text.Trim(), OrderDespatchService.GetWarehouseOfPacker(dr.Cells["createdBy"].Value.ToString().Trim()) , txtCourier.Text.Trim());
                if (aup.DeleteShipment(dr.Cells["shipmentID"].Value.ToString().Trim()))
                    aup.UpdateConnoteNo(dr.Cells["despatchID"].Value.ToString().Trim(), "");

                //DataSource.ExecuteNonQuery(@"update tblWhAusPostDespatch set deleted = 1 where shipmentID = @shipmentID and courier = @courier",
                //    "NetCRMAU", new object[,] { { "@shipmentID", dr.Cells["shipmentID"].Value.ToString().Trim() }, { "@courier", dr.Cells["courier"].Value.ToString().Trim() } });
            }

            initialiseList();
        }
    }
}
