﻿namespace OrderDespatcher
{
    partial class OrderDespatcher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblCourier = new System.Windows.Forms.Label();
            this.cboCourier = new System.Windows.Forms.ComboBox();
            this.txtNoOfCartons = new System.Windows.Forms.TextBox();
            this.lblTotalWeight = new System.Windows.Forms.Label();
            this.txtTotalWeight = new System.Windows.Forms.TextBox();
            this.lblNoOfLabels = new System.Windows.Forms.Label();
            this.gbxParameters = new System.Windows.Forms.GroupBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnRetrieveInfo = new System.Windows.Forms.Button();
            this.txtDespatchID = new System.Windows.Forms.TextBox();
            this.lblDespatchID = new System.Windows.Forms.Label();
            this.btnLoadPrevious = new System.Windows.Forms.Button();
            this.chkInvoiceWithGoods = new System.Windows.Forms.CheckBox();
            this.gbxPackingDetails = new System.Windows.Forms.GroupBox();
            this.txtTotalCartons = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDG = new System.Windows.Forms.Button();
            this.gbxPackingEntry = new System.Windows.Forms.GroupBox();
            this.nmrPackingCartonsPerPallet = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.nmrPackingQty = new System.Windows.Forms.NumericUpDown();
            this.btnDeletePacking = new System.Windows.Forms.Button();
            this.btnEditPacking = new System.Windows.Forms.Button();
            this.btnAddPacking = new System.Windows.Forms.Button();
            this.chkPackingDG = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPackingEstCost = new System.Windows.Forms.TextBox();
            this.txtPackingComments = new System.Windows.Forms.TextBox();
            this.txtPackingConnoteNo = new System.Windows.Forms.TextBox();
            this.txtPackingWeight = new System.Windows.Forms.TextBox();
            this.txtPackingHeight = new System.Windows.Forms.TextBox();
            this.txtPackingWidth = new System.Windows.Forms.TextBox();
            this.txtPackingLength = new System.Windows.Forms.TextBox();
            this.cboPackingType = new System.Windows.Forms.ComboBox();
            this.lblTypePacking = new System.Windows.Forms.Label();
            this.dgrPackingDetails = new System.Windows.Forms.DataGridView();
            this.lblTotalDimensions = new System.Windows.Forms.Label();
            this.txtTotalDimensions = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtNoOfPallets = new System.Windows.Forms.TextBox();
            this.txtEstimatedCost = new System.Windows.Forms.TextBox();
            this.lblEstimatedCost = new System.Windows.Forms.Label();
            this.txtConnoteNo = new System.Windows.Forms.TextBox();
            this.lblConnote = new System.Windows.Forms.Label();
            this.btnProcessOrder = new System.Windows.Forms.Button();
            this.gbxItemDetails = new System.Windows.Forms.GroupBox();
            this.dgrItems = new System.Windows.Forms.DataGridView();
            this.gbxCustomerOrderDetails = new System.Windows.Forms.GroupBox();
            this.txtCustomerComments = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.btnPrintDG = new System.Windows.Forms.Button();
            this.btnRePrintLabel = new System.Windows.Forms.Button();
            this.btnDeleteConnote = new System.Windows.Forms.Button();
            this.btnRePrintPackingSlip = new System.Windows.Forms.Button();
            this.txtDeliveryPostcode = new System.Windows.Forms.TextBox();
            this.txtDeliveryState = new System.Windows.Forms.TextBox();
            this.txtDeliverySuburb = new System.Windows.Forms.TextBox();
            this.chkPrintPackingSlip = new System.Windows.Forms.CheckBox();
            this.txtCountry = new System.Windows.Forms.TextBox();
            this.txtDeliverToAddress3 = new System.Windows.Forms.TextBox();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.txtDeliverToAddress2 = new System.Windows.Forms.TextBox();
            this.txtExactNo = new System.Windows.Forms.TextBox();
            this.txtShippingDate = new System.Windows.Forms.TextBox();
            this.txtOrderDate = new System.Windows.Forms.TextBox();
            this.txtSpecialInstructions2 = new System.Windows.Forms.TextBox();
            this.txtSpecialInstructions = new System.Windows.Forms.TextBox();
            this.txtWarehouse = new System.Windows.Forms.TextBox();
            this.txtPackedBy = new System.Windows.Forms.TextBox();
            this.txtOrderStatus = new System.Windows.Forms.TextBox();
            this.txtDeliveryName = new System.Windows.Forms.TextBox();
            this.txtDeliverToAddress1 = new System.Windows.Forms.TextBox();
            this.txtCustomerNo = new System.Windows.Forms.TextBox();
            this.lblCustomerName = new System.Windows.Forms.Label();
            this.lblExactNo = new System.Windows.Forms.Label();
            this.lblShippingDate = new System.Windows.Forms.Label();
            this.lblOrderDate = new System.Windows.Forms.Label();
            this.lblSpecialInstructions = new System.Windows.Forms.Label();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.lblPackedBy = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblDeliveryName = new System.Windows.Forms.Label();
            this.lblCountry = new System.Windows.Forms.Label();
            this.lblDeliveryAddress = new System.Windows.Forms.Label();
            this.lblCusCode = new System.Windows.Forms.Label();
            this.spltOrderDetails = new System.Windows.Forms.SplitContainer();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnGenerateManifest = new System.Windows.Forms.Button();
            this.btnPrintManifest = new System.Windows.Forms.Button();
            this.btnPrintSettings = new System.Windows.Forms.Button();
            this.btnAddRemoveOrder = new System.Windows.Forms.Button();
            this.btnSendToOE = new System.Windows.Forms.Button();
            this.btnTestConnect = new System.Windows.Forms.Button();
            this.lblVersion = new System.Windows.Forms.Label();
            this.btnValidate = new System.Windows.Forms.Button();
            this.lblTimer = new System.Windows.Forms.Label();
            this.bgwTimer = new System.ComponentModel.BackgroundWorker();
            this.gbxParameters.SuspendLayout();
            this.gbxPackingDetails.SuspendLayout();
            this.gbxPackingEntry.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmrPackingCartonsPerPallet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmrPackingQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgrPackingDetails)).BeginInit();
            this.gbxItemDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrItems)).BeginInit();
            this.gbxCustomerOrderDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spltOrderDetails)).BeginInit();
            this.spltOrderDetails.Panel1.SuspendLayout();
            this.spltOrderDetails.Panel2.SuspendLayout();
            this.spltOrderDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.Location = new System.Drawing.Point(95, 25);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(166, 20);
            this.txtOrderNo.TabIndex = 0;
            this.txtOrderNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOrderNo_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Order No:";
            // 
            // lblCourier
            // 
            this.lblCourier.AutoSize = true;
            this.lblCourier.Location = new System.Drawing.Point(19, 54);
            this.lblCourier.Name = "lblCourier";
            this.lblCourier.Size = new System.Drawing.Size(43, 13);
            this.lblCourier.TabIndex = 1;
            this.lblCourier.Text = "Courier:";
            // 
            // cboCourier
            // 
            this.cboCourier.FormattingEnabled = true;
            this.cboCourier.Location = new System.Drawing.Point(95, 51);
            this.cboCourier.Name = "cboCourier";
            this.cboCourier.Size = new System.Drawing.Size(166, 21);
            this.cboCourier.TabIndex = 2;
            // 
            // txtNoOfCartons
            // 
            this.txtNoOfCartons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNoOfCartons.Location = new System.Drawing.Point(1096, 107);
            this.txtNoOfCartons.Name = "txtNoOfCartons";
            this.txtNoOfCartons.ReadOnly = true;
            this.txtNoOfCartons.Size = new System.Drawing.Size(69, 20);
            this.txtNoOfCartons.TabIndex = 0;
            this.txtNoOfCartons.Text = "0";
            // 
            // lblTotalWeight
            // 
            this.lblTotalWeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalWeight.AutoSize = true;
            this.lblTotalWeight.Location = new System.Drawing.Point(999, 60);
            this.lblTotalWeight.Name = "lblTotalWeight";
            this.lblTotalWeight.Size = new System.Drawing.Size(92, 13);
            this.lblTotalWeight.TabIndex = 1;
            this.lblTotalWeight.Text = "Total Weight (kg):";
            // 
            // txtTotalWeight
            // 
            this.txtTotalWeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalWeight.Location = new System.Drawing.Point(1096, 57);
            this.txtTotalWeight.Name = "txtTotalWeight";
            this.txtTotalWeight.ReadOnly = true;
            this.txtTotalWeight.Size = new System.Drawing.Size(94, 20);
            this.txtTotalWeight.TabIndex = 0;
            this.txtTotalWeight.Text = "0.0000";
            // 
            // lblNoOfLabels
            // 
            this.lblNoOfLabels.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNoOfLabels.AutoSize = true;
            this.lblNoOfLabels.Location = new System.Drawing.Point(1013, 110);
            this.lblNoOfLabels.Name = "lblNoOfLabels";
            this.lblNoOfLabels.Size = new System.Drawing.Size(78, 13);
            this.lblNoOfLabels.TabIndex = 1;
            this.lblNoOfLabels.Text = "No. of Cartons:";
            // 
            // gbxParameters
            // 
            this.gbxParameters.Controls.Add(this.btnClear);
            this.gbxParameters.Controls.Add(this.btnRetrieveInfo);
            this.gbxParameters.Controls.Add(this.label1);
            this.gbxParameters.Controls.Add(this.txtOrderNo);
            this.gbxParameters.Controls.Add(this.cboCourier);
            this.gbxParameters.Controls.Add(this.lblCourier);
            this.gbxParameters.Controls.Add(this.txtDespatchID);
            this.gbxParameters.Controls.Add(this.lblDespatchID);
            this.gbxParameters.Location = new System.Drawing.Point(12, 12);
            this.gbxParameters.Name = "gbxParameters";
            this.gbxParameters.Size = new System.Drawing.Size(274, 150);
            this.gbxParameters.TabIndex = 4;
            this.gbxParameters.TabStop = false;
            this.gbxParameters.Text = "Enter Details";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(215, 110);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(46, 33);
            this.btnClear.TabIndex = 5;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnRetrieveInfo
            // 
            this.btnRetrieveInfo.Location = new System.Drawing.Point(96, 110);
            this.btnRetrieveInfo.Name = "btnRetrieveInfo";
            this.btnRetrieveInfo.Size = new System.Drawing.Size(113, 34);
            this.btnRetrieveInfo.TabIndex = 4;
            this.btnRetrieveInfo.Text = "New Entry";
            this.btnRetrieveInfo.UseVisualStyleBackColor = true;
            this.btnRetrieveInfo.Click += new System.EventHandler(this.btnRetrieveInfo_Click);
            // 
            // txtDespatchID
            // 
            this.txtDespatchID.Location = new System.Drawing.Point(95, 80);
            this.txtDespatchID.Name = "txtDespatchID";
            this.txtDespatchID.ReadOnly = true;
            this.txtDespatchID.Size = new System.Drawing.Size(94, 20);
            this.txtDespatchID.TabIndex = 23;
            // 
            // lblDespatchID
            // 
            this.lblDespatchID.AutoSize = true;
            this.lblDespatchID.Location = new System.Drawing.Point(16, 83);
            this.lblDespatchID.Name = "lblDespatchID";
            this.lblDespatchID.Size = new System.Drawing.Size(70, 13);
            this.lblDespatchID.TabIndex = 16;
            this.lblDespatchID.Text = "Despatch ID:";
            // 
            // btnLoadPrevious
            // 
            this.btnLoadPrevious.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLoadPrevious.Location = new System.Drawing.Point(104, 619);
            this.btnLoadPrevious.Name = "btnLoadPrevious";
            this.btnLoadPrevious.Size = new System.Drawing.Size(74, 45);
            this.btnLoadPrevious.TabIndex = 6;
            this.btnLoadPrevious.Text = "Load Previous";
            this.btnLoadPrevious.UseVisualStyleBackColor = true;
            this.btnLoadPrevious.Click += new System.EventHandler(this.btnLoadPrevious_Click);
            // 
            // chkInvoiceWithGoods
            // 
            this.chkInvoiceWithGoods.AutoSize = true;
            this.chkInvoiceWithGoods.Location = new System.Drawing.Point(424, 98);
            this.chkInvoiceWithGoods.Name = "chkInvoiceWithGoods";
            this.chkInvoiceWithGoods.Size = new System.Drawing.Size(190, 17);
            this.chkInvoiceWithGoods.TabIndex = 6;
            this.chkInvoiceWithGoods.Text = "Send Printed Invoice With Goods?";
            this.chkInvoiceWithGoods.UseVisualStyleBackColor = true;
            this.chkInvoiceWithGoods.CheckedChanged += new System.EventHandler(this.chkInvoiceWithGoods_CheckedChanged);
            // 
            // gbxPackingDetails
            // 
            this.gbxPackingDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxPackingDetails.Controls.Add(this.txtTotalCartons);
            this.gbxPackingDetails.Controls.Add(this.label3);
            this.gbxPackingDetails.Controls.Add(this.btnDG);
            this.gbxPackingDetails.Controls.Add(this.gbxPackingEntry);
            this.gbxPackingDetails.Controls.Add(this.dgrPackingDetails);
            this.gbxPackingDetails.Controls.Add(this.lblTotalDimensions);
            this.gbxPackingDetails.Controls.Add(this.txtTotalDimensions);
            this.gbxPackingDetails.Controls.Add(this.label13);
            this.gbxPackingDetails.Controls.Add(this.txtNoOfPallets);
            this.gbxPackingDetails.Controls.Add(this.lblNoOfLabels);
            this.gbxPackingDetails.Controls.Add(this.txtNoOfCartons);
            this.gbxPackingDetails.Controls.Add(this.txtTotalWeight);
            this.gbxPackingDetails.Controls.Add(this.lblTotalWeight);
            this.gbxPackingDetails.Controls.Add(this.txtEstimatedCost);
            this.gbxPackingDetails.Controls.Add(this.lblEstimatedCost);
            this.gbxPackingDetails.Location = new System.Drawing.Point(3, 3);
            this.gbxPackingDetails.Name = "gbxPackingDetails";
            this.gbxPackingDetails.Size = new System.Drawing.Size(1200, 235);
            this.gbxPackingDetails.TabIndex = 5;
            this.gbxPackingDetails.TabStop = false;
            this.gbxPackingDetails.Text = "Packing Details";
            // 
            // txtTotalCartons
            // 
            this.txtTotalCartons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalCartons.Location = new System.Drawing.Point(1096, 155);
            this.txtTotalCartons.Name = "txtTotalCartons";
            this.txtTotalCartons.ReadOnly = true;
            this.txtTotalCartons.Size = new System.Drawing.Size(69, 20);
            this.txtTotalCartons.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1018, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Total Cartons:";
            // 
            // btnDG
            // 
            this.btnDG.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDG.Location = new System.Drawing.Point(1081, 25);
            this.btnDG.Name = "btnDG";
            this.btnDG.Size = new System.Drawing.Size(113, 23);
            this.btnDG.TabIndex = 22;
            this.btnDG.Text = "Dangerous Goods";
            this.btnDG.UseVisualStyleBackColor = true;
            this.btnDG.Click += new System.EventHandler(this.btnDG_Click);
            // 
            // gbxPackingEntry
            // 
            this.gbxPackingEntry.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxPackingEntry.Controls.Add(this.nmrPackingCartonsPerPallet);
            this.gbxPackingEntry.Controls.Add(this.label2);
            this.gbxPackingEntry.Controls.Add(this.nmrPackingQty);
            this.gbxPackingEntry.Controls.Add(this.btnDeletePacking);
            this.gbxPackingEntry.Controls.Add(this.btnEditPacking);
            this.gbxPackingEntry.Controls.Add(this.btnAddPacking);
            this.gbxPackingEntry.Controls.Add(this.chkPackingDG);
            this.gbxPackingEntry.Controls.Add(this.label8);
            this.gbxPackingEntry.Controls.Add(this.label7);
            this.gbxPackingEntry.Controls.Add(this.label11);
            this.gbxPackingEntry.Controls.Add(this.label12);
            this.gbxPackingEntry.Controls.Add(this.label10);
            this.gbxPackingEntry.Controls.Add(this.label9);
            this.gbxPackingEntry.Controls.Add(this.label6);
            this.gbxPackingEntry.Controls.Add(this.label5);
            this.gbxPackingEntry.Controls.Add(this.label14);
            this.gbxPackingEntry.Controls.Add(this.label4);
            this.gbxPackingEntry.Controls.Add(this.txtPackingEstCost);
            this.gbxPackingEntry.Controls.Add(this.txtPackingComments);
            this.gbxPackingEntry.Controls.Add(this.txtPackingConnoteNo);
            this.gbxPackingEntry.Controls.Add(this.txtPackingWeight);
            this.gbxPackingEntry.Controls.Add(this.txtPackingHeight);
            this.gbxPackingEntry.Controls.Add(this.txtPackingWidth);
            this.gbxPackingEntry.Controls.Add(this.txtPackingLength);
            this.gbxPackingEntry.Controls.Add(this.cboPackingType);
            this.gbxPackingEntry.Controls.Add(this.lblTypePacking);
            this.gbxPackingEntry.Location = new System.Drawing.Point(6, 13);
            this.gbxPackingEntry.Name = "gbxPackingEntry";
            this.gbxPackingEntry.Size = new System.Drawing.Size(977, 58);
            this.gbxPackingEntry.TabIndex = 21;
            this.gbxPackingEntry.TabStop = false;
            // 
            // nmrPackingCartonsPerPallet
            // 
            this.nmrPackingCartonsPerPallet.Enabled = false;
            this.nmrPackingCartonsPerPallet.Location = new System.Drawing.Point(475, 27);
            this.nmrPackingCartonsPerPallet.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.nmrPackingCartonsPerPallet.Name = "nmrPackingCartonsPerPallet";
            this.nmrPackingCartonsPerPallet.Size = new System.Drawing.Size(70, 20);
            this.nmrPackingCartonsPerPallet.TabIndex = 6;
            this.nmrPackingCartonsPerPallet.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nmrPackingCartonsPerPallet.Enter += new System.EventHandler(this.nmrPackingCartonsPerPallet_Enter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label2.Location = new System.Drawing.Point(469, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "CTNs Per Pallet";
            // 
            // nmrPackingQty
            // 
            this.nmrPackingQty.Location = new System.Drawing.Point(99, 28);
            this.nmrPackingQty.Name = "nmrPackingQty";
            this.nmrPackingQty.Size = new System.Drawing.Size(69, 20);
            this.nmrPackingQty.TabIndex = 1;
            this.nmrPackingQty.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nmrPackingQty.Enter += new System.EventHandler(this.nmrPackingQty_Enter);
            // 
            // btnDeletePacking
            // 
            this.btnDeletePacking.Location = new System.Drawing.Point(940, 25);
            this.btnDeletePacking.Name = "btnDeletePacking";
            this.btnDeletePacking.Size = new System.Drawing.Size(34, 23);
            this.btnDeletePacking.TabIndex = 13;
            this.btnDeletePacking.Text = "Delete";
            this.btnDeletePacking.UseVisualStyleBackColor = true;
            this.btnDeletePacking.Click += new System.EventHandler(this.btnDeletePacking_Click);
            // 
            // btnEditPacking
            // 
            this.btnEditPacking.Location = new System.Drawing.Point(900, 25);
            this.btnEditPacking.Name = "btnEditPacking";
            this.btnEditPacking.Size = new System.Drawing.Size(34, 23);
            this.btnEditPacking.TabIndex = 12;
            this.btnEditPacking.Text = "Edit";
            this.btnEditPacking.UseVisualStyleBackColor = true;
            this.btnEditPacking.Click += new System.EventHandler(this.btnEditPacking_Click);
            // 
            // btnAddPacking
            // 
            this.btnAddPacking.Location = new System.Drawing.Point(859, 25);
            this.btnAddPacking.Name = "btnAddPacking";
            this.btnAddPacking.Size = new System.Drawing.Size(35, 23);
            this.btnAddPacking.TabIndex = 11;
            this.btnAddPacking.Text = "Add";
            this.btnAddPacking.UseVisualStyleBackColor = true;
            this.btnAddPacking.Click += new System.EventHandler(this.btnAddPacking_Click);
            // 
            // chkPackingDG
            // 
            this.chkPackingDG.AutoSize = true;
            this.chkPackingDG.Location = new System.Drawing.Point(692, 30);
            this.chkPackingDG.Name = "chkPackingDG";
            this.chkPackingDG.Size = new System.Drawing.Size(58, 17);
            this.chkPackingDG.TabIndex = 9;
            this.chkPackingDG.Text = "is DG?";
            this.chkPackingDG.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(391, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "x";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(314, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(12, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "x";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(635, 10);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Est. Cost";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(750, 11);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Comments";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(548, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Connote No";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(171, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Weight (kg)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(401, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Height (cm)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(324, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Width (cm)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(103, 11);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(23, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Qty";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(246, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Length (cm)";
            // 
            // txtPackingEstCost
            // 
            this.txtPackingEstCost.Location = new System.Drawing.Point(638, 28);
            this.txtPackingEstCost.Name = "txtPackingEstCost";
            this.txtPackingEstCost.Size = new System.Drawing.Size(46, 20);
            this.txtPackingEstCost.TabIndex = 8;
            this.txtPackingEstCost.Enter += new System.EventHandler(this.txtPackingEstCost_Enter);
            // 
            // txtPackingComments
            // 
            this.txtPackingComments.Location = new System.Drawing.Point(753, 29);
            this.txtPackingComments.Name = "txtPackingComments";
            this.txtPackingComments.Size = new System.Drawing.Size(99, 20);
            this.txtPackingComments.TabIndex = 10;
            this.txtPackingComments.Enter += new System.EventHandler(this.txtPackingComments_Enter);
            // 
            // txtPackingConnoteNo
            // 
            this.txtPackingConnoteNo.Location = new System.Drawing.Point(551, 28);
            this.txtPackingConnoteNo.Name = "txtPackingConnoteNo";
            this.txtPackingConnoteNo.Size = new System.Drawing.Size(79, 20);
            this.txtPackingConnoteNo.TabIndex = 7;
            this.txtPackingConnoteNo.Enter += new System.EventHandler(this.txtPackingConnoteNo_Enter);
            this.txtPackingConnoteNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPackingConnoteNo_KeyDown);
            // 
            // txtPackingWeight
            // 
            this.txtPackingWeight.Location = new System.Drawing.Point(174, 27);
            this.txtPackingWeight.Name = "txtPackingWeight";
            this.txtPackingWeight.Size = new System.Drawing.Size(62, 20);
            this.txtPackingWeight.TabIndex = 2;
            this.txtPackingWeight.Enter += new System.EventHandler(this.txtPackingWeight_Enter);
            this.txtPackingWeight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPackingWeight_KeyDown);
            // 
            // txtPackingHeight
            // 
            this.txtPackingHeight.Location = new System.Drawing.Point(404, 27);
            this.txtPackingHeight.Name = "txtPackingHeight";
            this.txtPackingHeight.Size = new System.Drawing.Size(62, 20);
            this.txtPackingHeight.TabIndex = 5;
            this.txtPackingHeight.Enter += new System.EventHandler(this.txtPackingHeight_Enter);
            this.txtPackingHeight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPackingHeight_KeyDown);
            // 
            // txtPackingWidth
            // 
            this.txtPackingWidth.Location = new System.Drawing.Point(327, 27);
            this.txtPackingWidth.Name = "txtPackingWidth";
            this.txtPackingWidth.Size = new System.Drawing.Size(62, 20);
            this.txtPackingWidth.TabIndex = 4;
            this.txtPackingWidth.Enter += new System.EventHandler(this.txtPackingWidth_Enter);
            this.txtPackingWidth.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPackingWidth_KeyDown);
            // 
            // txtPackingLength
            // 
            this.txtPackingLength.Location = new System.Drawing.Point(249, 27);
            this.txtPackingLength.Name = "txtPackingLength";
            this.txtPackingLength.Size = new System.Drawing.Size(62, 20);
            this.txtPackingLength.TabIndex = 3;
            this.txtPackingLength.Enter += new System.EventHandler(this.txtPackingLength_Enter);
            this.txtPackingLength.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPackingLength_KeyDown);
            // 
            // cboPackingType
            // 
            this.cboPackingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPackingType.FormattingEnabled = true;
            this.cboPackingType.Location = new System.Drawing.Point(11, 29);
            this.cboPackingType.Name = "cboPackingType";
            this.cboPackingType.Size = new System.Drawing.Size(82, 21);
            this.cboPackingType.TabIndex = 0;
            this.cboPackingType.SelectedIndexChanged += new System.EventHandler(this.cboPackingType_SelectedIndexChanged);
            this.cboPackingType.Enter += new System.EventHandler(this.cboPackingType_Enter);
            // 
            // lblTypePacking
            // 
            this.lblTypePacking.AutoSize = true;
            this.lblTypePacking.Location = new System.Drawing.Point(8, 11);
            this.lblTypePacking.Name = "lblTypePacking";
            this.lblTypePacking.Size = new System.Drawing.Size(31, 13);
            this.lblTypePacking.TabIndex = 1;
            this.lblTypePacking.Text = "Type";
            // 
            // dgrPackingDetails
            // 
            this.dgrPackingDetails.AllowUserToAddRows = false;
            this.dgrPackingDetails.AllowUserToDeleteRows = false;
            this.dgrPackingDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgrPackingDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgrPackingDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrPackingDetails.Location = new System.Drawing.Point(6, 77);
            this.dgrPackingDetails.MultiSelect = false;
            this.dgrPackingDetails.Name = "dgrPackingDetails";
            this.dgrPackingDetails.ReadOnly = true;
            this.dgrPackingDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrPackingDetails.Size = new System.Drawing.Size(977, 152);
            this.dgrPackingDetails.TabIndex = 0;
            this.dgrPackingDetails.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgrPackingDetails_CellClick);
            // 
            // lblTotalDimensions
            // 
            this.lblTotalDimensions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalDimensions.AutoSize = true;
            this.lblTotalDimensions.Location = new System.Drawing.Point(989, 86);
            this.lblTotalDimensions.Name = "lblTotalDimensions";
            this.lblTotalDimensions.Size = new System.Drawing.Size(101, 13);
            this.lblTotalDimensions.TabIndex = 1;
            this.lblTotalDimensions.Text = "Total Volume (cbm):";
            // 
            // txtTotalDimensions
            // 
            this.txtTotalDimensions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalDimensions.Location = new System.Drawing.Point(1096, 83);
            this.txtTotalDimensions.Name = "txtTotalDimensions";
            this.txtTotalDimensions.ReadOnly = true;
            this.txtTotalDimensions.Size = new System.Drawing.Size(69, 20);
            this.txtTotalDimensions.TabIndex = 0;
            this.txtTotalDimensions.Text = "0.0000";
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(1018, 132);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "No. of Pallets:";
            // 
            // txtNoOfPallets
            // 
            this.txtNoOfPallets.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNoOfPallets.Location = new System.Drawing.Point(1096, 129);
            this.txtNoOfPallets.Name = "txtNoOfPallets";
            this.txtNoOfPallets.ReadOnly = true;
            this.txtNoOfPallets.Size = new System.Drawing.Size(69, 20);
            this.txtNoOfPallets.TabIndex = 0;
            this.txtNoOfPallets.Text = "0";
            // 
            // txtEstimatedCost
            // 
            this.txtEstimatedCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEstimatedCost.Location = new System.Drawing.Point(1096, 181);
            this.txtEstimatedCost.Name = "txtEstimatedCost";
            this.txtEstimatedCost.Size = new System.Drawing.Size(94, 20);
            this.txtEstimatedCost.TabIndex = 20;
            this.txtEstimatedCost.Text = "0.00";
            // 
            // lblEstimatedCost
            // 
            this.lblEstimatedCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEstimatedCost.AutoSize = true;
            this.lblEstimatedCost.Location = new System.Drawing.Point(1011, 184);
            this.lblEstimatedCost.Name = "lblEstimatedCost";
            this.lblEstimatedCost.Size = new System.Drawing.Size(80, 13);
            this.lblEstimatedCost.TabIndex = 19;
            this.lblEstimatedCost.Text = "Estimated Cost:";
            // 
            // txtConnoteNo
            // 
            this.txtConnoteNo.Location = new System.Drawing.Point(463, 43);
            this.txtConnoteNo.Name = "txtConnoteNo";
            this.txtConnoteNo.ReadOnly = true;
            this.txtConnoteNo.Size = new System.Drawing.Size(94, 20);
            this.txtConnoteNo.TabIndex = 13;
            // 
            // lblConnote
            // 
            this.lblConnote.AutoSize = true;
            this.lblConnote.Location = new System.Drawing.Point(389, 46);
            this.lblConnote.Name = "lblConnote";
            this.lblConnote.Size = new System.Drawing.Size(67, 13);
            this.lblConnote.TabIndex = 19;
            this.lblConnote.Text = "Connote No:";
            // 
            // btnProcessOrder
            // 
            this.btnProcessOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProcessOrder.Location = new System.Drawing.Point(1015, 618);
            this.btnProcessOrder.Name = "btnProcessOrder";
            this.btnProcessOrder.Size = new System.Drawing.Size(202, 46);
            this.btnProcessOrder.TabIndex = 6;
            this.btnProcessOrder.Text = "Process Order";
            this.btnProcessOrder.UseVisualStyleBackColor = true;
            this.btnProcessOrder.Click += new System.EventHandler(this.btnProcessOrder_Click);
            // 
            // gbxItemDetails
            // 
            this.gbxItemDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxItemDetails.Controls.Add(this.dgrItems);
            this.gbxItemDetails.Location = new System.Drawing.Point(3, 3);
            this.gbxItemDetails.Name = "gbxItemDetails";
            this.gbxItemDetails.Size = new System.Drawing.Size(1199, 193);
            this.gbxItemDetails.TabIndex = 7;
            this.gbxItemDetails.TabStop = false;
            this.gbxItemDetails.Text = "Item Details";
            // 
            // dgrItems
            // 
            this.dgrItems.AllowUserToAddRows = false;
            this.dgrItems.AllowUserToDeleteRows = false;
            this.dgrItems.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgrItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgrItems.Location = new System.Drawing.Point(3, 16);
            this.dgrItems.Name = "dgrItems";
            this.dgrItems.Size = new System.Drawing.Size(1193, 174);
            this.dgrItems.TabIndex = 0;
            this.dgrItems.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgrItems_RowPostPaint);
            // 
            // gbxCustomerOrderDetails
            // 
            this.gbxCustomerOrderDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxCustomerOrderDetails.Controls.Add(this.txtCustomerComments);
            this.gbxCustomerOrderDetails.Controls.Add(this.label15);
            this.gbxCustomerOrderDetails.Controls.Add(this.btnPrintDG);
            this.gbxCustomerOrderDetails.Controls.Add(this.btnRePrintLabel);
            this.gbxCustomerOrderDetails.Controls.Add(this.btnDeleteConnote);
            this.gbxCustomerOrderDetails.Controls.Add(this.btnRePrintPackingSlip);
            this.gbxCustomerOrderDetails.Controls.Add(this.txtDeliveryPostcode);
            this.gbxCustomerOrderDetails.Controls.Add(this.txtDeliveryState);
            this.gbxCustomerOrderDetails.Controls.Add(this.txtDeliverySuburb);
            this.gbxCustomerOrderDetails.Controls.Add(this.chkInvoiceWithGoods);
            this.gbxCustomerOrderDetails.Controls.Add(this.chkPrintPackingSlip);
            this.gbxCustomerOrderDetails.Controls.Add(this.txtCountry);
            this.gbxCustomerOrderDetails.Controls.Add(this.txtDeliverToAddress3);
            this.gbxCustomerOrderDetails.Controls.Add(this.txtCustomerName);
            this.gbxCustomerOrderDetails.Controls.Add(this.txtDeliverToAddress2);
            this.gbxCustomerOrderDetails.Controls.Add(this.txtExactNo);
            this.gbxCustomerOrderDetails.Controls.Add(this.txtShippingDate);
            this.gbxCustomerOrderDetails.Controls.Add(this.txtOrderDate);
            this.gbxCustomerOrderDetails.Controls.Add(this.txtSpecialInstructions2);
            this.gbxCustomerOrderDetails.Controls.Add(this.txtSpecialInstructions);
            this.gbxCustomerOrderDetails.Controls.Add(this.txtConnoteNo);
            this.gbxCustomerOrderDetails.Controls.Add(this.lblConnote);
            this.gbxCustomerOrderDetails.Controls.Add(this.txtWarehouse);
            this.gbxCustomerOrderDetails.Controls.Add(this.txtPackedBy);
            this.gbxCustomerOrderDetails.Controls.Add(this.txtOrderStatus);
            this.gbxCustomerOrderDetails.Controls.Add(this.txtDeliveryName);
            this.gbxCustomerOrderDetails.Controls.Add(this.txtDeliverToAddress1);
            this.gbxCustomerOrderDetails.Controls.Add(this.txtCustomerNo);
            this.gbxCustomerOrderDetails.Controls.Add(this.lblCustomerName);
            this.gbxCustomerOrderDetails.Controls.Add(this.lblExactNo);
            this.gbxCustomerOrderDetails.Controls.Add(this.lblShippingDate);
            this.gbxCustomerOrderDetails.Controls.Add(this.lblOrderDate);
            this.gbxCustomerOrderDetails.Controls.Add(this.lblSpecialInstructions);
            this.gbxCustomerOrderDetails.Controls.Add(this.lblWarehouse);
            this.gbxCustomerOrderDetails.Controls.Add(this.lblPackedBy);
            this.gbxCustomerOrderDetails.Controls.Add(this.lblStatus);
            this.gbxCustomerOrderDetails.Controls.Add(this.lblDeliveryName);
            this.gbxCustomerOrderDetails.Controls.Add(this.lblCountry);
            this.gbxCustomerOrderDetails.Controls.Add(this.lblDeliveryAddress);
            this.gbxCustomerOrderDetails.Controls.Add(this.lblCusCode);
            this.gbxCustomerOrderDetails.Location = new System.Drawing.Point(292, 12);
            this.gbxCustomerOrderDetails.Name = "gbxCustomerOrderDetails";
            this.gbxCustomerOrderDetails.Size = new System.Drawing.Size(925, 150);
            this.gbxCustomerOrderDetails.TabIndex = 9;
            this.gbxCustomerOrderDetails.TabStop = false;
            this.gbxCustomerOrderDetails.Text = "Customer Order Details";
            // 
            // txtCustomerComments
            // 
            this.txtCustomerComments.Location = new System.Drawing.Point(463, 68);
            this.txtCustomerComments.Name = "txtCustomerComments";
            this.txtCustomerComments.Size = new System.Drawing.Size(181, 20);
            this.txtCustomerComments.TabIndex = 39;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(373, 71);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 13);
            this.label15.TabIndex = 38;
            this.label15.Text = "Cust Comments:";
            // 
            // btnPrintDG
            // 
            this.btnPrintDG.Location = new System.Drawing.Point(610, 115);
            this.btnPrintDG.Name = "btnPrintDG";
            this.btnPrintDG.Size = new System.Drawing.Size(63, 23);
            this.btnPrintDG.TabIndex = 37;
            this.btnPrintDG.Text = "Print DG";
            this.btnPrintDG.UseVisualStyleBackColor = true;
            this.btnPrintDG.Click += new System.EventHandler(this.btnPrintDG_Click);
            // 
            // btnRePrintLabel
            // 
            this.btnRePrintLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.btnRePrintLabel.Location = new System.Drawing.Point(564, 41);
            this.btnRePrintLabel.Name = "btnRePrintLabel";
            this.btnRePrintLabel.Size = new System.Drawing.Size(40, 23);
            this.btnRePrintLabel.TabIndex = 36;
            this.btnRePrintLabel.Text = "Print";
            this.btnRePrintLabel.UseVisualStyleBackColor = true;
            this.btnRePrintLabel.Click += new System.EventHandler(this.btnRePrintLabel_Click);
            // 
            // btnDeleteConnote
            // 
            this.btnDeleteConnote.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.btnDeleteConnote.Location = new System.Drawing.Point(610, 41);
            this.btnDeleteConnote.Name = "btnDeleteConnote";
            this.btnDeleteConnote.Size = new System.Drawing.Size(34, 23);
            this.btnDeleteConnote.TabIndex = 35;
            this.btnDeleteConnote.Text = "Del";
            this.btnDeleteConnote.UseVisualStyleBackColor = true;
            this.btnDeleteConnote.Visible = false;
            this.btnDeleteConnote.Click += new System.EventHandler(this.btnDeleteConnote_Click);
            // 
            // btnRePrintPackingSlip
            // 
            this.btnRePrintPackingSlip.Location = new System.Drawing.Point(545, 115);
            this.btnRePrintPackingSlip.Name = "btnRePrintPackingSlip";
            this.btnRePrintPackingSlip.Size = new System.Drawing.Size(59, 23);
            this.btnRePrintPackingSlip.TabIndex = 34;
            this.btnRePrintPackingSlip.Text = "Print PS";
            this.btnRePrintPackingSlip.UseVisualStyleBackColor = true;
            this.btnRePrintPackingSlip.Click += new System.EventHandler(this.btnRePrintPackingSlip_Click);
            // 
            // txtDeliveryPostcode
            // 
            this.txtDeliveryPostcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDeliveryPostcode.Location = new System.Drawing.Point(865, 91);
            this.txtDeliveryPostcode.Name = "txtDeliveryPostcode";
            this.txtDeliveryPostcode.Size = new System.Drawing.Size(49, 20);
            this.txtDeliveryPostcode.TabIndex = 33;
            // 
            // txtDeliveryState
            // 
            this.txtDeliveryState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDeliveryState.Location = new System.Drawing.Point(805, 91);
            this.txtDeliveryState.Name = "txtDeliveryState";
            this.txtDeliveryState.Size = new System.Drawing.Size(54, 20);
            this.txtDeliveryState.TabIndex = 33;
            // 
            // txtDeliverySuburb
            // 
            this.txtDeliverySuburb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDeliverySuburb.Location = new System.Drawing.Point(718, 91);
            this.txtDeliverySuburb.Name = "txtDeliverySuburb";
            this.txtDeliverySuburb.Size = new System.Drawing.Size(81, 20);
            this.txtDeliverySuburb.TabIndex = 33;
            // 
            // chkPrintPackingSlip
            // 
            this.chkPrintPackingSlip.AutoSize = true;
            this.chkPrintPackingSlip.Checked = true;
            this.chkPrintPackingSlip.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPrintPackingSlip.Location = new System.Drawing.Point(424, 119);
            this.chkPrintPackingSlip.Name = "chkPrintPackingSlip";
            this.chkPrintPackingSlip.Size = new System.Drawing.Size(115, 17);
            this.chkPrintPackingSlip.TabIndex = 15;
            this.chkPrintPackingSlip.Text = "Print Packing Slip?";
            this.chkPrintPackingSlip.UseVisualStyleBackColor = true;
            // 
            // txtCountry
            // 
            this.txtCountry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCountry.Location = new System.Drawing.Point(855, 117);
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.Size = new System.Drawing.Size(59, 20);
            this.txtCountry.TabIndex = 28;
            // 
            // txtDeliverToAddress3
            // 
            this.txtDeliverToAddress3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDeliverToAddress3.Location = new System.Drawing.Point(718, 67);
            this.txtDeliverToAddress3.Name = "txtDeliverToAddress3";
            this.txtDeliverToAddress3.Size = new System.Drawing.Size(196, 20);
            this.txtDeliverToAddress3.TabIndex = 27;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Location = new System.Drawing.Point(104, 68);
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.ReadOnly = true;
            this.txtCustomerName.Size = new System.Drawing.Size(109, 20);
            this.txtCustomerName.TabIndex = 26;
            // 
            // txtDeliverToAddress2
            // 
            this.txtDeliverToAddress2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDeliverToAddress2.Location = new System.Drawing.Point(718, 43);
            this.txtDeliverToAddress2.Name = "txtDeliverToAddress2";
            this.txtDeliverToAddress2.Size = new System.Drawing.Size(196, 20);
            this.txtDeliverToAddress2.TabIndex = 29;
            // 
            // txtExactNo
            // 
            this.txtExactNo.Location = new System.Drawing.Point(104, 44);
            this.txtExactNo.Name = "txtExactNo";
            this.txtExactNo.ReadOnly = true;
            this.txtExactNo.Size = new System.Drawing.Size(109, 20);
            this.txtExactNo.TabIndex = 32;
            // 
            // txtShippingDate
            // 
            this.txtShippingDate.Location = new System.Drawing.Point(104, 118);
            this.txtShippingDate.Name = "txtShippingDate";
            this.txtShippingDate.ReadOnly = true;
            this.txtShippingDate.Size = new System.Drawing.Size(56, 20);
            this.txtShippingDate.TabIndex = 31;
            // 
            // txtOrderDate
            // 
            this.txtOrderDate.Location = new System.Drawing.Point(104, 95);
            this.txtOrderDate.Name = "txtOrderDate";
            this.txtOrderDate.ReadOnly = true;
            this.txtOrderDate.Size = new System.Drawing.Size(56, 20);
            this.txtOrderDate.TabIndex = 30;
            // 
            // txtSpecialInstructions2
            // 
            this.txtSpecialInstructions2.Location = new System.Drawing.Point(274, 118);
            this.txtSpecialInstructions2.Name = "txtSpecialInstructions2";
            this.txtSpecialInstructions2.Size = new System.Drawing.Size(130, 20);
            this.txtSpecialInstructions2.TabIndex = 21;
            // 
            // txtSpecialInstructions
            // 
            this.txtSpecialInstructions.Location = new System.Drawing.Point(274, 95);
            this.txtSpecialInstructions.Name = "txtSpecialInstructions";
            this.txtSpecialInstructions.Size = new System.Drawing.Size(130, 20);
            this.txtSpecialInstructions.TabIndex = 21;
            // 
            // txtWarehouse
            // 
            this.txtWarehouse.Location = new System.Drawing.Point(299, 44);
            this.txtWarehouse.Name = "txtWarehouse";
            this.txtWarehouse.ReadOnly = true;
            this.txtWarehouse.Size = new System.Drawing.Size(38, 20);
            this.txtWarehouse.TabIndex = 21;
            // 
            // txtPackedBy
            // 
            this.txtPackedBy.Location = new System.Drawing.Point(299, 67);
            this.txtPackedBy.Name = "txtPackedBy";
            this.txtPackedBy.ReadOnly = true;
            this.txtPackedBy.Size = new System.Drawing.Size(61, 20);
            this.txtPackedBy.TabIndex = 20;
            // 
            // txtOrderStatus
            // 
            this.txtOrderStatus.Location = new System.Drawing.Point(299, 19);
            this.txtOrderStatus.Name = "txtOrderStatus";
            this.txtOrderStatus.ReadOnly = true;
            this.txtOrderStatus.Size = new System.Drawing.Size(38, 20);
            this.txtOrderStatus.TabIndex = 20;
            // 
            // txtDeliveryName
            // 
            this.txtDeliveryName.Location = new System.Drawing.Point(463, 13);
            this.txtDeliveryName.Name = "txtDeliveryName";
            this.txtDeliveryName.Size = new System.Drawing.Size(94, 20);
            this.txtDeliveryName.TabIndex = 22;
            // 
            // txtDeliverToAddress1
            // 
            this.txtDeliverToAddress1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDeliverToAddress1.Location = new System.Drawing.Point(718, 19);
            this.txtDeliverToAddress1.Name = "txtDeliverToAddress1";
            this.txtDeliverToAddress1.Size = new System.Drawing.Size(196, 20);
            this.txtDeliverToAddress1.TabIndex = 22;
            // 
            // txtCustomerNo
            // 
            this.txtCustomerNo.Location = new System.Drawing.Point(104, 20);
            this.txtCustomerNo.Name = "txtCustomerNo";
            this.txtCustomerNo.ReadOnly = true;
            this.txtCustomerNo.Size = new System.Drawing.Size(109, 20);
            this.txtCustomerNo.TabIndex = 23;
            // 
            // lblCustomerName
            // 
            this.lblCustomerName.AutoSize = true;
            this.lblCustomerName.Location = new System.Drawing.Point(16, 71);
            this.lblCustomerName.Name = "lblCustomerName";
            this.lblCustomerName.Size = new System.Drawing.Size(85, 13);
            this.lblCustomerName.TabIndex = 13;
            this.lblCustomerName.Text = "Customer Name:";
            // 
            // lblExactNo
            // 
            this.lblExactNo.AutoSize = true;
            this.lblExactNo.Location = new System.Drawing.Point(16, 47);
            this.lblExactNo.Name = "lblExactNo";
            this.lblExactNo.Size = new System.Drawing.Size(54, 13);
            this.lblExactNo.TabIndex = 14;
            this.lblExactNo.Text = "Exact No:";
            // 
            // lblShippingDate
            // 
            this.lblShippingDate.AutoSize = true;
            this.lblShippingDate.Location = new System.Drawing.Point(16, 121);
            this.lblShippingDate.Name = "lblShippingDate";
            this.lblShippingDate.Size = new System.Drawing.Size(77, 13);
            this.lblShippingDate.TabIndex = 12;
            this.lblShippingDate.Text = "Shipping Date:";
            // 
            // lblOrderDate
            // 
            this.lblOrderDate.AutoSize = true;
            this.lblOrderDate.Location = new System.Drawing.Point(16, 98);
            this.lblOrderDate.Name = "lblOrderDate";
            this.lblOrderDate.Size = new System.Drawing.Size(62, 13);
            this.lblOrderDate.TabIndex = 10;
            this.lblOrderDate.Text = "Order Date:";
            // 
            // lblSpecialInstructions
            // 
            this.lblSpecialInstructions.AutoSize = true;
            this.lblSpecialInstructions.Location = new System.Drawing.Point(166, 98);
            this.lblSpecialInstructions.Name = "lblSpecialInstructions";
            this.lblSpecialInstructions.Size = new System.Drawing.Size(102, 13);
            this.lblSpecialInstructions.TabIndex = 18;
            this.lblSpecialInstructions.Text = "Special Instructions:";
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.Location = new System.Drawing.Point(224, 47);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(65, 13);
            this.lblWarehouse.TabIndex = 18;
            this.lblWarehouse.Text = "Warehouse:";
            // 
            // lblPackedBy
            // 
            this.lblPackedBy.AutoSize = true;
            this.lblPackedBy.Location = new System.Drawing.Point(224, 70);
            this.lblPackedBy.Name = "lblPackedBy";
            this.lblPackedBy.Size = new System.Drawing.Size(62, 13);
            this.lblPackedBy.TabIndex = 19;
            this.lblPackedBy.Text = "Packed By:";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(224, 22);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(69, 13);
            this.lblStatus.TabIndex = 19;
            this.lblStatus.Text = "Order Status:";
            // 
            // lblDeliveryName
            // 
            this.lblDeliveryName.AutoSize = true;
            this.lblDeliveryName.Location = new System.Drawing.Point(384, 16);
            this.lblDeliveryName.Name = "lblDeliveryName";
            this.lblDeliveryName.Size = new System.Drawing.Size(79, 13);
            this.lblDeliveryName.TabIndex = 15;
            this.lblDeliveryName.Text = "Delivery Name:";
            // 
            // lblCountry
            // 
            this.lblCountry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCountry.AutoSize = true;
            this.lblCountry.Location = new System.Drawing.Point(810, 120);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(46, 13);
            this.lblCountry.TabIndex = 17;
            this.lblCountry.Text = "Country:";
            // 
            // lblDeliveryAddress
            // 
            this.lblDeliveryAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDeliveryAddress.AutoSize = true;
            this.lblDeliveryAddress.Location = new System.Drawing.Point(660, 22);
            this.lblDeliveryAddress.Name = "lblDeliveryAddress";
            this.lblDeliveryAddress.Size = new System.Drawing.Size(59, 13);
            this.lblDeliveryAddress.TabIndex = 15;
            this.lblDeliveryAddress.Text = "Deliver To:";
            // 
            // lblCusCode
            // 
            this.lblCusCode.AutoSize = true;
            this.lblCusCode.Location = new System.Drawing.Point(16, 23);
            this.lblCusCode.Name = "lblCusCode";
            this.lblCusCode.Size = new System.Drawing.Size(71, 13);
            this.lblCusCode.TabIndex = 16;
            this.lblCusCode.Text = "Customer No:";
            // 
            // spltOrderDetails
            // 
            this.spltOrderDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.spltOrderDetails.Location = new System.Drawing.Point(12, 168);
            this.spltOrderDetails.Name = "spltOrderDetails";
            this.spltOrderDetails.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spltOrderDetails.Panel1
            // 
            this.spltOrderDetails.Panel1.Controls.Add(this.gbxItemDetails);
            // 
            // spltOrderDetails.Panel2
            // 
            this.spltOrderDetails.Panel2.Controls.Add(this.gbxPackingDetails);
            this.spltOrderDetails.Size = new System.Drawing.Size(1205, 444);
            this.spltOrderDetails.SplitterDistance = 199;
            this.spltOrderDetails.TabIndex = 10;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(15, 619);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(83, 45);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Save Despatch";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnGenerateManifest
            // 
            this.btnGenerateManifest.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnGenerateManifest.Location = new System.Drawing.Point(492, 618);
            this.btnGenerateManifest.Name = "btnGenerateManifest";
            this.btnGenerateManifest.Size = new System.Drawing.Size(129, 45);
            this.btnGenerateManifest.TabIndex = 16;
            this.btnGenerateManifest.Text = "Generate Manifest";
            this.btnGenerateManifest.UseVisualStyleBackColor = true;
            this.btnGenerateManifest.Click += new System.EventHandler(this.btnGenerateManifest_Click);
            // 
            // btnPrintManifest
            // 
            this.btnPrintManifest.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnPrintManifest.Location = new System.Drawing.Point(627, 618);
            this.btnPrintManifest.Name = "btnPrintManifest";
            this.btnPrintManifest.Size = new System.Drawing.Size(91, 45);
            this.btnPrintManifest.TabIndex = 17;
            this.btnPrintManifest.Text = "Print Manifest";
            this.btnPrintManifest.UseVisualStyleBackColor = true;
            this.btnPrintManifest.Click += new System.EventHandler(this.btnPrintManifest_Click);
            // 
            // btnPrintSettings
            // 
            this.btnPrintSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrintSettings.Location = new System.Drawing.Point(293, 619);
            this.btnPrintSettings.Name = "btnPrintSettings";
            this.btnPrintSettings.Size = new System.Drawing.Size(100, 45);
            this.btnPrintSettings.TabIndex = 19;
            this.btnPrintSettings.Text = "Printer Settings";
            this.btnPrintSettings.UseVisualStyleBackColor = true;
            this.btnPrintSettings.Click += new System.EventHandler(this.btnPrintSettings_Click);
            // 
            // btnAddRemoveOrder
            // 
            this.btnAddRemoveOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddRemoveOrder.Location = new System.Drawing.Point(184, 618);
            this.btnAddRemoveOrder.Name = "btnAddRemoveOrder";
            this.btnAddRemoveOrder.Size = new System.Drawing.Size(106, 46);
            this.btnAddRemoveOrder.TabIndex = 21;
            this.btnAddRemoveOrder.Text = "+ Additional Order";
            this.btnAddRemoveOrder.UseVisualStyleBackColor = true;
            this.btnAddRemoveOrder.Click += new System.EventHandler(this.btnAddRemoveOrder_Click);
            // 
            // btnSendToOE
            // 
            this.btnSendToOE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendToOE.Location = new System.Drawing.Point(849, 619);
            this.btnSendToOE.Name = "btnSendToOE";
            this.btnSendToOE.Size = new System.Drawing.Size(151, 45);
            this.btnSendToOE.TabIndex = 22;
            this.btnSendToOE.Text = "Send To OE";
            this.btnSendToOE.UseVisualStyleBackColor = true;
            this.btnSendToOE.Click += new System.EventHandler(this.btnSendToOE_Click);
            // 
            // btnTestConnect
            // 
            this.btnTestConnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnTestConnect.Location = new System.Drawing.Point(399, 619);
            this.btnTestConnect.Name = "btnTestConnect";
            this.btnTestConnect.Size = new System.Drawing.Size(75, 45);
            this.btnTestConnect.TabIndex = 23;
            this.btnTestConnect.Text = "Test Connect";
            this.btnTestConnect.UseVisualStyleBackColor = true;
            this.btnTestConnect.Click += new System.EventHandler(this.btnTestConnect_Click);
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(1178, 1);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(28, 13);
            this.lblVersion.TabIndex = 24;
            this.lblVersion.Text = "v4.4";
            // 
            // btnValidate
            // 
            this.btnValidate.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnValidate.Location = new System.Drawing.Point(727, 640);
            this.btnValidate.Name = "btnValidate";
            this.btnValidate.Size = new System.Drawing.Size(75, 23);
            this.btnValidate.TabIndex = 25;
            this.btnValidate.Text = "Validate";
            this.btnValidate.UseVisualStyleBackColor = true;
            this.btnValidate.Click += new System.EventHandler(this.btnValidate_Click);
            // 
            // lblTimer
            // 
            this.lblTimer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTimer.AutoSize = true;
            this.lblTimer.Location = new System.Drawing.Point(1036, 1);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(30, 13);
            this.lblTimer.TabIndex = 26;
            this.lblTimer.Text = "Time";
            // 
            // bgwTimer
            // 
            this.bgwTimer.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwTimer_DoWork);
            // 
            // OrderDespatcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1229, 670);
            this.Controls.Add(this.lblTimer);
            this.Controls.Add(this.btnValidate);
            this.Controls.Add(this.btnLoadPrevious);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.btnTestConnect);
            this.Controls.Add(this.btnSendToOE);
            this.Controls.Add(this.btnAddRemoveOrder);
            this.Controls.Add(this.btnPrintSettings);
            this.Controls.Add(this.btnPrintManifest);
            this.Controls.Add(this.btnGenerateManifest);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.spltOrderDetails);
            this.Controls.Add(this.gbxCustomerOrderDetails);
            this.Controls.Add(this.btnProcessOrder);
            this.Controls.Add(this.gbxParameters);
            this.Name = "OrderDespatcher";
            this.Text = "Warehouse Order Despatcher";
            this.gbxParameters.ResumeLayout(false);
            this.gbxParameters.PerformLayout();
            this.gbxPackingDetails.ResumeLayout(false);
            this.gbxPackingDetails.PerformLayout();
            this.gbxPackingEntry.ResumeLayout(false);
            this.gbxPackingEntry.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmrPackingCartonsPerPallet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmrPackingQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgrPackingDetails)).EndInit();
            this.gbxItemDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgrItems)).EndInit();
            this.gbxCustomerOrderDetails.ResumeLayout(false);
            this.gbxCustomerOrderDetails.PerformLayout();
            this.spltOrderDetails.Panel1.ResumeLayout(false);
            this.spltOrderDetails.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltOrderDetails)).EndInit();
            this.spltOrderDetails.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtOrderNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblCourier;
        private System.Windows.Forms.ComboBox cboCourier;
        private System.Windows.Forms.TextBox txtNoOfCartons;
        private System.Windows.Forms.Label lblTotalWeight;
        private System.Windows.Forms.TextBox txtTotalWeight;
        private System.Windows.Forms.Label lblNoOfLabels;
        private System.Windows.Forms.GroupBox gbxParameters;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnRetrieveInfo;
        private System.Windows.Forms.GroupBox gbxPackingDetails;
        private System.Windows.Forms.Button btnProcessOrder;
        private System.Windows.Forms.DataGridView dgrPackingDetails;
        private System.Windows.Forms.GroupBox gbxItemDetails;
        private System.Windows.Forms.DataGridView dgrItems;
        private System.Windows.Forms.CheckBox chkInvoiceWithGoods;
        private System.Windows.Forms.GroupBox gbxCustomerOrderDetails;
        private System.Windows.Forms.TextBox txtCountry;
        private System.Windows.Forms.TextBox txtDeliverToAddress3;
        private System.Windows.Forms.TextBox txtCustomerName;
        private System.Windows.Forms.TextBox txtDeliverToAddress2;
        private System.Windows.Forms.TextBox txtExactNo;
        private System.Windows.Forms.TextBox txtShippingDate;
        private System.Windows.Forms.TextBox txtOrderDate;
        private System.Windows.Forms.TextBox txtWarehouse;
        private System.Windows.Forms.TextBox txtOrderStatus;
        private System.Windows.Forms.TextBox txtDeliverToAddress1;
        private System.Windows.Forms.TextBox txtCustomerNo;
        private System.Windows.Forms.Label lblCustomerName;
        private System.Windows.Forms.Label lblExactNo;
        private System.Windows.Forms.Label lblShippingDate;
        private System.Windows.Forms.Label lblOrderDate;
        private System.Windows.Forms.Label lblWarehouse;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblCountry;
        private System.Windows.Forms.Label lblDeliveryAddress;
        private System.Windows.Forms.Label lblCusCode;
        private System.Windows.Forms.TextBox txtSpecialInstructions;
        private System.Windows.Forms.Label lblSpecialInstructions;
        private System.Windows.Forms.SplitContainer spltOrderDetails;
        private System.Windows.Forms.TextBox txtPackedBy;
        private System.Windows.Forms.Label lblPackedBy;
        private System.Windows.Forms.Label lblTotalDimensions;
        private System.Windows.Forms.TextBox txtTotalDimensions;
        private System.Windows.Forms.TextBox txtDeliveryName;
        private System.Windows.Forms.Label lblDeliveryName;
        private System.Windows.Forms.Button btnLoadPrevious;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtConnoteNo;
        private System.Windows.Forms.Label lblConnote;
        private System.Windows.Forms.TextBox txtEstimatedCost;
        private System.Windows.Forms.Label lblEstimatedCost;
        private System.Windows.Forms.TextBox txtSpecialInstructions2;
        private System.Windows.Forms.GroupBox gbxPackingEntry;
        private System.Windows.Forms.TextBox txtPackingLength;
        private System.Windows.Forms.ComboBox cboPackingType;
        private System.Windows.Forms.Label lblTypePacking;
        private System.Windows.Forms.CheckBox chkPackingDG;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPackingEstCost;
        private System.Windows.Forms.TextBox txtPackingConnoteNo;
        private System.Windows.Forms.TextBox txtPackingWeight;
        private System.Windows.Forms.TextBox txtPackingHeight;
        private System.Windows.Forms.TextBox txtPackingWidth;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtPackingComments;
        private System.Windows.Forms.Button btnDeletePacking;
        private System.Windows.Forms.Button btnEditPacking;
        private System.Windows.Forms.Button btnAddPacking;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtNoOfPallets;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown nmrPackingQty;
        private System.Windows.Forms.TextBox txtDespatchID;
        private System.Windows.Forms.Label lblDespatchID;
        private System.Windows.Forms.Button btnDG;
        private System.Windows.Forms.CheckBox chkPrintPackingSlip;
        private System.Windows.Forms.TextBox txtDeliveryPostcode;
        private System.Windows.Forms.TextBox txtDeliveryState;
        private System.Windows.Forms.TextBox txtDeliverySuburb;
        private System.Windows.Forms.Button btnGenerateManifest;
        private System.Windows.Forms.Button btnPrintManifest;
        private System.Windows.Forms.Button btnPrintSettings;
        private System.Windows.Forms.Button btnAddRemoveOrder;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTotalCartons;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nmrPackingCartonsPerPallet;
        private System.Windows.Forms.Button btnSendToOE;
        private System.Windows.Forms.Button btnRePrintPackingSlip;
        private System.Windows.Forms.Button btnDeleteConnote;
        private System.Windows.Forms.Button btnRePrintLabel;
        private System.Windows.Forms.Button btnPrintDG;
        private System.Windows.Forms.Button btnTestConnect;
        private System.Windows.Forms.TextBox txtCustomerComments;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Button btnValidate;
        private System.Windows.Forms.Label lblTimer;
        private System.ComponentModel.BackgroundWorker bgwTimer;
    }
}

