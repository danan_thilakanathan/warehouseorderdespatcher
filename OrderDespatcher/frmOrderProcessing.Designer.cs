﻿namespace OrderDespatcher
{
    partial class frmOrderProcessing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pgrProgress = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.clbProgress = new System.Windows.Forms.CheckedListBox();
            this.txtProgress = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.bgwDespatcher = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // pgrProgress
            // 
            this.pgrProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pgrProgress.Location = new System.Drawing.Point(13, 41);
            this.pgrProgress.Name = "pgrProgress";
            this.pgrProgress.Size = new System.Drawing.Size(501, 23);
            this.pgrProgress.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Processing Order";
            // 
            // clbProgress
            // 
            this.clbProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.clbProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbProgress.FormattingEnabled = true;
            this.clbProgress.Items.AddRange(new object[] {
            "Validated Order",
            "Generated and Printed Courier Labels",
            "Updated Order Status",
            "Printed Packing Slip"});
            this.clbProgress.Location = new System.Drawing.Point(13, 84);
            this.clbProgress.Name = "clbProgress";
            this.clbProgress.Size = new System.Drawing.Size(501, 124);
            this.clbProgress.TabIndex = 2;
            // 
            // txtProgress
            // 
            this.txtProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProgress.Location = new System.Drawing.Point(13, 215);
            this.txtProgress.Multiline = true;
            this.txtProgress.Name = "txtProgress";
            this.txtProgress.Size = new System.Drawing.Size(501, 181);
            this.txtProgress.TabIndex = 3;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(439, 402);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // bgwDespatcher
            // 
            this.bgwDespatcher.WorkerReportsProgress = true;
            this.bgwDespatcher.WorkerSupportsCancellation = true;
            this.bgwDespatcher.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwDespatcher_DoWork);
            this.bgwDespatcher.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwDespatcher_ProgressChanged);
            this.bgwDespatcher.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwDespatcher_RunWorkerCompleted);
            // 
            // frmOrderProcessing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 446);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtProgress);
            this.Controls.Add(this.clbProgress);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pgrProgress);
            this.Name = "frmOrderProcessing";
            this.Text = "Order Processing...";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmOrderProcessing_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar pgrProgress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox clbProgress;
        private System.Windows.Forms.TextBox txtProgress;
        private System.Windows.Forms.Button btnCancel;
        private System.ComponentModel.BackgroundWorker bgwDespatcher;
    }
}