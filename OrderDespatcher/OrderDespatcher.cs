﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Configuration;
using System.Globalization;
using System.Diagnostics;
using System.Threading;
using Livingstone.Library;
using OrderDespatcher.Courier;

namespace OrderDespatcher
{
    public partial class OrderDespatcher : Form
    {

        private string despatchID = "-1";  /*No despatch entry created yet*/
        private string loginName = "";

        frmOrderProcessing orderProcessed = new frmOrderProcessing();

        Stopwatch stopWatch = new Stopwatch();
        string elapsedTime = "";
        bool stopTimer = true;



        public OrderDespatcher()
        {
            InitializeComponent();

            initialiseDB("DANANT");

            initialiseForm();

        }

        public OrderDespatcher(string loginName)
        {
            this.loginName = loginName;

            InitializeComponent();

            initialiseDB(loginName);

            initialiseForm();
        }

        private void initialiseDB(string userName)
        {
            Database.DBaseUse = GetDBaseType(ClsGeneralFunction.PrepareProperText("Australia").Trim());
            DataSource.initDataBase((int)Database.DBaseUse, userName);
            Database.User = userName;
            DataLayer.Data.initDataBase((int)Database.DBaseUse, userName);
            DataLayer.Data.sUser = Database.User;

            //Database.DBaseUse = GetDBaseType(ClsGeneralFunction.PrepareProperText(cboCompany.SelectedItem).Trim());
            //DataSource.initDataBase((int)Database.DBaseUse, txtUserName.Text.Trim());
            //Database.User = txtUserName.Text.Trim();
            //DataLayer.Data.initDataBase((int)Database.DBaseUse, txtUserName.Text.Trim());
            //DataLayer.Data.sUser = Database.User;
        }

        private Database.DBaseType GetDBaseType(string CompanyParam)
        {
            Database.DBaseType Result = Database.DBaseType.dbNetCRMAU;
            if (ClsGeneralFunction.PrepareProperText(CompanyParam).Trim() != string.Empty)
            {
                string SQL = ("SELECT * FROM tblUserCompany WHERE RTRIM(LTRIM(LOWER(company))) = '" + ClsGeneralFunction.PrepareProperText(CompanyParam).ToLower().Trim() + "'");
                string Company = "NetCRMAU";
                if (CompanyParam == "Hongkong") Company = "LivMacola";
                if (CompanyParam == "Philippines") Company = "ExactPH";
                if (CompanyParam == "TestAU") Company = "TestAU";
                if (ClsGeneralFunction.PrepareProperText(Company).Trim() != string.Empty)
                {
                    MemberInfo[] Members = typeof(Database.DBaseType).GetMembers();
                    foreach (MemberInfo Member in Members)
                    {
                        if (Member.DeclaringType == typeof(Database.DBaseType) && Member.Name.IndexOf("__") < 0)
                        {
                            string EnumString = ClsGeneralFunction.PrepareProperText(Member.Name).Trim();
                            EnumString = ClsGeneralFunction.PrepareProperText(EnumString.Substring(EnumString.IndexOf("db") + 2)).Trim();
                            if (ClsGeneralFunction.PrepareProperText(EnumString).ToLower().Trim() == ClsGeneralFunction.PrepareProperText(Company).ToLower().Trim())
                            {
                                if (ClsGeneralFunction.PrepareProperText(EnumString).ToLower().Trim() == "NetCRMAU".ToLower().Trim())
                                    Result = Database.DBaseType.dbNetCRMAU;
                                else if (ClsGeneralFunction.PrepareProperText(EnumString).ToLower().Trim() == "LivMacola".ToLower().Trim())
                                    Result = Database.DBaseType.dbLivMacola;
                                else if (ClsGeneralFunction.PrepareProperText(EnumString).ToLower().Trim() == "ExactPH".ToLower().Trim())
                                    Result = Database.DBaseType.dbExactPH;
                                else if (ClsGeneralFunction.PrepareProperText(EnumString).ToLower().Trim() == "TestAU".ToLower().Trim())
                                    Result = Database.DBaseType.dbTestAU;
                                break;
                            }
                        }
                    }
                }
            }
            return Result;
        }


        private void initialiseForm()
        {
            //Get list of most commonly used couriers and fill combobox for users to select
            DataTable dt = OrderDespatchService.GetMostUsedCouriers();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows) 
                {
                    cboCourier.Items.Add(dr["courier"].ToString().Trim());
                }
            }

            cboPackingType.Items.Add("CARTON");
            cboPackingType.Items.Add("PALLET");
            cboPackingType.SelectedIndex = 0;

            nmrPackingQty.Text = "1";
            nmrPackingCartonsPerPallet.Text = "1";

            txtPackedBy.Text = loginName;

        }

        private void RetrieveHeaderInfo()
        {
            //Initialisers
            string customerNo = "";
            string exactNo = "";
            string customerName = "";
            string netCRMOrderNo = "";
            string exactOrderNo = "";
            string orderDate = "";
            string shipToName = "";
            string shipToAddr1 = "";
            string shipToAddr2 = "";
            string shipToAddr3 = "";
            string shipToSuburb = "";
            string shipToState = "";
            string shipToPostcode = "";
            string shipToCountry = "";
            string shippingDate = "";
            string orderStatus = "";
            string warehouse = "";
            string shipInstruction1 = "";
            string shipInstruction2 = "";
            string invoiceWithGoods = "";
            string customerComments = "";



            //Fill out customer order header info

            DataTable dt = OrderDespatchService.GetCustomerOrderDetails(txtOrderNo.Text.Trim());

            if (dt != null && dt.Rows.Count > 0)
            {
                customerNo = dt.Rows[0]["cus_no"].ToString().Trim();
                exactNo = dt.Rows[0]["exact_no"].ToString().Trim();
                customerName = dt.Rows[0]["cus_name"].ToString().Trim();
                netCRMOrderNo = dt.Rows[0]["ord_no"].ToString().Trim();
                exactOrderNo = dt.Rows[0]["ExactOrderNo"].ToString().Trim();
                orderDate = dt.Rows[0]["ord_dt"].ToString().Trim();
                shipToName = dt.Rows[0]["ship_to_name"].ToString().Trim();
                shipToAddr1 = dt.Rows[0]["del_addr_1"].ToString().Trim();
                shipToAddr2 = dt.Rows[0]["del_addr_2"].ToString().Trim();
                shipToAddr3 = dt.Rows[0]["del_addr_3"].ToString().Trim();
                shipToSuburb = dt.Rows[0]["del_City"].ToString().Trim();
                shipToState = dt.Rows[0]["del_state"].ToString().Trim();
                shipToPostcode = dt.Rows[0]["del_PostCode"].ToString().Trim();
                shipToCountry = dt.Rows[0]["del_country"].ToString().Trim();
                shippingDate = dt.Rows[0]["shipping_dt"].ToString().Trim();
                orderStatus = dt.Rows[0]["OrderStatus"].ToString().Trim();
                warehouse = dt.Rows[0]["warehouse"].ToString().Trim();
                shipInstruction1 = dt.Rows[0]["ship_instruction_1"].ToString().Trim();
                shipInstruction2 = dt.Rows[0]["ship_instruction_2"].ToString().Trim();
                invoiceWithGoods = dt.Rows[0]["InvoiceWithGoods"].ToString().Trim();
                customerComments = dt.Rows[0]["CustomerComments"].ToString().Trim();

            }

            txtCustomerNo.Text = customerNo;
            txtExactNo.Text = exactNo;
            txtCustomerName.Text = customerName;
            txtOrderDate.Text = orderDate;
            txtShippingDate.Text = shippingDate;
            txtOrderStatus.Text = orderStatus;
            txtWarehouse.Text = warehouse;
            txtSpecialInstructions.Text = shipInstruction1;
            txtSpecialInstructions2.Text = shipInstruction2;
            txtCustomerComments.Text = customerComments;

            txtDeliveryName.Text = shipToName;
            txtDeliverToAddress1.Text = shipToAddr1;
            txtDeliverToAddress2.Text = shipToAddr2;
            txtDeliverToAddress3.Text = shipToAddr3;
            txtDeliverySuburb.Text = shipToSuburb;
            txtDeliveryState.Text = shipToState;
            txtDeliveryPostcode.Text = shipToPostcode;
            txtCountry.Text = shipToCountry;

            chkInvoiceWithGoods.Checked = (invoiceWithGoods == "True" ? true : false);

            txtPackedBy.Text = loginName;


        }

        private void btnRetrieveInfo_Click(object sender, EventArgs e)
        {
            string tempOrderNo = txtOrderNo.Text;
            string tempCourier = cboCourier.Text.ToUpper().Trim();

            ResetForm();

            txtOrderNo.Text = tempOrderNo;
            cboCourier.Text = tempCourier;

            if (string.IsNullOrEmpty(txtOrderNo.Text) || string.IsNullOrEmpty(cboCourier.Text))
            {
                MessageBox.Show("Please enter order number and courier");
                return;
            }

            if (txtOrderNo.Text.ToUpper().StartsWith("DD"))
            {
                txtOrderNo.Text = txtOrderNo.Text.Substring(2);
            }

            string existingDespatchID = "-1";
            if (OrderDespatchService.VerifyOrderHasDespatchEntry(txtOrderNo.Text, out existingDespatchID))
            {
                despatchID = existingDespatchID;

                LoadDespatch();
                //if (MessageBox.Show("Despatch Entry already exists for this order. Continue from previous entry?", "Despatch Entry Exists for Order", MessageBoxButtons.YesNo) == DialogResult.Yes) 
                //{
                //    btnLoadPrevious_Click(sender, new EventArgs());
                //    return;
                //}

                cboCourier.Text = tempCourier;
            }

            RetrieveHeaderInfo();

            chkPrintPackingSlip.Checked = true;

            if (despatchID == "-1" || string.IsNullOrEmpty(despatchID))
            {
                //Fill out picked item info
                DataTable itemDetails = OrderDespatchService.GetOrderLines(txtOrderNo.Text.Trim(), OrderDespatchService.GetWarehouseOfPacker(txtPackedBy.Text));
                dgrItems.DataSource = itemDetails;
            }



            //Fill out packed item info
            DataTable packedItemDetails = OrderDespatchService.GetPackingDetails(despatchID);
            dgrPackingDetails.DataSource = packedItemDetails;

            HighlightDangerousGoods();

            if (despatchID == "-1" || string.IsNullOrEmpty(despatchID))
                CreateDespatch();
            else
                SaveDespatch();


            if (chkInvoiceWithGoods.Checked)
            {
                MessageBox.Show("Customer Requires Invoice With Goods. Send To OE!", "SEND TO OE: Invoice needed with Goods", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                SendToOE();
            }

            stopWatch.Reset();
            stopTimer = false;

            bgwTimer.RunWorkerAsync();

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ResetForm();

            stopWatch.Reset();
            stopTimer = true;
            //bgwTimer.CancelAsync();
        }

        private void ResetForm()
        {
            ClearForm(gbxCustomerOrderDetails);
            ClearForm(gbxPackingDetails);
            ClearForm(gbxItemDetails);

            despatchID = "-1"; /*Reset despatch ID*/
            txtDespatchID.Text = "";

            txtTotalDimensions.Text = "0.0000";
            txtTotalWeight.Text = "0.0000";
            txtNoOfCartons.Text = "0";
            txtNoOfPallets.Text = "0";
            txtEstimatedCost.Text = "0.00";

            txtOrderNo.Text = "";
            cboCourier.Text = "";

            nmrPackingQty.Text = "1";

        }


        private void ClearForm(Control control)
        {
            foreach (Control c in control.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Clear();
                }

                if (c is ComboBox)
                {
                    ((ComboBox)c).Text = "";
                }

                if (c.HasChildren)
                {
                    ClearForm(c);
                }


                if (c is CheckBox)
                {

                    ((CheckBox)c).Checked = false;
                }

                if (c is RadioButton)
                {
                    ((RadioButton)c).Checked = false;
                }

                if (c is DataGridView)
                {
                    ((DataGridView)c).DataSource = null;
                }
            }
        }

        private void HighlightDangerousGoods()
        {
            foreach (DataGridViewRow dr in dgrItems.Rows)
            {
                if (!dr.IsNewRow)
                    if (OrderDespatchService.verifyDangerousGood(dr.Cells["Item No"].Value.ToString().Trim()))
                    {
                        dr.DefaultCellStyle.BackColor = Color.PaleVioletRed;
                    }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveDespatch();
        }

        private void CreateDespatch()
        {
            int noOfCartons = 0;
            int noOfPallets = 0;

            int totalCartons = 0;

            txtNoOfCartons.Text = noOfCartons.ToString();
            txtNoOfPallets.Text = noOfPallets.ToString();
            txtTotalCartons.Text = totalCartons.ToString();

            DataTable packingDetails = (DataTable)dgrPackingDetails.DataSource;
            DataTable itemDetails = (DataTable)dgrItems.DataSource;

            despatchID = OrderDespatchService.CreateDespatchEntry(txtCustomerNo.Text, txtExactNo.Text, txtCustomerName.Text, txtOrderNo.Text.Trim(), cboCourier.Text.Trim(), 
                txtDeliveryName.Text, txtDeliverToAddress1.Text, txtDeliverToAddress2.Text, txtDeliverToAddress3.Text, txtDeliverySuburb.Text, txtDeliveryState.Text, txtDeliveryPostcode.Text, txtCountry.Text,
                chkInvoiceWithGoods.Checked, chkPrintPackingSlip.Checked,  txtConnoteNo.Text, txtSpecialInstructions.Text, txtSpecialInstructions2.Text, txtCustomerComments.Text, txtEstimatedCost.Text, double.Parse(txtTotalWeight.Text), double.Parse(txtTotalDimensions.Text),
                noOfCartons, noOfPallets, packingDetails, itemDetails, txtOrderDate.Text, txtShippingDate.Text, txtWarehouse.Text, totalCartons, txtPackedBy.Text.Trim(), loginName);

            txtDespatchID.Text = despatchID;
        
        }

        private void SaveDespatch()
        {
            if (string.IsNullOrEmpty(txtOrderNo.Text) || string.IsNullOrEmpty(cboCourier.Text) || string.IsNullOrEmpty(despatchID.Trim()))
            {
                MessageBox.Show("Cannot save despatch. Need order no, courier and valid despatch ID");
                return;
            }

            if (dgrItems == null)
            {
                MessageBox.Show("Need to have item list loaded.");
                return;
            }

            int noOfCartons = 0;
            int noOfPallets = 0;

            int totalCartons = 0;

            noOfCartons = Int32.Parse(txtNoOfCartons.Text);
            noOfPallets = Int32.Parse(txtNoOfPallets.Text);
            totalCartons = Int32.Parse(txtTotalCartons.Text);

            DataTable packingDetails = (DataTable)dgrPackingDetails.DataSource;
            DataTable itemDetails = (DataTable)dgrItems.DataSource;

            /*If despatch entry does not exist, create new entry in db, else save the current entry*/
            if (despatchID == "-1") 
            {
                OrderDespatchService.CreateDespatchEntry(txtCustomerNo.Text, txtExactNo.Text, txtCustomerName.Text, txtOrderNo.Text.Trim(), cboCourier.Text.Trim(),
                    txtDeliveryName.Text, txtDeliverToAddress1.Text, txtDeliverToAddress2.Text, txtDeliverToAddress3.Text, txtDeliverySuburb.Text, txtDeliveryState.Text, txtDeliveryPostcode.Text, txtCountry.Text,
                    chkInvoiceWithGoods.Checked, chkPrintPackingSlip.Checked, txtConnoteNo.Text, txtSpecialInstructions.Text, txtSpecialInstructions2.Text, txtCustomerComments.Text, txtEstimatedCost.Text, double.Parse(txtTotalWeight.Text), double.Parse(txtTotalDimensions.Text),
                    noOfCartons, noOfPallets, packingDetails, itemDetails, txtOrderDate.Text, txtShippingDate.Text, txtWarehouse.Text, totalCartons, txtPackedBy.Text.Trim(), loginName);
            }
            else
            {
                OrderDespatchService.SaveDespatchOrder(txtCustomerNo.Text, txtExactNo.Text, txtCustomerName.Text, despatchID, cboCourier.Text.Trim(),
                    txtDeliveryName.Text, txtDeliverToAddress1.Text, txtDeliverToAddress2.Text, txtDeliverToAddress3.Text, txtDeliverySuburb.Text, txtDeliveryState.Text, txtDeliveryPostcode.Text, txtCountry.Text,
                    chkInvoiceWithGoods.Checked, chkPrintPackingSlip.Checked, txtConnoteNo.Text, txtSpecialInstructions.Text, txtSpecialInstructions2.Text, txtCustomerComments.Text, txtEstimatedCost.Text, double.Parse(txtTotalWeight.Text), double.Parse(txtTotalDimensions.Text),
                    noOfCartons, noOfPallets, packingDetails, itemDetails, txtOrderDate.Text, txtShippingDate.Text, totalCartons, txtPackedBy.Text.Trim(), loginName);  
            }

        }

        private void LoadDespatch()
        {

            //Initialisers
            string customerNo = "";
            string exactNo = "";
            string customerName = "";
            string exactOrderNo = "";
            string courier = "";
            string orderDate = "";
            string shipToName = "";
            string shipToAddr1 = "";
            string shipToAddr2 = "";
            string shipToAddr3 = "";
            string shipToSuburb = "";
            string shipToState = "";
            string shipToPostcode = "";
            string shipToCountry = "";
            string shippingDate = "";
            string warehouse = "";
            string shipInstruction1 = "";
            string shipInstruction2 = "";
            string packedBy = "";

            string totalWeight = "";
            string totalDimensions = "";
            string totalCartons = "";
            string totalPallets = "";
            string totalCompleteCartons = "";
            string estimatedCost = "";
            string connoteNo = "";

            string invoiceWithGoods = "";
            string printPackingSlip = "";

            DataTable existingOrder = OrderDespatchService.LoadExistingCustomerOrderDetails(despatchID.Trim());
            
            if (existingOrder != null && existingOrder.Rows.Count > 0) 
            {

                //Get existing order
                RetrieveHeaderInfo();

                despatchID = existingOrder.Rows[0]["id"].ToString().Trim();

                customerNo = existingOrder.Rows[0]["cusNo"].ToString().Trim();
                exactNo = existingOrder.Rows[0]["exactNo"].ToString().Trim();
                customerName = existingOrder.Rows[0]["cusName"].ToString().Trim();
                //netCRMOrderNo = existingOrder.Rows[0]["ord_no"].ToString().Trim();
                exactOrderNo = existingOrder.Rows[0]["orderNo"].ToString().Trim();
                courier = existingOrder.Rows[0]["courier"].ToString().Trim();
                orderDate = existingOrder.Rows[0]["formattedOrderDate"].ToString().Trim();
                shipToName = existingOrder.Rows[0]["delName"].ToString().Trim();
                shipToAddr1 = existingOrder.Rows[0]["delAddr1"].ToString().Trim();
                shipToAddr2 = existingOrder.Rows[0]["delAddr2"].ToString().Trim();
                shipToAddr3 = existingOrder.Rows[0]["delAddr3"].ToString().Trim();
                shipToSuburb = existingOrder.Rows[0]["delSuburb"].ToString().Trim();
                shipToState = existingOrder.Rows[0]["delState"].ToString().Trim();
                shipToPostcode = existingOrder.Rows[0]["delPostcode"].ToString().Trim();
                shipToCountry = existingOrder.Rows[0]["country"].ToString().Trim();
                shippingDate = existingOrder.Rows[0]["shippingDate"].ToString().Trim();
                //orderStatus = existingOrder.Rows[0]["OrderStatus"].ToString().Trim();
                warehouse = existingOrder.Rows[0]["warehouse"].ToString().Trim();
                shipInstruction1 = existingOrder.Rows[0]["specialInstructions"].ToString().Trim();
                shipInstruction2 = existingOrder.Rows[0]["specialInstructions2"].ToString().Trim();
                packedBy = existingOrder.Rows[0]["packedBy"].ToString().Trim();

                totalWeight = existingOrder.Rows[0]["totalWeight"].ToString().Trim();
                totalDimensions = existingOrder.Rows[0]["totalDimensions"].ToString().Trim();
                totalCartons = existingOrder.Rows[0]["noOfCartons"].ToString().Trim();
                totalPallets = existingOrder.Rows[0]["noOfPallets"].ToString().Trim();
                totalCompleteCartons = existingOrder.Rows[0]["totalCartons"].ToString().Trim();
                estimatedCost = existingOrder.Rows[0]["estimatedFreightCost"].ToString().Trim();
                connoteNo = existingOrder.Rows[0]["connoteNo"].ToString().Trim();

                invoiceWithGoods = existingOrder.Rows[0]["invoiceWithGoods"].ToString().Trim();
                printPackingSlip = existingOrder.Rows[0]["printPackingSlip"].ToString().Trim();

                txtDespatchID.Text = despatchID;

                txtCustomerNo.Text = customerNo;
                txtExactNo.Text = exactNo;
                txtCustomerName.Text = customerName;
                cboCourier.Text = courier;
                txtOrderDate.Text = orderDate;
                txtShippingDate.Text = shippingDate;
                txtWarehouse.Text = warehouse;
                txtSpecialInstructions.Text = shipInstruction1;
                txtSpecialInstructions2.Text = shipInstruction2;
                txtPackedBy.Text = packedBy;

                txtDeliveryName.Text = shipToName;
                txtDeliverToAddress1.Text = shipToAddr1;
                txtDeliverToAddress2.Text = shipToAddr2;
                txtDeliverToAddress3.Text = shipToAddr3;
                txtDeliverySuburb.Text = shipToSuburb;
                txtDeliveryState.Text = shipToState;
                txtDeliveryPostcode.Text = shipToPostcode;
                txtCountry.Text = shipToCountry;

                txtTotalWeight.Text = totalWeight;
                txtTotalDimensions.Text = totalDimensions;
                txtNoOfCartons.Text = totalCartons;
                txtNoOfPallets.Text = totalPallets;
                txtTotalCartons.Text = totalCompleteCartons;
                txtEstimatedCost.Text = estimatedCost;
                txtConnoteNo.Text = connoteNo;

                if (invoiceWithGoods == "True") chkInvoiceWithGoods.Checked = true;
                if (printPackingSlip == "True") chkPrintPackingSlip.Checked = true;

                if (!string.IsNullOrEmpty(txtConnoteNo.Text))
                {
                    btnDeleteConnote.Visible = true;
                }


                //Fill out packed item info
                DataTable packedItemDetails = OrderDespatchService.GetPackingDetails(despatchID);
                dgrPackingDetails.DataSource = packedItemDetails;

                //Fill out picked item list
                DataTable packedItems = OrderDespatchService.LoadExistingItemDetails(despatchID);
                dgrItems.DataSource = packedItems;

                HighlightDangerousGoods();

            }

            stopWatch.Reset();
            stopTimer = false;
            bgwTimer.RunWorkerAsync();


           
        }

        private void btnProcessOrder_Click(object sender, EventArgs e)
        {
            ProcessOrder();

            stopWatch.Reset();
            stopTimer = true;

        }

        private bool ValidateOrderForProcessing()
        {
            //Check user entered order no and courier code
            if (string.IsNullOrEmpty(txtOrderNo.Text) || string.IsNullOrEmpty(cboCourier.Text))
            {
                MessageBox.Show("Please enter the order number and courier code");
                return false;
            }

            //Check packing details contain entries
            if (dgrPackingDetails == null || dgrPackingDetails.Rows.Count <= 0)
            {
                MessageBox.Show("Please enter at least 1 packing type.");
                return false;
            }

            //Check packing details contain entries
            if (dgrItems == null || dgrItems.Rows.Count <= 0)
            {
                MessageBox.Show("There are no items. Cannot process.");
                return false;
            }

            //Check estimated cost is a double number entry
            if (!string.IsNullOrEmpty(txtEstimatedCost.Text))
            {
                double estCost = 0;
                if (!double.TryParse(txtEstimatedCost.Text, out estCost))
                {
                    return false;
                }
            }

            //Check connote doesn't already exist
            if (!string.IsNullOrEmpty(txtConnoteNo.Text) /*&& cboCourier.Text != "STE"*/)
            {
                MessageBox.Show("Connote No already exists. Cannot process. Please delete connote if you need to process (or send to OE if you cannot delete).");
                return false;
            }

            ////Check there is at least 1 included item from the item list
            //bool isIncluded = false;
            //foreach (DataGridViewRow dr in dgrItems.Rows)
            //{
            //    if (!dr.IsNewRow)
            //        if (dr.Cells["Included"].Value.ToString() == "True")
            //        {
            //            isIncluded = true;
            //            break;
            //        }
            //}
            //if (!isIncluded)
            //{
            //    MessageBox.Show("Need to include at least 1 item in despatch. Please ensure you tick the relevant checkboxes for the items.");
            //    return false;
            //}


            //Check that packing details has at least 1 isDG ticked if one of the items is marked as DG
            bool dgItemIncluded = false;
            bool pickedMoreThanOrdered = false;

            string pickedMoreThanOrderedErrorMessage = "";
            foreach (DataGridViewRow dr in dgrItems.Rows) 
            {
                if (!dr.IsNewRow)
                    if (dr.DefaultCellStyle.BackColor == Color.PaleVioletRed && Int32.Parse(dr.Cells["Qty Picked"].Value.ToString()) > 0)
                    {
                        dgItemIncluded = true;
                        break;
                    }

                //Check qty picked is always less than or equal to qty packed
                if (Int32.Parse(dr.Cells["Qty Picked"].Value.ToString()) > Int32.Parse(dr.Cells["Qty Ordered"].Value.ToString())) 
                {
                    pickedMoreThanOrderedErrorMessage += "Item: " + dr.Cells["Item No"].Value.ToString() + "           -QtyPacked:" + dr.Cells["Qty Picked"].Value.ToString() + " -QtyOrdered:" + dr.Cells["Qty Ordered"].Value.ToString() + "\n";
                    pickedMoreThanOrdered = true;
                }
            }

            if (pickedMoreThanOrdered)
            {
                MessageBox.Show("Error! Qty Packed cannot be higher than Qty Ordered for the following items:\n" + pickedMoreThanOrderedErrorMessage + "\nPlease fix.");
                return false;
            }

            if (dgItemIncluded)
                if (OrderDespatchService.GetNoOfDangerousGoods(despatchID.Trim(), "-1") <= 0)
                {
                    DialogResult dgBox = MessageBox.Show("WARNING: There is an item marked as dangerous in this order but the carton/pallet has not been marked as dangerous. Abort processing?", "WARNING: DG Not Marked", MessageBoxButtons.YesNo);

                    if (dgBox == DialogResult.Yes)
                    {
                        return false;
                    }                  
                }

            //Weight Check
            double connoteWeight = double.Parse(txtTotalWeight.Text);
            double systemWeight = OrderDespatchService.GetSystemTotalWeight(despatchID.Trim());
            double weightVariant = (connoteWeight / (systemWeight * 1.2)) * 100;

            ////Volume Check
            //double connoteVolume = double.Parse(txtTotalDimensions.Text);
            //double systemVolume = OrderDespatchService.GetSystemTotalCBM(despatchID.Trim());
            //double volumeVariant = (connoteVolume / (systemVolume * 1.2)) * 100;

            //No Of Cartons Check
            int connoteNoOfCartons = Int32.Parse(txtNoOfCartons.Text);
            int estimatedCartons = OrderDespatchService.GetEstimatedCartons(despatchID.Trim());


            string title = "Oversupply Alert: ";
            string message = "";

            if (connoteNoOfCartons > estimatedCartons)
            {
                title += "Please check TOTAL CARTONS!";
                message = @"Total Cartons is higher than estimated no of cartons. Please double check to ensure no over supply of qty or items.

If you are sure the total no of cartons is correct, please click ""Process Anyway"". Note: You will be tracked if oversupply issues repeatedly occur.";
  
            }
//            else if (volumeVariant > 100)
//            {
//                title += "Please check VOLUME!";
//                message = @"Total Volume is significantly different compared to the system calculated volume. Please double check to ensure no over supply of qty or items.
//
//If you are sure the total volume is correct, please click ""Process Anyway"". Note: You will be tracked if oversupply issues repeatedly occur.";
//            }
            else if ( weightVariant > 100 )
            {
                title += "Please check WEIGHT!";
                message =@"Total Weight is significantly different compared to the system calculated weight. Please double check to ensure no over supply of qty or items.

If you are sure the total weight is correct, please click ""Process Anyway"". Note: You will be tracked if oversupply issues repeatedly occur.";           
            }


            if (!string.IsNullOrEmpty(message))
            {
                frmOversupplyCheckWarehouseStaff weightCheck = new frmOversupplyCheckWarehouseStaff(despatchID, txtPackedBy.Text, title, message);
                weightCheck.ShowDialog();

                if (weightCheck.DialogResult != DialogResult.OK)
                {
                    return false;
                }
            }


            return true;

        }


        private void ProcessOrder()
        {

            //Validate Entries before Despatch
            if (!ValidateOrderForProcessing()) return;


            //Apply CBM and weight reduction
            if (despatchID != "-1")
            {
                OrderDespatchService.ScaleDimensions(despatchID, 0.9);
                OrderDespatchService.ScaleWeight(despatchID, 0.8);
                UpdatePackingTotals();
            }

            //1. Create (or Save) Despatch Entry
            if (despatchID == "-1") CreateDespatch();
            else SaveDespatch();

            //orderProcessed.Show();

         


            frmOrderProcessing op = new frmOrderProcessing(despatchID.Trim(), txtOrderNo.Text.Trim(), txtCustomerNo.Text, txtExactNo.Text, cboCourier.Text.Trim(), txtPackedBy.Text, chkPrintPackingSlip.Checked, chkInvoiceWithGoods.Checked, 
                                            Int32.Parse(txtNoOfCartons.Text), Int32.Parse(txtNoOfPallets.Text), Int32.Parse(txtTotalCartons.Text), double.Parse(txtTotalWeight.Text), double.Parse(txtTotalDimensions.Text));

            op.ShowDialog();

            if (op.returnConnoteNo == "[NEED TO FIX]")
                return;

            //11. Display instructions to user
            string area = op.area;
            string userMessage = op.userMessage;


            frmOrderProcessed orderProcessed = new frmOrderProcessed(despatchID.Trim(), area, userMessage);
            orderProcessed.ShowDialog();

            ResetForm();
           // bgwDespatcher.RunWorkerAsync();

        }


        private void btnLoadPrevious_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(txtOrderNo.Text))
            {
                MessageBox.Show("Must enter/scan in order no.");
                return;
            }

            if (txtOrderNo.Text.ToUpper().StartsWith("DD"))
            {
                txtOrderNo.Text = txtOrderNo.Text.Substring(2);
            }

            frmDespatchList despatchList = new frmDespatchList(txtOrderNo.Text);
            despatchList.ShowDialog();

            if (despatchList.DialogResult == DialogResult.OK && despatchList.despatchID != "-1")
            {
                string tempOrderNo = txtOrderNo.Text;
                string tempCourier = cboCourier.Text;

                ResetForm();

                txtOrderNo.Text = tempOrderNo;
                cboCourier.Text = tempCourier;

                despatchID = despatchList.despatchID;

                LoadDespatch();
            }
            else
            {
                MessageBox.Show("Could not load previous despatch entry.");
            }

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            PrintService ps = new PrintService();
            //ps.printPDFWithAcrobat(@"D:\User Files\Danan\QUOTE781830.pdf");

           // ps.TestPrint(@"\\livdoc9\mydocs\IT\Danan\testPDF.pdf", "Kyocera-Up");
           // ps.TestPrint(@"\\livdoc9\mydocs\IT\Danan\testPDF.pdf", "Kyocera-Ground");
           // ps.TestPrint1(@"\\livdoc9\mydocs\IT\Danan\QUOTES_enc726746_special.pdf");

            //List of printers
            //foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            //{
            //    MessageBox.Show(printer);
            //}

            ps.TestPrint2(@"\\livdoc9\mydocs\IT\Danan\testPDF.pdf", "Kyocera-Up");
            ps.TestPrint2(@"\\livdoc9\mydocs\IT\Danan\testPDF.pdf", "Kyocera-Ground");
        }

        private void UpdatePackingTotals()
        {
            double totalWeight = 0;
            double totalVolume = 0;
            int noOfCartons = 0;
            int noOfPallets = 0;

            int totalCartons = 0;

            OrderDespatchService.GetTotalPackingDetails(despatchID.Trim(), out totalWeight, out totalVolume, out noOfCartons, out noOfPallets, out totalCartons);

            txtTotalWeight.Text = totalWeight.ToString();
            txtTotalDimensions.Text = totalVolume.ToString();
            txtNoOfCartons.Text = noOfCartons.ToString();
            txtNoOfPallets.Text = noOfPallets.ToString();
            txtTotalCartons.Text = totalCartons.ToString();
        }

        private bool ValidatePackingEntry()
        {
            //Must have an order number and courier entered
            if (string.IsNullOrEmpty(txtOrderNo.Text) || string.IsNullOrEmpty(cboCourier.Text))
            {
                MessageBox.Show("Please input DD order no and courier code.");
                return false;
            }

            //Must have a valid despatch entry
            if (despatchID == "-1" || string.IsNullOrEmpty(despatchID))
            {
                MessageBox.Show("Must load a despatch entry.");
                return false;
            }

            int qty = 0;
            double length = 0;
            double width = 0;
            double height = 0;
            double weight = 0;
            double estCost = 0.00;

            int cartonsPerPallet = 1;

            //Qty Validation
            if (!Int32.TryParse(nmrPackingQty.Value.ToString(), out qty))
            {
                MessageBox.Show("Invalid qty value. Please ensure it is a proper number.");
                return false;
            }

            if (qty <= 0)
            {
                MessageBox.Show("Invalid Qty. Please type a number greater than 0.");
                return false;
            }


            //Length Validation
            if (!double.TryParse(txtPackingLength.Text, out length))
            {
                MessageBox.Show("Invalid length. Please ensure it is a proper number.");
                return false;
            }

            if (length <= 0)
            {
                MessageBox.Show("Invalid length. Please type a number greater than 0.");
                return false;
            }

            //Width Validation
            if (!double.TryParse(txtPackingWidth.Text, out width))
            {
                MessageBox.Show("Invalid width. Please ensure it is a proper number.");
                return false;
            }

            if (width <= 0)
            {
                MessageBox.Show("Invalid width. Please type a number greater than 0.");
                return false;
            }

            //Height Validation
            if (!double.TryParse(txtPackingHeight.Text, out height))
            {
                MessageBox.Show("Invalid height. Please ensure it is a proper number.");
                return false;
            }

            if (height <= 0)
            {
                MessageBox.Show("Invalid height. Please type a number greater than 0.");
                return false;
            }

            //Weight Validation
            if (!double.TryParse(txtPackingWeight.Text, out weight))
            {
                MessageBox.Show("Invalid weight. Please ensure it is a proper number.");
                return false;
            }

            if (weight <= 0)
            {
                MessageBox.Show("Invalid weight. Please type a number greater than 0.");
                return false;
            }
            if (weight > 25 && cboPackingType.Text == "CARTON")
            {
                MessageBox.Show("Weight cannot be greater than 25kg for CARTON");
                return false;
            }
            if (weight > 2500 && cboPackingType.Text == "PALLET")
            {
                MessageBox.Show("Weight cannot be greater than 2500kg for PALLET");
                return false;
            }

            if (!Int32.TryParse(nmrPackingCartonsPerPallet.Value.ToString(), out cartonsPerPallet))
            {
                MessageBox.Show("Invalid cartons per pallet value. Please ensure it is a proper number.");
                return false;
            }

            if (cboPackingType.Text == "PALLET" && cartonsPerPallet <= 1)
            {
                MessageBox.Show("You have entered type PALLET but you haven't changed the number of cartons per pallet. This is needed for invoicing purposes. Please make sure to include this.");
                return false;
            }


            //Est Cost Validation
            string estCostString = txtPackingEstCost.Text;
            if (string.IsNullOrEmpty(estCostString)) estCostString = "0";

            if (!double.TryParse(estCostString, out estCost))
            {
                MessageBox.Show("Invalid estimated cost. Please ensure it is a valid amount. No text or $ sign should be used.");
                return false;
            }

            //if (estCost <= (decimal)0)
            //{
            //    MessageBox.Show("Invalid estimated cost. Estimated costs should be greater than or equal to 0.");
            //    return false;
            //}

            if (length > 220 || width > 220 || height > 220)
            {
                MessageBox.Show("Length, width and/or height too large. Please fix it first.");
                return false;
            }

            if (cboPackingType.Text == "CARTON" && (length > 99 || width > 99 || height > 99))
            {
                if (MessageBox.Show("Please double check that you entered the unit in CM (centimeters). Are you sure you want to go ahead with this?", "WARNING: Entered Dimesion Too High", MessageBoxButtons.YesNo) == DialogResult.No) 
                {
                    return false;
                }
            }

            if (cboPackingType.Text == "PALLET" && height > 99)
            {
                if (MessageBox.Show(" Please double check that you entered the height in CM (centimeters). Are you sure you want to go ahead with this?", "WARNING: Entered Dimesion Too High", MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    return false;
                }
            }

            return true;


        }

        private void PostValidatePackingEntry(string packingID)
        {

        }

        private void ResetPackingDetails()
        {
            nmrPackingQty.Text = "1";
            txtPackingLength.Text = "";
            txtPackingWidth.Text = "";
            txtPackingHeight.Text = "";
            txtPackingWeight.Text = "";
            txtPackingConnoteNo.Text = "";
            nmrPackingCartonsPerPallet.Text = "1";
            txtPackingEstCost.Text = "";
            chkPackingDG.Checked = false;
            txtPackingComments.Text = "";
        }

        private void btnAddPacking_Click(object sender, EventArgs e)
        {
            //Validate the carton/pallet dimensions and other details entered by warehouse staff
            if (!ValidatePackingEntry()) return;

            string estCostString = txtPackingEstCost.Text;
            if (estCostString == "") estCostString = "0";

            //Add the entry
            string packingID = OrderDespatchService.AddPackingEntry(txtOrderNo.Text.Trim(), despatchID.Trim(), cboPackingType.Text, Int32.Parse(nmrPackingQty.Value.ToString()),
                double.Parse(txtPackingLength.Text), double.Parse(txtPackingWidth.Text), double.Parse(txtPackingHeight.Text), double.Parse(txtPackingWeight.Text),
                txtPackingConnoteNo.Text.Trim(), double.Parse(estCostString), chkPackingDG.Checked, Int32.Parse(nmrPackingCartonsPerPallet.Value.ToString()), txtPackingComments.Text, loginName);

            //Update the packing details totals such as no of cartons, pallets, volume and weight
            UpdatePackingTotals();

            //isDG validation
            if (chkPackingDG.Checked)
            {
                if (checkContainsDG())
                {
                    frmDangerousGoods dangerousGoods = new frmDangerousGoods(despatchID, packingID, loginName);
                    dangerousGoods.ShowDialog();
                }
                else
                {
                    MessageBox.Show("There are no dangerous good items in this despatch entry.");
                }

            }

            //Refresh the datagrid
            DataTable packingDetails = OrderDespatchService.GetPackingDetails(despatchID);
            dgrPackingDetails.DataSource = packingDetails;

            ResetPackingDetails();
            nmrPackingQty.Focus();


        }

        private void btnEditPacking_Click(object sender, EventArgs e)
        {
            //Add same validation code as the add button
            //if (!ValidatePackingEntry()) return;

            if (dgrPackingDetails.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select an entry to edit.");
                return;
            }

            //Edit the entry with the selected ID
            string packingID = dgrPackingDetails.SelectedRows[0].Cells["ID"].Value.ToString();

            string edit_packingType = dgrPackingDetails.SelectedRows[0].Cells["Packing Type"].Value.ToString();
            int edit_qty = Int32.Parse(dgrPackingDetails.SelectedRows[0].Cells["Qty"].Value.ToString());
            double edit_length = double.Parse(dgrPackingDetails.SelectedRows[0].Cells["Length (cm)"].Value.ToString());
            double edit_width = double.Parse(dgrPackingDetails.SelectedRows[0].Cells["Width (cm)"].Value.ToString());
            double edit_height = double.Parse(dgrPackingDetails.SelectedRows[0].Cells["Height (cm)"].Value.ToString());
            double edit_weight = double.Parse(dgrPackingDetails.SelectedRows[0].Cells["Weight (kg)"].Value.ToString());
            string edit_connoteNo = dgrPackingDetails.SelectedRows[0].Cells["connoteNo"].Value.ToString();
            double edit_estCost = double.Parse(dgrPackingDetails.SelectedRows[0].Cells["estimatedFreightCost"].Value.ToString());
            bool edit_isDG = (dgrPackingDetails.SelectedRows[0].Cells["containsDangerousGoods?"].Value.ToString() == "True" ? true : false);
            int edit_cartonsPerPallet = Int32.Parse(dgrPackingDetails.SelectedRows[0].Cells["totalCartons"].Value.ToString());
            string edit_comments = dgrPackingDetails.SelectedRows[0].Cells["Comments"].Value.ToString();

            frmEditEntry editEntry = new frmEditEntry(txtOrderNo.Text, cboCourier.Text, despatchID, packingID, edit_packingType, edit_qty,
                edit_length, edit_width, edit_height, edit_weight, edit_connoteNo.Trim(), edit_estCost, edit_isDG, edit_cartonsPerPallet, edit_comments, loginName);
            editEntry.ShowDialog();

            if (editEntry.DialogResult == DialogResult.OK)
            {
                //Update the packing details totals such as no of cartons, pallets, volume and weight
                UpdatePackingTotals();


                //isDG validation
                if (editEntry.isDG)
                {
                    if (checkContainsDG())
                    {
                        frmDangerousGoods dangerousGoods = new frmDangerousGoods(despatchID, packingID, loginName);
                        dangerousGoods.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("There are no dangerous good items in this despatch entry.");
                    }
                }

                //Refresh the datagrid
                DataTable packingDetails = OrderDespatchService.GetPackingDetails(despatchID);
                dgrPackingDetails.DataSource = packingDetails;

                foreach (DataGridViewRow dr in dgrPackingDetails.Rows)
                {
                    if (dr.Cells["ID"].Value.ToString() == packingID)
                    {
                        dr.Selected = true;
                    }
                }
            }

           
            
        }

        private void btnDeletePacking_Click(object sender, EventArgs e)
        {
            //Check if the warehouse staff has clicked on a line in the grid to delete
            if (dgrPackingDetails.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select an entry to edit.");
                return;
            }

            if (MessageBox.Show("Are you sure you want to delete?", "Delete Packing Entry", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                //Delete the selected entry
                string id = dgrPackingDetails.SelectedRows[0].Cells["ID"].Value.ToString();
                OrderDespatchService.DeletePackingEntry(despatchID.Trim(), id);

                //Update the packing details totals such as no of cartons, pallets, volume and weight
                UpdatePackingTotals();

                //Refresh the datagrid
                DataTable packingDetails = OrderDespatchService.GetPackingDetails(despatchID);
                dgrPackingDetails.DataSource = packingDetails;
            }


        }

        private void dgrPackingDetails_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            PackingRowChanged();
        }

        private void PackingRowChanged()
        {
            if (dgrPackingDetails.SelectedRows[0].IsNewRow) return;

            if (dgrPackingDetails.SelectedRows[0].Cells["Packing Type"].Value.ToString() == "CARTON")
            {
                cboPackingType.SelectedIndex = 0;
            }
            else if (dgrPackingDetails.SelectedRows[0].Cells["Packing Type"].Value.ToString() == "PALLET")
            {
                cboPackingType.SelectedIndex = 1;
                nmrPackingCartonsPerPallet.Text = dgrPackingDetails.SelectedRows[0].Cells["totalCartons"].Value.ToString();
            }

            if (nmrPackingQty.Text == "")
            {
                // If the value in the numeric updown is an empty string, replace with 0.
                nmrPackingQty.Text = "0";
            }

            nmrPackingQty.Text = dgrPackingDetails.SelectedRows[0].Cells["Qty"].Value.ToString();
            txtPackingLength.Text = dgrPackingDetails.SelectedRows[0].Cells["Length (cm)"].Value.ToString();
            txtPackingWidth.Text = dgrPackingDetails.SelectedRows[0].Cells["Width (cm)"].Value.ToString();
            txtPackingHeight.Text = dgrPackingDetails.SelectedRows[0].Cells["Height (cm)"].Value.ToString();
            txtPackingWeight.Text = dgrPackingDetails.SelectedRows[0].Cells["Weight (kg)"].Value.ToString();
            txtPackingConnoteNo.Text = dgrPackingDetails.SelectedRows[0].Cells["connoteNo"].Value.ToString();
            txtPackingEstCost.Text = dgrPackingDetails.SelectedRows[0].Cells["estimatedFreightCost"].Value.ToString();
            chkPackingDG.Checked = (dgrPackingDetails.SelectedRows[0].Cells["containsDangerousGoods?"].Value.ToString() == "True" ? true : false);
            txtPackingComments.Text = dgrPackingDetails.SelectedRows[0].Cells["comments"].Value.ToString();

        }

        private void btnDG_Click(object sender, EventArgs e)
        {
            if (dgrPackingDetails.SelectedRows.Count <= 0 ||
                string.IsNullOrEmpty(despatchID))
            {
                MessageBox.Show("Please select the packing detail line in the grid.");
                return;
            }


            string packingID = dgrPackingDetails.SelectedRows[0].Cells["ID"].Value.ToString();

            //Open dangerous goods form
            if (checkContainsDG())
            {
                frmDangerousGoods dangerousGoods = new frmDangerousGoods(despatchID, packingID, loginName);
                dangerousGoods.ShowDialog();
            }
            else
            {
                MessageBox.Show("There are no dangerous good items in this despatch entry.");
            }

            //Refresh the datagrid
            DataTable packingDetails = OrderDespatchService.GetPackingDetails(despatchID);
            dgrPackingDetails.DataSource = packingDetails;

        }

        private bool checkContainsDG()
        {
            foreach (DataGridViewRow dr in dgrItems.Rows)
            {
                if (!dr.IsNewRow)
                    if (OrderDespatchService.verifyDangerousGood(dr.Cells["Item No"].Value.ToString().Trim()))
                    {
                        return true;
                    }
            }
            return false;
        }


        private void btnConnectCourier_Click(object sender, EventArgs e)
        {
            //2. Create Courier Service
            CourierService courier;

            if (cboCourier.Text == "STE")
                courier = new StarTrackCourier();
            else if (cboCourier.Text == "CAP")
                courier = new CapitalTransportCourier();
            else if (cboCourier.Text == "COP")
                courier = new CouriersPleaseCourier();
            else if (cboCourier.Text == "BSL")
                courier = new BlueStarCourier();
            else if (cboCourier.Text == "AUP")
                courier = new AustraliaPostCourier();
            else
                courier = new UnknownCourier();


        }

        private void btnGenerateManifest_Click(object sender, EventArgs e)
        {
            CourierService courier;

            if (cboCourier.Text == "STE")
                courier = new StarTrackCourier();
            else if (cboCourier.Text == "CAP")
                courier = new CapitalTransportCourier();
            else if (cboCourier.Text == "COP")
                courier = new CouriersPleaseCourier();
            else if (cboCourier.Text == "BSL")
                courier = new BlueStarCourier();
            else if (cboCourier.Text == "AUP")
                courier = new AustraliaPostCourier();
            else
                courier = new UnknownCourier();

            courier.GenerateManifest(txtPackedBy.Text);


        }

        private void btnPrintManifest_Click(object sender, EventArgs e)
        {
            CourierService courier;

            if (cboCourier.Text == "STE")
                courier = new StarTrackCourier();
            else if (cboCourier.Text == "CAP")
                courier = new CapitalTransportCourier();
            else if (cboCourier.Text == "COP")
                courier = new CouriersPleaseCourier();
            else if (cboCourier.Text == "BSL")
                courier = new BlueStarCourier();
            else if (cboCourier.Text == "AUP")
                courier = new AustraliaPostCourier();
            else
                courier = new UnknownCourier();

            courier.PrintManifest(txtPackedBy.Text);


        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Environment.ExpandEnvironmentVariables("%CLIENTNAME%"));
        }

        private void btnPrintSettings_Click(object sender, EventArgs e)
        {
            frmPrintSettings ps = new frmPrintSettings();
            ps.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PrintService ps = new PrintService();
            ps.TestPrint(@"\\livdoc9\mydocs\IT\Danan\OrderTest\Label_4cd4a34e-e32e-4602-8d53-0f80e4c46bab.pdf", "");


        }

        private void btnAddRemoveOrder_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(despatchID) || despatchID == "-1")
            {
                MessageBox.Show("Need to load an order first");
                return;
            }
            

            frmAdditionalOrder additionalOrder = new frmAdditionalOrder(despatchID, txtPackedBy.Text);
            additionalOrder.ShowDialog();

            string tempDespatch = despatchID;

            string tempOrderNo = txtOrderNo.Text;
            string tempCourier = cboCourier.Text;

            ResetForm();

            txtOrderNo.Text = tempOrderNo;
            cboCourier.Text = tempCourier;

            despatchID = tempDespatch;

            LoadDespatch();
        }

        private void btnSendToOE_Click(object sender, EventArgs e)
        {
            SendToOE();

            stopWatch.Reset();
            stopTimer = true;
        }

        private void SendToOE() 
        {
            frmOrderProcessing op = new frmOrderProcessing(despatchID.Trim(), txtOrderNo.Text.Trim(), "OE_PRINT_LABEL", cboCourier.Text.Trim(), Int32.Parse(txtNoOfCartons.Text), Int32.Parse(txtNoOfPallets.Text), txtPackedBy.Text.Trim());

            op.ShowDialog();

            //Display instructions to user
            string area = op.area;
            string userMessage = op.userMessage;

            frmOrderProcessed orderProcessed = new frmOrderProcessed(despatchID.Trim(), area, userMessage);
            orderProcessed.Show();

            ResetForm();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            CapitalTransportCourier courier = new CapitalTransportCourier();

            courier.testRequest();

        }

        private void txtPackingLength_Enter(object sender, EventArgs e)
        {
            txtPackingLength.SelectionStart = 0;
            txtPackingLength.SelectionLength = txtPackingLength.Text.Length;
        }

        private void cboPackingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboPackingType.Text == "PALLET")
            {
                nmrPackingCartonsPerPallet.Enabled = true;
            }
            else if (cboPackingType.Text == "CARTON")
            {
                nmrPackingCartonsPerPallet.Enabled = false;
                nmrPackingCartonsPerPallet.Text = "1";
            }
        }

        private void txtPackingWidth_Enter(object sender, EventArgs e)
        {
            txtPackingWidth.SelectionStart = 0;
            txtPackingWidth.SelectionLength = txtPackingWidth.Text.Length;

        }

        private void txtPackingHeight_Enter(object sender, EventArgs e)
        {
            txtPackingHeight.SelectionStart = 0;
            txtPackingHeight.SelectionLength = txtPackingHeight.Text.Length;
        }

        private void txtPackingWeight_Enter(object sender, EventArgs e)
        {
            txtPackingWeight.SelectionStart = 0;
            txtPackingWeight.SelectionLength = txtPackingWeight.Text.Length;
        }

        private void txtPackingConnoteNo_Enter(object sender, EventArgs e)
        {
            txtPackingConnoteNo.SelectionStart = 0;
            txtPackingConnoteNo.SelectionLength = txtPackingConnoteNo.Text.Length;
        }

        private void txtPackingEstCost_Enter(object sender, EventArgs e)
        {
            txtPackingEstCost.SelectionStart = 0;
            txtPackingEstCost.SelectionLength = txtPackingEstCost.Text.Length;
        }

        private void txtPackingComments_Enter(object sender, EventArgs e)
        {
            txtPackingComments.SelectionStart = 0;
            txtPackingComments.SelectionLength = txtPackingComments.Text.Length;
        }

        private void cboPackingType_Enter(object sender, EventArgs e)
        {

        }

        private void nmrPackingQty_Enter(object sender, EventArgs e)
        {
            nmrPackingQty.Select(0, nmrPackingQty.Text.Length);
        }

        private void nmrPackingCartonsPerPallet_Enter(object sender, EventArgs e)
        {
            nmrPackingCartonsPerPallet.Select(0, nmrPackingQty.Text.Length);

        }


        private void btnDeleteConnote_Click(object sender, EventArgs e)
        {
            if (cboCourier.Text.Trim() == "STE" && !string.IsNullOrEmpty(txtConnoteNo.Text))
            {
                string connoteNo = txtConnoteNo.Text.Trim();
                StarTrackCourier steCourier = new StarTrackCourier();

                if (steCourier.DeleteConnote(despatchID, connoteNo))
                {
                    txtConnoteNo.Text = "";
                    MessageBox.Show("Connote Deleted!");
                    OrderDespatchService.UpdateConnoteNo(despatchID, "");
                    OrderDespatchService.UpdateDespatchOrderStatus(despatchID, "Connote Deleted");
                }
                else
                {
                    MessageBox.Show("Error! Could not delete connote.");
                }


            }
            else if (cboCourier.Text.Trim() == "CAP" && !string.IsNullOrEmpty(txtConnoteNo.Text))
            {
                MessageBox.Show("Cannot delete or update CAP bookings via this program. Please call OE staff ASAP and note the Connote No (Job No). OE staff will call CAP directly to cancel the booking.");
            }
            else
            {
                MessageBox.Show("Connote Deleting NOT SUPPORTED for this courier.");
            }
        }

        private void btnRePrintPackingSlip_Click(object sender, EventArgs e)
        {
            PrintService ps = new PrintService();
            if (ps.PrintPackingSlip(despatchID))
                MessageBox.Show("Packing Slip Print Job Sent.");
        }

        private void btnRePrintLabel_Click(object sender, EventArgs e)
        {
            if (cboCourier.Text.Trim() == "STE")
            {
                string message = "";
                StarTrackCourier steCourier = new StarTrackCourier();
                steCourier.PrintConnote(txtDespatchID.Text.Trim(), txtConnoteNo.Text.Trim(), 0, ref message);
            }
            else if (cboCourier.Text.Trim() == "AUP")
            {
                string message = "";
                AustraliaPostCourier aupCourier = new AustraliaPostCourier();
                aupCourier.PrintConnote(txtDespatchID.Text.Trim(), txtConnoteNo.Text.Trim(), 0, ref message);
            }
            else if (cboCourier.Text.Trim() == "COP")
            {
                string message = "";
                CouriersPleaseCourier copCourier = new CouriersPleaseCourier();
                copCourier.PrintConnote(txtDespatchID.Text.Trim(), txtConnoteNo.Text.Trim(), Int32.Parse(txtTotalCartons.Text), ref message);
            }
            else
            {
                PrintService ps = new PrintService();
                ps.RePrintLabelForDespatch(txtDespatchID.Text.Trim());
            }

            //Update fields in Exact
            //OrderDespatchService.UpdateSalesOrderInExactForDespatch(despatchID.Trim(), cboCourier.Text.Trim(), txtConnoteNo.Text.Trim(), Int32.Parse(txtTotalCartons.Text), Int32.Parse(txtNoOfPallets.Text), double.Parse(txtTotalWeight.Text), double.Parse(txtTotalDimensions.Text));


        }

        private void btnPrintDG_Click(object sender, EventArgs e)
        {
            PrintService ps = new PrintService();
            ps.PrintDangerousGoodsForm(despatchID);

            MessageBox.Show("Dangerous Goods Form(s) Print Job Sent.");
        }

        private void btnTestConnect_Click(object sender, EventArgs e)
        {
            if (cboCourier.Text == "STE") 
            {
                StarTrackCourier steCourier = new StarTrackCourier();
                steCourier.CheckConnected();
            }

            //frmCAPQuestions capq = new frmCAPQuestions();
            //capq.ShowDialog();

            //CapitalTransportCourier courier = new CapitalTransportCourier();

            ////courier.testRequest();
            //courier.testCargoLoading();
        }

        private void txtOrderNo_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    btnRetrieveInfo_Click(this, new EventArgs());
            //}
        }

        private void chkInvoiceWithGoods_CheckedChanged(object sender, EventArgs e)
        {
            if (chkInvoiceWithGoods.Checked)
            {
                chkPrintPackingSlip.Enabled = false;
            }
            else
                chkPrintPackingSlip.Enabled = true;
        }

        private void txtPackingHeight_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnAddPacking_Click(this, new EventArgs());
            }
        }

        private void txtPackingWidth_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnAddPacking_Click(this, new EventArgs());
            }
        }

        private void txtPackingLength_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnAddPacking_Click(this, new EventArgs());
            }
        }

        private void txtPackingWeight_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnAddPacking_Click(this, new EventArgs());
            }
        }

        private void txtPackingConnoteNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnAddPacking_Click(this, new EventArgs());
            }
        }

        private void dgrItems_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var grid = sender as DataGridView;
            var rowIdx = (e.RowIndex + 1).ToString();

            var centerFormat = new StringFormat()
            {
                // right alignment might actually make more sense for numbers
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            var headerBounds = new Rectangle(e.RowBounds.Left, e.RowBounds.Top, grid.RowHeadersWidth, e.RowBounds.Height);
            e.Graphics.DrawString(rowIdx, this.Font, SystemBrushes.ControlText, headerBounds, centerFormat);
        }

        private void btnValidate_Click(object sender, EventArgs e)
        {
            ValidateOrderForProcessing();
        }

        private void bgwTimer_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!stopTimer)
            {
                stopWatch.Start();
                Thread.Sleep(1000);
                stopWatch.Stop();
                // Get the elapsed time as a TimeSpan value.
                TimeSpan ts = stopWatch.Elapsed;

                // Format and display the TimeSpan value.
                elapsedTime = "Time: " + String.Format("{0:00}:{1:00}:{2:00}",
                    ts.Hours, ts.Minutes, ts.Seconds,
                    ts.Milliseconds / 10);

                UpdateTimerLabel();
            }
        }

        private void UpdateTimerLabel()
        {
            if (this.lblTimer.InvokeRequired)
            {
                this.lblTimer.BeginInvoke((MethodInvoker)delegate() { this.lblTimer.Text = elapsedTime; });
            }
            else
            {
                this.lblTimer.Text = this.elapsedTime.ToString(); ;
            }
        }

     




    }
}
