﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OrderDespatcher
{
    public partial class frmWeightCheck : Form
    {
        private string despatchID { get; set; }

        public frmWeightCheck()
        {
            InitializeComponent();
        }

        public frmWeightCheck(string despatchID)
        {
            InitializeComponent();
            this.despatchID = despatchID;
        }

        private void btnAmend_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnAuthorise_Click(object sender, EventArgs e)
        {
            //Check login is warehouse manager
            if (OrderDespatchService.CheckUserCanAuthoriseDespatch(txtAuthoriseLogin.Text, txtAuthorisePassword.Text))
            {
                //Store authoriser in DB
                OrderDespatchService.SetAuthorisation(this.despatchID, txtAuthoriseLogin.Text.ToUpper().Trim(), "Connote and System Weight Discrepancy");

                //Proceed with order
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Access Denied! User does not have permission to authorise despatch orders.");
            }
        }
    }
}
