﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Windows.Forms;
using Livingstone.Library;

namespace OrderDespatcher
{
    public partial class frmAdditionalOrder : Form
    {
        private string packerName { get; set; }

        public frmAdditionalOrder()
        {
            InitializeComponent();
        }

        public frmAdditionalOrder(string despatchID, string packer)
        {
            InitializeComponent();

            txtDespatchID.Text = despatchID;
            this.packerName = packer;

            RetrieveAdditionalOrders();
        }

        public void RetrieveAdditionalOrders() 
        {

            dgrAdditionalOrder.DataSource = OrderDespatchService.RetrieveOrdersForDespatch(txtDespatchID.Text.Trim());
        }

        private void btnAddOrder_Click(object sender, EventArgs e)
        {
            string orderToAdd = txtAddOrder.Text.Trim();

            if (orderToAdd.ToUpper().StartsWith("DD"))
            {
                orderToAdd = orderToAdd.Substring(2);
            }

            OrderDespatchService.AddOrderToDespatch(txtDespatchID.Text.Trim(), orderToAdd, packerName);

            RetrieveAdditionalOrders();
        }

        private void btnRemoveOrder_Click(object sender, EventArgs e)
        {
            if (dgrAdditionalOrder == null || dgrAdditionalOrder.Rows.Count <= 0) return;

            string orderToRemove = dgrAdditionalOrder.CurrentRow.Cells["orderNo"].Value.ToString().Trim();

            OrderDespatchService.RemoveOrderFromDespatch(txtDespatchID.Text, orderToRemove);

            RetrieveAdditionalOrders();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
