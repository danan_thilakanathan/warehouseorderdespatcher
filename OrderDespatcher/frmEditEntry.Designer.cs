﻿namespace OrderDespatcher
{
    partial class frmEditEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nmrPackingCartonsPerPallet = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.nmrPackingQty = new System.Windows.Forms.NumericUpDown();
            this.chkPackingDG = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPackingEstCost = new System.Windows.Forms.TextBox();
            this.txtPackingComments = new System.Windows.Forms.TextBox();
            this.txtPackingConnoteNo = new System.Windows.Forms.TextBox();
            this.txtPackingWeight = new System.Windows.Forms.TextBox();
            this.txtPackingHeight = new System.Windows.Forms.TextBox();
            this.txtPackingWidth = new System.Windows.Forms.TextBox();
            this.txtPackingLength = new System.Windows.Forms.TextBox();
            this.cboPackingType = new System.Windows.Forms.ComboBox();
            this.lblTypePacking = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblPackingID = new System.Windows.Forms.Label();
            this.txtPackingID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDespatchID = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.nmrPackingCartonsPerPallet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmrPackingQty)).BeginInit();
            this.SuspendLayout();
            // 
            // nmrPackingCartonsPerPallet
            // 
            this.nmrPackingCartonsPerPallet.Enabled = false;
            this.nmrPackingCartonsPerPallet.Location = new System.Drawing.Point(494, 72);
            this.nmrPackingCartonsPerPallet.Name = "nmrPackingCartonsPerPallet";
            this.nmrPackingCartonsPerPallet.Size = new System.Drawing.Size(70, 20);
            this.nmrPackingCartonsPerPallet.TabIndex = 7;
            this.nmrPackingCartonsPerPallet.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nmrPackingCartonsPerPallet.Enter += new System.EventHandler(this.nmrPackingCartonsPerPallet_Enter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label2.Location = new System.Drawing.Point(488, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 36;
            this.label2.Text = "CTNs Per Pallet";
            // 
            // nmrPackingQty
            // 
            this.nmrPackingQty.Location = new System.Drawing.Point(99, 72);
            this.nmrPackingQty.Name = "nmrPackingQty";
            this.nmrPackingQty.Size = new System.Drawing.Size(69, 20);
            this.nmrPackingQty.TabIndex = 2;
            this.nmrPackingQty.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nmrPackingQty.Enter += new System.EventHandler(this.nmrPackingQty_Enter);
            // 
            // chkPackingDG
            // 
            this.chkPackingDG.AutoSize = true;
            this.chkPackingDG.Location = new System.Drawing.Point(153, 134);
            this.chkPackingDG.Name = "chkPackingDG";
            this.chkPackingDG.Size = new System.Drawing.Size(58, 17);
            this.chkPackingDG.TabIndex = 10;
            this.chkPackingDG.Text = "is DG?";
            this.chkPackingDG.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(399, 76);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "x";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(322, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(12, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "x";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(96, 114);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Est. Cost";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(211, 115);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "Comments?";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 114);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "Connote No";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(175, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "Weight (kg)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(409, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Height (cm)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(332, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Width (cm)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(103, 55);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(23, 13);
            this.label14.TabIndex = 20;
            this.label14.Text = "Qty";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(254, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Length (cm)";
            // 
            // txtPackingEstCost
            // 
            this.txtPackingEstCost.Location = new System.Drawing.Point(99, 132);
            this.txtPackingEstCost.Name = "txtPackingEstCost";
            this.txtPackingEstCost.Size = new System.Drawing.Size(46, 20);
            this.txtPackingEstCost.TabIndex = 9;
            this.txtPackingEstCost.Enter += new System.EventHandler(this.txtPackingEstCost_Enter);
            // 
            // txtPackingComments
            // 
            this.txtPackingComments.Location = new System.Drawing.Point(214, 133);
            this.txtPackingComments.Name = "txtPackingComments";
            this.txtPackingComments.Size = new System.Drawing.Size(176, 20);
            this.txtPackingComments.TabIndex = 11;
            this.txtPackingComments.Enter += new System.EventHandler(this.txtPackingComments_Enter);
            // 
            // txtPackingConnoteNo
            // 
            this.txtPackingConnoteNo.Location = new System.Drawing.Point(12, 132);
            this.txtPackingConnoteNo.Name = "txtPackingConnoteNo";
            this.txtPackingConnoteNo.Size = new System.Drawing.Size(79, 20);
            this.txtPackingConnoteNo.TabIndex = 8;
            this.txtPackingConnoteNo.Enter += new System.EventHandler(this.txtPackingConnoteNo_Enter);
            // 
            // txtPackingWeight
            // 
            this.txtPackingWeight.Location = new System.Drawing.Point(175, 73);
            this.txtPackingWeight.Name = "txtPackingWeight";
            this.txtPackingWeight.Size = new System.Drawing.Size(62, 20);
            this.txtPackingWeight.TabIndex = 3;
            this.txtPackingWeight.Enter += new System.EventHandler(this.txtPackingWeight_Enter);
            // 
            // txtPackingHeight
            // 
            this.txtPackingHeight.Location = new System.Drawing.Point(412, 73);
            this.txtPackingHeight.Name = "txtPackingHeight";
            this.txtPackingHeight.Size = new System.Drawing.Size(62, 20);
            this.txtPackingHeight.TabIndex = 6;
            this.txtPackingHeight.Enter += new System.EventHandler(this.txtPackingHeight_Enter);
            // 
            // txtPackingWidth
            // 
            this.txtPackingWidth.Location = new System.Drawing.Point(335, 73);
            this.txtPackingWidth.Name = "txtPackingWidth";
            this.txtPackingWidth.Size = new System.Drawing.Size(62, 20);
            this.txtPackingWidth.TabIndex = 5;
            this.txtPackingWidth.Enter += new System.EventHandler(this.txtPackingWidth_Enter);
            // 
            // txtPackingLength
            // 
            this.txtPackingLength.Location = new System.Drawing.Point(257, 73);
            this.txtPackingLength.Name = "txtPackingLength";
            this.txtPackingLength.Size = new System.Drawing.Size(62, 20);
            this.txtPackingLength.TabIndex = 4;
            this.txtPackingLength.Enter += new System.EventHandler(this.txtPackingLength_Enter);
            // 
            // cboPackingType
            // 
            this.cboPackingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPackingType.FormattingEnabled = true;
            this.cboPackingType.Location = new System.Drawing.Point(12, 72);
            this.cboPackingType.Name = "cboPackingType";
            this.cboPackingType.Size = new System.Drawing.Size(82, 21);
            this.cboPackingType.TabIndex = 1;
            this.cboPackingType.SelectedIndexChanged += new System.EventHandler(this.cboPackingType_SelectedIndexChanged);
            // 
            // lblTypePacking
            // 
            this.lblTypePacking.AutoSize = true;
            this.lblTypePacking.Location = new System.Drawing.Point(9, 54);
            this.lblTypePacking.Name = "lblTypePacking";
            this.lblTypePacking.Size = new System.Drawing.Size(31, 13);
            this.lblTypePacking.TabIndex = 15;
            this.lblTypePacking.Text = "Type";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Location = new System.Drawing.Point(380, 184);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(102, 45);
            this.btnUpdate.TabIndex = 12;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(494, 184);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 45);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblPackingID
            // 
            this.lblPackingID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPackingID.AutoSize = true;
            this.lblPackingID.Location = new System.Drawing.Point(405, 9);
            this.lblPackingID.Name = "lblPackingID";
            this.lblPackingID.Size = new System.Drawing.Size(63, 13);
            this.lblPackingID.TabIndex = 39;
            this.lblPackingID.Text = "Packing ID:";
            // 
            // txtPackingID
            // 
            this.txtPackingID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPackingID.Location = new System.Drawing.Point(474, 6);
            this.txtPackingID.Name = "txtPackingID";
            this.txtPackingID.ReadOnly = true;
            this.txtPackingID.Size = new System.Drawing.Size(90, 20);
            this.txtPackingID.TabIndex = 40;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(233, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 39;
            this.label1.Text = "Despatch ID:";
            // 
            // txtDespatchID
            // 
            this.txtDespatchID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDespatchID.Location = new System.Drawing.Point(309, 6);
            this.txtDespatchID.Name = "txtDespatchID";
            this.txtDespatchID.ReadOnly = true;
            this.txtDespatchID.Size = new System.Drawing.Size(90, 20);
            this.txtDespatchID.TabIndex = 40;
            // 
            // frmEditEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 241);
            this.Controls.Add(this.txtDespatchID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPackingID);
            this.Controls.Add(this.lblPackingID);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.nmrPackingCartonsPerPallet);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nmrPackingQty);
            this.Controls.Add(this.chkPackingDG);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtPackingEstCost);
            this.Controls.Add(this.txtPackingComments);
            this.Controls.Add(this.txtPackingConnoteNo);
            this.Controls.Add(this.txtPackingWeight);
            this.Controls.Add(this.txtPackingHeight);
            this.Controls.Add(this.txtPackingWidth);
            this.Controls.Add(this.txtPackingLength);
            this.Controls.Add(this.cboPackingType);
            this.Controls.Add(this.lblTypePacking);
            this.Name = "frmEditEntry";
            this.Text = "Edit Entry";
            ((System.ComponentModel.ISupportInitialize)(this.nmrPackingCartonsPerPallet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmrPackingQty)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown nmrPackingCartonsPerPallet;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nmrPackingQty;
        private System.Windows.Forms.CheckBox chkPackingDG;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPackingEstCost;
        private System.Windows.Forms.TextBox txtPackingComments;
        private System.Windows.Forms.TextBox txtPackingConnoteNo;
        private System.Windows.Forms.TextBox txtPackingWeight;
        private System.Windows.Forms.TextBox txtPackingHeight;
        private System.Windows.Forms.TextBox txtPackingWidth;
        private System.Windows.Forms.TextBox txtPackingLength;
        private System.Windows.Forms.ComboBox cboPackingType;
        private System.Windows.Forms.Label lblTypePacking;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblPackingID;
        private System.Windows.Forms.TextBox txtPackingID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDespatchID;
    }
}