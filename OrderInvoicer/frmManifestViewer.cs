﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OrderInvoicer
{
    public partial class frmManifestViewer : Form
    {
        public frmManifestViewer()
        {
            InitializeComponent();

            cboCourier.Text = "STE";
            cboCompany.Text = "LIVINGSTONE";
        }

        private void btnRetrieve_Click(object sender, EventArgs e)
        {
            if (cboCourier.Text.ToUpper().Trim() == "STE" || cboCourier.Text.ToUpper().Trim() == "AUP")
            {
                if (!string.IsNullOrEmpty(txtManifestNo.Text))
                {
                    if (string.IsNullOrEmpty(cboCompany.Text)) 
                    {
                        MessageBox.Show("Please select a company!");
                        return;
                    }

                    //Show all connotes for selected order id and courier
                    dgrManifestViewer.DataSource = OrderInvoicerService.retrieveManifestListPerOrderIDForSTEAUP(txtManifestNo.Text, cboCourier.Text.ToUpper().Trim());
                }
                else
                {
                    DateTime from = dtpFrom.Value.Date.AddHours(0).AddMinutes(0).AddSeconds(0);
                    DateTime to = dtpTo.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);

                    //Show all connotes for orders where orderIDs were generated between the selected date range and company
                    dgrManifestViewer.DataSource = OrderInvoicerService.retrieveManifestListPerDateForSTEAUP(cboCourier.Text.ToUpper().Trim(), cboCompany.Text.ToUpper().Trim(), from, to);
                }
                
            }
        }

        private void btnExportToExcel_Click(object sender, EventArgs e)
        {
        }
    }
}
