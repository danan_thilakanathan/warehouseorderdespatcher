﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;
using Livingstone.Library;
using OrderDespatcher;
using OrderDespatcher.Courier;


namespace OrderInvoicer
{
    class OrderInvoicerService
    {

        public static DataTable GetListOfDespatchesWithNoInvoices()
        {
            DataTable dt = DataSource.GetTableFrom(@"
                select despatchLines.despatchID, despatchLines.orderNo, despatch.courier, despatch.connoteNo, despatch.despatchStatus, despatch.cusNo, despatch.exactNo, despatch.ts as entryCreated, despatch.packedBy, despatch.invoiceWithGoods, despatchLines.invoiceNo
                from tblWhOrderDespatch_Orders despatchLines
                left join tblWhOrderDespatch despatch
	                on (despatchLines.despatchID = despatch.id)
                where despatchLines.invoiceNo is null and connoteNo is not null and connoteNo <> '' and sentToOE <> 1
                order by despatchLines.despatchID desc, despatchLines.orderNo desc
            ", "NetCRMAU");

            return dt;
        }

        public static DataTable GetListOfDespatchesWithInvoicesButNoEmailSent()
        {
            DataTable dt = DataSource.GetTableFrom(@"
                select cast(0 as bit) as DoNotSend, despatchLines.despatchID, despatchLines.orderNo, despatch.cusNo, despatch.exactNo, despatchLines.invoiceNo
                from tblWhOrderDespatch_Orders despatchLines
                left join tblWhOrderDespatch despatch
	                on (despatchLines.despatchID = despatch.id)
                where despatchLines.invoiceNo is not null and emailInvoiceSent <> 1
                order by despatchLines.despatchID desc, despatchLines.orderNo desc
            ", "NetCRMAU");

            return dt;
        }

        public static bool updateInvoiceForDespatch(string despatchID, string invoiceNo)
        {
            bool updated = false;

            object[,] xargs = {
                { "@despatchID", despatchID.Trim() },
                { "@invoiceNo", invoiceNo.Trim() }
            };

            DataTable dt = DataSource.GetTableFrom(@"
                update tblWhOrderDespatch
                set invoiceNo = @invoiceNo
                where id = @despatchID

                select 1 from tblWhOrderDespatch where id = @despatchID and invoiceNo = @invoiceNo
            ", "NetCRMAU", xargs);

            if (dt != null && dt.Rows.Count > 0)
                if (dt.Rows[0][0].ToString().Trim() == "1") updated = true;


            return updated;
        }

        public static bool updateInvoiceForDespatchInExact(string despatchID)
        {
            bool updated = false;

            object[,] xargs = {
                { "@despatchID", despatchID.Trim() }
            };

            DataTable dt = DataSource.GetTableFrom(@"
                update tblWhOrderDespatch
                set invoiceNo = @invoiceNo
                where id = @despatchID

                UPDATE [frhkrg]
                SET levwijze = @courier,
                    freefield3 = @connote,
                    bruto_gew = @weight,
                    refer3 = @cubic,
                    freefield4 = @noOfCartons,
                    freefield5 = @noOfPLT
                WHERE faknr = str(@invoiceNo, 8)

                select 1 from tblWhOrderDespatch where id = @despatchID and invoiceNo = @invoiceNo
            ", "NetCRMAU", xargs);

            return updated;
        }


        public static DataTable retrieveManifestListPerOrderIDForSTEAUP(string orderID, string courier)
        {
            object[,] xargs = {
                { "@orderID", orderID.Trim() },
                { "@courier", courier.Trim() },
            };

            DataTable dt = DataSource.GetTableFrom(@"
                select o.despatchID,o.orderNo, a.connoteNo, o.invoiceNo, a.totalCostExclGST, a.totalGST, a.totalCost, a.cusNo, a.orderID, a.ts as timeGenerated, a.courier
                from tblWhOrderDespatch_Orders o
                left join tblWhAusPostDespatch a
	                on (o.despatchID = a.despatchID)
                where orderID = @orderID and courier = @courier
                order by o.despatchID desc
            ", "NetCRMAU", xargs);

            return dt;

        }


        public static DataTable retrieveManifestListPerDateForSTEAUP(string courier, string company, DateTime from, DateTime to)
        {
            object[,] xargs = {
                { "@courier", courier.Trim() },
                { "@company", company.Trim() },
                { "@from", from },
                { "@to", to },
            };

            DataTable dt = DataSource.GetTableFrom(@"
                select orderID as ManifestNo, m.courier, s.Company as company, 
	                totalCost - totalGST as totalWithoutGST, totalGST, totalCost, orderCreationDate, orderReference, generatedBy, isPrinted  
                from tblWhauspostdespatchManifest m
                left join tblWhAusPostDespatch_Settings s
	                on (m.account = s.ACCOUNT_NO)
                where m.courier = @courier and s.Company = @company and orderCreationDate between @from and @to
                order by orderCreationDate desc
            ", "NetCRMAU", xargs);

            return dt;

        }


        public static DataTable searchByConnote(string inputConnote, out int noOfresults)
        {
            object[,] xargs = {
                { "@inputConnote", inputConnote.Trim() }
            };

            noOfresults = 0;

            DataTable dt = DataSource.GetTableFrom(@"
                select lin.orderNo, lin.despatchID, hdr.connoteNo, lin.invoiceNo, hdr.despatchStatus, hdr.packedBy, hdr.ts as created
                from tblWhOrderDespatch hdr
                left join tblWhOrderDespatch_Orders lin
	                on (hdr.id = lin.despatchID)
                where hdr.connoteNo like '%' + @inputConnote + '%'
            ", "NetCRMAU", xargs);

            if (dt != null)
                noOfresults = dt.Rows.Count;

            return dt;
        }


        public static bool deleteConnote(string despatchID, string connoteNo, string courier)
        {
            object[,] xargs = {
                {"@despatchID", despatchID},
                {"@connoteNo", connoteNo},
                {"@courier", courier }
            };

            if (courier == "COP")
            {
                DataSource.ExecuteNonQuery(@"
                    update tblWhOrderDespatch set connoteNo = '' where id = @despatchID

                    update tblWhPackingDetails set connoteNo = '' where despatchID = @despatchID
                ", "NetCRMAU", xargs);

                MessageBox.Show("Connote Deleted!");

                return true;
            }
            else if (courier == "STE") 
            {
                StarTrackCourier steCourier = new StarTrackCourier();

                if (steCourier.DeleteConnote(despatchID, connoteNo))
                {
                    MessageBox.Show("Connote Deleted!");
                    OrderDespatchService.UpdateConnoteNo(despatchID, "");
                    OrderDespatchService.UpdateDespatchOrderStatus(despatchID, "Connote Deleted");

                    return true;
                }
                else
                {
                    OrderDespatchService.UpdateConnoteNo(despatchID, "");
                    MessageBox.Show("Connote does not exist in STE/AUP. Connote deleted from Order Despatcher.");

                    return false;
                }
            }
            else if (courier == "CAP")
            {
                OrderDespatchService.UpdateConnoteNo(despatchID, "");
                MessageBox.Show("Job No deleted from system!");

                return true;
            }

            return true;
        }

    }
}
