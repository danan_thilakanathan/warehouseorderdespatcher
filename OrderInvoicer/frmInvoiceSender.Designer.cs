﻿namespace OrderInvoicer
{
    partial class frmInvoiceSender
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgrInvoicesToSend = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gbpProgress = new System.Windows.Forms.GroupBox();
            this.pnlProgress = new System.Windows.Forms.Panel();
            this.lblProgress = new System.Windows.Forms.Label();
            this.btnMarkAsSent = new System.Windows.Forms.Button();
            this.btnSendInvoice = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.EmailSent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgrInvoicesToSend)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.gbpProgress.SuspendLayout();
            this.pnlProgress.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgrInvoicesToSend
            // 
            this.dgrInvoicesToSend.AllowUserToAddRows = false;
            this.dgrInvoicesToSend.AllowUserToOrderColumns = true;
            this.dgrInvoicesToSend.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgrInvoicesToSend.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrInvoicesToSend.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EmailSent});
            this.dgrInvoicesToSend.Location = new System.Drawing.Point(6, 19);
            this.dgrInvoicesToSend.Name = "dgrInvoicesToSend";
            this.dgrInvoicesToSend.Size = new System.Drawing.Size(784, 239);
            this.dgrInvoicesToSend.TabIndex = 0;
            this.dgrInvoicesToSend.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgrInvoicesToSend_RowPostPaint);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnMarkAsSent);
            this.groupBox1.Controls.Add(this.dgrInvoicesToSend);
            this.groupBox1.Location = new System.Drawing.Point(2, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(796, 291);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Invoices To Send";
            // 
            // gbpProgress
            // 
            this.gbpProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbpProgress.Controls.Add(this.pnlProgress);
            this.gbpProgress.Location = new System.Drawing.Point(2, 309);
            this.gbpProgress.Name = "gbpProgress";
            this.gbpProgress.Size = new System.Drawing.Size(796, 291);
            this.gbpProgress.TabIndex = 2;
            this.gbpProgress.TabStop = false;
            this.gbpProgress.Text = "Progress";
            // 
            // pnlProgress
            // 
            this.pnlProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlProgress.AutoScroll = true;
            this.pnlProgress.Controls.Add(this.lblProgress);
            this.pnlProgress.Location = new System.Drawing.Point(11, 20);
            this.pnlProgress.Name = "pnlProgress";
            this.pnlProgress.Size = new System.Drawing.Size(779, 265);
            this.pnlProgress.TabIndex = 0;
            // 
            // lblProgress
            // 
            this.lblProgress.AutoSize = true;
            this.lblProgress.Location = new System.Drawing.Point(3, 2);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(57, 13);
            this.lblProgress.TabIndex = 0;
            this.lblProgress.Text = "Progress...";
            // 
            // btnMarkAsSent
            // 
            this.btnMarkAsSent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMarkAsSent.Location = new System.Drawing.Point(704, 263);
            this.btnMarkAsSent.Name = "btnMarkAsSent";
            this.btnMarkAsSent.Size = new System.Drawing.Size(86, 22);
            this.btnMarkAsSent.TabIndex = 1;
            this.btnMarkAsSent.Text = "Mark As Sent";
            this.btnMarkAsSent.UseVisualStyleBackColor = true;
            // 
            // btnSendInvoice
            // 
            this.btnSendInvoice.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSendInvoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.btnSendInvoice.Location = new System.Drawing.Point(337, 609);
            this.btnSendInvoice.Name = "btnSendInvoice";
            this.btnSendInvoice.Size = new System.Drawing.Size(75, 42);
            this.btnSendInvoice.TabIndex = 3;
            this.btnSendInvoice.Text = "Send";
            this.btnSendInvoice.UseVisualStyleBackColor = true;
            this.btnSendInvoice.Click += new System.EventHandler(this.btnSendInvoice_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.btnCancel.Location = new System.Drawing.Point(418, 609);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 42);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // EmailSent
            // 
            this.EmailSent.HeaderText = "EmailSent";
            this.EmailSent.Name = "EmailSent";
            // 
            // frmInvoiceSender
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 663);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSendInvoice);
            this.Controls.Add(this.gbpProgress);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmInvoiceSender";
            this.Text = "Invoice Email Sender";
            ((System.ComponentModel.ISupportInitialize)(this.dgrInvoicesToSend)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.gbpProgress.ResumeLayout(false);
            this.pnlProgress.ResumeLayout(false);
            this.pnlProgress.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgrInvoicesToSend;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnMarkAsSent;
        private System.Windows.Forms.GroupBox gbpProgress;
        private System.Windows.Forms.Panel pnlProgress;
        private System.Windows.Forms.Label lblProgress;
        private System.Windows.Forms.Button btnSendInvoice;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmailSent;
    }
}