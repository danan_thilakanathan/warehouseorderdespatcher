﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Livingstone.Library;

namespace OrderInvoicer
{
    public partial class frmInvoiceSender : Form
    {
        public frmInvoiceSender()
        {
            InitializeComponent();

            initialiseForm();
        }

        private void btnSendInvoice_Click(object sender, EventArgs e)
        {

        }

        public void initialiseForm()
        {
            dgrInvoicesToSend.DataSource = OrderInvoicerService.GetListOfDespatchesWithInvoicesButNoEmailSent();
        }

        private void dgrInvoicesToSend_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var grid = sender as DataGridView;
            var rowIdx = (e.RowIndex + 1).ToString();

            var centerFormat = new StringFormat()
            {
                // right alignment might actually make more sense for numbers
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            var headerBounds = new Rectangle(e.RowBounds.Left, e.RowBounds.Top, grid.RowHeadersWidth, e.RowBounds.Height);
            e.Graphics.DrawString(rowIdx, this.Font, SystemBrushes.ControlText, headerBounds, centerFormat);
        }
    }
}
