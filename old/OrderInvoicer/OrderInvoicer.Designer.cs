﻿namespace OrderInvoicer
{
    partial class OrderInvoicer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OrderViewContainer = new System.Windows.Forms.SplitContainer();
            this.spltOrderSelect = new System.Windows.Forms.SplitContainer();
            this.txtSearchConnoteNo = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.btnSearchByDespatch = new System.Windows.Forms.Button();
            this.txtSearchByDespatchID = new System.Windows.Forms.TextBox();
            this.txtSearchOrderNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblOrderNo = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.gbxOrdersToInvoice = new System.Windows.Forms.GroupBox();
            this.dgrOrdersToInvoice = new System.Windows.Forms.DataGridView();
            this.OrderDetailsContainer = new System.Windows.Forms.SplitContainer();
            this.gbxItemDetails = new System.Windows.Forms.GroupBox();
            this.dgrItemDetails = new System.Windows.Forms.DataGridView();
            this.txtInvoiceNo = new System.Windows.Forms.TextBox();
            this.lblInvoiceNo = new System.Windows.Forms.Label();
            this.txtDespatchID = new System.Windows.Forms.TextBox();
            this.lblDespatchID = new System.Windows.Forms.Label();
            this.chkInvoiceWithGoods = new System.Windows.Forms.CheckBox();
            this.txtSpecialInstructions2 = new System.Windows.Forms.TextBox();
            this.txtSpecialInstructions = new System.Windows.Forms.TextBox();
            this.txtConnoteNo = new System.Windows.Forms.TextBox();
            this.lblConnote = new System.Windows.Forms.Label();
            this.txtDeliveryName = new System.Windows.Forms.TextBox();
            this.lblSpecialInstructions = new System.Windows.Forms.Label();
            this.lblDeliveryName = new System.Windows.Forms.Label();
            this.txtDeliveryPostcode = new System.Windows.Forms.TextBox();
            this.txtDeliveryState = new System.Windows.Forms.TextBox();
            this.txtDeliverySuburb = new System.Windows.Forms.TextBox();
            this.txtCountry = new System.Windows.Forms.TextBox();
            this.txtDeliverToAddress3 = new System.Windows.Forms.TextBox();
            this.txtDeliverToAddress2 = new System.Windows.Forms.TextBox();
            this.txtDeliverToAddress1 = new System.Windows.Forms.TextBox();
            this.lblCountry = new System.Windows.Forms.Label();
            this.lblDeliveryAddress = new System.Windows.Forms.Label();
            this.btnDangerousGoods = new System.Windows.Forms.Button();
            this.txtPackedBy = new System.Windows.Forms.TextBox();
            this.lblPackedBy = new System.Windows.Forms.Label();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.txtExactNo = new System.Windows.Forms.TextBox();
            this.txtShippingDate = new System.Windows.Forms.TextBox();
            this.txtOrderDate = new System.Windows.Forms.TextBox();
            this.txtCourier = new System.Windows.Forms.TextBox();
            this.txtWarehouse = new System.Windows.Forms.TextBox();
            this.txtDespatchStatus = new System.Windows.Forms.TextBox();
            this.txtOrderStatus = new System.Windows.Forms.TextBox();
            this.txtCustomerNo = new System.Windows.Forms.TextBox();
            this.lblCustomerName = new System.Windows.Forms.Label();
            this.lblExactNo = new System.Windows.Forms.Label();
            this.lblETADate = new System.Windows.Forms.Label();
            this.lblOrderDate = new System.Windows.Forms.Label();
            this.lblCourier = new System.Windows.Forms.Label();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblCusCode = new System.Windows.Forms.Label();
            this.tabDetails = new System.Windows.Forms.TabControl();
            this.tabPackingDetails = new System.Windows.Forms.TabPage();
            this.gbxPackingDetails = new System.Windows.Forms.GroupBox();
            this.txtTotalCartons = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblTotalDimensions = new System.Windows.Forms.Label();
            this.txtTotalDimensions = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtNoOfPallets = new System.Windows.Forms.TextBox();
            this.lblNoOfLabels = new System.Windows.Forms.Label();
            this.txtNoOfCartons = new System.Windows.Forms.TextBox();
            this.txtTotalWeight = new System.Windows.Forms.TextBox();
            this.lblTotalWeight = new System.Windows.Forms.Label();
            this.txtEstimatedCost = new System.Windows.Forms.TextBox();
            this.lblEstimatedCost = new System.Windows.Forms.Label();
            this.dgrPackingDetails = new System.Windows.Forms.DataGridView();
            this.tabOtherDetails = new System.Windows.Forms.TabPage();
            this.txtManifestNo = new System.Windows.Forms.TextBox();
            this.txtFreefield3 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFreefield2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtLabelPrintedDate = new System.Windows.Forms.TextBox();
            this.txtFreefield1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFreightCostWithGST = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtFreightGST = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtFreightCost = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtViewManifest = new System.Windows.Forms.Button();
            this.btnSendInvoices = new System.Windows.Forms.Button();
            this.btnDeleteConnote = new System.Windows.Forms.Button();
            this.btnAuthorise = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.OrderViewContainer)).BeginInit();
            this.OrderViewContainer.Panel1.SuspendLayout();
            this.OrderViewContainer.Panel2.SuspendLayout();
            this.OrderViewContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spltOrderSelect)).BeginInit();
            this.spltOrderSelect.Panel1.SuspendLayout();
            this.spltOrderSelect.Panel2.SuspendLayout();
            this.spltOrderSelect.SuspendLayout();
            this.gbxOrdersToInvoice.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrOrdersToInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderDetailsContainer)).BeginInit();
            this.OrderDetailsContainer.Panel1.SuspendLayout();
            this.OrderDetailsContainer.Panel2.SuspendLayout();
            this.OrderDetailsContainer.SuspendLayout();
            this.gbxItemDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrItemDetails)).BeginInit();
            this.tabDetails.SuspendLayout();
            this.tabPackingDetails.SuspendLayout();
            this.gbxPackingDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrPackingDetails)).BeginInit();
            this.tabOtherDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // OrderViewContainer
            // 
            this.OrderViewContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OrderViewContainer.Location = new System.Drawing.Point(3, 5);
            this.OrderViewContainer.Name = "OrderViewContainer";
            // 
            // OrderViewContainer.Panel1
            // 
            this.OrderViewContainer.Panel1.Controls.Add(this.spltOrderSelect);
            // 
            // OrderViewContainer.Panel2
            // 
            this.OrderViewContainer.Panel2.Controls.Add(this.OrderDetailsContainer);
            this.OrderViewContainer.Size = new System.Drawing.Size(1130, 665);
            this.OrderViewContainer.SplitterDistance = 389;
            this.OrderViewContainer.TabIndex = 1;
            // 
            // spltOrderSelect
            // 
            this.spltOrderSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltOrderSelect.Location = new System.Drawing.Point(0, 0);
            this.spltOrderSelect.Name = "spltOrderSelect";
            this.spltOrderSelect.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spltOrderSelect.Panel1
            // 
            this.spltOrderSelect.Panel1.Controls.Add(this.txtSearchConnoteNo);
            this.spltOrderSelect.Panel1.Controls.Add(this.label12);
            this.spltOrderSelect.Panel1.Controls.Add(this.lblVersion);
            this.spltOrderSelect.Panel1.Controls.Add(this.btnSearchByDespatch);
            this.spltOrderSelect.Panel1.Controls.Add(this.txtSearchByDespatchID);
            this.spltOrderSelect.Panel1.Controls.Add(this.txtSearchOrderNo);
            this.spltOrderSelect.Panel1.Controls.Add(this.label1);
            this.spltOrderSelect.Panel1.Controls.Add(this.lblOrderNo);
            // 
            // spltOrderSelect.Panel2
            // 
            this.spltOrderSelect.Panel2.Controls.Add(this.btnRefresh);
            this.spltOrderSelect.Panel2.Controls.Add(this.gbxOrdersToInvoice);
            this.spltOrderSelect.Size = new System.Drawing.Size(389, 665);
            this.spltOrderSelect.SplitterDistance = 98;
            this.spltOrderSelect.TabIndex = 1;
            // 
            // txtSearchConnoteNo
            // 
            this.txtSearchConnoteNo.Location = new System.Drawing.Point(92, 52);
            this.txtSearchConnoteNo.Name = "txtSearchConnoteNo";
            this.txtSearchConnoteNo.Size = new System.Drawing.Size(193, 20);
            this.txtSearchConnoteNo.TabIndex = 6;
            this.txtSearchConnoteNo.TextChanged += new System.EventHandler(this.txtSearchConnoteNo_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 56);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Connote No:";
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(355, 72);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(28, 13);
            this.lblVersion.TabIndex = 4;
            this.lblVersion.Text = "v1.4";
            // 
            // btnSearchByDespatch
            // 
            this.btnSearchByDespatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearchByDespatch.Location = new System.Drawing.Point(291, 7);
            this.btnSearchByDespatch.Name = "btnSearchByDespatch";
            this.btnSearchByDespatch.Size = new System.Drawing.Size(92, 42);
            this.btnSearchByDespatch.TabIndex = 3;
            this.btnSearchByDespatch.Text = "Load";
            this.btnSearchByDespatch.UseVisualStyleBackColor = true;
            this.btnSearchByDespatch.Click += new System.EventHandler(this.btnSearchByDespatch_Click);
            // 
            // txtSearchByDespatchID
            // 
            this.txtSearchByDespatchID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearchByDespatchID.Location = new System.Drawing.Point(92, 29);
            this.txtSearchByDespatchID.Name = "txtSearchByDespatchID";
            this.txtSearchByDespatchID.Size = new System.Drawing.Size(193, 20);
            this.txtSearchByDespatchID.TabIndex = 2;
            this.txtSearchByDespatchID.TextChanged += new System.EventHandler(this.txtSearchByDespatchID_TextChanged);
            // 
            // txtSearchOrderNo
            // 
            this.txtSearchOrderNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearchOrderNo.Location = new System.Drawing.Point(92, 7);
            this.txtSearchOrderNo.Name = "txtSearchOrderNo";
            this.txtSearchOrderNo.Size = new System.Drawing.Size(193, 20);
            this.txtSearchOrderNo.TabIndex = 2;
            this.txtSearchOrderNo.TextChanged += new System.EventHandler(this.txtSearchOrderNo_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Despatch No:";
            // 
            // lblOrderNo
            // 
            this.lblOrderNo.AutoSize = true;
            this.lblOrderNo.Location = new System.Drawing.Point(13, 7);
            this.lblOrderNo.Name = "lblOrderNo";
            this.lblOrderNo.Size = new System.Drawing.Size(53, 13);
            this.lblOrderNo.TabIndex = 1;
            this.lblOrderNo.Text = "Order No:";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Location = new System.Drawing.Point(291, 10);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(92, 46);
            this.btnRefresh.TabIndex = 1;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // gbxOrdersToInvoice
            // 
            this.gbxOrdersToInvoice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxOrdersToInvoice.Controls.Add(this.dgrOrdersToInvoice);
            this.gbxOrdersToInvoice.Location = new System.Drawing.Point(3, 62);
            this.gbxOrdersToInvoice.Name = "gbxOrdersToInvoice";
            this.gbxOrdersToInvoice.Size = new System.Drawing.Size(383, 501);
            this.gbxOrdersToInvoice.TabIndex = 0;
            this.gbxOrdersToInvoice.TabStop = false;
            this.gbxOrdersToInvoice.Text = "Orders To Invoice";
            // 
            // dgrOrdersToInvoice
            // 
            this.dgrOrdersToInvoice.AllowUserToAddRows = false;
            this.dgrOrdersToInvoice.AllowUserToDeleteRows = false;
            this.dgrOrdersToInvoice.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgrOrdersToInvoice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrOrdersToInvoice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgrOrdersToInvoice.Location = new System.Drawing.Point(3, 16);
            this.dgrOrdersToInvoice.MultiSelect = false;
            this.dgrOrdersToInvoice.Name = "dgrOrdersToInvoice";
            this.dgrOrdersToInvoice.ReadOnly = true;
            this.dgrOrdersToInvoice.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrOrdersToInvoice.Size = new System.Drawing.Size(377, 482);
            this.dgrOrdersToInvoice.TabIndex = 0;
            this.dgrOrdersToInvoice.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgrOrdersToInvoice_RowPostPaint);
            this.dgrOrdersToInvoice.SelectionChanged += new System.EventHandler(this.dgrOrdersToInvoice_SelectionChanged);
            // 
            // OrderDetailsContainer
            // 
            this.OrderDetailsContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OrderDetailsContainer.Location = new System.Drawing.Point(0, 0);
            this.OrderDetailsContainer.Name = "OrderDetailsContainer";
            this.OrderDetailsContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // OrderDetailsContainer.Panel1
            // 
            this.OrderDetailsContainer.Panel1.Controls.Add(this.gbxItemDetails);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtInvoiceNo);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.lblInvoiceNo);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtDespatchID);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.lblDespatchID);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.chkInvoiceWithGoods);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtSpecialInstructions2);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtSpecialInstructions);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtConnoteNo);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.lblConnote);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtDeliveryName);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.lblSpecialInstructions);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.lblDeliveryName);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtDeliveryPostcode);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtDeliveryState);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtDeliverySuburb);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtCountry);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtDeliverToAddress3);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtDeliverToAddress2);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtDeliverToAddress1);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.lblCountry);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.lblDeliveryAddress);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.btnDangerousGoods);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtPackedBy);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.lblPackedBy);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtCustomerName);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtExactNo);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtShippingDate);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtOrderDate);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtCourier);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtWarehouse);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtDespatchStatus);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtOrderStatus);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.txtCustomerNo);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.lblCustomerName);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.lblExactNo);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.lblETADate);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.lblOrderDate);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.lblCourier);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.lblWarehouse);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.label2);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.lblStatus);
            this.OrderDetailsContainer.Panel1.Controls.Add(this.lblCusCode);
            // 
            // OrderDetailsContainer.Panel2
            // 
            this.OrderDetailsContainer.Panel2.Controls.Add(this.tabDetails);
            this.OrderDetailsContainer.Size = new System.Drawing.Size(737, 665);
            this.OrderDetailsContainer.SplitterDistance = 402;
            this.OrderDetailsContainer.TabIndex = 1;
            // 
            // gbxItemDetails
            // 
            this.gbxItemDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxItemDetails.Controls.Add(this.dgrItemDetails);
            this.gbxItemDetails.Location = new System.Drawing.Point(4, 207);
            this.gbxItemDetails.Name = "gbxItemDetails";
            this.gbxItemDetails.Size = new System.Drawing.Size(730, 192);
            this.gbxItemDetails.TabIndex = 0;
            this.gbxItemDetails.TabStop = false;
            this.gbxItemDetails.Text = "Item Details";
            // 
            // dgrItemDetails
            // 
            this.dgrItemDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrItemDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgrItemDetails.Location = new System.Drawing.Point(3, 16);
            this.dgrItemDetails.Name = "dgrItemDetails";
            this.dgrItemDetails.Size = new System.Drawing.Size(724, 173);
            this.dgrItemDetails.TabIndex = 0;
            this.dgrItemDetails.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgrItemDetails_RowPostPaint);
            // 
            // txtInvoiceNo
            // 
            this.txtInvoiceNo.Location = new System.Drawing.Point(288, 178);
            this.txtInvoiceNo.Name = "txtInvoiceNo";
            this.txtInvoiceNo.ReadOnly = true;
            this.txtInvoiceNo.Size = new System.Drawing.Size(100, 20);
            this.txtInvoiceNo.TabIndex = 52;
            // 
            // lblInvoiceNo
            // 
            this.lblInvoiceNo.AutoSize = true;
            this.lblInvoiceNo.Location = new System.Drawing.Point(219, 182);
            this.lblInvoiceNo.Name = "lblInvoiceNo";
            this.lblInvoiceNo.Size = new System.Drawing.Size(62, 13);
            this.lblInvoiceNo.TabIndex = 51;
            this.lblInvoiceNo.Text = "Invoice No:";
            // 
            // txtDespatchID
            // 
            this.txtDespatchID.Location = new System.Drawing.Point(98, 142);
            this.txtDespatchID.Name = "txtDespatchID";
            this.txtDespatchID.ReadOnly = true;
            this.txtDespatchID.Size = new System.Drawing.Size(109, 20);
            this.txtDespatchID.TabIndex = 25;
            // 
            // lblDespatchID
            // 
            this.lblDespatchID.AutoSize = true;
            this.lblDespatchID.Location = new System.Drawing.Point(11, 145);
            this.lblDespatchID.Name = "lblDespatchID";
            this.lblDespatchID.Size = new System.Drawing.Size(70, 13);
            this.lblDespatchID.TabIndex = 24;
            this.lblDespatchID.Text = "Despatch ID:";
            // 
            // chkInvoiceWithGoods
            // 
            this.chkInvoiceWithGoods.AutoSize = true;
            this.chkInvoiceWithGoods.Enabled = false;
            this.chkInvoiceWithGoods.Location = new System.Drawing.Point(288, 144);
            this.chkInvoiceWithGoods.Name = "chkInvoiceWithGoods";
            this.chkInvoiceWithGoods.Size = new System.Drawing.Size(120, 17);
            this.chkInvoiceWithGoods.TabIndex = 43;
            this.chkInvoiceWithGoods.Text = "Invoice With Goods";
            this.chkInvoiceWithGoods.UseVisualStyleBackColor = true;
            // 
            // txtSpecialInstructions2
            // 
            this.txtSpecialInstructions2.Location = new System.Drawing.Point(288, 117);
            this.txtSpecialInstructions2.Name = "txtSpecialInstructions2";
            this.txtSpecialInstructions2.ReadOnly = true;
            this.txtSpecialInstructions2.Size = new System.Drawing.Size(143, 20);
            this.txtSpecialInstructions2.TabIndex = 48;
            // 
            // txtSpecialInstructions
            // 
            this.txtSpecialInstructions.Location = new System.Drawing.Point(288, 94);
            this.txtSpecialInstructions.Name = "txtSpecialInstructions";
            this.txtSpecialInstructions.ReadOnly = true;
            this.txtSpecialInstructions.Size = new System.Drawing.Size(143, 20);
            this.txtSpecialInstructions.TabIndex = 49;
            // 
            // txtConnoteNo
            // 
            this.txtConnoteNo.Location = new System.Drawing.Point(98, 178);
            this.txtConnoteNo.Name = "txtConnoteNo";
            this.txtConnoteNo.ReadOnly = true;
            this.txtConnoteNo.Size = new System.Drawing.Size(94, 20);
            this.txtConnoteNo.TabIndex = 44;
            // 
            // lblConnote
            // 
            this.lblConnote.AutoSize = true;
            this.lblConnote.Location = new System.Drawing.Point(14, 181);
            this.lblConnote.Name = "lblConnote";
            this.lblConnote.Size = new System.Drawing.Size(67, 13);
            this.lblConnote.TabIndex = 47;
            this.lblConnote.Text = "Connote No:";
            // 
            // txtDeliveryName
            // 
            this.txtDeliveryName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDeliveryName.Location = new System.Drawing.Point(532, 13);
            this.txtDeliveryName.Name = "txtDeliveryName";
            this.txtDeliveryName.ReadOnly = true;
            this.txtDeliveryName.Size = new System.Drawing.Size(196, 20);
            this.txtDeliveryName.TabIndex = 50;
            // 
            // lblSpecialInstructions
            // 
            this.lblSpecialInstructions.AutoSize = true;
            this.lblSpecialInstructions.Location = new System.Drawing.Point(180, 97);
            this.lblSpecialInstructions.Name = "lblSpecialInstructions";
            this.lblSpecialInstructions.Size = new System.Drawing.Size(102, 13);
            this.lblSpecialInstructions.TabIndex = 46;
            this.lblSpecialInstructions.Text = "Special Instructions:";
            // 
            // lblDeliveryName
            // 
            this.lblDeliveryName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDeliveryName.AutoSize = true;
            this.lblDeliveryName.Location = new System.Drawing.Point(453, 16);
            this.lblDeliveryName.Name = "lblDeliveryName";
            this.lblDeliveryName.Size = new System.Drawing.Size(79, 13);
            this.lblDeliveryName.TabIndex = 45;
            this.lblDeliveryName.Text = "Delivery Name:";
            // 
            // txtDeliveryPostcode
            // 
            this.txtDeliveryPostcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDeliveryPostcode.Location = new System.Drawing.Point(679, 111);
            this.txtDeliveryPostcode.Name = "txtDeliveryPostcode";
            this.txtDeliveryPostcode.ReadOnly = true;
            this.txtDeliveryPostcode.Size = new System.Drawing.Size(49, 20);
            this.txtDeliveryPostcode.TabIndex = 40;
            // 
            // txtDeliveryState
            // 
            this.txtDeliveryState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDeliveryState.Location = new System.Drawing.Point(619, 111);
            this.txtDeliveryState.Name = "txtDeliveryState";
            this.txtDeliveryState.ReadOnly = true;
            this.txtDeliveryState.Size = new System.Drawing.Size(54, 20);
            this.txtDeliveryState.TabIndex = 41;
            // 
            // txtDeliverySuburb
            // 
            this.txtDeliverySuburb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDeliverySuburb.Location = new System.Drawing.Point(532, 111);
            this.txtDeliverySuburb.Name = "txtDeliverySuburb";
            this.txtDeliverySuburb.ReadOnly = true;
            this.txtDeliverySuburb.Size = new System.Drawing.Size(81, 20);
            this.txtDeliverySuburb.TabIndex = 42;
            // 
            // txtCountry
            // 
            this.txtCountry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCountry.Location = new System.Drawing.Point(669, 137);
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.ReadOnly = true;
            this.txtCountry.Size = new System.Drawing.Size(59, 20);
            this.txtCountry.TabIndex = 38;
            // 
            // txtDeliverToAddress3
            // 
            this.txtDeliverToAddress3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDeliverToAddress3.Location = new System.Drawing.Point(532, 87);
            this.txtDeliverToAddress3.Name = "txtDeliverToAddress3";
            this.txtDeliverToAddress3.ReadOnly = true;
            this.txtDeliverToAddress3.Size = new System.Drawing.Size(196, 20);
            this.txtDeliverToAddress3.TabIndex = 37;
            // 
            // txtDeliverToAddress2
            // 
            this.txtDeliverToAddress2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDeliverToAddress2.Location = new System.Drawing.Point(532, 63);
            this.txtDeliverToAddress2.Name = "txtDeliverToAddress2";
            this.txtDeliverToAddress2.ReadOnly = true;
            this.txtDeliverToAddress2.Size = new System.Drawing.Size(196, 20);
            this.txtDeliverToAddress2.TabIndex = 39;
            // 
            // txtDeliverToAddress1
            // 
            this.txtDeliverToAddress1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDeliverToAddress1.Location = new System.Drawing.Point(532, 39);
            this.txtDeliverToAddress1.Name = "txtDeliverToAddress1";
            this.txtDeliverToAddress1.ReadOnly = true;
            this.txtDeliverToAddress1.Size = new System.Drawing.Size(196, 20);
            this.txtDeliverToAddress1.TabIndex = 36;
            // 
            // lblCountry
            // 
            this.lblCountry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCountry.AutoSize = true;
            this.lblCountry.Location = new System.Drawing.Point(624, 140);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(46, 13);
            this.lblCountry.TabIndex = 35;
            this.lblCountry.Text = "Country:";
            // 
            // lblDeliveryAddress
            // 
            this.lblDeliveryAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDeliveryAddress.AutoSize = true;
            this.lblDeliveryAddress.Location = new System.Drawing.Point(474, 42);
            this.lblDeliveryAddress.Name = "lblDeliveryAddress";
            this.lblDeliveryAddress.Size = new System.Drawing.Size(59, 13);
            this.lblDeliveryAddress.TabIndex = 34;
            this.lblDeliveryAddress.Text = "Deliver To:";
            // 
            // btnDangerousGoods
            // 
            this.btnDangerousGoods.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDangerousGoods.Location = new System.Drawing.Point(616, 176);
            this.btnDangerousGoods.Name = "btnDangerousGoods";
            this.btnDangerousGoods.Size = new System.Drawing.Size(112, 25);
            this.btnDangerousGoods.TabIndex = 25;
            this.btnDangerousGoods.Text = "Dangerous Goods";
            this.btnDangerousGoods.UseVisualStyleBackColor = true;
            this.btnDangerousGoods.Click += new System.EventHandler(this.btnDangerousGoods_Click);
            // 
            // txtPackedBy
            // 
            this.txtPackedBy.Location = new System.Drawing.Point(353, 65);
            this.txtPackedBy.Name = "txtPackedBy";
            this.txtPackedBy.ReadOnly = true;
            this.txtPackedBy.Size = new System.Drawing.Size(78, 20);
            this.txtPackedBy.TabIndex = 24;
            // 
            // lblPackedBy
            // 
            this.lblPackedBy.AutoSize = true;
            this.lblPackedBy.Location = new System.Drawing.Point(285, 68);
            this.lblPackedBy.Name = "lblPackedBy";
            this.lblPackedBy.Size = new System.Drawing.Size(62, 13);
            this.lblPackedBy.TabIndex = 22;
            this.lblPackedBy.Text = "Packed By:";
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Location = new System.Drawing.Point(98, 64);
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.ReadOnly = true;
            this.txtCustomerName.Size = new System.Drawing.Size(166, 20);
            this.txtCustomerName.TabIndex = 2;
            // 
            // txtExactNo
            // 
            this.txtExactNo.Location = new System.Drawing.Point(98, 40);
            this.txtExactNo.Name = "txtExactNo";
            this.txtExactNo.ReadOnly = true;
            this.txtExactNo.Size = new System.Drawing.Size(109, 20);
            this.txtExactNo.TabIndex = 2;
            // 
            // txtShippingDate
            // 
            this.txtShippingDate.Location = new System.Drawing.Point(98, 114);
            this.txtShippingDate.Name = "txtShippingDate";
            this.txtShippingDate.ReadOnly = true;
            this.txtShippingDate.Size = new System.Drawing.Size(56, 20);
            this.txtShippingDate.TabIndex = 2;
            // 
            // txtOrderDate
            // 
            this.txtOrderDate.Location = new System.Drawing.Point(98, 91);
            this.txtOrderDate.Name = "txtOrderDate";
            this.txtOrderDate.ReadOnly = true;
            this.txtOrderDate.Size = new System.Drawing.Size(56, 20);
            this.txtOrderDate.TabIndex = 2;
            // 
            // txtCourier
            // 
            this.txtCourier.Location = new System.Drawing.Point(288, 40);
            this.txtCourier.Name = "txtCourier";
            this.txtCourier.ReadOnly = true;
            this.txtCourier.Size = new System.Drawing.Size(38, 20);
            this.txtCourier.TabIndex = 2;
            // 
            // txtWarehouse
            // 
            this.txtWarehouse.Location = new System.Drawing.Point(400, 16);
            this.txtWarehouse.Name = "txtWarehouse";
            this.txtWarehouse.ReadOnly = true;
            this.txtWarehouse.Size = new System.Drawing.Size(28, 20);
            this.txtWarehouse.TabIndex = 2;
            // 
            // txtDespatchStatus
            // 
            this.txtDespatchStatus.Location = new System.Drawing.Point(492, 179);
            this.txtDespatchStatus.Name = "txtDespatchStatus";
            this.txtDespatchStatus.ReadOnly = true;
            this.txtDespatchStatus.Size = new System.Drawing.Size(100, 20);
            this.txtDespatchStatus.TabIndex = 2;
            // 
            // txtOrderStatus
            // 
            this.txtOrderStatus.Location = new System.Drawing.Point(288, 16);
            this.txtOrderStatus.Name = "txtOrderStatus";
            this.txtOrderStatus.ReadOnly = true;
            this.txtOrderStatus.Size = new System.Drawing.Size(38, 20);
            this.txtOrderStatus.TabIndex = 2;
            // 
            // txtCustomerNo
            // 
            this.txtCustomerNo.Location = new System.Drawing.Point(98, 16);
            this.txtCustomerNo.Name = "txtCustomerNo";
            this.txtCustomerNo.ReadOnly = true;
            this.txtCustomerNo.Size = new System.Drawing.Size(109, 20);
            this.txtCustomerNo.TabIndex = 2;
            // 
            // lblCustomerName
            // 
            this.lblCustomerName.AutoSize = true;
            this.lblCustomerName.Location = new System.Drawing.Point(10, 67);
            this.lblCustomerName.Name = "lblCustomerName";
            this.lblCustomerName.Size = new System.Drawing.Size(85, 13);
            this.lblCustomerName.TabIndex = 1;
            this.lblCustomerName.Text = "Customer Name:";
            // 
            // lblExactNo
            // 
            this.lblExactNo.AutoSize = true;
            this.lblExactNo.Location = new System.Drawing.Point(10, 43);
            this.lblExactNo.Name = "lblExactNo";
            this.lblExactNo.Size = new System.Drawing.Size(54, 13);
            this.lblExactNo.TabIndex = 1;
            this.lblExactNo.Text = "Exact No:";
            // 
            // lblETADate
            // 
            this.lblETADate.AutoSize = true;
            this.lblETADate.Location = new System.Drawing.Point(10, 117);
            this.lblETADate.Name = "lblETADate";
            this.lblETADate.Size = new System.Drawing.Size(77, 13);
            this.lblETADate.TabIndex = 1;
            this.lblETADate.Text = "Shipping Date:";
            // 
            // lblOrderDate
            // 
            this.lblOrderDate.AutoSize = true;
            this.lblOrderDate.Location = new System.Drawing.Point(10, 94);
            this.lblOrderDate.Name = "lblOrderDate";
            this.lblOrderDate.Size = new System.Drawing.Size(62, 13);
            this.lblOrderDate.TabIndex = 1;
            this.lblOrderDate.Text = "Order Date:";
            // 
            // lblCourier
            // 
            this.lblCourier.AutoSize = true;
            this.lblCourier.Location = new System.Drawing.Point(213, 43);
            this.lblCourier.Name = "lblCourier";
            this.lblCourier.Size = new System.Drawing.Size(43, 13);
            this.lblCourier.TabIndex = 1;
            this.lblCourier.Text = "Courier:";
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.Location = new System.Drawing.Point(332, 19);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(65, 13);
            this.lblWarehouse.TabIndex = 1;
            this.lblWarehouse.Text = "Warehouse:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(397, 181);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Despatch Status:";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(213, 19);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(69, 13);
            this.lblStatus.TabIndex = 1;
            this.lblStatus.Text = "Order Status:";
            // 
            // lblCusCode
            // 
            this.lblCusCode.AutoSize = true;
            this.lblCusCode.Location = new System.Drawing.Point(10, 19);
            this.lblCusCode.Name = "lblCusCode";
            this.lblCusCode.Size = new System.Drawing.Size(71, 13);
            this.lblCusCode.TabIndex = 1;
            this.lblCusCode.Text = "Customer No:";
            // 
            // tabDetails
            // 
            this.tabDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabDetails.Controls.Add(this.tabPackingDetails);
            this.tabDetails.Controls.Add(this.tabOtherDetails);
            this.tabDetails.Location = new System.Drawing.Point(7, 3);
            this.tabDetails.Name = "tabDetails";
            this.tabDetails.SelectedIndex = 0;
            this.tabDetails.Size = new System.Drawing.Size(727, 253);
            this.tabDetails.TabIndex = 37;
            // 
            // tabPackingDetails
            // 
            this.tabPackingDetails.Controls.Add(this.gbxPackingDetails);
            this.tabPackingDetails.Location = new System.Drawing.Point(4, 22);
            this.tabPackingDetails.Name = "tabPackingDetails";
            this.tabPackingDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tabPackingDetails.Size = new System.Drawing.Size(719, 227);
            this.tabPackingDetails.TabIndex = 0;
            this.tabPackingDetails.Text = "Packing Details";
            this.tabPackingDetails.UseVisualStyleBackColor = true;
            // 
            // gbxPackingDetails
            // 
            this.gbxPackingDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxPackingDetails.Controls.Add(this.txtTotalCartons);
            this.gbxPackingDetails.Controls.Add(this.label3);
            this.gbxPackingDetails.Controls.Add(this.lblTotalDimensions);
            this.gbxPackingDetails.Controls.Add(this.txtTotalDimensions);
            this.gbxPackingDetails.Controls.Add(this.label13);
            this.gbxPackingDetails.Controls.Add(this.txtNoOfPallets);
            this.gbxPackingDetails.Controls.Add(this.lblNoOfLabels);
            this.gbxPackingDetails.Controls.Add(this.txtNoOfCartons);
            this.gbxPackingDetails.Controls.Add(this.txtTotalWeight);
            this.gbxPackingDetails.Controls.Add(this.lblTotalWeight);
            this.gbxPackingDetails.Controls.Add(this.txtEstimatedCost);
            this.gbxPackingDetails.Controls.Add(this.lblEstimatedCost);
            this.gbxPackingDetails.Controls.Add(this.dgrPackingDetails);
            this.gbxPackingDetails.Location = new System.Drawing.Point(6, 6);
            this.gbxPackingDetails.Name = "gbxPackingDetails";
            this.gbxPackingDetails.Size = new System.Drawing.Size(710, 215);
            this.gbxPackingDetails.TabIndex = 0;
            this.gbxPackingDetails.TabStop = false;
            this.gbxPackingDetails.Text = "Packing Details";
            // 
            // txtTotalCartons
            // 
            this.txtTotalCartons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalCartons.Location = new System.Drawing.Point(655, 119);
            this.txtTotalCartons.Name = "txtTotalCartons";
            this.txtTotalCartons.ReadOnly = true;
            this.txtTotalCartons.Size = new System.Drawing.Size(49, 20);
            this.txtTotalCartons.TabIndex = 36;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(577, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 35;
            this.label3.Text = "Total Cartons:";
            // 
            // lblTotalDimensions
            // 
            this.lblTotalDimensions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalDimensions.AutoSize = true;
            this.lblTotalDimensions.Location = new System.Drawing.Point(548, 50);
            this.lblTotalDimensions.Name = "lblTotalDimensions";
            this.lblTotalDimensions.Size = new System.Drawing.Size(101, 13);
            this.lblTotalDimensions.TabIndex = 29;
            this.lblTotalDimensions.Text = "Total Volume (cbm):";
            // 
            // txtTotalDimensions
            // 
            this.txtTotalDimensions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalDimensions.Location = new System.Drawing.Point(655, 47);
            this.txtTotalDimensions.Name = "txtTotalDimensions";
            this.txtTotalDimensions.ReadOnly = true;
            this.txtTotalDimensions.Size = new System.Drawing.Size(49, 20);
            this.txtTotalDimensions.TabIndex = 25;
            this.txtTotalDimensions.Text = "0.0000";
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(577, 96);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 13);
            this.label13.TabIndex = 30;
            this.label13.Text = "No. of Pallets:";
            // 
            // txtNoOfPallets
            // 
            this.txtNoOfPallets.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNoOfPallets.Location = new System.Drawing.Point(655, 93);
            this.txtNoOfPallets.Name = "txtNoOfPallets";
            this.txtNoOfPallets.ReadOnly = true;
            this.txtNoOfPallets.Size = new System.Drawing.Size(49, 20);
            this.txtNoOfPallets.TabIndex = 26;
            this.txtNoOfPallets.Text = "0";
            // 
            // lblNoOfLabels
            // 
            this.lblNoOfLabels.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNoOfLabels.AutoSize = true;
            this.lblNoOfLabels.Location = new System.Drawing.Point(572, 74);
            this.lblNoOfLabels.Name = "lblNoOfLabels";
            this.lblNoOfLabels.Size = new System.Drawing.Size(78, 13);
            this.lblNoOfLabels.TabIndex = 31;
            this.lblNoOfLabels.Text = "No. of Cartons:";
            // 
            // txtNoOfCartons
            // 
            this.txtNoOfCartons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNoOfCartons.Location = new System.Drawing.Point(655, 71);
            this.txtNoOfCartons.Name = "txtNoOfCartons";
            this.txtNoOfCartons.ReadOnly = true;
            this.txtNoOfCartons.Size = new System.Drawing.Size(49, 20);
            this.txtNoOfCartons.TabIndex = 27;
            this.txtNoOfCartons.Text = "0";
            // 
            // txtTotalWeight
            // 
            this.txtTotalWeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalWeight.Location = new System.Drawing.Point(655, 21);
            this.txtTotalWeight.Name = "txtTotalWeight";
            this.txtTotalWeight.ReadOnly = true;
            this.txtTotalWeight.Size = new System.Drawing.Size(49, 20);
            this.txtTotalWeight.TabIndex = 28;
            this.txtTotalWeight.Text = "0.0000";
            // 
            // lblTotalWeight
            // 
            this.lblTotalWeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalWeight.AutoSize = true;
            this.lblTotalWeight.Location = new System.Drawing.Point(558, 24);
            this.lblTotalWeight.Name = "lblTotalWeight";
            this.lblTotalWeight.Size = new System.Drawing.Size(92, 13);
            this.lblTotalWeight.TabIndex = 32;
            this.lblTotalWeight.Text = "Total Weight (kg):";
            // 
            // txtEstimatedCost
            // 
            this.txtEstimatedCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEstimatedCost.Location = new System.Drawing.Point(655, 145);
            this.txtEstimatedCost.Name = "txtEstimatedCost";
            this.txtEstimatedCost.Size = new System.Drawing.Size(49, 20);
            this.txtEstimatedCost.TabIndex = 34;
            this.txtEstimatedCost.Text = "0.00";
            // 
            // lblEstimatedCost
            // 
            this.lblEstimatedCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEstimatedCost.AutoSize = true;
            this.lblEstimatedCost.Location = new System.Drawing.Point(570, 148);
            this.lblEstimatedCost.Name = "lblEstimatedCost";
            this.lblEstimatedCost.Size = new System.Drawing.Size(80, 13);
            this.lblEstimatedCost.TabIndex = 33;
            this.lblEstimatedCost.Text = "Estimated Cost:";
            // 
            // dgrPackingDetails
            // 
            this.dgrPackingDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgrPackingDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrPackingDetails.Location = new System.Drawing.Point(3, 16);
            this.dgrPackingDetails.Name = "dgrPackingDetails";
            this.dgrPackingDetails.Size = new System.Drawing.Size(538, 193);
            this.dgrPackingDetails.TabIndex = 0;
            // 
            // tabOtherDetails
            // 
            this.tabOtherDetails.Controls.Add(this.txtManifestNo);
            this.tabOtherDetails.Controls.Add(this.txtFreefield3);
            this.tabOtherDetails.Controls.Add(this.label10);
            this.tabOtherDetails.Controls.Add(this.label7);
            this.tabOtherDetails.Controls.Add(this.txtFreefield2);
            this.tabOtherDetails.Controls.Add(this.label6);
            this.tabOtherDetails.Controls.Add(this.txtLabelPrintedDate);
            this.tabOtherDetails.Controls.Add(this.txtFreefield1);
            this.tabOtherDetails.Controls.Add(this.label11);
            this.tabOtherDetails.Controls.Add(this.label5);
            this.tabOtherDetails.Controls.Add(this.txtFreightCostWithGST);
            this.tabOtherDetails.Controls.Add(this.label9);
            this.tabOtherDetails.Controls.Add(this.txtFreightGST);
            this.tabOtherDetails.Controls.Add(this.label8);
            this.tabOtherDetails.Controls.Add(this.txtFreightCost);
            this.tabOtherDetails.Controls.Add(this.label4);
            this.tabOtherDetails.Location = new System.Drawing.Point(4, 22);
            this.tabOtherDetails.Name = "tabOtherDetails";
            this.tabOtherDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tabOtherDetails.Size = new System.Drawing.Size(719, 227);
            this.tabOtherDetails.TabIndex = 1;
            this.tabOtherDetails.Text = "Other Details";
            this.tabOtherDetails.UseVisualStyleBackColor = true;
            // 
            // txtManifestNo
            // 
            this.txtManifestNo.Location = new System.Drawing.Point(81, 163);
            this.txtManifestNo.Name = "txtManifestNo";
            this.txtManifestNo.Size = new System.Drawing.Size(172, 20);
            this.txtManifestNo.TabIndex = 1;
            // 
            // txtFreefield3
            // 
            this.txtFreefield3.Location = new System.Drawing.Point(67, 111);
            this.txtFreefield3.Name = "txtFreefield3";
            this.txtFreefield3.Size = new System.Drawing.Size(186, 20);
            this.txtFreefield3.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 166);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Manifest No:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 114);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "freefield3:";
            // 
            // txtFreefield2
            // 
            this.txtFreefield2.Location = new System.Drawing.Point(67, 85);
            this.txtFreefield2.Name = "txtFreefield2";
            this.txtFreefield2.Size = new System.Drawing.Size(186, 20);
            this.txtFreefield2.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 88);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "freefield2:";
            // 
            // txtLabelPrintedDate
            // 
            this.txtLabelPrintedDate.Location = new System.Drawing.Point(376, 163);
            this.txtLabelPrintedDate.Name = "txtLabelPrintedDate";
            this.txtLabelPrintedDate.Size = new System.Drawing.Size(186, 20);
            this.txtLabelPrintedDate.TabIndex = 1;
            // 
            // txtFreefield1
            // 
            this.txtFreefield1.Location = new System.Drawing.Point(67, 59);
            this.txtFreefield1.Name = "txtFreefield1";
            this.txtFreefield1.Size = new System.Drawing.Size(186, 20);
            this.txtFreefield1.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(272, 166);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Label Printed Date:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "freefield1:";
            // 
            // txtFreightCostWithGST
            // 
            this.txtFreightCostWithGST.Location = new System.Drawing.Point(582, 25);
            this.txtFreightCostWithGST.Name = "txtFreightCostWithGST";
            this.txtFreightCostWithGST.Size = new System.Drawing.Size(100, 20);
            this.txtFreightCostWithGST.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(463, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Est Freight (incl. GST):";
            // 
            // txtFreightGST
            // 
            this.txtFreightGST.Location = new System.Drawing.Point(347, 25);
            this.txtFreightGST.Name = "txtFreightGST";
            this.txtFreightGST.Size = new System.Drawing.Size(100, 20);
            this.txtFreightGST.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(274, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Freight GST:";
            // 
            // txtFreightCost
            // 
            this.txtFreightCost.Location = new System.Drawing.Point(153, 25);
            this.txtFreightCost.Name = "txtFreightCost";
            this.txtFreightCost.Size = new System.Drawing.Size(100, 20);
            this.txtFreightCost.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Est Freight Cost (excl. GST):";
            // 
            // txtViewManifest
            // 
            this.txtViewManifest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtViewManifest.Location = new System.Drawing.Point(1043, 676);
            this.txtViewManifest.Name = "txtViewManifest";
            this.txtViewManifest.Size = new System.Drawing.Size(87, 37);
            this.txtViewManifest.TabIndex = 2;
            this.txtViewManifest.Text = "View Manifest";
            this.txtViewManifest.UseVisualStyleBackColor = true;
            this.txtViewManifest.Click += new System.EventHandler(this.txtViewManifest_Click);
            // 
            // btnSendInvoices
            // 
            this.btnSendInvoices.Location = new System.Drawing.Point(599, 676);
            this.btnSendInvoices.Name = "btnSendInvoices";
            this.btnSendInvoices.Size = new System.Drawing.Size(92, 36);
            this.btnSendInvoices.TabIndex = 3;
            this.btnSendInvoices.Text = "Send Invoices";
            this.btnSendInvoices.UseVisualStyleBackColor = true;
            this.btnSendInvoices.Visible = false;
            this.btnSendInvoices.Click += new System.EventHandler(this.btnSendInvoices_Click);
            // 
            // btnDeleteConnote
            // 
            this.btnDeleteConnote.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteConnote.Location = new System.Drawing.Point(952, 676);
            this.btnDeleteConnote.Name = "btnDeleteConnote";
            this.btnDeleteConnote.Size = new System.Drawing.Size(75, 37);
            this.btnDeleteConnote.TabIndex = 4;
            this.btnDeleteConnote.Text = "Delete Connote";
            this.btnDeleteConnote.UseVisualStyleBackColor = true;
            this.btnDeleteConnote.Click += new System.EventHandler(this.btnDeleteConnote_Click);
            // 
            // btnAuthorise
            // 
            this.btnAuthorise.Location = new System.Drawing.Point(871, 676);
            this.btnAuthorise.Name = "btnAuthorise";
            this.btnAuthorise.Size = new System.Drawing.Size(75, 37);
            this.btnAuthorise.TabIndex = 5;
            this.btnAuthorise.Text = "Authorise";
            this.btnAuthorise.UseVisualStyleBackColor = true;
            this.btnAuthorise.Click += new System.EventHandler(this.btnAuthorise_Click);
            // 
            // OrderInvoicer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1136, 725);
            this.Controls.Add(this.btnAuthorise);
            this.Controls.Add(this.btnDeleteConnote);
            this.Controls.Add(this.btnSendInvoices);
            this.Controls.Add(this.txtViewManifest);
            this.Controls.Add(this.OrderViewContainer);
            this.Name = "OrderInvoicer";
            this.Text = "Order Invoicer";
            this.OrderViewContainer.Panel1.ResumeLayout(false);
            this.OrderViewContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OrderViewContainer)).EndInit();
            this.OrderViewContainer.ResumeLayout(false);
            this.spltOrderSelect.Panel1.ResumeLayout(false);
            this.spltOrderSelect.Panel1.PerformLayout();
            this.spltOrderSelect.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltOrderSelect)).EndInit();
            this.spltOrderSelect.ResumeLayout(false);
            this.gbxOrdersToInvoice.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgrOrdersToInvoice)).EndInit();
            this.OrderDetailsContainer.Panel1.ResumeLayout(false);
            this.OrderDetailsContainer.Panel1.PerformLayout();
            this.OrderDetailsContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OrderDetailsContainer)).EndInit();
            this.OrderDetailsContainer.ResumeLayout(false);
            this.gbxItemDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgrItemDetails)).EndInit();
            this.tabDetails.ResumeLayout(false);
            this.tabPackingDetails.ResumeLayout(false);
            this.gbxPackingDetails.ResumeLayout(false);
            this.gbxPackingDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrPackingDetails)).EndInit();
            this.tabOtherDetails.ResumeLayout(false);
            this.tabOtherDetails.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer OrderViewContainer;
        private System.Windows.Forms.GroupBox gbxOrdersToInvoice;
        private System.Windows.Forms.DataGridView dgrOrdersToInvoice;
        private System.Windows.Forms.SplitContainer OrderDetailsContainer;
        private System.Windows.Forms.GroupBox gbxPackingDetails;
        private System.Windows.Forms.DataGridView dgrPackingDetails;
        private System.Windows.Forms.GroupBox gbxItemDetails;
        private System.Windows.Forms.DataGridView dgrItemDetails;
        private System.Windows.Forms.TextBox txtCustomerName;
        private System.Windows.Forms.TextBox txtExactNo;
        private System.Windows.Forms.TextBox txtOrderStatus;
        private System.Windows.Forms.TextBox txtCustomerNo;
        private System.Windows.Forms.Label lblCustomerName;
        private System.Windows.Forms.Label lblExactNo;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblCusCode;
        private System.Windows.Forms.TextBox txtCourier;
        private System.Windows.Forms.Label lblCourier;
        private System.Windows.Forms.TextBox txtOrderDate;
        private System.Windows.Forms.Label lblOrderDate;
        private System.Windows.Forms.TextBox txtShippingDate;
        private System.Windows.Forms.Label lblETADate;
        private System.Windows.Forms.TextBox txtWarehouse;
        private System.Windows.Forms.Label lblWarehouse;
        private System.Windows.Forms.TextBox txtPackedBy;
        private System.Windows.Forms.Label lblPackedBy;
        private System.Windows.Forms.SplitContainer spltOrderSelect;
        private System.Windows.Forms.TextBox txtSearchOrderNo;
        private System.Windows.Forms.Label lblOrderNo;
        private System.Windows.Forms.TextBox txtSearchByDespatchID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSearchByDespatch;
        private System.Windows.Forms.Button btnDangerousGoods;
        private System.Windows.Forms.TextBox txtDeliveryPostcode;
        private System.Windows.Forms.TextBox txtDeliveryState;
        private System.Windows.Forms.TextBox txtDeliverySuburb;
        private System.Windows.Forms.TextBox txtCountry;
        private System.Windows.Forms.TextBox txtDeliverToAddress3;
        private System.Windows.Forms.TextBox txtDeliverToAddress2;
        private System.Windows.Forms.TextBox txtDeliverToAddress1;
        private System.Windows.Forms.Label lblCountry;
        private System.Windows.Forms.Label lblDeliveryAddress;
        private System.Windows.Forms.CheckBox chkInvoiceWithGoods;
        private System.Windows.Forms.TextBox txtSpecialInstructions2;
        private System.Windows.Forms.TextBox txtSpecialInstructions;
        private System.Windows.Forms.TextBox txtConnoteNo;
        private System.Windows.Forms.Label lblConnote;
        private System.Windows.Forms.TextBox txtDeliveryName;
        private System.Windows.Forms.Label lblSpecialInstructions;
        private System.Windows.Forms.Label lblDeliveryName;
        private System.Windows.Forms.TextBox txtDespatchID;
        private System.Windows.Forms.Label lblDespatchID;
        private System.Windows.Forms.TextBox txtInvoiceNo;
        private System.Windows.Forms.Label lblInvoiceNo;
        private System.Windows.Forms.TextBox txtDespatchStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTotalCartons;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblTotalDimensions;
        private System.Windows.Forms.TextBox txtTotalDimensions;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtNoOfPallets;
        private System.Windows.Forms.Label lblNoOfLabels;
        private System.Windows.Forms.TextBox txtNoOfCartons;
        private System.Windows.Forms.TextBox txtTotalWeight;
        private System.Windows.Forms.Label lblTotalWeight;
        private System.Windows.Forms.TextBox txtEstimatedCost;
        private System.Windows.Forms.Label lblEstimatedCost;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.TabControl tabDetails;
        private System.Windows.Forms.TabPage tabPackingDetails;
        private System.Windows.Forms.TabPage tabOtherDetails;
        private System.Windows.Forms.TextBox txtManifestNo;
        private System.Windows.Forms.TextBox txtFreefield3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFreefield2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtFreefield1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtFreightCostWithGST;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtFreightGST;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtFreightCost;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtLabelPrintedDate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button txtViewManifest;
        private System.Windows.Forms.Button btnSendInvoices;
        private System.Windows.Forms.Button btnDeleteConnote;
        private System.Windows.Forms.TextBox txtSearchConnoteNo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnAuthorise;
    }
}

