﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OrderDespatcher;

namespace OrderInvoicer
{
    public partial class frmDespatchRetriever : Form
    {
        public string despatchID = "-1";

        public frmDespatchRetriever()
        {
            InitializeComponent();
        }

        public frmDespatchRetriever(string orderNo)
        {
            InitializeComponent();

            dgrDespatchList.DataSource = OrderDespatchService.GetAllDespatches(orderNo.Trim());

            if (dgrDespatchList != null && dgrDespatchList.Rows.Count == 1)
            {
                this.despatchID = dgrDespatchList.Rows[0].Cells["despatchID"].Value.ToString();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (dgrDespatchList.SelectedRows.Count == 1)
                this.despatchID = dgrDespatchList.SelectedRows[0].Cells["despatchID"].Value.ToString();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
