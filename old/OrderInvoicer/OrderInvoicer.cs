﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using Livingstone.Library;
using OrderDespatcher;

namespace OrderInvoicer
{
    public partial class OrderInvoicer : Form
    {

        public string despatchID { get; set; }

        public OrderInvoicer()
        {
            InitializeComponent();

            initialiseDB("DANANT");

            GetDespatchesWithNoInvoices();

            txtSearchOrderNo.Text = "";
            txtSearchByDespatchID.Text = "";


        }

        public OrderInvoicer(string loginName)
        {
            InitializeComponent();

            initialiseDB(loginName);

            GetDespatchesWithNoInvoices();

        }

        private void initialiseDB(string userName)
        {
            Database.DBaseUse = GetDBaseType(ClsGeneralFunction.PrepareProperText("Australia").Trim());
            DataSource.initDataBase((int)Database.DBaseUse, userName);
            Database.User = userName;
            DataLayer.Data.initDataBase((int)Database.DBaseUse, userName);
            DataLayer.Data.sUser = Database.User;

            //Database.DBaseUse = GetDBaseType(ClsGeneralFunction.PrepareProperText(cboCompany.SelectedItem).Trim());
            //DataSource.initDataBase((int)Database.DBaseUse, txtUserName.Text.Trim());
            //Database.User = txtUserName.Text.Trim();
            //DataLayer.Data.initDataBase((int)Database.DBaseUse, txtUserName.Text.Trim());
            //DataLayer.Data.sUser = Database.User;
        }

        private Database.DBaseType GetDBaseType(string CompanyParam)
        {
            Database.DBaseType Result = Database.DBaseType.dbNetCRMAU;
            if (ClsGeneralFunction.PrepareProperText(CompanyParam).Trim() != string.Empty)
            {
                string SQL = ("SELECT * FROM tblUserCompany WHERE RTRIM(LTRIM(LOWER(company))) = '" + ClsGeneralFunction.PrepareProperText(CompanyParam).ToLower().Trim() + "'");
                string Company = "NetCRMAU";
                if (CompanyParam == "Hongkong") Company = "LivMacola";
                if (CompanyParam == "Philippines") Company = "ExactPH";
                if (CompanyParam == "TestAU") Company = "TestAU";
                if (ClsGeneralFunction.PrepareProperText(Company).Trim() != string.Empty)
                {
                    MemberInfo[] Members = typeof(Database.DBaseType).GetMembers();
                    foreach (MemberInfo Member in Members)
                    {
                        if (Member.DeclaringType == typeof(Database.DBaseType) && Member.Name.IndexOf("__") < 0)
                        {
                            string EnumString = ClsGeneralFunction.PrepareProperText(Member.Name).Trim();
                            EnumString = ClsGeneralFunction.PrepareProperText(EnumString.Substring(EnumString.IndexOf("db") + 2)).Trim();
                            if (ClsGeneralFunction.PrepareProperText(EnumString).ToLower().Trim() == ClsGeneralFunction.PrepareProperText(Company).ToLower().Trim())
                            {
                                if (ClsGeneralFunction.PrepareProperText(EnumString).ToLower().Trim() == "NetCRMAU".ToLower().Trim())
                                    Result = Database.DBaseType.dbNetCRMAU;
                                else if (ClsGeneralFunction.PrepareProperText(EnumString).ToLower().Trim() == "LivMacola".ToLower().Trim())
                                    Result = Database.DBaseType.dbLivMacola;
                                else if (ClsGeneralFunction.PrepareProperText(EnumString).ToLower().Trim() == "ExactPH".ToLower().Trim())
                                    Result = Database.DBaseType.dbExactPH;
                                else if (ClsGeneralFunction.PrepareProperText(EnumString).ToLower().Trim() == "TestAU".ToLower().Trim())
                                    Result = Database.DBaseType.dbTestAU;
                                break;
                            }
                        }
                    }
                }
            }
            return Result;
        }



        //Get List of Despatches that do not have invoices yet
        public void GetDespatchesWithNoInvoices()
        {
            DataTable dt = OrderInvoicerService.GetListOfDespatchesWithNoInvoices();

            dgrOrdersToInvoice.DataSource = dt;
        }


        private void LoadDespatch()
        {

            //Initialisers
            string customerNo = "";
            string exactNo = "";
            string customerName = "";
            string exactOrderNo = "";
            string courier = "";
            string orderDate = "";
            string shipToName = "";
            string shipToAddr1 = "";
            string shipToAddr2 = "";
            string shipToAddr3 = "";
            string shipToSuburb = "";
            string shipToState = "";
            string shipToPostcode = "";
            string shipToCountry = "";
            string shippingDate = "";
            string warehouse = "";
            string shipInstruction1 = "";
            string shipInstruction2 = "";
            string packedBy = "";

            string totalWeight = "";
            string totalDimensions = "";
            string totalCartons = "";
            string totalPallets = "";
            string totalCompleteCartons = "";
            string estimatedCost = "";
            string estimatedGST = "";
            string estimatedCostWithoutGST = "";
            string connoteNo = "";

            string freefield1 = "";
            string freefield2 = "";
            string freefield3 = "";

            string labelPrintedDate = "";

            string despatchStatus = "";

            string invoiceWithGoods = "";

            DataTable existingOrder = OrderDespatchService.LoadExistingCustomerOrderDetails(despatchID.Trim());

            if (existingOrder != null && existingOrder.Rows.Count > 0)
            {

                //Get existing order
                //RetrieveHeaderInfo();

                despatchID = existingOrder.Rows[0]["id"].ToString().Trim();

                customerNo = existingOrder.Rows[0]["cusNo"].ToString().Trim();
                exactNo = existingOrder.Rows[0]["exactNo"].ToString().Trim();
                customerName = existingOrder.Rows[0]["cusName"].ToString().Trim();
                //netCRMOrderNo = existingOrder.Rows[0]["ord_no"].ToString().Trim();
                exactOrderNo = existingOrder.Rows[0]["orderNo"].ToString().Trim();
                courier = existingOrder.Rows[0]["courier"].ToString().Trim();
                orderDate = existingOrder.Rows[0]["formattedOrderDate"].ToString().Trim();
                shipToName = existingOrder.Rows[0]["delName"].ToString().Trim();
                shipToAddr1 = existingOrder.Rows[0]["delAddr1"].ToString().Trim();
                shipToAddr2 = existingOrder.Rows[0]["delAddr2"].ToString().Trim();
                shipToAddr3 = existingOrder.Rows[0]["delAddr3"].ToString().Trim();
                shipToSuburb = existingOrder.Rows[0]["delSuburb"].ToString().Trim();
                shipToState = existingOrder.Rows[0]["delState"].ToString().Trim();
                shipToPostcode = existingOrder.Rows[0]["delPostcode"].ToString().Trim();
                shipToCountry = existingOrder.Rows[0]["country"].ToString().Trim();
                shippingDate = existingOrder.Rows[0]["shippingDate"].ToString().Trim();
                //orderStatus = existingOrder.Rows[0]["OrderStatus"].ToString().Trim();
                warehouse = existingOrder.Rows[0]["warehouse"].ToString().Trim();
                shipInstruction1 = existingOrder.Rows[0]["specialInstructions"].ToString().Trim();
                shipInstruction2 = existingOrder.Rows[0]["specialInstructions2"].ToString().Trim();
                packedBy = existingOrder.Rows[0]["packedBy"].ToString().Trim();

                totalWeight = existingOrder.Rows[0]["totalWeight"].ToString().Trim();
                totalDimensions = existingOrder.Rows[0]["totalDimensions"].ToString().Trim();
                totalCartons = existingOrder.Rows[0]["noOfCartons"].ToString().Trim();
                totalPallets = existingOrder.Rows[0]["noOfPallets"].ToString().Trim();
                totalCompleteCartons = existingOrder.Rows[0]["totalCartons"].ToString().Trim();
                estimatedCost = existingOrder.Rows[0]["estimatedFreightCost"].ToString().Trim();
                estimatedGST = existingOrder.Rows[0]["estimatedFreightGST"].ToString().Trim();
                estimatedCostWithoutGST = existingOrder.Rows[0]["estimatedFreightCostExclGST"].ToString().Trim();
                connoteNo = existingOrder.Rows[0]["connoteNo"].ToString().Trim();

                freefield1 = existingOrder.Rows[0]["freefield1"].ToString().Trim();
                freefield2 = existingOrder.Rows[0]["freefield2"].ToString().Trim();
                freefield3 = existingOrder.Rows[0]["freefield3"].ToString().Trim();

                labelPrintedDate = existingOrder.Rows[0]["labelPrintedDate"].ToString().Trim();

                despatchStatus = existingOrder.Rows[0]["despatchStatus"].ToString().Trim();


                invoiceWithGoods = existingOrder.Rows[0]["invoiceWithGoods"].ToString().Trim();

                txtDespatchID.Text = despatchID;

                txtCustomerNo.Text = customerNo;
                txtExactNo.Text = exactNo;
                txtCustomerName.Text = customerName;
                txtCourier.Text = courier;
                txtOrderDate.Text = orderDate;
                txtShippingDate.Text = shippingDate;
                txtWarehouse.Text = warehouse;
                txtSpecialInstructions.Text = shipInstruction1;
                txtSpecialInstructions2.Text = shipInstruction2;
                txtPackedBy.Text = packedBy;
                txtDespatchStatus.Text = despatchStatus;

                txtDeliveryName.Text = shipToName;
                txtDeliverToAddress1.Text = shipToAddr1;
                txtDeliverToAddress2.Text = shipToAddr2;
                txtDeliverToAddress3.Text = shipToAddr3;
                txtDeliverySuburb.Text = shipToSuburb;
                txtDeliveryState.Text = shipToState;
                txtDeliveryPostcode.Text = shipToPostcode;
                txtCountry.Text = shipToCountry;

                txtTotalWeight.Text = totalWeight;
                txtTotalDimensions.Text = totalDimensions;
                txtNoOfCartons.Text = totalCartons;
                txtNoOfPallets.Text = totalPallets;
                txtTotalCartons.Text = totalCompleteCartons;
                txtEstimatedCost.Text = estimatedCost;
                txtFreightCostWithGST.Text = estimatedCost;
                txtFreightGST.Text = estimatedGST;
                txtFreightCost.Text = estimatedCostWithoutGST;
                txtConnoteNo.Text = connoteNo;

                txtFreefield1.Text = freefield1;
                txtFreefield2.Text = freefield2;
                txtFreefield3.Text = freefield3;

                txtLabelPrintedDate.Text = labelPrintedDate;
             

                if (invoiceWithGoods == "True") chkInvoiceWithGoods.Checked = true;


                //Fill out packed item info
                DataTable packedItemDetails = OrderDespatchService.GetPackingDetails(despatchID);
                dgrPackingDetails.DataSource = packedItemDetails;

                //Fill out picked item list
                DataTable packedItems = OrderDespatchService.LoadExistingItemDetails(despatchID);
                dgrItemDetails.DataSource = packedItems;

            //    HighlightDangerousGoods();

                txtInvoiceNo.Text = (string)DataSource.GetValue("select isnull(invoiceNo,'') from tblWhOrderDespatch_Orders where despatchID = @despatchID", "NetCRMAU", new object[,]{{"@despatchID", despatchID}});

            }



        }




        private void LoadDespatch(string inputDespatchID)
        {

            //Initialisers
            string customerNo = "";
            string exactNo = "";
            string customerName = "";
            string exactOrderNo = "";
            string courier = "";
            string orderDate = "";
            string shipToName = "";
            string shipToAddr1 = "";
            string shipToAddr2 = "";
            string shipToAddr3 = "";
            string shipToSuburb = "";
            string shipToState = "";
            string shipToPostcode = "";
            string shipToCountry = "";
            string shippingDate = "";
            string warehouse = "";
            string shipInstruction1 = "";
            string shipInstruction2 = "";
            string packedBy = "";

            string totalWeight = "";
            string totalDimensions = "";
            string totalCartons = "";
            string totalPallets = "";
            string totalCompleteCartons = "";
            string estimatedCost = "";
            string estimatedGST = "";
            string estimatedCostWithoutGST = "";
            string connoteNo = "";

            string freefield1 = "";
            string freefield2 = "";
            string freefield3 = "";

            string labelPrintedDate = "";

            string despatchStatus = "";

            string invoiceWithGoods = "";

            DataTable existingOrder = OrderDespatchService.LoadExistingCustomerOrderDetails(inputDespatchID.Trim());

            if (existingOrder != null && existingOrder.Rows.Count > 0)
            {

                //Get existing order
                //RetrieveHeaderInfo();

                despatchID = existingOrder.Rows[0]["id"].ToString().Trim();

                customerNo = existingOrder.Rows[0]["cusNo"].ToString().Trim();
                exactNo = existingOrder.Rows[0]["exactNo"].ToString().Trim();
                customerName = existingOrder.Rows[0]["cusName"].ToString().Trim();
                //netCRMOrderNo = existingOrder.Rows[0]["ord_no"].ToString().Trim();
                exactOrderNo = existingOrder.Rows[0]["orderNo"].ToString().Trim();
                courier = existingOrder.Rows[0]["courier"].ToString().Trim();
                orderDate = existingOrder.Rows[0]["formattedOrderDate"].ToString().Trim();
                shipToName = existingOrder.Rows[0]["delName"].ToString().Trim();
                shipToAddr1 = existingOrder.Rows[0]["delAddr1"].ToString().Trim();
                shipToAddr2 = existingOrder.Rows[0]["delAddr2"].ToString().Trim();
                shipToAddr3 = existingOrder.Rows[0]["delAddr3"].ToString().Trim();
                shipToSuburb = existingOrder.Rows[0]["delSuburb"].ToString().Trim();
                shipToState = existingOrder.Rows[0]["delState"].ToString().Trim();
                shipToPostcode = existingOrder.Rows[0]["delPostcode"].ToString().Trim();
                shipToCountry = existingOrder.Rows[0]["country"].ToString().Trim();
                shippingDate = existingOrder.Rows[0]["shippingDate"].ToString().Trim();
                //orderStatus = existingOrder.Rows[0]["OrderStatus"].ToString().Trim();
                warehouse = existingOrder.Rows[0]["warehouse"].ToString().Trim();
                shipInstruction1 = existingOrder.Rows[0]["specialInstructions"].ToString().Trim();
                shipInstruction2 = existingOrder.Rows[0]["specialInstructions2"].ToString().Trim();
                packedBy = existingOrder.Rows[0]["packedBy"].ToString().Trim();

                totalWeight = existingOrder.Rows[0]["totalWeight"].ToString().Trim();
                totalDimensions = existingOrder.Rows[0]["totalDimensions"].ToString().Trim();
                totalCartons = existingOrder.Rows[0]["noOfCartons"].ToString().Trim();
                totalPallets = existingOrder.Rows[0]["noOfPallets"].ToString().Trim();
                totalCompleteCartons = existingOrder.Rows[0]["totalCartons"].ToString().Trim();
                estimatedCost = existingOrder.Rows[0]["estimatedFreightCost"].ToString().Trim();
                estimatedGST = existingOrder.Rows[0]["estimatedFreightGST"].ToString().Trim();
                estimatedCostWithoutGST = existingOrder.Rows[0]["estimatedFreightCostExclGST"].ToString().Trim();
                connoteNo = existingOrder.Rows[0]["connoteNo"].ToString().Trim();

                freefield1 = existingOrder.Rows[0]["freefield1"].ToString().Trim();
                freefield2 = existingOrder.Rows[0]["freefield2"].ToString().Trim();
                freefield3 = existingOrder.Rows[0]["freefield3"].ToString().Trim();

                labelPrintedDate = existingOrder.Rows[0]["labelPrintedDate"].ToString().Trim();

                despatchStatus = existingOrder.Rows[0]["despatchStatus"].ToString().Trim();


                invoiceWithGoods = existingOrder.Rows[0]["invoiceWithGoods"].ToString().Trim();

                txtDespatchID.Text = despatchID;

                txtCustomerNo.Text = customerNo;
                txtExactNo.Text = exactNo;
                txtCustomerName.Text = customerName;
                txtCourier.Text = courier;
                txtOrderDate.Text = orderDate;
                txtShippingDate.Text = shippingDate;
                txtWarehouse.Text = warehouse;
                txtSpecialInstructions.Text = shipInstruction1;
                txtSpecialInstructions2.Text = shipInstruction2;
                txtPackedBy.Text = packedBy;
                txtDespatchStatus.Text = despatchStatus;

                txtDeliveryName.Text = shipToName;
                txtDeliverToAddress1.Text = shipToAddr1;
                txtDeliverToAddress2.Text = shipToAddr2;
                txtDeliverToAddress3.Text = shipToAddr3;
                txtDeliverySuburb.Text = shipToSuburb;
                txtDeliveryState.Text = shipToState;
                txtDeliveryPostcode.Text = shipToPostcode;
                txtCountry.Text = shipToCountry;

                txtTotalWeight.Text = totalWeight;
                txtTotalDimensions.Text = totalDimensions;
                txtNoOfCartons.Text = totalCartons;
                txtNoOfPallets.Text = totalPallets;
                txtTotalCartons.Text = totalCompleteCartons;
                txtEstimatedCost.Text = estimatedCost;
                txtFreightCostWithGST.Text = estimatedCost;
                txtFreightGST.Text = estimatedGST;
                txtFreightCost.Text = estimatedCostWithoutGST;
                txtConnoteNo.Text = connoteNo;

                txtFreefield1.Text = freefield1;
                txtFreefield2.Text = freefield2;
                txtFreefield3.Text = freefield3;

                txtLabelPrintedDate.Text = labelPrintedDate;


                if (invoiceWithGoods == "True") chkInvoiceWithGoods.Checked = true;


                //Fill out packed item info
                DataTable packedItemDetails = OrderDespatchService.GetPackingDetails(despatchID);
                dgrPackingDetails.DataSource = packedItemDetails;

                //Fill out picked item list
                DataTable packedItems = OrderDespatchService.LoadExistingItemDetails(despatchID);
                dgrItemDetails.DataSource = packedItems;

                //    HighlightDangerousGoods();

            }



        }




        private void btnSearchByOrder_Click(object sender, EventArgs e)
        {
            RetrieveFromDespatchList();
        }


        private void ResetForm()
        {
            ClearForm(OrderDetailsContainer);

            despatchID = "-1"; /*Reset despatch ID*/
            txtSearchByDespatchID.Text = "";
        }


        private void ClearForm(Control control)
        {
            foreach (Control c in control.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Clear();
                }

                if (c is ComboBox)
                {
                    ((ComboBox)c).Text = "";
                }

                if (c.HasChildren)
                {
                    ClearForm(c);
                }


                if (c is CheckBox)
                {

                    ((CheckBox)c).Checked = false;
                }

                if (c is RadioButton)
                {
                    ((RadioButton)c).Checked = false;
                }

                if (c is DataGridView)
                {
                    ((DataGridView)c).DataSource = null;
                }
            }
        }

        private void btnSearchByDespatch_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtSearchOrderNo.Text))
            {

                RetrieveFromDespatchList();
            }
            else if (!string.IsNullOrEmpty(txtSearchByDespatchID.Text))
            {
                despatchID = txtSearchByDespatchID.Text.Trim();

                foreach (DataGridViewRow dr in dgrOrdersToInvoice.Rows)
                {
                    if (dr.Cells["DespatchID"].Value.ToString().Trim() == despatchID.Trim())
                    {
                        dr.Selected = true;
                        break;
                    }
                }

                    LoadDespatch();
            }
            else if (!string.IsNullOrEmpty(txtSearchConnoteNo.Text))
            {
                int noOfResults;
                DataTable dt = OrderInvoicerService.searchByConnote(txtSearchConnoteNo.Text, out noOfResults);

                if (noOfResults == 1)
                {
                    despatchID = dt.Rows[0]["despatchID"].ToString().Trim();
                    txtSearchByDespatchID.Text = despatchID;

                    foreach (DataGridViewRow dr in dgrOrdersToInvoice.Rows)
                    {
                        if (dr.Cells["DespatchID"].Value.ToString().Trim() == despatchID.Trim())
                        {
                            dr.Selected = true;
                            break;
                        }
                    }

                    LoadDespatch();
                }
                else if (noOfResults > 1)
                {
                    frmConnoteSelector connoteSelector = new frmConnoteSelector(txtSearchConnoteNo.Text, dt);
                    connoteSelector.ShowDialog();

                    if (connoteSelector.DialogResult == DialogResult.OK)
                    {
                        despatchID = connoteSelector.selectedDespatchID;
                        txtSearchByDespatchID.Text = despatchID;

                        foreach (DataGridViewRow dr in dgrOrdersToInvoice.Rows)
                        {
                            if (dr.Cells["DespatchID"].Value.ToString().Trim() == despatchID.Trim())
                            {
                                dr.Selected = true;
                                break;
                            }
                        }

                        LoadDespatch();
                    }
                }
                else
                {
                    MessageBox.Show("Cannot recognise this connote no.");
                }

            }

            //txtSearchByDespatchID.Text = "";
            //txtSearchOrderNo.Text = "";
            //txtSearchConnoteNo.Text = "";
        }

        private void RetrieveFromDespatchList()
        {
            if (string.IsNullOrEmpty(txtSearchOrderNo.Text))
            {
                MessageBox.Show("Must enter/scan in order no.");
                return;
            }

            if (txtSearchOrderNo.Text.ToUpper().StartsWith("DD"))
            {
                txtSearchOrderNo.Text = txtSearchOrderNo.Text.Substring(2);
            }

            frmDespatchRetriever despatchList = new frmDespatchRetriever(txtSearchOrderNo.Text);

            if (despatchList.despatchID != "-1") 
            {
                ResetForm();

                despatchID = despatchList.despatchID;
                txtSearchByDespatchID.Text = despatchID;

                foreach (DataGridViewRow dr in dgrOrdersToInvoice.Rows)
                {
                    if (dr.Cells["despatchID"].Value.ToString().Trim() == despatchID.Trim())
                    {
                        dr.Selected = true;
                        break;
                    }
                }

                LoadDespatch();

            }        
            else
            {
                despatchList.ShowDialog();

                if (despatchList.DialogResult == DialogResult.OK && despatchList.despatchID != "-1")
                {
                    ResetForm();

                    despatchID = despatchList.despatchID;
                    txtSearchByDespatchID.Text = despatchID;

                    foreach (DataGridViewRow dr in dgrOrdersToInvoice.Rows)
                    {
                        if (dr.Cells["despatchID"].Value.ToString().Trim() == despatchID.Trim())
                        {
                            dr.Selected = true;
                            break;
                        }
                    }

                    LoadDespatch();
                }
            }

        }

        private void dgrOrdersToInvoice_SelectionChanged(object sender, EventArgs e)
        {
            if (dgrOrdersToInvoice != null && dgrOrdersToInvoice.Rows.Count > 0 && dgrOrdersToInvoice.SelectedRows.Count > 0)
            {
                despatchID = dgrOrdersToInvoice.SelectedRows[0].Cells["DespatchID"].Value.ToString().Trim();
                txtSearchByDespatchID.Text = despatchID;
                txtSearchOrderNo.Text = "";

                LoadDespatch();
            }
        }

        private void btnSendInvoice_Click(object sender, EventArgs e)
        {
            string invoiceNo = txtInvoiceNo.Text.Trim();
            string despatchID = txtDespatchID.Text.Trim();

        }

        private void btnDangerousGoods_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"\\crmdoc\netcrm\CompiledReports\Dangerous Goods Declaration (For Packers).exe");
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            GetDespatchesWithNoInvoices();
        }

        private void txtViewManifest_Click(object sender, EventArgs e)
        {
            frmManifestViewer manifestViewer = new frmManifestViewer();
            manifestViewer.Show();
        }

        private void btnSendInvoices_Click(object sender, EventArgs e)
        {
            frmInvoiceSender invoiceSender = new frmInvoiceSender();
            invoiceSender.Show();
        }

        private void btnDeleteConnote_Click(object sender, EventArgs e)
        {
            OrderInvoicerService.deleteConnote(txtDespatchID.Text, txtConnoteNo.Text, txtCourier.Text);
            LoadDespatch(txtDespatchID.Text);
            
        }

        private void txtSearchConnoteNo_TextChanged(object sender, EventArgs e)
        {
            txtSearchByDespatchID.Text = "";
            txtSearchOrderNo.Text = "";
        }

        private void txtSearchByDespatchID_TextChanged(object sender, EventArgs e)
        {
            txtSearchOrderNo.Text = "";
            txtSearchConnoteNo.Text = "";
        }

        private void txtSearchOrderNo_TextChanged(object sender, EventArgs e)
        {
            txtSearchByDespatchID.Text = "";
            txtSearchConnoteNo.Text = "";
        }

        private void dgrOrdersToInvoice_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var grid = sender as DataGridView;
            var rowIdx = (e.RowIndex + 1).ToString();

            var centerFormat = new StringFormat()
            {
                // right alignment might actually make more sense for numbers
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            var headerBounds = new Rectangle(e.RowBounds.Left, e.RowBounds.Top, grid.RowHeadersWidth, e.RowBounds.Height);
            e.Graphics.DrawString(rowIdx, this.Font, SystemBrushes.ControlText, headerBounds, centerFormat);
        }

        private void dgrItemDetails_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var grid = sender as DataGridView;
            var rowIdx = (e.RowIndex + 1).ToString();

            var centerFormat = new StringFormat()
            {
                // right alignment might actually make more sense for numbers
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            var headerBounds = new Rectangle(e.RowBounds.Left, e.RowBounds.Top, grid.RowHeadersWidth, e.RowBounds.Height);
            e.Graphics.DrawString(rowIdx, this.Font, SystemBrushes.ControlText, headerBounds, centerFormat);
        }

        private void btnAuthorise_Click(object sender, EventArgs e)
        {
            double connoteWeight = double.Parse(txtTotalWeight.Text);
            double systemWeight = OrderDespatchService.GetSystemTotalWeight(despatchID.Trim());

            double variant = (connoteWeight / systemWeight) * 100;

            if (connoteWeight > 1 && (variant <= 80 || variant >= 120))
            {
                if (!OrderDespatchService.CheckDespatchAuthorised(despatchID))
                {
                    frmWeightCheck weightCheck = new frmWeightCheck(despatchID);
                    weightCheck.ShowDialog();

                    if (weightCheck.DialogResult == DialogResult.OK)
                    {
                        MessageBox.Show("Thank you. Despatch has now been authorised.");
                    }
                }
                else
                {
                    MessageBox.Show("Despatch is already authorised.");
                }
            }
            else
            {
                MessageBox.Show("Authorisation not required.");
            }
        }


    }
}
