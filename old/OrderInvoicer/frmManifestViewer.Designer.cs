﻿namespace OrderInvoicer
{
    partial class frmManifestViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbpSearch = new System.Windows.Forms.GroupBox();
            this.dgrManifestViewer = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.cboCourier = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.txtManifestNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnExportToExcel = new System.Windows.Forms.Button();
            this.btnRetrieve = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.gbpSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrManifestViewer)).BeginInit();
            this.SuspendLayout();
            // 
            // gbpSearch
            // 
            this.gbpSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbpSearch.Controls.Add(this.btnRetrieve);
            this.gbpSearch.Controls.Add(this.label4);
            this.gbpSearch.Controls.Add(this.txtManifestNo);
            this.gbpSearch.Controls.Add(this.dtpTo);
            this.gbpSearch.Controls.Add(this.dtpFrom);
            this.gbpSearch.Controls.Add(this.label3);
            this.gbpSearch.Controls.Add(this.label2);
            this.gbpSearch.Controls.Add(this.cboCompany);
            this.gbpSearch.Controls.Add(this.label5);
            this.gbpSearch.Controls.Add(this.cboCourier);
            this.gbpSearch.Controls.Add(this.label1);
            this.gbpSearch.Location = new System.Drawing.Point(1, 2);
            this.gbpSearch.Name = "gbpSearch";
            this.gbpSearch.Size = new System.Drawing.Size(942, 111);
            this.gbpSearch.TabIndex = 0;
            this.gbpSearch.TabStop = false;
            this.gbpSearch.Text = "Search";
            // 
            // dgrManifestViewer
            // 
            this.dgrManifestViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgrManifestViewer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrManifestViewer.Location = new System.Drawing.Point(1, 119);
            this.dgrManifestViewer.Name = "dgrManifestViewer";
            this.dgrManifestViewer.Size = new System.Drawing.Size(942, 409);
            this.dgrManifestViewer.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Courier:";
            // 
            // cboCourier
            // 
            this.cboCourier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCourier.FormattingEnabled = true;
            this.cboCourier.Items.AddRange(new object[] {
            "STE"});
            this.cboCourier.Location = new System.Drawing.Point(85, 17);
            this.cboCourier.Name = "cboCourier";
            this.cboCourier.Size = new System.Drawing.Size(121, 21);
            this.cboCourier.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(212, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Date From:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(212, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Date To:";
            // 
            // dtpFrom
            // 
            this.dtpFrom.Location = new System.Drawing.Point(276, 21);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(200, 20);
            this.dtpFrom.TabIndex = 4;
            // 
            // dtpTo
            // 
            this.dtpTo.Location = new System.Drawing.Point(276, 46);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(200, 20);
            this.dtpTo.TabIndex = 4;
            // 
            // txtManifestNo
            // 
            this.txtManifestNo.Location = new System.Drawing.Point(85, 50);
            this.txtManifestNo.Name = "txtManifestNo";
            this.txtManifestNo.Size = new System.Drawing.Size(121, 20);
            this.txtManifestNo.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Manifest No:";
            // 
            // btnExportToExcel
            // 
            this.btnExportToExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportToExcel.Location = new System.Drawing.Point(843, 534);
            this.btnExportToExcel.Name = "btnExportToExcel";
            this.btnExportToExcel.Size = new System.Drawing.Size(100, 30);
            this.btnExportToExcel.TabIndex = 2;
            this.btnExportToExcel.Text = "Export To Excel";
            this.btnExportToExcel.UseVisualStyleBackColor = true;
            this.btnExportToExcel.Click += new System.EventHandler(this.btnExportToExcel_Click);
            // 
            // btnRetrieve
            // 
            this.btnRetrieve.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRetrieve.Location = new System.Drawing.Point(841, 72);
            this.btnRetrieve.Name = "btnRetrieve";
            this.btnRetrieve.Size = new System.Drawing.Size(95, 33);
            this.btnRetrieve.TabIndex = 7;
            this.btnRetrieve.Text = "Retrieve";
            this.btnRetrieve.UseVisualStyleBackColor = true;
            this.btnRetrieve.Click += new System.EventHandler(this.btnRetrieve_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(492, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Company:";
            // 
            // cboCompany
            // 
            this.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.Items.AddRange(new object[] {
            "LIVINGSTONE",
            "UNIVERSAL"});
            this.cboCompany.Location = new System.Drawing.Point(552, 20);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(99, 21);
            this.cboCompany.TabIndex = 1;
            // 
            // frmManifestViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(943, 597);
            this.Controls.Add(this.btnExportToExcel);
            this.Controls.Add(this.dgrManifestViewer);
            this.Controls.Add(this.gbpSearch);
            this.Name = "frmManifestViewer";
            this.Text = "Manifest Viewer";
            this.gbpSearch.ResumeLayout(false);
            this.gbpSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrManifestViewer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbpSearch;
        private System.Windows.Forms.DataGridView dgrManifestViewer;
        private System.Windows.Forms.ComboBox cboCourier;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRetrieve;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtManifestNo;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnExportToExcel;
        private System.Windows.Forms.ComboBox cboCompany;
        private System.Windows.Forms.Label label5;
    }
}