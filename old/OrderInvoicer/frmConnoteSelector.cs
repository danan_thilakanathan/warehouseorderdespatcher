﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OrderInvoicer
{
    public partial class frmConnoteSelector : Form
    {

        public string selectedDespatchID { get; set; }

        public frmConnoteSelector()
        {
            InitializeComponent();
        }

        public frmConnoteSelector(string inputConnoteNo, DataTable connoteSearch)
        {
            InitializeComponent();
            txtInputConnote.Text = inputConnoteNo;
            dgrConnoteSelector.DataSource = connoteSearch;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.selectedDespatchID = dgrConnoteSelector.CurrentRow.Cells["despatchID"].Value.ToString().Trim();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }


    }
}
