﻿namespace OrderDespatcher
{
    partial class frmStart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxParameters = new System.Windows.Forms.GroupBox();
            this.btnLoadPrevious = new System.Windows.Forms.Button();
            this.btnRetrieveInfo = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.cboCourier = new System.Windows.Forms.ComboBox();
            this.lblCourier = new System.Windows.Forms.Label();
            this.gbxParameters.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxParameters
            // 
            this.gbxParameters.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxParameters.Controls.Add(this.btnLoadPrevious);
            this.gbxParameters.Controls.Add(this.btnRetrieveInfo);
            this.gbxParameters.Controls.Add(this.label1);
            this.gbxParameters.Controls.Add(this.txtOrderNo);
            this.gbxParameters.Controls.Add(this.cboCourier);
            this.gbxParameters.Controls.Add(this.lblCourier);
            this.gbxParameters.Location = new System.Drawing.Point(12, 12);
            this.gbxParameters.Name = "gbxParameters";
            this.gbxParameters.Size = new System.Drawing.Size(353, 204);
            this.gbxParameters.TabIndex = 5;
            this.gbxParameters.TabStop = false;
            this.gbxParameters.Text = "Enter Details";
            // 
            // btnLoadPrevious
            // 
            this.btnLoadPrevious.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadPrevious.Location = new System.Drawing.Point(261, 9);
            this.btnLoadPrevious.Name = "btnLoadPrevious";
            this.btnLoadPrevious.Size = new System.Drawing.Size(86, 22);
            this.btnLoadPrevious.TabIndex = 6;
            this.btnLoadPrevious.Text = "Load Previous";
            this.btnLoadPrevious.UseVisualStyleBackColor = true;
            // 
            // btnRetrieveInfo
            // 
            this.btnRetrieveInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRetrieveInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetrieveInfo.Location = new System.Drawing.Point(232, 151);
            this.btnRetrieveInfo.Name = "btnRetrieveInfo";
            this.btnRetrieveInfo.Size = new System.Drawing.Size(115, 47);
            this.btnRetrieveInfo.TabIndex = 4;
            this.btnRetrieveInfo.Text = "New Entry";
            this.btnRetrieveInfo.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Order No:";
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.Location = new System.Drawing.Point(92, 64);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(244, 20);
            this.txtOrderNo.TabIndex = 0;
            // 
            // cboCourier
            // 
            this.cboCourier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCourier.FormattingEnabled = true;
            this.cboCourier.Location = new System.Drawing.Point(92, 105);
            this.cboCourier.Name = "cboCourier";
            this.cboCourier.Size = new System.Drawing.Size(157, 21);
            this.cboCourier.TabIndex = 2;
            // 
            // lblCourier
            // 
            this.lblCourier.AutoSize = true;
            this.lblCourier.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCourier.Location = new System.Drawing.Point(12, 104);
            this.lblCourier.Name = "lblCourier";
            this.lblCourier.Size = new System.Drawing.Size(61, 18);
            this.lblCourier.TabIndex = 1;
            this.lblCourier.Text = "Courier:";
            // 
            // frmStart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 230);
            this.Controls.Add(this.gbxParameters);
            this.Name = "frmStart";
            this.Text = "Scan DD";
            this.gbxParameters.ResumeLayout(false);
            this.gbxParameters.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxParameters;
        private System.Windows.Forms.Button btnLoadPrevious;
        private System.Windows.Forms.Button btnRetrieveInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtOrderNo;
        private System.Windows.Forms.ComboBox cboCourier;
        private System.Windows.Forms.Label lblCourier;
    }
}