﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Threading;
//using System.Management
using System.Data;
using Livingstone.Library;
using OrderDespatcher.Utilities;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
//using PdfSharp.Pdf;
//using MigraDoc.Rendering;
using OrderDespatcher.Courier;
using System.Data.SqlClient;

namespace OrderDespatcher
{
    public class PrintService
    {
        public string labelPrinterName { get; set; }
        public string A4PrinterName  { get; set;}
        public string DangerousGoodsPrinterName { get; set; }


        public PrintService()
        {
            initialisePrinter();
        }


        //Setup the printers to use to print the labels, invoices and dangerous goods forms
        public void SetupPrinter(string labelPrinterName, string A4PrinterName, string dangerousGoodsPrinterName)
        {
            this.labelPrinterName = labelPrinterName;
            this.A4PrinterName = A4PrinterName;
            this.DangerousGoodsPrinterName = dangerousGoodsPrinterName;
        }


        private void initialisePrinter()
        {
            try
            {
                SqlConnection con = DataSource.getConn("NetCRMAU");

                string localComputerName = Environment.ExpandEnvironmentVariables("%CLIENTNAME%");

                DataTable printerTable = DataSource.GetTableFrom(@"
                    select A4Printer, labelPrinter, dangerousGoodsPrinter
                    from tblWhOrderDespatch_ComputerSettings
                    where computerName = @computerName
                ", "NetCRMAU", new object[,] { { "@computerName", localComputerName.Trim() } });

                if (printerTable != null && printerTable.Rows.Count == 1)
                {
                    labelPrinterName = printerTable.Rows[0]["labelPrinter"].ToString();
                    A4PrinterName = printerTable.Rows[0]["A4Printer"].ToString();
                    DangerousGoodsPrinterName = printerTable.Rows[0]["dangerousGoodsPrinter"].ToString();

                }
                else
                {
                    labelPrinterName = "Zebra";
                    A4PrinterName = "Kyocera-Up";
                    DangerousGoodsPrinterName = "Kyocera-Ground";
                }

                con.Close();
                con.Dispose();
            }
            catch
            {
            }

        }


        public void SavePrinterPreferences(string labelPrinterName, string A4PrinterName, string dangerousGoodsPrinterName)
        {
            string localComputerName = Environment.ExpandEnvironmentVariables("%CLIENTNAME%");

            object[,] xargs = {
                { "@labelPrinter", labelPrinterName.Trim() },
                { "@A4Printer", A4PrinterName.Trim() },
                { "@dangerousGoodsPrinter", dangerousGoodsPrinterName.Trim() },
                { "@computerName", localComputerName }
            };

            DataSource.ExecuteNonQuery(@"
                if (exists(select 1 from tblWhOrderDespatch_ComputerSettings where computerName = @computerName))
                    update tblWhOrderDespatch_ComputerSettings
                    set A4Printer = @A4Printer, labelPrinter = @labelPrinter, dangerousGoodsPrinter = @dangerousGoodsPrinter
                    where computerName = @computerName
                else
                    insert into tblWhOrderDespatch_ComputerSettings (computerName, A4Printer, labelPrinter, dangerousGoodsPrinter)
                    values (@computerName, @A4Printer, @labelPrinter, @dangerousGoodsPrinter)
            ", "NetCRMAU", xargs);
        }


        //Print file with in-built settings
        public void PrintFile(string filePath)
        {
            string printScript = @"Start-Process -FilePath """ + filePath + @""" -Verb PrintTo -PassThru -ArgumentList """ + A4PrinterName + @""" | %{sleep 10;$_} | kill";
            LivPowerShell.RunScript(printScript, "", "");
        }


        //Print file with in-built settings
        public void PrintLabel(string filePath)
        {
            string printScript = @"Start-Process -FilePath """ + filePath + @""" -Verb PrintTo -PassThru -ArgumentList """ + labelPrinterName + @""" | %{sleep 10;$_} | kill";
            LivPowerShell.RunScript(printScript, "", "");
        }

        //Print file with in-built settings
        public void PrintLabelAlt(string filePath)
        {

            string printScript = @"Start-Process -FilePath """ + filePath + @""" -Verb PrintTo -PassThru -ArgumentList """ + labelPrinterName + @""" | %{sleep 10;$_} | kill";
            LivPowerShell.RunScript(printScript, "", "");
        }

        //Generate and print the packing slip for a despatch order
        public bool PrintCAPLabel(string orderNo, int noOfLabels, string jobNo)
        {
            try
            {
                CrystalDecisions.CrystalReports.Engine.ReportDocument doc = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
                doc.Load(@"\\crmdoc\netcrm\CompiledReports\Ship_labels_2505_V2_WithCAPJobNo.rpt");

                doc.SetParameterValue("Courier", "CAP");
                doc.SetParameterValue("Number of Labels", noOfLabels);
                doc.SetParameterValue("@ord_no", orderNo.Trim());
                doc.SetParameterValue("@JobNo", jobNo.Trim());

                doc.PrintOptions.PrinterName = labelPrinterName;
                doc.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.Paper10x14;
                //doc.PrintOptions.ApplyPageMargins(new PageMargins(60, 60, 60, 60));

                doc.PrintToPrinter(1, false, 0, 0);

                doc.Close();


                return true;
            }
            catch (Exception e)
            {
                return false;
            }


        }


        public void RePrintLabel(string courier, string connoteNo)
        {
            if (courier == "STE")
            {
                string message = "";
                StarTrackCourier steCourier = new StarTrackCourier();
                steCourier.PrintConnote("", connoteNo, 0, ref message);
                MessageBox.Show("Label Print Job Sent. Printer--> " + labelPrinterName);

            }
            else if (courier == "AUP")
            {
                string message = "";
                AustraliaPostCourier aupCourier = new AustraliaPostCourier();
                aupCourier.PrintConnote("", connoteNo, 0, ref message);
                MessageBox.Show("Label Print Job Sent. Printer--> " + labelPrinterName);

            }

        }

        public void RePrintLabelForDespatch(string despatchNo)
        {
            DataTable dt = DataSource.GetTableFrom(@"
                    select courier, connoteNo, orderNo, noOfCartons, noOfPallets
                    from tblWhOrderDespatch
                    where id = @despatchNo
                ", "NetCRMAU", new object[,] { { "@despatchNo", despatchNo.Trim() } });

            if (dt != null && dt.Rows.Count > 0)
            {
                string courier = dt.Rows[0]["courier"].ToString().Trim();
                string connoteNo = dt.Rows[0]["connoteNo"].ToString().Trim();

                if (!string.IsNullOrEmpty(connoteNo) && !string.IsNullOrEmpty(courier))
                {
                    if (courier == "CAP")
                    {
                        string orderNo = dt.Rows[0]["orderNo"].ToString().Trim();
                        int totalLabels = Int32.Parse(dt.Rows[0]["noOfCartons"].ToString()) + Int32.Parse(dt.Rows[0]["noOfPallets"].ToString());
                        PrintCAPLabel(orderNo, totalLabels, connoteNo);
                    } else
                        RePrintLabel(courier, connoteNo);
                }
                else
                {
                    MessageBox.Show("Invalid courier and/or connote");
                }
            }
            else
            {
                MessageBox.Show("Could not retrieve label data");
            }
        }

        //Print the dangerous goods form(s) for a despatch order
        public bool PrintDangerousGoodsForm(string despatchNo)
        {
            DataTable dt = DataSource.GetTableFrom(@"
                select di.despatchID, di.orderNo, di.itemNo, di.weight, dg.* 
                from tblWhPackingDetailsDangerousItems di
                left join tblDangerousGoods dg
	                on (di.itemNo = dg.Item_no)
                where despatchID = @despatchNo
            ", "NetCRMAU", new object[,] { { "@despatchNo", despatchNo.Trim() } });

            foreach (DataRow dr in dt.Rows)
            {
                CrystalDecisions.CrystalReports.Engine.ReportDocument doc = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
                doc.Load(@"\\crmdoc\netcrm\CompiledReports\Dangerous Goods Declaration (For Packers).rpt");

                doc.SetParameterValue("@DespatchNo", despatchNo.Trim());
                doc.SetParameterValue("@ItemNo", dr["itemNo"].ToString().Trim());
                doc.SetParameterValue("Weight", dr["weight"].ToString().Trim());
                doc.SetParameterValue("Transportation", "Road");
                doc.SetParameterValue("Header", "Auto");


                doc.PrintOptions.PrinterName = A4PrinterName;
                doc.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;

                doc.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait;
                doc.PrintOptions.ApplyPageMargins(new PageMargins(180, 200, 170, 190));

                //doc.PrintOptions.ApplyPageMargins(new CrystalDecisions.Shared.PageMargins(400, 400, 400, 400));

                doc.PrintToPrinter(1, false, 0, 0);

                doc.Close();
            }

            return true;
            //return path;

        }



        //Generate and print the packing slip for a despatch order
        public bool PrintPackingSlip(string despatchNo)
        {
            try
            {
                CrystalDecisions.CrystalReports.Engine.ReportDocument doc = new CrystalDecisions.CrystalReports.Engine.ReportDocument();

                string packingSlipPath = GetPackingSlipPath(despatchNo);

                doc.Load(packingSlipPath);

                doc.SetParameterValue("@DespatchNo", despatchNo.Trim());

                doc.PrintOptions.PrinterName = A4PrinterName;
                doc.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                
                doc.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Landscape;
                doc.PrintOptions.ApplyPageMargins(new CrystalDecisions.Shared.PageMargins(300, 300, 300, 300));

                //string filePath = (string)DataSource.GetValue("select top 1 labelFilePath from tblWhOrderDespatch_Settings", "NetCRMAU");
                //string fullFilePath = filePath + @"PS_" + despatchNo.Trim() + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".pdf";

                //doc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fullFilePath);

                //PrintFile(fullFilePath);

                //if (File.Exists(fullFilePath))
                //{
                //    File.Delete(fullFilePath);
                //}

                doc.PrintToPrinter(1, false, 0, 0);


                doc.Close();


                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("-" + A4PrinterName + "-" + e.ToString());
                return false;
            }


        }

        public string GetPackingSlipPath(string despatchNo)
        {
            string packingSlipPlainPath = @"\\crmdoc\netcrm\CompiledReports\customer order packing slip.rpt";
            string packingSlipInvoiceFormatPath = @"\\crmdoc\netcrm\CompiledReports\customer order packing slip invoice format.rpt";
            try
            {
                SqlConnection con = DataSource.getConn("NetCRMAU");

                string customerNo = (string)DataSource.GetValue(@"
                    select exactNo from tblWhOrderDespatch where id = @despatchID
                ", con, new object[,]{{"@despatchID", despatchNo}});

                //Check plain packing slip customers
                DataTable dt = DataSource.GetTableFrom(@"
                      select cus.exact_no
                      from tblNSWHospitalBillingInfo b
                      left join ARCUSFIL_SQL cus 
	                    on (b.customerNo = cus.cus_no)
                ", con);

                con.Close();
                con.Dispose();

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["exact_no"].ToString().ToUpper().Trim() == customerNo.ToUpper().Trim())
                            return packingSlipInvoiceFormatPath;
                    }
                }

                return packingSlipPlainPath;
                
            }
            catch (Exception ex)
            {
                return packingSlipPlainPath;
            }
        }


        //Generate and print the packing slip for a despatch order
        public bool PrintShippingLabel(string orderNo, string courier, int noOfLabels)
        {
            try
            {
                CrystalDecisions.CrystalReports.Engine.ReportDocument doc = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
                doc.Load(@"\\crmdoc\netcrm\CompiledReports\Ship_labels_2505_V2_OrderDespatcher.rpt");

                doc.SetParameterValue("Courier", courier.Trim());
                doc.SetParameterValue("Number of Labels", noOfLabels);
                doc.SetParameterValue("@ord_no", orderNo.Trim());

                doc.PrintOptions.PrinterName = labelPrinterName;
                doc.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.Paper10x14;
                //doc.PrintOptions.ApplyPageMargins(new PageMargins(60, 60, 60, 60));

                doc.PrintToPrinter(1, false, 0, 0);

                doc.Close();


                return true;
            }
            catch (Exception e)
            {
                return false;
            }


        }


        //Print the courier label
        public void PrintCourierLabel(string courierLabelPath, string printerName)
        {

        }



        //Print the invoices
        public void PrintInvoice(string invoiceNo)
        {

        }


        //Email invoices
        public void EmailInvoice(string invoiceNo, string[] emailAddressList)
        {

        }

        //Test print  --WORKING but not with all printers
        public void TestPrint(string filePath, string printerName)
        {
            //PrintDialog dialog = new PrintDialog();
            //if (dialog.ShowDialog() == DialogResult.Cancel)
            //    return; 

            System.Drawing.Printing.PrintDocument pdf = new System.Drawing.Printing.PrintDocument();
            Process processInstance = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.UseShellExecute = true;
            startInfo.Verb = "Print";
            startInfo.CreateNoWindow = true;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.Arguments = @"/p /h /" + filePath + "/" + /*pdf.PrinterSettings.PrinterName*/ (printerName == "" ? labelPrinterName : printerName) + " /";//pd.PrinterSettings.PrinterName;

            startInfo.FileName = filePath;
            processInstance.StartInfo = startInfo;
            //processInstance.Exited += new EventHandler(processInstance_Exited);
            processInstance.Start();
        }

        public void TestPrint1(string filePath)
        {
            Process p = new Process();
            p.StartInfo = new ProcessStartInfo()
            {
                CreateNoWindow = true,
                Verb = "print",
                FileName = filePath //put the correct path here
            };
            p.Start();
        }


        public void TestPrint2(string filePath, string printerName)
        {
            string printScript = @"Start-Process -FilePath """ + filePath + @""" -Verb PrintTo -PassThru -ArgumentList " + printerName + " | %{sleep 10;$_} | kill";
            LivPowerShell.RunScript(printScript, "", "");
        }

        private void processInstance_Exited(object sender, EventArgs e)
        {
            string strFilePath = @"M:\MSDS Temp\temptests.pdf";
            if (File.Exists(strFilePath))
            {
                File.Delete(strFilePath);
            }
        }


        public static string GenerateCrystalReport(string reportName, string reportLocation, string parameters)
        {
            string command = @"
                " + reportName + @"
                @@output -> PDF
                @@schedule ->  triggerBySQL
                @@run time -> 16:00:00
                @@subscriber -> bo_hu@livingstone.com.au
                @@sourcetype -> sql / crystal report 
                @@parameters ->  " + parameters + @"
                @@source -> " + reportLocation;

            return LivCrystalReport.RunReport(command);
           
        }


        public static void PrintFile(string filePath, string printerName)
        {
            string printScript = @"Start-Process -FilePath """ + filePath + @""" -Verb PrintTo -PassThru -ArgumentList " + printerName + " | %{sleep 10;$_} | kill";
            LivPowerShell.RunScript(printScript, "", "");
        }

        //Generate dangerous goods form for order and return filepath where PDF is stored
        public static string GenerateDangerousGoodsForm(string invoice_no, double weight)
        {
            string path = Path.GetTempPath();
            Directory.CreateDirectory(path);

            path = @"\\livdoc9\mydocs\IT\Danan\testing12.pdf";

            CrystalDecisions.CrystalReports.Engine.ReportDocument doc = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
            doc.Load(@"\\crmdoc\netcrm\CompiledReports\Dangerous Goods Declaration.rpt");

            doc.SetParameterValue("@InvoiceNo", " " + invoice_no);
            doc.SetParameterValue("@ItemNo", "");
            doc.SetParameterValue("Weight", weight);
            doc.SetParameterValue("Transportation", "Road");
            doc.SetParameterValue("Header", "Auto");

            doc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, path);
            doc.Close();

            return path;

        }


//        //Print the dangerous goods form(s) for a despatch order
//        public static bool PrintDangerousGoodsForm(string orderNo, string despatchNo)
//        {
//            DataTable dt = DataSource.GetTableFrom(@"
//                select di.despatchID, di.orderNo, di.itemNo, di.weight, dg.* 
//                from tblWhPackingDetailsDangerousItems di
//                left join tblDangerousGoods dg
//	                on (di.itemNo = dg.Item_no)
//                where despatchID = @despatchNo and orderNo = @orderNo
//            ", "NetCRMAU", new object[,]{{"@orderNo", orderNo.Trim()}, {"@despatchNo", despatchNo.Trim()}});

//            foreach (DataRow dr in dt.Rows)
//            {
//                string path = @"\\livdoc9\mydocs\IT\Danan\OrderTest\DG_" + orderNo + "_" + despatchNo + "_" + dr["itemNo"].ToString().Trim() +  ".pdf";

//                CrystalDecisions.CrystalReports.Engine.ReportDocument doc = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
//                doc.Load(@"\\crmdoc\netcrm\CompiledReports\Dangerous Goods Declaration (For Packers).rpt");

//                doc.SetParameterValue("@DespatchNo", despatchNo.Trim());
//                doc.SetParameterValue("@ItemNo", dr["itemNo"].ToString().Trim());
//                doc.SetParameterValue("Weight", dr["weight"].ToString().Trim());
//                doc.SetParameterValue("Transportation", "Road");
//                doc.SetParameterValue("Header", "Auto");


//                doc.PrintOptions.PrinterName = "Kyocera-Up";
//                doc.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
//                doc.PrintOptions.ApplyPageMargins(new PageMargins(80, 120, 70, 90));


//                doc.PrintToPrinter(1, false, 0, 0);

//                //doc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, path);
//                doc.Close();

//                //PrintFile(path, "Kyocera-Up");
//            }

//            return true;
//            //return path;

//        }



        ////Generate and print the packing slip for a despatch order
        //public static bool PrintPackingSlip(string orderNo, string despatchNo)
        //{
        //    try
        //    {
        //        string path = @"\\livdoc9\mydocs\IT\Danan\OrderTest\PackingSlip_" + orderNo.Trim() + "_" + despatchNo.Trim() + ".pdf";

        //        CrystalDecisions.CrystalReports.Engine.ReportDocument doc = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
        //        doc.Load(@"\\crmdoc\netcrm\CompiledReports\customer order packing slip.rpt");

        //        doc.SetParameterValue("@DespatchNo", despatchNo.Trim());
        //        doc.SetParameterValue("@OrderNo", orderNo.Trim());
                
        //        doc.PrintOptions.PrinterName = "Kyocera-Up";
        //        doc.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
        //        //doc.PrintOptions.ApplyPageMargins(new PageMargins(60, 60, 60, 60));


        //        doc.PrintToPrinter(1, false, 0, 0);

        //       // doc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, path);
        //        doc.Close();

        //        //PrintFile(path, "Kyocera-Up");

        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        return false;
        //    }


        //}




    }
}
