﻿namespace OrderDespatcher
{
    partial class frmDangerousGoods
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxOrderDetails = new System.Windows.Forms.GroupBox();
            this.txtPackingID = new System.Windows.Forms.TextBox();
            this.txtDespatchID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDespatchID = new System.Windows.Forms.Label();
            this.dgrDGItems = new System.Windows.Forms.DataGridView();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.gbxOrderDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrDGItems)).BeginInit();
            this.SuspendLayout();
            // 
            // gbxOrderDetails
            // 
            this.gbxOrderDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxOrderDetails.Controls.Add(this.txtPackingID);
            this.gbxOrderDetails.Controls.Add(this.txtDespatchID);
            this.gbxOrderDetails.Controls.Add(this.label1);
            this.gbxOrderDetails.Controls.Add(this.lblDespatchID);
            this.gbxOrderDetails.Location = new System.Drawing.Point(3, 3);
            this.gbxOrderDetails.Name = "gbxOrderDetails";
            this.gbxOrderDetails.Size = new System.Drawing.Size(668, 51);
            this.gbxOrderDetails.TabIndex = 0;
            this.gbxOrderDetails.TabStop = false;
            // 
            // txtPackingID
            // 
            this.txtPackingID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPackingID.Location = new System.Drawing.Point(583, 17);
            this.txtPackingID.Name = "txtPackingID";
            this.txtPackingID.ReadOnly = true;
            this.txtPackingID.Size = new System.Drawing.Size(75, 20);
            this.txtPackingID.TabIndex = 1;
            // 
            // txtDespatchID
            // 
            this.txtDespatchID.Location = new System.Drawing.Point(85, 13);
            this.txtDespatchID.Name = "txtDespatchID";
            this.txtDespatchID.ReadOnly = true;
            this.txtDespatchID.Size = new System.Drawing.Size(75, 20);
            this.txtDespatchID.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(507, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Packing ID:";
            // 
            // lblDespatchID
            // 
            this.lblDespatchID.AutoSize = true;
            this.lblDespatchID.Location = new System.Drawing.Point(9, 16);
            this.lblDespatchID.Name = "lblDespatchID";
            this.lblDespatchID.Size = new System.Drawing.Size(70, 13);
            this.lblDespatchID.TabIndex = 0;
            this.lblDespatchID.Text = "Despatch ID:";
            // 
            // dgrDGItems
            // 
            this.dgrDGItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgrDGItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrDGItems.Location = new System.Drawing.Point(3, 91);
            this.dgrDGItems.Name = "dgrDGItems";
            this.dgrDGItems.Size = new System.Drawing.Size(668, 233);
            this.dgrDGItems.TabIndex = 1;
            this.dgrDGItems.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgrDGItems_CellValueChanged);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Location = new System.Drawing.Point(227, 330);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(110, 55);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(343, 330);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(110, 55);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(60, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(574, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Please tick to ADD item as a dangerous good or untick to REMOVE item from dangero" +
    "us goods";
            // 
            // frmDangerousGoods
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(673, 397);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.dgrDGItems);
            this.Controls.Add(this.gbxOrderDetails);
            this.Name = "frmDangerousGoods";
            this.Text = "Dangerous Goods Declaration";
            this.gbxOrderDetails.ResumeLayout(false);
            this.gbxOrderDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrDGItems)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxOrderDetails;
        private System.Windows.Forms.DataGridView dgrDGItems;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtPackingID;
        private System.Windows.Forms.TextBox txtDespatchID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDespatchID;
        private System.Windows.Forms.Label label2;
    }
}