﻿namespace OrderDespatcher
{
    partial class frmPrintSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboA4Printer = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboLabelPrinter = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSetPrinter = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cboA4Printer
            // 
            this.cboA4Printer.FormattingEnabled = true;
            this.cboA4Printer.Location = new System.Drawing.Point(89, 15);
            this.cboA4Printer.Name = "cboA4Printer";
            this.cboA4Printer.Size = new System.Drawing.Size(139, 21);
            this.cboA4Printer.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "A4 Printer:";
            // 
            // cboLabelPrinter
            // 
            this.cboLabelPrinter.FormattingEnabled = true;
            this.cboLabelPrinter.Location = new System.Drawing.Point(89, 50);
            this.cboLabelPrinter.Name = "cboLabelPrinter";
            this.cboLabelPrinter.Size = new System.Drawing.Size(139, 21);
            this.cboLabelPrinter.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Label Printer:";
            // 
            // btnSetPrinter
            // 
            this.btnSetPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetPrinter.Location = new System.Drawing.Point(89, 92);
            this.btnSetPrinter.Name = "btnSetPrinter";
            this.btnSetPrinter.Size = new System.Drawing.Size(75, 23);
            this.btnSetPrinter.TabIndex = 2;
            this.btnSetPrinter.Text = "Set Printer";
            this.btnSetPrinter.UseVisualStyleBackColor = true;
            this.btnSetPrinter.Click += new System.EventHandler(this.btnSetPrinter_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(170, 92);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(61, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmPrintSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(240, 127);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSetPrinter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboLabelPrinter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboA4Printer);
            this.Name = "frmPrintSettings";
            this.Text = "Printer Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboA4Printer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboLabelPrinter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSetPrinter;
        private System.Windows.Forms.Button btnCancel;
    }
}