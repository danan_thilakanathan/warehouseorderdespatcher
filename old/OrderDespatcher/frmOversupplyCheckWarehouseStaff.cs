﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OrderDespatcher
{
    public partial class frmOversupplyCheckWarehouseStaff : Form
    {
        private string despatchID { get; set; }

        public frmOversupplyCheckWarehouseStaff()
        {
            InitializeComponent();
        }

        public frmOversupplyCheckWarehouseStaff(string despatchID, string userName, string title, string message)
        {
            InitializeComponent();
            this.despatchID = despatchID;

            //Store authoriser in DB
            OrderDespatchService.SetAuthorisation(this.despatchID, userName.ToUpper().Trim(), "Warehouse Staff - Connote and System Weight Discrepancy");
        }

        private void btnAmend_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
