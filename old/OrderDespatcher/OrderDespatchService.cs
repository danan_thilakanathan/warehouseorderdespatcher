﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Windows.Forms;
using Livingstone.Library;

namespace OrderDespatcher
{
    public class OrderDespatchService
    {
        //Get the customer and order details
        public static DataTable GetCustomerOrderDetails(string exactOrderNo)
        {
            try
            {
                SqlConnection con = new SqlConnection();
                con = DataSource.getConn("NetCRMAU");

                DataTable dt = DataSource.GetTableFrom(@"
                    select c.cus_no, c.exact_no, c.cus_name, o.ord_no, o.ExactOrderNo, o.ord_dt, 
                        o.ship_to_name, oe.del_AddressLine1 as del_addr_1, oe.del_AddressLine2 as del_addr_2, oe.del_AddressLine3 as del_addr_3, 
	                    oe.del_City, oe.del_StateCode as del_state, oe.del_PostCode, oe.del_landcode as del_country,
                        convert(varchar, oe.afldat, 112) as shipping_dt, o.OrderStatus, oe.magcode as warehouse, 
					    isnull(case when a.CourierInstructions is null or a.CourierInstructions = '' then q.CourierInstructions else a.CourierInstructions end, '') as ship_instruction_1, '' as ship_instruction_2, 
                        isnull(a.InvoiceWithGoods, c.invoiceWithGoods) as invoiceWithGoods, q.CustomerComments  
                    from oeordhdr_sql o
                    left join liv_vu_orkrg oe
	                    on (o.ExactOrderNo = ltrim(oe.ordernr))
				    left join quotehdr q
					    on (o.job_no = q.QuoteNo)
				    left join address a
					    on (q.AltAddress = a.AddressID)
                    left join arcusfil_sql c
	                    on (o.cus_no = c.exact_no)
                    where o.ExactOrderNo = @orderNo", con, new object[,] { { "@orderNo", exactOrderNo.Trim() } });

                con.Close();
                con.Dispose();

                return dt;
            }
            catch
            {
                return null;
            }

        }


        //Get the order line details
        public static DataTable GetOrderLines(string exactOrderNo, string warehouse)
        {
             try
            {
                SqlConnection con = new SqlConnection();
                con = DataSource.getConn("NetCRMAU");

                DataTable dt = DataSource.GetTableFrom(@"

				    select ltrim(o.ordernr) as orderNo,  o.artcode as [Item No], o.unitcode as UOM, o.oms45 as [Item Description], /*wpr.Picker,*/ '' as [Carton/Pallet ID], 
						    sum(o.esr_aantal) as [Qty Ordered], isnull(wpr.QtyPicked, 0) as [Qty Picked], '' as Comments, wpr.picker as Picker,
						    d.unit_wgt / 1000 as [Unit Weight (kg)], d.unit_cube_l as [Length (cm)], d.unit_cube_w as [Width (cm)], d.unit_cube_h as [Height (cm)], cast((d.unit_cube_l * d.unit_cube_w * d.unit_cube_h) / 1000000 as decimal(18,7)) as [Volume (cbm)], oh.docnumber as [PO Number]
                    from liv_vu_orsrg o
                    left join liv_vu_orkrg oh
                        on (o.ordernr = oh.ordernr)
                    left join 
                    (
					    select DD, itemNo, sum(QtyPicked) as QtyPicked, stuff((select ',' + a.Picker from tblWhPickingRecords a where a.DD = b.DD and a.ItemNo = b.itemNo for XML PATH('') ), 1, 1, '' ) as picker
	                    from tblWhPickingRecords  b
	                    group by DD, itemNo
                    ) wpr
	                    on (ltrim(o.ordernr) = wpr.DD and o.artcode = wpr.ItemNo)
                    left join imitmidx_sql i
	                    on (o.artcode = i.item_no)
                    left join imcatfil_sql c
	                    on (i.item_filler = c.prod_cat)
				    left join IMCUBE_SQL d
					    on (o.artcode = d.item_no)
                    where ltrim(o.ordernr) = @orderNo
	                    and c.is_physical_item = 1 
                        and o.reeds_fakt = 0

				    group by o.ordernr, o.artcode, o.unitcode, o.oms45, wpr.DD, d.unit_cube_l, d.unit_cube_w, d.unit_cube_h, d.unit_wgt, wpr.picker, wpr.QtyPicked, oh.docnumber
                    order by o.ordernr, o.artcode", con, new object[,] { { "@orderNo", exactOrderNo.Trim() }, { "@warehouse", warehouse.Trim() } });

                con.Close();
                con.Dispose();

                return dt;
            }
            catch
            {
                return null;
            }
        }

        public static DataTable RetrieveOrdersForDespatch(string despatchID)
        {
             try
            {
                SqlConnection con = new SqlConnection();
                con = DataSource.getConn("NetCRMAU");

                DataTable dt = DataSource.GetTableFrom(@" 
                    select orderNo, addedBy from tblWhOrderDespatch_Orders where despatchID = @despatchID
                ",con, new object[,] { { "@despatchID", despatchID.Trim() } });

                con.Close();
                con.Dispose();

                return dt;
            }
            catch
            {
                return null;
            }
        }

        public static void AddOrderToDespatch(string despatchID, string newOrderNo, string packerName)
        {
            DataTable dt = DataSource.GetTableFrom(@"
                if (not exists(select * from tblWhOrderDespatch_Orders where despatchID = @despatchID and orderNo = @orderNo))
                begin
                    insert into tblWhOrderDespatch_Orders (despatchID, orderNo, addedBy)
                    values (@despatchID, @orderNo, @packer)

                    insert into tblWhPackedItems (despatchID, orderNo, itemNo, uom, itemDescription, qtyOrdered, qtyPicked, comments, pickedBy, length, width, height, weight, volume, customerRef)
				    select cast(@despatchID as decimal(18,0)), cast(ltrim(o.ordernr) as numeric(18,0)),  o.artcode, o.unitcode, o.oms45, sum(o.esr_aantal), isnull(wpr.QtyPicked, 0), '' , wpr.picker,
						    d.unit_cube_l, d.unit_cube_w, d.unit_cube_h, d.unit_wgt / 1000, cast((d.unit_cube_l * d.unit_cube_w * d.unit_cube_h) / 1000000 as decimal(18,7)), oh.docnumber
                    from liv_vu_orsrg o
                    left join liv_vu_orkrg oh
                        on (o.ordernr = oh.ordernr)
                    left join 
                    (
					    select DD, itemNo, sum(QtyPicked) as QtyPicked, stuff((select ',' + a.Picker from tblWhPickingRecords a where a.DD = b.DD and a.ItemNo = b.itemNo for XML PATH('') ), 1, 1, '' ) as picker
	                    from tblWhPickingRecords  b
	                    group by DD, itemNo
                    ) wpr
	                    on (ltrim(o.ordernr) = wpr.DD and o.artcode = wpr.ItemNo)
                    left join imitmidx_sql i
	                    on (o.artcode = i.item_no)
                    left join imcatfil_sql c
	                    on (i.item_filler = c.prod_cat)
				    left join IMCUBE_SQL d
					    on (o.artcode = d.item_no)
                    where ltrim(o.ordernr) = @orderNo
	                    and c.is_physical_item = 1
                        and o.reeds_fakt = 0 /*Already invoiced*/
				    group by o.ordernr, o.artcode, o.unitcode, o.oms45, wpr.DD, d.unit_cube_l, d.unit_cube_w, d.unit_cube_h, d.unit_wgt, wpr.picker, wpr.QtyPicked, oh.docnumber
                    order by o.ordernr, o.artcode
                end", "NetCRMAU", new object[,] { { "@despatchID", despatchID.Trim() }, { "@orderNo", newOrderNo.Trim() }, { "@packer", packerName } });
        }

        public static void RemoveOrderFromDespatch(string despatchID, string orderToRemove)
        {
            DataSource.ExecuteNonQuery(@"
                delete from tblWhOrderDespatch_Orders where despatchID = @despatchId and orderNo = @orderNo

                delete from tblWhPackedItems where despatchID = @despatchID and orderNo = @orderNo
            ", "NetCRMAU", new object[,] { { "@despatchID",despatchID.Trim() }, { "@orderNo", orderToRemove.Trim() } });
        }


        //Get the packing details
        public static DataTable GetPackingDetails(string despatchID)
        {
             try
            {
                SqlConnection con = DataSource.getConn("NetCRMAU");

                DataTable dt = DataSource.GetTableFrom(@"
                        select id as ID, type as [Packing Type], qty as Qty, weight as [Weight (kg)], length as [Length (cm)], width as [Width (cm)], height as [Height (cm)], connoteNo, estimatedFreightCost, isDG as [containsDangerousGoods?], totalCartons, comments as [Comments], addedBy as [added by], modifiedBy as [modified by] 
                        from tblWhPackingDetails
                        where despatchID = @despatchID
                        order by id   
                ", con, new object[,] { {"@despatchID", despatchID.Trim()} });

                con.Close();
                con.Dispose();

                return dt;
            }
            catch
            {
                return null;
            }

        }


        public static DataTable LoadExistingCustomerOrderDetails(string despatchID)
        {
             try
            {
                SqlConnection con = DataSource.getConn("NetCRMAU");

                DataTable dt = DataSource.GetTableFrom(@"
                        select *, convert(VARCHAR, orderDate, 112) as formattedOrderDate, convert(VARCHAR, etaDate, 112) as shippingDate 
                        from tblWhOrderDespatch
                        where id = @despatchID   
                        order by ts desc
                ", con, new object[,] { { "@despatchID", despatchID.Trim() } });

                con.Close();
                con.Dispose();

                return dt;
            }
            catch
            {
                return null;
            }
        }

        public static DataTable LoadExistingItemDetails(string despatchID)
        {
            try
            {
                SqlConnection con = DataSource.getConn("NetCRMAU");

                DataTable dt = DataSource.GetTableFrom(@"
				        select orderNo, itemNo as [Item No], uom as UOM, itemDescription as [Item Description], packingID as [Carton/Pallet ID],
							    qtyOrdered as [Qty Ordered], qtyPicked as [Qty Picked], comments as Comments, pickedBy as Picker,
							    weight as [Unit Weight (kg)], length as [Length (cm)], width as [Width (cm)], height as [Height (cm)], volume as [Volume (cbm)], customerRef as [PO Number]
                        from tblWhPackedItems
                        where despatchID = @despatchID  
                        order by orderNo, itemNo 
                ", con, new object[,] { { "@despatchID", despatchID.Trim() } });

                con.Close();
                con.Dispose();

                return dt;
            }
            catch
            {
                return null;
            }
        }


        //Update Order In Exact. Just before the order is fulfilled and invoiced, need to fill in some entries in the freefield columns in orkrg
        public static bool UpdateSalesOrderInExact(string exactOrderNo, string courier, string connoteNo,
                                                int noOfTotalCartons, int noOfPallets, double totalWeight, double totalDimensions)
        {
            object[,] xargs = {
                { "@orderNo", exactOrderNo.Trim() },
                { "@courier", courier.Trim() },
                { "@connoteNo", connoteNo.Trim() },
                { "@noOfTotalCartons", (noOfTotalCartons != 0 ? noOfTotalCartons.ToString().Trim() : "") },
                { "@noOfPallets", (noOfPallets != 0 ? noOfPallets.ToString().Trim() : "") },
                { "@totalWeight", totalWeight.ToString().Trim() },
                { "@totalDimensions", totalDimensions.ToString().Trim() + "CBM" }
            };

            DataTable dt = DataSource.GetTableFrom(@"
                    update orkrg
                    set levwijze = @courier, 
                        freefield3 = @connoteNo,
                        bruto_gew = @totalWeight,
                        refer3 = @totalDimensions, 
                        freefield4 = @noOfTotalCartons, 
                        freefield5 = @noOfPallets
                    where ltrim(ordernr) = @orderNo  

                    select 1 
                    from orkrg
                    where ltrim(ordernr) = @orderNo 
                        and levwijze = @courier
                        and freefield3 = @connoteNo
                        and freefield4 = @noOfTotalCartons
                        and freefield5 = @noOfPallets

            ", "LivExactAus", xargs);

            if (dt != null && dt.Rows.Count > 0) return true;

            return false;

        }




        //Update the dispatch order status
        public static bool UpdateDespatchOrderStatus(string despatchID, string message)
        {
            try
            {
                SqlConnection con = DataSource.getConn("NetCRMAU");

                object[,] xargs = {
                    { "@despatchID", despatchID.Trim() },
                    { "@message", message.Trim() }
                };

                DataTable dt = DataSource.GetTableFrom(@"
                        update tblWhOrderDespatch
                        set despatchStatus = @message
                        where id = @despatchID 

                        select 1 
                        from tblWhOrderDespatch
                        where id = @despatchID
                            and despatchStatus = @message
                ", con, xargs);

                con.Close();
                con.Dispose();

                if (dt != null && dt.Rows.Count > 0) return true;

                return false;

            }
            catch
            {
                return false;
            }

        }

        //Set this despatch as Sent to OE
        public static bool SetSendToOE(string despatchID, int isSent)
        {
            try
            {
                SqlConnection con = DataSource.getConn("NetCRMAU");

                object[,] xargs = {
                    { "@despatchID", despatchID.Trim() },
                    { "@isSent", isSent },
                };

                DataTable dt = DataSource.GetTableFrom(@"
                        update tblWhOrderDespatch
                        set sentToOE = @isSent
                        where id = @despatchID 

                        select 1 
                        from tblWhOrderDespatch
                        where id = @despatchID
                            and sentToOE = @isSent
                ", con, xargs);
                con.Close();
                con.Dispose();

                if (dt != null && dt.Rows.Count > 0) return true;


            }
            catch
            {
                return false;
            }



            return false;
        }


        //Update the connote no for this despatch
        public static void UpdateConnoteNo(string despatchID, string connoteNo)
        {
            try
            {
                SqlConnection con = DataSource.getConn("NetCRMAU");

                object[,] xargs = {
                    { "@despatchID", despatchID.Trim() },
                    { "@connoteNo", connoteNo.Trim() }
                };

                if (!string.IsNullOrEmpty(connoteNo))
                {
                    DataTable dt = DataSource.GetTableFrom(@"
                        update tblWhOrderDespatch
                        set connoteNo = @connoteNo, labelPrintedDate = getdate()
                        where id = @despatchID 

                        select 1 
                        from tblWhOrderDespatch
                        where id = @despatchID
                            and connoteNo = @connoteNo
                ", con, xargs);

                }
                else
                {
                    DataTable dt = DataSource.GetTableFrom(@"
                        update tblWhOrderDespatch
                        set connoteNo = @connoteNo
                        where id = @despatchID 

                        select 1 
                        from tblWhOrderDespatch
                        where id = @despatchID
                            and connoteNo = @connoteNo
                ", con, xargs);


                }

                con.Close();
                con.Dispose();
            }
            catch
            {

            }



        }


        //Update the freefield value
        public static bool UpdateFreefield(string despatchID, int freefieldNumber, string freefieldValue)
        {
            try
            {
                SqlConnection con = DataSource.getConn("NetCRMAU");

                object[,] xargs = {
                    { "@despatchID", despatchID.Trim() },
                    { "@freefieldValue", freefieldValue },
                };

                DataTable dt = DataSource.GetTableFrom(@"
                        update tblWhOrderDespatch
                        set freefield" + freefieldNumber.ToString() + @" = @freefieldValue
                        where id = @despatchID 

                        select 1 
                        from tblWhOrderDespatch
                        where id = @despatchID
                            and freefield" + freefieldNumber.ToString() + @" = @freefieldValue
                ", con, xargs);

                if (dt != null && dt.Rows.Count > 0) return true;

                con.Close();
                con.Dispose();
            }
            catch { }


            return false;
        }



        //Add Packing Entry
        public static string AddPackingEntry(string exactOrderNo, string despatchID, string packingType, int qty, double length, double width, double height, double weight,
                                string connoteNo, double estimatedCost, bool isDG, int totalCartons, string comments, string addedBy)
        {
            object[,] xargs = {
                { "@orderNo", exactOrderNo.Trim() },
                { "@despatchID", despatchID.Trim() },
                { "@packingType", packingType.Trim() },
                { "@qty", qty },
                { "@length", length },
                { "@width", width },
                { "@height", height },
                { "@weight", weight },
                { "@connoteNo", connoteNo.Trim() },
                { "@estimatedCost", estimatedCost },
                { "@isDG", (isDG ? "1" : "0") },
                { "@comments", comments.Trim() },
                { "@totalCartons", totalCartons },
                { "@addedBy", addedBy }
            };

            DataTable dt = DataSource.GetTableFrom(@"
                    insert into tblWhPackingDetails (orderNo, despatchID, type, qty, length, width, height, weight, connoteNo, estimatedFreightCost, isDG, totalCartons, comments, addedBy)
                    values (@orderNo, @despatchID, @packingType, @qty, @length, @width, @height, @weight, @connoteNo, @estimatedCost, @isDG, @totalCartons, @comments, @addedBy)

                    select @@identity
            ", "NetCRMAU", xargs);

            if (dt != null && dt.Rows.Count > 0)
                    return dt.Rows[0][0].ToString();

            return "0";
        }


        //Edit Packing Entry
        public static bool EditPackingEntry(string despatchID, string packingID, string packingType, int qty, double length, double width, double height, double weight,
                                string connoteNo, double estimatedCost, bool isDG, int totalCartons, string comments, string modifiedBy)
        {
            object[,] xargs = {
                { "@despatchID", despatchID.Trim() },
                { "@packingID", packingID.Trim() },
                { "@packingType", packingType.Trim() },
                { "@qty", qty },
                { "@length", length },
                { "@width", width },
                { "@height", height },
                { "@weight", weight },
                { "@connoteNo", connoteNo.Trim() },
                { "@estimatedCost", estimatedCost },
                { "@isDG", (isDG ? "1" : "0") },
                { "@comments", comments.Trim() },
                { "@totalCartons", totalCartons },
                { "@modifiedBy", modifiedBy }
            };

            DataTable dt = DataSource.GetTableFrom(@"
                    update tblWhPackingDetails
                    set type = @packingType, qty = @qty, length = @length, width = @width, height = @height, weight = @weight, connoteNo = @connoteNo, 
                            estimatedFreightCost = @estimatedCost, isDG = @isDG, totalCartons = @totalCartons, comments = @comments, modifiedBy = @modifiedBy
                    where despatchID = @despatchID and id = @packingID

                    select 1
                    from tblWhPackingDetails
                    where despatchID = @despatchID and id = @packingID and type = @packingType and qty = @qty and length = @length and width = @width and height = @height and weight = @weight and connoteNo = @connoteNo and  
                            estimatedFreightCost = @estimatedCost and isDG = @isDG and totalCartons = @totalCartons and comments = @comments and modifiedBy = @modifiedBy

            ", "NetCRMAU", xargs);
            
            if (dt != null && dt.Rows.Count == 1)
                if (dt.Rows[0][0].ToString().Trim() == "1")
                    return true;

            return false;
        }


        //Delete Packing Entry
        public static bool DeletePackingEntry(string despatchID, string id)
        {
            object[,] xargs = {
                { "@despatchID", despatchID.Trim() },
                { "@id", id.Trim() }
            };

            DataTable dt = DataSource.GetTableFrom(@"
                    delete from tblWhPackingDetails
                    where despatchID = @despatchID and id = @id

                    delete from tblWhPackingDetailsDangerousItems
                    where despatchID = @despatchID and packingID = @id

            ", "NetCRMAU", xargs);

            return true;
        }


        public static void ScaleDimensions(string despatchID, double factor)
        {
            try 
            {
                SqlConnection con = DataSource.getConn("NetCRMAU");

                DataSource.ExecuteNonQuery(@"
                    update tblWhPackingDetails
                    set length = @factor * length, width = @factor * width, height = @factor * height
                    where despatchID = @despatchID
                ", con, new object[,]{{"@despatchID", despatchID}, {"@factor", factor}});

                con.Close();
                con.Dispose();
            }
            catch (Exception ex) 
            {

            }
        }

        public static void ScaleWeight(string despatchID, double factor)
        {
            try
            {
                SqlConnection con = DataSource.getConn("NetCRMAU");

                DataSource.ExecuteNonQuery(@"
                    update tblWhPackingDetails
                    set weight = @factor * weight
                    where despatchID = @despatchID
                ", con, new object[,] { { "@despatchID", despatchID }, { "@factor", factor } });

                con.Close();
                con.Dispose();
            }
            catch (Exception ex)
            {

            }
        }


        public static void GetTotalPackingDetails(string despatchID, out double totalWeight, out double totalVolume, out int noOfCartons, out int noOfPallets, out int totalCartons  )
        {

            totalWeight = 0;
            totalVolume = 0;

            noOfCartons = 0;
            noOfPallets = 0;

            totalCartons = 0;

            object[,] xargs = {
                { "@despatchID", despatchID.Trim() }
            };

            DataTable dt = DataSource.GetTableFrom(@"
                select cast(sum(qty * weight) as decimal(18,4)) as totalWeight, 
                        cast(sum(qty * (length * width * height) / 1000000 ) as decimal(18,4)) as totalVolume,
                        sum(case when type = 'CARTON' then qty else 0 end) as totalCartons, 
                        sum(case when type = 'PALLET' then qty else 0 end) as totalPallets,
                        sum(qty * totalCartons) as totalCompleteCartons
                from tblWhPackingDetails
                where despatchID = @despatchID
            ", "NetCRMAU", xargs);

            if (dt != null && dt.Rows.Count > 0)
            {
                if (!double.TryParse(dt.Rows[0]["totalWeight"].ToString(), out totalWeight))
                {

                }

                if (!double.TryParse(dt.Rows[0]["totalVolume"].ToString(), out totalVolume))
                {

                }

                if (!Int32.TryParse(dt.Rows[0]["totalCartons"].ToString(), out noOfCartons))
                {

                }

                if (!Int32.TryParse(dt.Rows[0]["totalPallets"].ToString(), out noOfPallets))
                {

                }

                if (!Int32.TryParse(dt.Rows[0]["totalCompleteCartons"].ToString(), out totalCartons))
                {

                }

                object[,] xargs1 = { 
                    { "@despatchID", despatchID.Trim() },
                    { "@totalWeight", totalWeight },
                    { "@totalVolume", totalVolume },
                    { "@noOfCartons", noOfCartons },
                    { "@noOfPallets", noOfPallets },
                    { "@totalCartons", totalCartons },
                };

                DataSource.ExecuteNonQuery(@"
                    update tblWhOrderDespatch
                    set totalWeight = @totalWeight, totalDimensions = @totalVolume, noOfCartons = @noOfCartons, noOfPallets = @noOfPallets, totalCartons = @totalCartons
                    where id = @despatchID
                ", "NetCRMAU", xargs1);

            }

        }


        public static DataTable GetMostUsedCouriers()
        {
            DataTable dt = DataSource.GetTableFrom(@"
                select levwijze as courier
                from liv_vu_orkrg
                where ord_soort = 'V'
	                and orddat > dateadd(YEAR, -1, getdate())
	                and levwijze <> 'U'
	                and magcode = 'R'
                group by levwijze
                order by count(*) desc
            ", "NetCRMAU");

            return dt;
        }


        //Update the despatch order. That is, save the packing detail information, courier, delivery address, invoiceWithGoods, connote and UN number.
        public static bool SaveDespatchOrder(string customerNo, string exactNo, string customerName, string despatchID, string courier,
            string delName, string delAddr1, string delAddr2, string delAddr3, string delSuburb, string delState, string delPostcode, string country,
                bool invoiceWithGoods, bool printPackingSlip, string connoteNo, string specialInstructions, string specialInstructions2, string customerComments, string estimatedFreightCost, 
                double totalWeight, double totalDimensions, int noOfCartons, int noOfPallets, DataTable packingDetails, DataTable itemDetails, string orderDate, string shippingDate, int totalCartons, string packedBy, string changedBy)
        {
            //Save Header
            object[,] xargs = {
                { "@customerNo", customerNo.Trim() },
                { "@exactNo", exactNo.Trim() },
                { "@customerName", customerName.Trim() },
                { "@despatchID", despatchID.Trim() },
                { "@courier", courier.Trim() },
                { "@delName", delName.Trim() },
                { "@delAddr1", delAddr1.Trim() },
                { "@delAddr2", delAddr2.Trim() },
                { "@delAddr3", delAddr3.Trim() },
                { "@delSuburb", delSuburb.Trim() },
                { "@delState", delState.Trim() },
                { "@delPostcode", delPostcode.Trim() },
                { "@country", country.Trim() },
                { "@invoiceWithGoods", (invoiceWithGoods ? "1" : "0") },
                { "@printPackingSlip", (printPackingSlip ? "1" : "0") },
                { "@connoteNo", connoteNo.Trim() },
                { "@estimatedFreightCost", estimatedFreightCost.Trim() },
                { "@totalWeight", totalWeight.ToString().Trim() },
                { "@totalDimensions", totalDimensions.ToString().Trim() },
                { "@noOfCartons", noOfCartons.ToString().Trim() },
                { "@noOfPallets", noOfPallets.ToString().Trim() },
                { "@specialInstructions", specialInstructions.Trim() },
                { "@specialInstructions2", specialInstructions2.Trim() },
                { "@customerComments", customerComments.Trim() },
                { "@orderDate", orderDate.Trim() },
                { "@shippingDate", shippingDate.Trim() },
                { "@totalCartons", totalCartons.ToString().Trim() },
                { "@packedBy", packedBy.Trim() },
                { "@changedBy", changedBy.Trim() }
            };

            DataTable dt = DataSource.GetTableFrom(@"
                    update tblWhOrderDespatch
                    set cusNo = @customerNo, exactNo = @exactNo, cusName = @customerName, courier = @courier, 
                        delName = @delName, delAddr1 = @delAddr1, delAddr2 = @delAddr2, delAddr3 = @delAddr3, 
                        delSuburb = @delSuburb, delState = @delState, delPostcode = @delPostcode, country = @country,
                        invoiceWithGoods = @invoiceWithGoods, printPackingSlip = @printPackingSlip, connoteNo = @connoteNo, 
                        estimatedFreightCost = @estimatedFreightCost, modifiedBy = @changedBy,
                        totalWeight = @totalWeight, totalDimensions = @totalDimensions, specialInstructions = @specialInstructions, specialInstructions2 = @specialInstructions2, customerComments = @customerComments,
                        noOfCartons = @noOfCartons, noOfPallets = @noOfPallets, totalCartons = @totalCartons, packedBy = @packedBy, orderDate = convert(DATETIME, @orderDate) , etaDate = convert(DATETIME, @shippingDate)
                    where id = @despatchID 

                    select 1 
                    from tblWhOrderDespatch
                    where id = @despatchID
            ", "NetCRMAU", xargs);


            //Remove packing details and item details stored info
            DataSource.ExecuteNonQuery(@"
                    delete from tblWhPackedItems where despatchID = @despatchID

                ", "NetCRMAU", xargs);


            //Store Updated Packing Details
//            foreach (DataRow dr in packingDetails.Rows)
//            {
//                object[,] xargsPD = {
//                    { "@id", dr["ID"].ToString().Trim()},
//                    { "@despatchID", despatchID.Trim() },
//                    { "@orderNo", exactOrderNo.Trim() },
//                    { "@type", dr["Packing Type"].ToString().Trim() },
//                    { "@qty", dr["Qty"].ToString().Trim() },
//                    { "@length", dr["Length (cm)"].ToString().Trim() },
//                    { "@width", dr["Width (cm)"].ToString().Trim() },
//                    { "@height", dr["Height (cm)"].ToString().Trim() },
//                    { "@weight", dr["Weight (kg)"].ToString().Trim() },
//                    { "@connoteNo", dr["connoteNo"].ToString().Trim() },
//                    { "@estimatedFreightCost", dr["estimatedFreightCost"].ToString().Trim() },
//                    { "@isDG", dr["containsDangerousGoods?"].ToString().Trim() },
//                    { "@comments", dr["comments"].ToString() .Trim()},
//                    { "@addedBy", dr["added by"].ToString() .Trim()},
//                    { "@changedBy", changedBy.Trim()}
//                };


//                DataTable dtPD = DataSource.GetTableFrom(@"
//                    insert into tblWhPackingDetails (despatchID, orderNo, type, qty, length, width, height, weight, connoteNo, estimatedFreightCost, isDG, comments, addedBy, modifiedBy )
//                    values (@despatchID, @orderNo, @type, @qty, @length, @width, @height, @weight, @connoteNo, @estimatedFreightCost, @isDG, @comments, @addedBy, @changedBy )
//
//                ", "NetCRMAU", xargsPD);

//            }

            //Store Update Picked Item Details


            foreach (DataRow dr in itemDetails.Rows)
            {
                object[,] xargsID = {
                    { "@despatchID", despatchID.Trim() },
                    { "@orderNo", dr["orderNo"].ToString().Trim() },
                    { "@itemNo", dr["Item No"].ToString().Trim() },
                    { "@uom", dr["UOM"].ToString() },
                    { "@itemDescription", dr["Item Description"].ToString() },
                    { "@qtyOrdered", dr["Qty Ordered"].ToString() },
                    { "@qtyPicked", dr["Qty Picked"].ToString() },
                    { "@packingID", dr["Carton/Pallet ID"].ToString() },
                    { "@length", !string.IsNullOrEmpty(dr["Length (cm)"].ToString()) ? dr["Length (cm)"].ToString() : "0" },
                    { "@width", !string.IsNullOrEmpty(dr["Width (cm)"].ToString()) ? dr["Width (cm)"].ToString() : "0" },
                    { "@height", !string.IsNullOrEmpty(dr["Height (cm)"].ToString()) ? dr["Height (cm)"].ToString() : "0" },
                    { "@weight", !string.IsNullOrEmpty(dr["Unit Weight (kg)"].ToString()) ? dr["Unit Weight (kg)"].ToString() : "0" },
                    { "@volume", !string.IsNullOrEmpty(dr["Volume (cbm)"].ToString()) ? dr["Volume (cbm)"].ToString() : "0" },
                    { "@comments", dr["Comments"].ToString() },
                    { "@customerRef", dr["PO Number"].ToString() },
                    { "@pickedBy", dr["Picker"].ToString() }
                };


                DataTable dtID = DataSource.GetTableFrom(@"       
                    insert into tblWhPackedItems (despatchID, orderNo, itemNo, uom, itemDescription, qtyOrdered, qtyPicked, length, width, height, weight, volume, packingID, customerRef, comments, pickedBy )
                    values (@despatchID, @orderNo, @itemNo, @uom, @itemDescription, @qtyOrdered, @qtyPicked, @length, @width, @height, @weight, @volume, @packingID, @customerRef, @comments, @pickedBy )

                ", "NetCRMAU", xargsID);
            }

            return true;
        }


        //Create a new despatch entry and return the generated despatch ID.
        public static string CreateDespatchEntry(string customerNo, string exactNo, string customerName, string exactOrderNo, string courier, 
                string delName, string delAddr1, string delAddr2, string delAddr3, string delSuburb, string delState, string delPostcode, string country,
                bool invoiceWithGoods, bool printPackingSlip, string connoteNo, string specialInstructions, string specialInstructions2, string customerComments, string estimatedFreightCost,
                double totalWeight, double totalDimensions, int noOfCartons, int noOfPallets, DataTable packingDetails, DataTable itemDetails, string orderDate, string shippingDate, string warehouse, int totalCartons, string packedBy, string createdBy)
        {
            //Save Header to tblWhOrderDespatch table
            object[,] xargs = {
                { "@customerNo", customerNo.Trim() },
                { "@exactNo", exactNo.Trim() },
                { "@customerName", customerName.Trim() },
                { "@orderNo", exactOrderNo.Trim() },
                { "@courier", courier.Trim() },
                { "@delName", delName.Trim() },
                { "@delAddr1", delAddr1.Trim() },
                { "@delAddr2", delAddr2.Trim() },
                { "@delAddr3", delAddr3.Trim() },
                { "@delSuburb", delSuburb.Trim() },
                { "@delState", delState.Trim() },
                { "@delPostcode", delPostcode.Trim() },
                { "@country", country.Trim() },
                { "@invoiceWithGoods", (invoiceWithGoods ? "1" : "0") },
                { "@printPackingSlip", (printPackingSlip ? "1" : "0") },
                { "@connoteNo", connoteNo.Trim() },
                { "@estimatedFreightCost", estimatedFreightCost.Trim() },
                { "@totalWeight", totalWeight.ToString().Trim() },
                { "@totalDimensions", totalDimensions.ToString().Trim() },
                { "@noOfCartons", noOfCartons.ToString().Trim() },
                { "@noOfPallets", noOfPallets.ToString().Trim() },
                { "@specialInstructions", specialInstructions.Trim() },
                { "@specialInstructions2", specialInstructions2.Trim() },
                { "@customerComments", customerComments.Trim() },
                { "@orderDate", orderDate.Trim() },
                { "@shippingDate", shippingDate.Trim() },
                { "@warehouse", warehouse.Trim() },
                { "@totalCartons", totalCartons.ToString().Trim() },
                { "@packedBy", packedBy.Trim() },
                { "@createdBy", createdBy.Trim() }
            };

            DataTable dt = DataSource.GetTableFrom(@"
                    insert into tblWhOrderDespatch (cusNo, exactNo, cusName, orderNo, courier, delName, delAddr1, delAddr2, delAddr3, delSuburb, delState, delPostcode, country, invoiceWithGoods, printPackingSlip,
                                                    connoteNo, estimatedFreightCost, totalWeight, totalDimensions,
                                                    specialInstructions, specialInstructions2, customerComments, noOfCartons, noOfPallets, packedBy, orderDate, etaDate, totalCartons, warehouse, createdBy)
                    values (@customerNo, @exactNo, @customerName, @orderNo, @courier, @delName, @delAddr1, @delAddr2, @delAddr3, @delSuburb, @delState, @delPostcode, @country, @invoiceWithGoods, @printPackingSlip, @connoteNo, @estimatedFreightCost,
                            @totalWeight, @totalDimensions, @specialInstructions, @specialInstructions2, @customerComments, @noOfCartons, @noOfPallets, @packedBy, convert(DATETIME, @orderDate), convert(DATETIME, @shippingDate), @totalCartons, @warehouse, @createdBy )

                    select @@IDENTITY

                    insert into tblWhOrderDespatch_Orders (despatchID, orderNo, isPrimary, addedBy)
                    values (@@IDENTITY, @orderNo, 1, @packedBy)

            ", "NetCRMAU", xargs);

            string despatchID = "";
            if (dt != null && dt.Rows.Count > 0)
            {
                despatchID = dt.Rows[0][0].ToString().Trim();
            }
            else despatchID = "0";

            //Store packing details
            foreach (DataRow dr in packingDetails.Rows)
            {

                object[,] xargsPD = {
                    { "@despatchID", despatchID.Trim() },
                    { "@orderNo", exactOrderNo.Trim() },
                    { "@type", dr["Packing Type"].ToString().Trim() },
                    { "@qty", dr["Qty"].ToString().Trim() },
                    { "@length", dr["Length (cm)"].ToString().Trim() },
                    { "@width", dr["Width (cm)"].ToString().Trim() },
                    { "@height", dr["Height (cm)"].ToString().Trim() },
                    { "@weight", dr["Weight (kg)"].ToString().Trim() },
                    { "@connoteNo", dr["connoteNo"].ToString().Trim() },
                    { "@estimatedFreightCost", dr["estimatedFreightCost"].ToString().Trim() },
                    { "@isDG", dr["containsDangerousGoods?"].ToString().Trim() },
                    { "@comments", dr["comments"].ToString().Trim() },
                    { "@addedBy", createdBy.Trim() }
                };


                DataTable dtPD = DataSource.GetTableFrom(@"
                    insert into tblWhPackingDetails (despatchID, orderNo, type, qty, length, width, height, weight, connoteNo, estimatedFreightCost, isDG, comments, addedBy )
                    values (@despatchID, @orderNo, @type, @qty, @length, @width, @height, @weight, @connoteNo, @estimatedFreightCost, @isDG, @comments, @addedBy)

                    select @@IDENTITY
                ", "NetCRMAU", xargsPD);

            }

            //Store Picked Item Details
            foreach (DataRow dr in itemDetails.Rows)
            {
                object[,] xargsID = {
                    { "@despatchID", despatchID.Trim() },
                    { "@orderNo", exactOrderNo.Trim() },
                    { "@itemNo", dr["Item No"].ToString().Trim() },
                    { "@uom", dr["UOM"].ToString() },
                    { "@itemDescription", dr["Item Description"].ToString() },
                    { "@qtyOrdered", dr["Qty Ordered"].ToString() },
                    { "@qtyPicked", dr["Qty Picked"].ToString() },
                    { "@length", !string.IsNullOrEmpty(dr["Length (cm)"].ToString()) ? dr["Length (cm)"].ToString() : "0" },
                    { "@width", !string.IsNullOrEmpty(dr["Width (cm)"].ToString()) ? dr["Width (cm)"].ToString() : "0" },
                    { "@height", !string.IsNullOrEmpty(dr["Height (cm)"].ToString()) ? dr["Height (cm)"].ToString() : "0" },
                    { "@weight", !string.IsNullOrEmpty(dr["Unit Weight (kg)"].ToString()) ? dr["Unit Weight (kg)"].ToString() : "0" },
                    { "@volume", !string.IsNullOrEmpty(dr["Volume (cbm)"].ToString()) ? dr["Volume (cbm)"].ToString() : "0" },
                    { "@packingID", dr["Carton/Pallet ID"].ToString() },
                    { "@comments", dr["Comments"].ToString() },
                    { "@pickedBy", dr["Picker"].ToString() },
                    { "@customerRef", dr["PO Number"].ToString() },
                    { "@modifiedBy", createdBy.Trim() }
                };


                DataTable dtID = DataSource.GetTableFrom(@"
                    insert into tblWhPackedItems (despatchID, orderNo, itemNo, uom, itemDescription, qtyOrdered, qtyPicked, length, width, height, weight, volume, packingID, customerRef, comments, pickedBy, modifiedBy )
                    values (@despatchID, @orderNo, @itemNo, @uom, @itemDescription, @qtyOrdered, @qtyPicked, @length, @width, @height, @weight, @volume, @packingID, @customerRef, @comments, @pickedBy, @modifiedBy )

                ", "NetCRMAU", xargsID);
            }

            return despatchID;
        }





        //Validate the sales order
        public static bool ValidateOrder(string exactOrderNo)
        {
            return false;
        }

        //Update the order's status
        public static bool UpdateOrderStatus(string exactOrderNo, string application, string status, string loginName)
        {

            object[,] xargs = {
                {"@orderNo", exactOrderNo.Trim()},
                {"@status", status.Trim()},
                {"@application", application},
                {"@loginName", loginName}
            };

            String SQL = @"
            declare @NOrderNo varchar(20)
            select @NOrderNo = ord_no from oeordhdr_sql where ExactOrderNo = @orderNo
            exec UpdateOrderStatus @NOrderNo, @status, @application, @loginName";

            string result = (string)DataSource.GetValue(SQL, "NetCRMAU", xargs);

            if (result == "Success") return true;

            return false;
        }





        public static DataTable GetDangerousGoodsList(string despatchID, string packingID)
        {
            object[,] xargs = {
                {"@despatchID", despatchID.Trim()},
                {"@packingID", packingID.Trim()}
            };

            DataTable dt = DataSource.GetTableFrom(@"
                select distinct cast(case when di.itemNo is null then 0 else 1 end as bit) as [Add/Remove], i.orderNo,  i.qtyPicked as Qty, i.itemNo as [Item No], i.itemDescription as [Item Description], i.qtyPicked * i.weight as [Total Weight],
                    dg.UN_no as [UN Number], dg.Proper_Shipping_name as [Proper Shipping Name], dg.Class
                from tblWhPackedItems i
                inner join tblDangerousGoods dg
	                on (i.itemNo = dg.Item_no)
                left join tblWhPackingDetailsDangerousItems di
	                on (i.itemNo = di.itemNo and i.despatchID = di.despatchID and i.orderNo = di.orderNo and di.packingID = @packingID)
                where i.despatchID = @despatchID
                order by i.itemNo
            ", "NetCRMAU", xargs);

            return dt;
        }


        //Update the dangerous goods list
        public static void UpdateDangerousGoodsList(string despatchID, string packingID, DataTable itemList , string loginName)
        {
            if (DeleteDangerousGoodsList(despatchID, packingID))
            {
                    foreach (DataRow dr in itemList.Rows)
                    {

                        if (string.IsNullOrEmpty(dr["Total Weight"].ToString()) && dr["Add/Remove"].ToString().Trim() == "True")
                        {
                            MessageBox.Show("Must enter weight!");
                            return;
                        }

                        if (dr["Add/Remove"].ToString().Trim() == "True") 
                        {
                                                    object[,] xargs = {
                            {"@despatchID", despatchID.Trim()},
                            {"@orderNo", dr["orderNo"].ToString().Trim()},
                            {"@packingID", packingID.Trim()},
                            {"@itemNo", dr["Item No"].ToString().Trim()},
                            {"@itemDescription", dr["Item Description"].ToString().Trim()},
                            {"@weight", dr["Total Weight"].ToString().Trim()},
                            {"@qty", dr["Qty"].ToString().Trim()},
                            {"@UNNumber", dr["UN Number"].ToString().Trim()},
                            {"@properShippingName", dr["Proper Shipping Name"].ToString().Trim()},
                            {"@DGClass", dr["Class"].ToString().Trim()},
                            {"@modifiedBy", loginName}
                        };

                        DataSource.ExecuteNonQuery(@"
                            insert into tblWhPackingDetailsDangerousItems (despatchID, orderNo, packingID, itemNo, itemDescription, weight, qty, UNNumber, DGClass, proper_shipping_name, modifiedBy)
                            values (@despatchID, @orderNo, @packingID, @itemNo, @itemDescription, @weight, @qty, @UNNumber, @DGClass, @properShippingName, @modifiedBy)
                        ", "NetCRMAU", xargs);
                        }
                    }
                
            }

            int noOfDG = GetNoOfDangerousGoods(despatchID, packingID);

            object[,] xargs1 = {
                {"@despatchID", despatchID},
                {"@packingID", packingID},
            };

            if (noOfDG > 0)
            {
                DataSource.ExecuteNonQuery(@"
                    update tblWhPackingDetails
                    set isDG = 1
                    where despatchID = @despatchID and id = @packingID
                ", "NetCRMAU", xargs1);
            }
            else if (noOfDG == 0)
            {
                DataSource.ExecuteNonQuery(@"
                    update tblWhPackingDetails
                    set isDG = 0
                    where despatchID = @despatchID and id = @packingID
                ", "NetCRMAU", xargs1);
            }
            
        }

        public static void VerifyPackingContainsDG(string despatchID, string packingID)
        {
            int noOfDG = GetNoOfDangerousGoods(despatchID, packingID);

            object[,] xargs1 = {
                {"@despatchID", despatchID},
                {"@packingID", packingID},
            };

            if (noOfDG > 0)
            {
                DataSource.ExecuteNonQuery(@"
                    update tblWhPackingDetails
                    set isDG = 1
                    where despatchID = @despatchID and id = @packingID
                ", "NetCRMAU", xargs1);
            }
            else if (noOfDG == 0)
            {
                DataSource.ExecuteNonQuery(@"
                    update tblWhPackingDetails
                    set isDG = 0
                    where despatchID = @despatchID and id = @packingID
                ", "NetCRMAU", xargs1);
            }
        }

        //Delete dangerous goods list
        public static bool DeleteDangerousGoodsList(string despatchID, string packingID)
        {
            try
            {
                DataSource.ExecuteNonQuery(@"
                    delete from tblWhPackingDetailsDangerousItems
                    where despatchID = @despatchID and packingID = @packingID
                ", "NetCRMAU", new object[,] { { "@despatchID", despatchID.Trim() }, { "@packingID", packingID.Trim() } });

                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }


        //Get No of Dangerous goods per despatch (with/without packingID)
        public static int GetNoOfDangerousGoods(string despatchID, string packingID)
        {
            try
            {
                object[,] xargs = {
                    {"@despatchID", despatchID },
                    {"@packingID", packingID }
                };

                int result = (int)DataSource.GetValue(@"
                    select count(*) as NoOfDGs
                    from tblWhPackingDetailsDangerousItems
                    where despatchID = @despatchID and (packingID = @packingID or @packingID = '-1')
                ", "NetCRMAU", xargs);

                return result;
            }
            catch (Exception e)
            {
                return -1;
            }
        }


        //Get No of Dangerous goods per despatch
        public static bool verifyDangerousGood(string itemNo)
        {
            try
            {
                object[,] xargs = {
                    {"@itemNo", itemNo }
                };

                DataTable dt = DataSource.GetTableFrom(@"
                    select 1 
                    from tblDangerousGoods
                    where item_no = @itemNo
                ", "NetCRMAU", xargs);

                if (dt != null && dt.Rows.Count == 1)
                    if (dt.Rows[0][0].ToString().Trim() == "1")
                        return true;

                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        //Get all orders that are ready to be invoiced. The packers have now sent the order to the dispatch area and all that is left is to invoice.
        public static DataTable GetAllReadyToInvoiceOrders()
        {

            DataTable dt = DataSource.GetTableFrom(@"
                    select orderNo, despatchStatus as [Status], invoiceNo, orderDate, ETADate, invoiceWithGoods
                    from tblWhOrderDespatch
                    where (despatchStatus = 'Ready to Despatch' or despatchStatus = 'Ready to Invoice')
                            and (invoiceNo is null or invoiceNo = '')  
            ", "NetCRMAU");


            return dt;
        }


        //Get all despatches per order
        public static DataTable GetAllDespatches(string exactOrderNo)
        {
            DataTable dt = DataSource.GetTableFrom(@"
                select despatchID, invoiceNo, ts as dateEntered, addedBy
                from tblWhOrderDespatch_Orders
                where orderNo = @orderNo
	            order by dateEntered desc
            ", "NetCRMAU", new object[,] {{"@orderNo", exactOrderNo.Trim()}});

            return dt;
        }

        //Get Warehouse of Packer
        public static string GetWarehouseOfPacker(string packer)
        {
            try
            {
                SqlConnection con = DataSource.getConn("NetCRMAU");
                
                string warehouse = (string)DataSource.GetValue(@"
                    select Office from [User] where usr = @packer
                ", con, new object[,] { { "@packer", packer.Trim() } });

                con.Close();
                con.Dispose();

                if (string.IsNullOrEmpty(warehouse)) return "R";

                return warehouse;

            }
            catch { return "R"; }

        }

        //Check if user has permission to authorise despatch orders
        public static bool CheckUserCanAuthoriseDespatch(string user, string password)
        {
            string resultUser = (string)DataSource.GetValue(@"
                select u.usr 
                from [User] u
                left join UserPermission p
	                on (u.usr = p.usr)
                where u.usr = @user and u.pass = @password
	                and p.permission = 'CanAuthoriseOrderDespatch'
            ", "NetCRMAU", new object[,] { { "@user", user.ToUpper().Trim() }, { "@password", password.Trim() } });

            if (resultUser.ToUpper() == user.ToUpper().Trim()) return true;

            return false;
        }

        public static bool CheckDespatchAuthorised(string despatchID)
        {
            string resultDespatchID = (string)DataSource.GetValue(@"
                select top 1 isnull(cast(despatchID as varchar), '')
                from tblWhOrderDespatch_Authorise
                where despatchID = @despatchID
            ", "NetCRMAU", new object[,] { { "@despatchID", despatchID.Trim() } });

            if (resultDespatchID.Trim() == despatchID.Trim()) return true;

            return false;
        }

        public static bool VerifyOrderHasDespatchEntry(string exactOrderNo, out string despatchID)
        {
            despatchID = "-1";
            DataTable dt = DataSource.GetTableFrom(@"
                select top 1 o.despatchID
                from tblWhOrderDespatch_Orders o
				left join tblWhOrderDespatch h
					on (o.despatchID = h.id)
                where o.orderNo = @orderNo and o.invoiceNo is null and h.sentToOE = 0 and cast(h.ts as date) = cast(getdate() as date)
                order by o.despatchID desc
            ", "NetCRMAU", new object[,] { { "@orderNo", exactOrderNo.Trim() } });

            if (dt != null && dt.Rows.Count > 0)
            {
                despatchID = dt.Rows[0]["despatchID"].ToString().Trim();
                if (despatchID != "-1" || !string.IsNullOrEmpty(despatchID))
                    return true;
                else
                    return false;
            }

            return false;
        }


        //Update the order's status
        public static bool UpdateOrderStatusForDespatch(string despatchID, string application, string status, string loginName)
        {

            object[,] xargs = {
                {"@despatchID", despatchID.Trim()},
                {"@status", status.Trim()},
                {"@application", application},
                {"@loginName", loginName}
            };

            String SQL = @"
                DECLARE @orderNo varchar(20)

                DECLARE db_cursor CURSOR FOR
	                select orderNo from tblWhOrderDespatch_Orders
	                where despatchID = @despatchID

                OPEN db_cursor
                FETCH NEXT FROM db_cursor INTO @orderNo;
	                WHILE @@FETCH_STATUS = 0   
		                BEGIN  
			                declare @NOrderNo varchar(20)
                            select @NOrderNo = ord_no from oeordhdr_sql where ExactOrderNo = @orderNo
                            exec UpdateOrderStatus @NOrderNo, @status, @application, @loginName

			                FETCH NEXT FROM db_cursor INTO @orderNo;
		                END

                CLOSE db_cursor   
                DEALLOCATE db_cursor
            ";

            string result = (string)DataSource.GetValue(SQL, "NetCRMAU", xargs);

            if (result == "Success") return true;

            return false;

            //return true;

        }


        //Update All Orders In Exact for Despatch Entry. Just before the order is fulfilled and invoiced, need to fill in some entries in the freefield columns in orkrg
        public static bool UpdateSalesOrderInExactForDespatch(string despatchID, string courier, string connoteNo,
                                                int noOfTotalCartons, int noOfPallets, double totalWeight, double totalDimensions)
        {
            object[,] xargs = {
                { "@despatchID", despatchID.Trim() },
                { "@courier", courier.Trim() },
                { "@connoteNo", connoteNo.Trim() },
                { "@noOfTotalCartons", (noOfTotalCartons != 0 ? noOfTotalCartons.ToString().Trim() : "") },
                { "@noOfPallets", (noOfPallets != 0 ? noOfPallets.ToString().Trim() : "") },
                { "@totalWeight", totalWeight.ToString().Trim() },
                { "@totalDimensions", totalDimensions.ToString().Trim() + "CBM" }
            };

            try
            {

                DataTable dt = DataSource.GetTableFrom(@"
                                        update orkrg
                                        set levwijze = @courier, 
                                            freefield3 = @connoteNo,
                                            bruto_gew = @totalWeight,
                                            refer3 = @totalDimensions, 
                                            freefield4 = @noOfTotalCartons, 
                                            freefield5 = @noOfPallets
                                        where ltrim(ordernr) in (select orderNo from netcrmau.netcrm.dbo.tblWhOrderDespatch_Orders where despatchID = @despatchID)  
                
                                ", "LivExactAus", xargs);

                return true;
            }
            catch
            {
                return false;
            }

        }


        public static double GetSystemTotalWeight(string despatchID)
        {
            string value = (string)DataSource.GetValue(@"
                select cast(isnull(sum(qtyPicked * weight), 0) as varchar) from tblWhPackedItems
                where despatchID = @despatchID
            ", "NetCRMAU", new object[,] { { "@despatchID", despatchID } });

            double totalWeight = double.Parse(value);

            return totalWeight;
        }

        public static double GetSystemTotalCBM(string despatchID)
        {
            string value = (string)DataSource.GetValue(@"
                select cast(isnull(sum(qtyPicked * volume), 0) as varchar) from tblWhPackedItems
                where despatchID = @despatchID
            ", "NetCRMAU", new object[,] { { "@despatchID", despatchID } });

            double totalCBM = double.Parse(value);

            return totalCBM;
        }

        public static int GetEstimatedCartons(string despatchID)
        {
            int value = (int)DataSource.GetValue(@"
			select isnull(cast(sum(case when qtyPicked = 0 then 0 
							when ((i.uom != 'CT' and c.carton_qty = 0) or c.carton_qty is null ) then 1 
							else case when i.qtyPicked / (case when i.uom = 'CT' then 1 else c.carton_qty end) < 1 then 1
									  else i.qtyPicked / (case when i.uom = 'CT' then 1 else c.carton_qty end)
									end
								end 
					) as int),0) as totalCartons
			from tblWhPackedItems i
			left join IMCUBE_SQL c
				on (i.itemNo = c.item_no)
			left join IMITMIDX_SQL itm
				on (i.itemNo = itm.item_no)
            where despatchID = @despatchID
			group by i.despatchID
            ", "NetCRMAU", new object[,] { { "@despatchID", despatchID } });

            return value;

            //int estimatedCartons;
            //if (Int32.TryParse(value, out estimatedCartons))
            //{
            //    return estimatedCartons;
            //}
            //else
            //    return -1;

        }


        public static void SetAuthorisation(string despatchID, string userName, string reason)
        {
            DataSource.ExecuteNonQuery(@"
                insert into tblWhOrderDespatch_Authorise (despatchID, authorisedBy, reason)
                values (@despatchID, @authorisedBy, @reason)
            ", "NetCRMAU", new object[,]{ { "@despatchID", despatchID }, { "@authorisedBy", userName }, { "@reason", reason } });
        }
    }
}
