﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace OrderDespatcher.Utilities
{
    public class LivCrystalReport
    {

            public static string RunReport(string report)
            {
                string output = "output";
                string schedule = "schedule";
                string runday = "run day";
                string runtime = "run time";
                string subscriber = "subscriber";
                string sourcetype = "sourcetype";
                string parameters = "parameters";
                string source = "sc";

                string[] cmd = report.Split(new string[] { "@@" }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string key in cmd)
                {

                    string[] keys = key.Split(new string[] { "->" }, StringSplitOptions.RemoveEmptyEntries);
                    if (keys.Length > 1)
                    {
                        //Response.Write("<br /> " + key);

                        switch (keys[0].Trim())
                        {
                            case "output": output = keys[1].Trim(); break;
                            case "schedule": schedule = keys[1].Trim(); break;
                            case "run day": runday = keys[1].Trim(); break;
                            case "run time": runtime = keys[1].Trim(); break;
                            case "subscriber": subscriber = keys[1].Trim(); break;
                            case "sourcetype": sourcetype = keys[1].Trim(); break;
                            case "parameters": parameters = keys[1].Trim(); break;
                            case "source": source = keys[1].Trim(); break;

                            default: break;

                        }
                    }

                }

                string filename = "rpt-" + DateTime.Now.ToString("yyyyMMddHHmmss");
                string pdffilepath = @"\\livdoc9\Mydocs\Report\" + filename + ".pdf";
                string xlsfilepath = @"\\livdoc9\Mydocs\Report\xls\" + filename + ".xls";
                string downloadpath = @"file://livdoc9/mydocs/report/" + filename + ".pdf";

                if (output == "Excel")
                {
                    downloadpath = @"file://livdoc9/mydocs/report/xls/" + filename + ".xls";
                }

                if (sourcetype == "sql")
                {
                    //string storedProcedure = source;

                    //DataTable dtReport = Livingstone.Library.DataSource.GetTableFrom("exec dbo." + storedProcedure, "NetCRMAU");

                    //SharedClass.ExportDataTableToExcel(dtReport, xlsfilepath);


                }
                else
                {
                    string reportPath = source;

                    // create and load new report document
                    using (ReportDocument cryRpt = new ReportDocument())
                    {
                        try
                        {
                            cryRpt.Load(source);


                        }
                        catch
                        {

                        }
                        //Response.Write(dt.Rows[0]["dtxt"].ToString());

                        // Set Parameter
                        string[] parmstrings = parameters.Split(',');
                        foreach (string parstring in parmstrings)
                        {
                            string[] parm = parstring.Split('=');
                            if (parm.Length > 1)
                            {
                                //Response.Write("<br/ >" + parm[0] + " :" + parm[1]);
                                string par = parm[1].Trim();
                                if (parm[1].Trim().StartsWith("@"))
                                {


                                }
                                else
                                {
                                    par = parm[1].Trim();

                                }
                                string reportinputparameter = parm[0].Trim().Replace("%20", " ");


                                cryRpt.SetParameterValue(reportinputparameter, par);
                            }
                        }
                        //Set PDF export options
                        PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                        DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                        CrDiskFileDestinationOptions.DiskFileName = pdffilepath;

                        if (output == "Excel")
                        {
                            cryRpt.ExportToDisk(ExportFormatType.ExcelRecord, xlsfilepath);
                        }
                        else
                        {
                            ExportOptions CrExportOptions;
                            CrExportOptions = cryRpt.ExportOptions;
                            {

                                CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile; // send to disk
                                CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat; //PDF
                                CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions; // save to filename set above
                                CrExportOptions.FormatOptions = CrFormatTypeOptions; // default PdfRtfWordFormatOptions
                            }

                            // export to pdf
                            try
                            {
                                cryRpt.Export();

                            }
                            catch (Exception e)
                            {

                            }
                        }
                    }
                }



                return downloadpath;
            }

        }
    
}
