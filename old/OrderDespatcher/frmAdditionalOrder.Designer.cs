﻿namespace OrderDespatcher
{
    partial class frmAdditionalOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgrAdditionalOrder = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.gbxAddRemoveOrder = new System.Windows.Forms.GroupBox();
            this.txtDespatchID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAddOrder = new System.Windows.Forms.Button();
            this.txtAddOrder = new System.Windows.Forms.TextBox();
            this.btnRemoveOrder = new System.Windows.Forms.Button();
            this.btnOpen = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgrAdditionalOrder)).BeginInit();
            this.gbxAddRemoveOrder.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgrAdditionalOrder
            // 
            this.dgrAdditionalOrder.AllowUserToAddRows = false;
            this.dgrAdditionalOrder.AllowUserToDeleteRows = false;
            this.dgrAdditionalOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgrAdditionalOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrAdditionalOrder.Location = new System.Drawing.Point(3, 72);
            this.dgrAdditionalOrder.MultiSelect = false;
            this.dgrAdditionalOrder.Name = "dgrAdditionalOrder";
            this.dgrAdditionalOrder.ReadOnly = true;
            this.dgrAdditionalOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrAdditionalOrder.Size = new System.Drawing.Size(367, 290);
            this.dgrAdditionalOrder.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(206, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Order No:";
            // 
            // gbxAddRemoveOrder
            // 
            this.gbxAddRemoveOrder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxAddRemoveOrder.Controls.Add(this.txtDespatchID);
            this.gbxAddRemoveOrder.Controls.Add(this.label2);
            this.gbxAddRemoveOrder.Controls.Add(this.btnAddOrder);
            this.gbxAddRemoveOrder.Controls.Add(this.txtAddOrder);
            this.gbxAddRemoveOrder.Controls.Add(this.label1);
            this.gbxAddRemoveOrder.Location = new System.Drawing.Point(3, 2);
            this.gbxAddRemoveOrder.Name = "gbxAddRemoveOrder";
            this.gbxAddRemoveOrder.Size = new System.Drawing.Size(455, 64);
            this.gbxAddRemoveOrder.TabIndex = 2;
            this.gbxAddRemoveOrder.TabStop = false;
            this.gbxAddRemoveOrder.Text = "Add Order";
            // 
            // txtDespatchID
            // 
            this.txtDespatchID.Location = new System.Drawing.Point(85, 25);
            this.txtDespatchID.Name = "txtDespatchID";
            this.txtDespatchID.ReadOnly = true;
            this.txtDespatchID.Size = new System.Drawing.Size(69, 20);
            this.txtDespatchID.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Despatch ID:";
            // 
            // btnAddOrder
            // 
            this.btnAddOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddOrder.Location = new System.Drawing.Point(371, 23);
            this.btnAddOrder.Name = "btnAddOrder";
            this.btnAddOrder.Size = new System.Drawing.Size(75, 23);
            this.btnAddOrder.TabIndex = 2;
            this.btnAddOrder.Text = "+ Add Order";
            this.btnAddOrder.UseVisualStyleBackColor = true;
            this.btnAddOrder.Click += new System.EventHandler(this.btnAddOrder_Click);
            // 
            // txtAddOrder
            // 
            this.txtAddOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAddOrder.Location = new System.Drawing.Point(265, 25);
            this.txtAddOrder.Name = "txtAddOrder";
            this.txtAddOrder.Size = new System.Drawing.Size(100, 20);
            this.txtAddOrder.TabIndex = 1;
            // 
            // btnRemoveOrder
            // 
            this.btnRemoveOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemoveOrder.Location = new System.Drawing.Point(376, 85);
            this.btnRemoveOrder.Name = "btnRemoveOrder";
            this.btnRemoveOrder.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveOrder.TabIndex = 4;
            this.btnRemoveOrder.Text = "Remove";
            this.btnRemoveOrder.UseVisualStyleBackColor = true;
            this.btnRemoveOrder.Click += new System.EventHandler(this.btnRemoveOrder_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnOpen.Location = new System.Drawing.Point(140, 368);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(97, 36);
            this.btnOpen.TabIndex = 3;
            this.btnOpen.Text = "OK";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // frmAdditionalOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 425);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.btnRemoveOrder);
            this.Controls.Add(this.gbxAddRemoveOrder);
            this.Controls.Add(this.dgrAdditionalOrder);
            this.Name = "frmAdditionalOrder";
            this.Text = "Additional Order";
            ((System.ComponentModel.ISupportInitialize)(this.dgrAdditionalOrder)).EndInit();
            this.gbxAddRemoveOrder.ResumeLayout(false);
            this.gbxAddRemoveOrder.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgrAdditionalOrder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbxAddRemoveOrder;
        private System.Windows.Forms.TextBox txtAddOrder;
        private System.Windows.Forms.TextBox txtDespatchID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAddOrder;
        private System.Windows.Forms.Button btnRemoveOrder;
        private System.Windows.Forms.Button btnOpen;
    }
}