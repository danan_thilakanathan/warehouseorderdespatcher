﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.Configuration;
using System.Globalization;
using System.Threading;
using Livingstone.Library;
using OrderDespatcher.Courier;

namespace OrderDespatcher
{
    public partial class frmOrderProcessing : Form
    {
        public string courierName { get; set; }
        public string customerNo { get; set; }
        public string exactNo { get; set; }
        public string orderNo { get; set; }
        public string despatchID { get; set; }
        public string packedBy { get; set; }
        public bool printPackSlip { get; set; }
        public bool invWithGoods { get; set; }
        public bool updatedOrderStatus { get; set; }
        public int noOfCartons { get; set; }
        public int noOfPallets { get; set; }
        public int noOfTotalCartons { get; set; }
        public double totalWeight { get; set; }
        public double totalDimensions { get; set; }

        public string returnConnoteNo { get; set; }
        public string processType { get; set; }

        public string area { get; set; }
        public string userMessage { get; set; }

        //private AutoResetEvent _workCompleted = new AutoResetEvent(false);

        public frmOrderProcessing()
        {
            InitializeComponent();
        }

        public frmOrderProcessing(string despatchID, string orderNo, string customerNo, string exactNo, string courierName, string packedBy, bool printPackSlip, bool invWithGoods, int noOfCartons, int noOfPallets, int totalCartons, double totalWeight, double totalDimensions)
        {
            InitializeComponent();

            this.despatchID = despatchID;
            this.orderNo = orderNo;
            this.customerNo = customerNo;
            this.exactNo = exactNo;
            this.courierName = courierName;
            this.packedBy = packedBy;
            this.printPackSlip = printPackSlip;
            this.invWithGoods = invWithGoods;
            this.noOfCartons = noOfCartons;
            this.noOfPallets = noOfPallets;
            this.noOfTotalCartons = totalCartons;
            this.totalWeight = totalWeight;
            this.totalDimensions = totalDimensions;
            this.returnConnoteNo = "[NEED TO FIX]";

            this.area = "DESPATCH";
            this.userMessage = "";

            bgwDespatcher.RunWorkerAsync();
        }

        public frmOrderProcessing(string despatchID, string orderNo, string processType, string courierName, int noOfCartons, int noOfPallets, string packedBy)
        {
            InitializeComponent();

            this.processType = processType;

            if (processType == "OE_PRINT_LABEL")
            {
                clbProgress.Items.Clear();
                clbProgress.Items.Add("Printing Shipping Labels");
                clbProgress.Items.Add("Setting Order Status to 6");

                this.despatchID = despatchID;
                this.orderNo = orderNo;
                this.processType = processType;
                this.courierName = courierName;
                this.noOfCartons = noOfCartons;
                this.noOfPallets = noOfPallets;
                this.packedBy = packedBy;

                this.area = "HOLD";
                this.userMessage = "Make sure to follow the usual process of marking the DD with the details. You may also update the details in the system. Once done, stick the shipping labels on the cartons and send to the HOLD area.";

                bgwDespatcher.RunWorkerAsync();

            }
            else if (processType == "UPDATE_CONNOTE")
            {
                clbProgress.Items.Clear();
                clbProgress.Items.Add("Connecting with Courier");
                clbProgress.Items.Add("Updating Connote");

                this.despatchID = despatchID;
                this.orderNo = orderNo;
                this.processType = processType;
                this.packedBy = packedBy;

                this.area = "DESPATCH";
                this.userMessage = "Connote updated.";

                bgwDespatcher.RunWorkerAsync();

            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            bgwDespatcher.CancelAsync();
            //this.DialogResult = DialogResult.Cancel;
            //this.Close();
        }


    

        public void UpdateStatus(string status, string message, int percentage)
        {
            this.pgrProgress.Value = percentage;
            clbProgress.SetItemChecked(GetItemIndex(status), true);
            addLine(message);

        }

        public void addLine(string text)
        {
            txtProgress.AppendText(text);
            txtProgress.AppendText(Environment.NewLine);
        }

        private int GetItemIndex(string item)
        {
            int index = 0;

            foreach (object o in clbProgress.Items)
            {
                if (item == o.ToString())
                {
                    return index;
                }

                index++;
            }

            return -1;
        }

        private void bgwDespatcher_DoWork(object sender, DoWorkEventArgs e)
        {
            //Setup printer
            PrintService printService = new PrintService();

            if (this.processType == "OE_PRINT_LABEL")
            {
                string[] reporterM = { "Printing Shipping Labels", "Shipping labels now being printed" };
                bgwDespatcher.ReportProgress(40, reporterM);

                //Print Shipping Label
                printService.PrintShippingLabel(orderNo, courierName, noOfCartons + noOfPallets);

                //Set Order To Status 6
                OrderDespatchService.UpdateOrderStatusForDespatch(despatchID.Trim(), "Warehouse Order Despatcher", "6", packedBy);
                OrderDespatchService.UpdateDespatchOrderStatus(despatchID.Trim(), "Awaiting Labels");
                OrderDespatchService.SetSendToOE(despatchID.Trim(), 1);

                string[] reporterM1 = { "Setting Order Status to 6", "Order " + orderNo + " validated" };
                bgwDespatcher.ReportProgress(80, reporterM1);

                this.area = "HOLD";
                this.userMessage = "Customer requires invoice with goods. Order Status set to 6. Please move this order to the HOLD area for OE to process.";
           

                return;


            }

            //Check not invoice with goods
            if (this.invWithGoods)
            {
                MessageBox.Show("Customer requires invoice with goods.");
                this.returnConnoteNo = "[NEED INVOICE WITH GOODS]";

                this.area = "HOLD";
                this.userMessage = "Customer requires invoice with goods. Order Status set to 6. Please move this order to the HOLD area for OE to process.";
                
                //Set Order Status to 6
                OrderDespatchService.UpdateOrderStatusForDespatch(despatchID.Trim(), "Warehouse Order Despatcher", "6", packedBy);
                OrderDespatchService.UpdateDespatchOrderStatus(despatchID.Trim(), "Awaiting Labels");
                OrderDespatchService.SetSendToOE(despatchID.Trim(), 1);


                //Print Shipping Label
                printService.PrintShippingLabel(orderNo, courierName, noOfCartons + noOfPallets);

                return;
            }


            //2. Create Courier Service
            CourierService courier;


            if (courierName == "STE")
                courier = new StarTrackCourier();
            else if (courierName == "CAP")
                courier = new CapitalTransportCourier();
            else if (courierName == "COP")
                courier = new CouriersPleaseCourier();
            //else if (courierName == "BSL")
            //    courier = new BlueStarCourier();
            else if (courierName == "AUP")
                courier = new AustraliaPostCourier();
            else
            {
                courier = new UnknownCourier();
            }


            //3. Generate and Print the Connote Label
            string connoteNo = "";


            DataTable otherDetails = new DataTable();


            string[] reporter = { "Validated Order", "Order " + orderNo + " validated" };
            bgwDespatcher.ReportProgress(20, reporter);
            if (bgwDespatcher.CancellationPending)
            {
                // Indicate that the task was canceled.
                e.Cancel = true;
                return;
            }

            string userMessage = "";
            connoteNo = courier.GenerateConnote(despatchID.Trim(), customerNo, exactNo, orderNo, otherDetails, packedBy, ref userMessage);

            this.returnConnoteNo = connoteNo;

            if (connoteNo == null)
            {
                MessageBox.Show("Connote is empty. Please call IT ASAP. Thanks.");
                this.area = "IT";
                this.userMessage = "Please call Danan (IT) on Ext 7385 and try not to close this screen.";
                return;
            }

            if (connoteNo == "[NEED TO FIX]") return;


            if (connoteNo == "[UNSUPPORTED COURIER]" || connoteNo == "[ERROR]" || connoteNo == "[SEND TO OE]")
            {
                this.area = "HOLD";

                if (connoteNo == "[UNSUPPORTED COURIER]")
                    this.userMessage = "Courier not supported. Order Status set to 6. Please move this order to the HOLD area for OE to process.";
                else if (connoteNo == "[ERROR]")
                    this.userMessage = "General Error! Order Status set to 6. Please move this order to the HOLD area for OE to process.";
                else if (connoteNo == "[SEND TO OE]")
                    this.userMessage = "Send this order to OE to process manually. Order Status set to 6. Please move this order to the HOLD area for OE to process.";


                //Set Order Status to 6
                OrderDespatchService.UpdateOrderStatusForDespatch(despatchID.Trim(), "Warehouse Order Despatcher", "6", packedBy);
                OrderDespatchService.UpdateDespatchOrderStatus(despatchID.Trim(), "Awaiting Labels");
                OrderDespatchService.SetSendToOE(despatchID.Trim(), 1);

                //Print Shipping Label
                printService.PrintShippingLabel(orderNo, courierName, noOfCartons + noOfPallets);

                return;

            }


            //4. Update Connote No to DB
            OrderDespatchService.UpdateConnoteNo(despatchID.Trim(), connoteNo);

            //5. Update Despatch Status to 'Ready to Despatch'
            OrderDespatchService.UpdateDespatchOrderStatus(despatchID.Trim(), "Ready to Despatch");


            this.returnConnoteNo = connoteNo;
            string[] reporter1 = { "Generated and Printed Courier Labels", "Connote generated: " + connoteNo };
            bgwDespatcher.ReportProgress(60, reporter1);


            //6. Update Order Status to 7
            updatedOrderStatus = OrderDespatchService.UpdateOrderStatusForDespatch(despatchID.Trim(), "Warehouse Order Despatcher", "7", packedBy);


            //if (updatedOrderStatus)
            //{
                string[] reporter2 = { "Updated Order Status", "Order Status updated to 7" };
                bgwDespatcher.ReportProgress(80, reporter2);

                

                //8. If contains DG, Print DG form for orderNo
                bool dgPrinted = printService.PrintDangerousGoodsForm(despatchID.Trim());

                //printService.PrintDangerousGoodsDeclaration(txtOrderNo.Text, despatchID.Trim());



                //9. Print Packing Slip
                if (printPackSlip)
                    printService.PrintPackingSlip(despatchID);
                //PrintService.PrintPackingSlip(txtOrderNo.Text, despatchID);

                //7. Update order in Exact to include courier and freefield info
                OrderDespatchService.UpdateSalesOrderInExactForDespatch(despatchID.Trim(), courierName, connoteNo, this.noOfTotalCartons, this.noOfPallets, this.totalWeight, this.totalDimensions);



                string[] reporter3 = { "Printed Packing Slip", "Packing slip now printed!" };
                bgwDespatcher.ReportProgress(100, reporter3);

                this.userMessage = "Order Processed. Order Status Updated to 7. \nPlease make sure to write the connote number on the DD.\nPlease send order to DESPATCH area.";


            //}
            //else userMessage += "\nOrder Status could not update.";

                return;

        }

        private void bgwDespatcher_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            string[] results = (string[])e.UserState;
            string status = results[0];
            string message = results[1];
            UpdateStatus(status, message, e.ProgressPercentage);
        }

        private void bgwDespatcher_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.DialogResult = DialogResult.OK;

            this.Close();
        }

        private void frmOrderProcessing_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (bgwDespatcher.IsBusy)
            {
                bgwDespatcher.CancelAsync();
            }
            this.DialogResult = DialogResult.Cancel;
            //e.Cancel = true;
            //this.Close();
        }

    }
}
