﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OrderDespatcher.Courier
{
    public partial class frmCAPQuestions : Form
    {
        public string closingTime { get; set; }

        public bool moreThan4Hours { get; set; }
        public bool advanceMoreThan4Hours { get; set; }
        public bool isUrgent { get; set; }
        public bool setAdvanceBooking { get; set; }

        public DateTime advanceBookingDate { get; set; }

        public frmCAPQuestions()
        {
            InitializeComponent();

            dtpNextAvailableDate.Format = DateTimePickerFormat.Custom;
            dtpNextAvailableDate.CustomFormat = "dd/MM/yyyy hh:mm tt"; // Only use hours and minutes

            //dtpNextAvailableDate.ShowUpDown = true;

            //this.isUrgent = false;
            //this.setAdvanceBooking = false;
            //this.result = "started";
            DateTime dateNow = DateTime.Now;
            string customerDefaultCloseTime = "17:00 PM";
            DateTime endTime = DateTime.Parse(customerDefaultCloseTime);


            populateDropdown(cboCustomerCloseToday, RoundUp(dateNow, TimeSpan.FromMinutes(15)), endTime, new TimeSpan(0, 15, 0));
            cboCustomerCloseToday.Text = customerDefaultCloseTime;
            this.closingTime = cboCustomerCloseToday.Text;

            populateDropdown(cboCustomerCloseAdvanceTime, RoundUp(DateTime.Parse("08:15 AM"), TimeSpan.FromMinutes(15)), endTime, new TimeSpan(0, 15, 0));
            cboCustomerCloseAdvanceTime.Text = customerDefaultCloseTime;

        }

        private void populateDropdown(ComboBox dropdown, DateTime startTime, DateTime endTime, TimeSpan interval)
        {
            dropdown.Items.Clear();

            DateTime time = startTime;

            while (time <= endTime)
            {
                dropdown.Items.Add(time.ToString("HH:mm tt"));
                time = time.Add(interval);
            }
        }

        DateTime RoundUp(DateTime dt, TimeSpan d)
        {
            return new DateTime(((dt.Ticks + d.Ticks - 1) / d.Ticks) * d.Ticks);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();

        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.advanceBookingDate = dtpNextAvailableDate.Value;
            //TESTING ONLY: MessageBox.Show("More than 4 hours? " + this.moreThan4Hours.ToString() + " Is urgent? " + this.isUrgent.ToString() + " AdvanceBooking? " + this.setAdvanceBooking.ToString() + " BookingDate:" + advanceBookingDate.ToShortDateString() );
            this.closingTime = cboCustomerCloseToday.Text;
            
            this.advanceMoreThan4Hours = true;
            if (gbxQuestion4.Visible)
            {
                this.closingTime = cboCustomerCloseAdvanceTime.Text;

                DateTime advanceClosingTime = DateTime.ParseExact(cboCustomerCloseAdvanceTime.Text, "HH:mm tt",
                    CultureInfo.InvariantCulture);

                if ((advanceClosingTime - DateTime.ParseExact(dtpNextAvailableDate.Value.ToString("HH:mm tt"), "HH:mm tt", CultureInfo.InvariantCulture)).TotalMinutes < 240 /*4 hours*/)
                {
                    this.advanceMoreThan4Hours = false;
                }
            }

            //MessageBox.Show("Advnace booking enough time? " + advanceMoreThan4Hours);

            this.Close();
        }

        private void rdoYes_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoYes.Checked)
            {
                this.isUrgent = true;
                this.setAdvanceBooking = false;
                gbxQuestion3.Visible = false;
                gbxQuestion4.Visible = false;
                btnSubmit.Visible = true;
            }
        }

        private void rdoNo_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoNo.Checked)
            {
                dtpNextAvailableDate.Value = DateTime.Now.AddDays(1);
                this.setAdvanceBooking = true;
                this.isUrgent = false;
                dtpNextAvailableDate.Value = new DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 8, 15, 0);

                gbxQuestion3.Visible = true;
                gbxQuestion4.Visible = true;
                btnSubmit.Visible = true;
            }
        }

        private void cboCustomerCloseToday_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateTime closingTime = DateTime.ParseExact(cboCustomerCloseToday.Text, "HH:mm tt",
                                        CultureInfo.InvariantCulture);
            //DateTime closingTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, cboCustomerCloseToday.Text);
            if ((closingTime - DateTime.Now).TotalMinutes > 240 /*4 hours*/)
            {
                this.moreThan4Hours = true;
                gbxQuestion2.Visible = false;
                gbxQuestion3.Visible = false;
                gbxQuestion4.Visible = false;
            }
            else
            {
                this.moreThan4Hours = false;
                btnSubmit.Visible = false;
                gbxQuestion2.Visible = true;
                if (rdoNo.Checked)
                {
                    this.isUrgent = false;
                    this.setAdvanceBooking = true;
                    dtpNextAvailableDate.Value = new DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 8, 15, 0);

                    gbxQuestion3.Visible = true;
                    gbxQuestion4.Visible = true;
                }
                else if (rdoYes.Checked)
                {
                    this.isUrgent = true;
                    this.setAdvanceBooking = false;
                    gbxQuestion3.Visible = false;
                    gbxQuestion4.Visible = false;
                }
                else
                {
                    this.isUrgent = false;
                    this.setAdvanceBooking = false;
                    btnSubmit.Visible = false;
                    return;
                }
            }

            btnSubmit.Visible = true;
        }

    }
}
