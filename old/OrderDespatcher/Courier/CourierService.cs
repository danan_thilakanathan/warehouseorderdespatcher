﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace OrderDespatcher.Courier
{
    public abstract class CourierService
    {

        public abstract string GenerateConnote(string despatchID, string cusNo, string exactNo, string exactOrderNo, DataTable otherDetails, string userName, ref string userMessage);

        public abstract void PrintConnote(string despatchID, string connoteNo, int noOfLabels, ref string userMessage);

        public abstract string GenerateManifest(string userName);

        public abstract string PrintManifest(string userName);
    }


}
