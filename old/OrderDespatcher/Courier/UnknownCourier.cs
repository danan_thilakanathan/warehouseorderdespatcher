﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;

namespace OrderDespatcher.Courier
{
    public class UnknownCourier : CourierService
    {
        public override string GenerateConnote(string despatchID, string cusNo, string exactNo, string exactOrderNo, DataTable otherDetails, string userName, ref string userMessage)
        {
            return "[UNSUPPORTED COURIER]";
        }

        public override void PrintConnote(string despatchID, string connoteNo, int noOfLabels, ref string userMessage)
        {
            MessageBox.Show("Unknown Courier! Please ensure you typed in the correct courier code. It should be the NetCRM courier code.", "Unknown Courier!");
        }

        public override string GenerateManifest(string userName)
        {
            return "Cannot Generate Manifest for unsupported courier. Please check that you have entered a valid courier";
        }

        public override string PrintManifest(string userName)
        {
            return "Cannot Generate Manifest for unsupported courier. Please check that you have entered a valid courier";
        }
    }
}
