﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Livingstone.Library;

namespace OrderDespatcher.Courier
{
    public partial class frmSTEOrders : Form
    {

        public string selectedOrderID { get; set; }

        public frmSTEOrders()
        {
            InitializeComponent();

            DataTable dt = DataSource.GetTableFrom(@"
                select top 100 *
	            from tblWhAusPostDespatchManifest
	            order by ts desc
            ", "NetCRMAU");

            dgrOrders.DataSource = dt;
        }

        public frmSTEOrders(string accountNo)
        {
            InitializeComponent();

            DataTable dt = DataSource.GetTableFrom(@"
                select *
	            from tblWhAusPostDespatchManifest
                where account = @accountNo
	            order by ts desc
            ", "NetCRMAU", new object[,] {{"@accountNo", accountNo}});

            dgrOrders.DataSource = dt;
        }

        private void btnPrinterSettings_Click(object sender, EventArgs e)
        {
            frmPrintSettings printerSettings = new frmPrintSettings();
            printerSettings.ShowDialog();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            string orderID = dgrOrders.CurrentRow.Cells["orderID"].Value.ToString().Trim();

            this.selectedOrderID = orderID;

            this.DialogResult = DialogResult.OK;
            this.Close();


        }
    }
}
