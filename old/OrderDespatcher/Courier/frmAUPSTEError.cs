﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
using Livingstone.Library;

namespace OrderDespatcher.Courier
{
    public partial class frmAUPSTEError : Form
    {
        public frmAUPSTEError()
        {
            InitializeComponent();
        }

        public frmAUPSTEError(string error, DataTable errorList)
        {
            InitializeComponent();

            lblError.Text = error;
            dgrErrorList.DataSource = errorList;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            dgrErrorList.SelectAll();
            DataObject dataObj = dgrErrorList.GetClipboardContent();
            Clipboard.SetDataObject(dataObj, true);
        }

        private void btnSendToIT_Click(object sender, EventArgs e)
        {
            try
            {
                MailMessage msg = new MailMessage();

                msg.From = new MailAddress("OrderDespatcher@livingstone.com.au");

                msg.To.Add("danan_thilakanathan@livingstone.com.au");


                msg.Subject = "STE AUP Error";


                string mailBody = "<table width='100%' style='border:Solid 1px Black;'>";

                foreach (DataGridViewRow row in dgrErrorList.Rows)
                {
                    mailBody += "<tr>";
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        mailBody += "<td stlye='color:blue;'>" + cell.Value + "</td>";
                    }
                    mailBody += "</tr>";
                }
                mailBody += "</table>";

                //your rest of the original code
                msg.Body = mailBody;

                SmtpClient client = new SmtpClient(ClsConfig.SmtpServer);
                client.Credentials = new System.Net.NetworkCredential(ClsConfig.SmtpServerUserName, ClsConfig.SmtpServerPassword);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(msg);
            }
            catch { }
        }
    }
}
