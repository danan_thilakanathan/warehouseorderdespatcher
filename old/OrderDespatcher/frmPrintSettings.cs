﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OrderDespatcher
{
    public partial class frmPrintSettings : Form
    {
        public frmPrintSettings()
        {
            InitializeComponent();

            GetPrinterSettings();
        }

        private void GetPrinterSettings()
        {
            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                cboA4Printer.Items.Add(printer);
                cboLabelPrinter.Items.Add(printer);
            }

            PrintService ps = new PrintService();
            cboA4Printer.Text = ps.A4PrinterName;
            cboLabelPrinter.Text = ps.labelPrinterName;


        }

        private void SavePrinterSettings()
        {
            PrintService ps = new PrintService();

            ps.SavePrinterPreferences(cboLabelPrinter.Text, cboA4Printer.Text, cboA4Printer.Text);
        }

        private void btnSetPrinter_Click(object sender, EventArgs e)
        {
            SavePrinterSettings();
            this.DialogResult = DialogResult.OK;

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
