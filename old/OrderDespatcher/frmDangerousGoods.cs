﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Livingstone.Library;

namespace OrderDespatcher
{
    public partial class frmDangerousGoods : Form
    {
        private string loginName = "UNKNOWN";

        public frmDangerousGoods()
        {
            InitializeComponent();
        }


        public frmDangerousGoods(string despatchID, string packingID, string loginName)
        {
            InitializeComponent();

            txtDespatchID.Text = despatchID.Trim();
            txtPackingID.Text = packingID.Trim();

            this.loginName = loginName;

            dgrDGItems.DataSource = OrderDespatchService.GetDangerousGoodsList(despatchID, packingID);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            OrderDespatchService.VerifyPackingContainsDG(txtDespatchID.Text.Trim(), txtPackingID.Text.Trim());

            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dr in dgrDGItems.Rows)
            {
                if (dr.IsNewRow) break;

                if (string.IsNullOrEmpty(dr.Cells["Total Weight"].Value.ToString()) && dr.Cells["Add/Remove"].Value.ToString().Trim() == "True")
                {
                    MessageBox.Show("Must enter weight!");
                    return;
                }
            }

            OrderDespatchService.UpdateDangerousGoodsList(txtDespatchID.Text.Trim(), txtPackingID.Text.Trim(), (DataTable)dgrDGItems.DataSource, loginName);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void dgrDGItems_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

            //Weight modification
            if (dgrDGItems.Columns[dgrDGItems.CurrentCell.ColumnIndex].Name == "Total Weight" && string.IsNullOrEmpty(dgrDGItems.CurrentRow.Cells["Total Weight"].Value.ToString()))
            {
                dgrDGItems.CurrentRow.Cells["Add/Remove"].Value = false;
            }
            if (dgrDGItems.Columns[dgrDGItems.CurrentCell.ColumnIndex].Name == "Total Weight" && !string.IsNullOrEmpty(dgrDGItems.CurrentRow.Cells["Total Weight"].Value.ToString()))
            {
                dgrDGItems.CurrentRow.Cells["Add/Remove"].Value = true;
            }

            //Add/Remove Modification
            //if (dgrDGItems.Columns[dgrDGItems.CurrentCell.ColumnIndex].Name == "Add/Remove" && dgrDGItems.CurrentRow.Cells["Add/Remove"].Value.ToString() == "False")
            //{
            //    dgrDGItems.CurrentRow.Cells["Total Weight"].Value = string.Empty;
            //}

        }
    }
}
