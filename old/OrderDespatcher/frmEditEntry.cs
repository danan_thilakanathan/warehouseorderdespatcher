﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OrderDespatcher
{
    public partial class frmEditEntry : Form
    {
        private string orderNo { get; set; }
        private string courier { get; set; }
        public bool isDG { get; set; }
        private string estCostString { get; set; }
        private string packedBy { get; set; } 
        
        public frmEditEntry()
        {
            InitializeComponent();
        }


        public frmEditEntry(string orderNo, string courier, string despatchID, string packingID, string packingType, int qty,
                double length, double width, double height, double weight,
                string connoteNo, double estCost, bool isDG, int cartonsPerPallet, string comments, string packedBy)
        {
            InitializeComponent();

            cboPackingType.Items.Add("CARTON");
            cboPackingType.Items.Add("PALLET");
            cboPackingType.SelectedIndex = 0;

            this.orderNo = orderNo;
            this.courier = courier;
            txtDespatchID.Text = despatchID;
            txtPackingID.Text = packingID;
            cboPackingType.Text = packingType;
            nmrPackingQty.Text = qty.ToString();
            txtPackingLength.Text = length.ToString();
            txtPackingWidth.Text = width.ToString();
            txtPackingHeight.Text = height.ToString();
            txtPackingWeight.Text = weight.ToString();
            txtPackingConnoteNo.Text = connoteNo;

            txtPackingEstCost.Text = estCost.ToString();
            this. estCostString = txtPackingEstCost.Text;
            if (this.estCostString == "") this.estCostString = "0";

            chkPackingDG.Checked = isDG;
            this.isDG = isDG;
            nmrPackingCartonsPerPallet.Text = cartonsPerPallet.ToString();
            txtPackingComments.Text = comments;
            this.packedBy = packedBy;

        }

        private bool ValidatePackingEntry()
        {

            if (string.IsNullOrEmpty(this.orderNo) || string.IsNullOrEmpty(this.courier))
            {
                MessageBox.Show("Please input DD order no and courier code.");
                return false;
            }

            int qty = 0;
            double length = 0;
            double width = 0;
            double height = 0;
            double weight = 0;
            double estCost = 0.00;

            int cartonsPerPallet = 1;

            //Qty Validation
            if (!Int32.TryParse(nmrPackingQty.Value.ToString(), out qty))
            {
                MessageBox.Show("Invalid qty value. Please ensure it is a proper number.");
                return false;
            }

            if (qty <= 0)
            {
                MessageBox.Show("Invalid Qty. Please type a number greater than 0.");
                return false;
            }


            //Length Validation
            if (!double.TryParse(txtPackingLength.Text, out length))
            {
                MessageBox.Show("Invalid length. Please ensure it is a proper number.");
                return false;
            }

            if (length <= 0)
            {
                MessageBox.Show("Invalid length. Please type a number greater than 0.");
                return false;
            }

            //Width Validation
            if (!double.TryParse(txtPackingWidth.Text, out width))
            {
                MessageBox.Show("Invalid width. Please ensure it is a proper number.");
                return false;
            }

            if (width <= 0)
            {
                MessageBox.Show("Invalid width. Please type a number greater than 0.");
                return false;
            }

            //Height Validation
            if (!double.TryParse(txtPackingHeight.Text, out height))
            {
                MessageBox.Show("Invalid height. Please ensure it is a proper number.");
                return false;
            }

            if (height <= 0)
            {
                MessageBox.Show("Invalid height. Please type a number greater than 0.");
                return false;
            }

            //Weight Validation
            if (!double.TryParse(txtPackingWeight.Text, out weight))
            {
                MessageBox.Show("Invalid weight. Please ensure it is a proper number.");
                return false;
            }

            if (weight <= 0)
            {
                MessageBox.Show("Invalid weight. Please type a number greater than 0.");
                return false;
            }
            if (weight > 25 && cboPackingType.Text == "CARTON")
            {
                MessageBox.Show("Weight cannot be greater than 25kg for CARTON");
                return false;
            }
            if (weight > 2500 && cboPackingType.Text == "PALLET")
            {
                MessageBox.Show("Weight cannot be greater than 2500kg for PALLET");
                return false;
            }

            if (!Int32.TryParse(nmrPackingCartonsPerPallet.Value.ToString(), out cartonsPerPallet))
            {
                MessageBox.Show("Invalid cartons per pallet value. Please ensure it is a proper number.");
                return false;
            }

            if (cboPackingType.Text == "PALLET" && cartonsPerPallet <= 1)
            {
                MessageBox.Show("You have entered type PALLET but you haven't changed the number of cartons per pallet. This is needed for invoicing purposes. Please make sure to include this.");
                return false;
            }


            //Est Cost Validation
            string estCostString = txtPackingEstCost.Text;
            if (string.IsNullOrEmpty(estCostString)) estCostString = "0";

            if (!double.TryParse(estCostString, out estCost))
            {
                MessageBox.Show("Invalid estimated cost. Please ensure it is a valid amount. No text or $ sign should be used.");
                return false;
            }

            //if (estCost <= (decimal)0)
            //{
            //    MessageBox.Show("Invalid estimated cost. Estimated costs should be greater than or equal to 0.");
            //    return false;
            //}

            if (length > 220 || width > 220 || height > 220)
            {
                MessageBox.Show("Length, width and/or height too large. Please fix it first.");
                return false;
            }

            if (cboPackingType.Text == "CARTON" && (length > 99 || width > 99 || height > 99))
            {
                if (MessageBox.Show("Please double check that you entered the unit in CM (centimeters). Are you sure you want to go ahead with this?", "WARNING: Entered Dimesion Too High", MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    return false;
                }
            }

            if (cboPackingType.Text == "PALLET" && height > 99)
            {
                if (MessageBox.Show(" Please double check that you entered the height in CM (centimeters). Are you sure you want to go ahead with this?", "WARNING: Entered Dimesion Too High", MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    return false;
                }
            }


            return true;


        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //Add same validation code as the add button
            if (!ValidatePackingEntry())
            {
                this.DialogResult = DialogResult.Cancel;
                this.Close();
                return;
            }

            OrderDespatchService.EditPackingEntry(txtDespatchID.Text, txtPackingID.Text, cboPackingType.Text, Int32.Parse(nmrPackingQty.Value.ToString()),
                double.Parse(txtPackingLength.Text), double.Parse(txtPackingWidth.Text), double.Parse(txtPackingHeight.Text), double.Parse(txtPackingWeight.Text),
                txtPackingConnoteNo.Text.Trim(), double.Parse(estCostString), chkPackingDG.Checked, Int32.Parse(nmrPackingCartonsPerPallet.Value.ToString()), txtPackingComments.Text, this.packedBy);

            this.isDG = chkPackingDG.Checked;

            this.DialogResult = DialogResult.OK;
            this.Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void cboPackingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboPackingType.Text == "PALLET")
            {
                nmrPackingCartonsPerPallet.Enabled = true;
            }
            else if (cboPackingType.Text == "CARTON")
            {
                nmrPackingCartonsPerPallet.Enabled = false;
                nmrPackingCartonsPerPallet.Text = "1";
            }
        }

        private void nmrPackingQty_Enter(object sender, EventArgs e)
        {
            nmrPackingQty.Select(0, nmrPackingQty.Text.Length);
        }

        private void txtPackingLength_Enter(object sender, EventArgs e)
        {
            txtPackingLength.SelectionStart = 0;
            txtPackingLength.SelectionLength = txtPackingLength.Text.Length;
        }

        private void txtPackingWidth_Enter(object sender, EventArgs e)
        {
            txtPackingWidth.SelectionStart = 0;
            txtPackingWidth.SelectionLength = txtPackingWidth.Text.Length;

        }

        private void txtPackingHeight_Enter(object sender, EventArgs e)
        {
            txtPackingHeight.SelectionStart = 0;
            txtPackingHeight.SelectionLength = txtPackingHeight.Text.Length;
        }

        private void txtPackingWeight_Enter(object sender, EventArgs e)
        {
            txtPackingWeight.SelectionStart = 0;
            txtPackingWeight.SelectionLength = txtPackingWeight.Text.Length;
        }


        private void nmrPackingCartonsPerPallet_Enter(object sender, EventArgs e)
        {
            nmrPackingCartonsPerPallet.Select(0, nmrPackingQty.Text.Length);
        }


        private void txtPackingConnoteNo_Enter(object sender, EventArgs e)
        {
            txtPackingConnoteNo.SelectionStart = 0;
            txtPackingConnoteNo.SelectionLength = txtPackingConnoteNo.Text.Length;
        }

        private void txtPackingEstCost_Enter(object sender, EventArgs e)
        {
            txtPackingEstCost.SelectionStart = 0;
            txtPackingEstCost.SelectionLength = txtPackingEstCost.Text.Length;
        }

        private void txtPackingComments_Enter(object sender, EventArgs e)
        {
            txtPackingComments.SelectionStart = 0;
            txtPackingComments.SelectionLength = txtPackingComments.Text.Length;
        }
    }
}
