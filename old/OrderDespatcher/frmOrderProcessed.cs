﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OrderDespatcher.Courier
{
    public partial class frmOrderProcessed : Form
    {
        public frmOrderProcessed()
        {
            InitializeComponent();
        }

        public frmOrderProcessed(string despatchID, string area, string instructions)
        {
            InitializeComponent();

            txtDespatchID.Text = despatchID.Trim();
            lblArea.Text = area;
            txtInstructions.Text = instructions;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnPrintLabel_Click(object sender, EventArgs e)
        {
            PrintService ps = new PrintService();
            ps.RePrintLabelForDespatch(txtDespatchID.Text.Trim());
        }

        private void btnPrintDG_Click(object sender, EventArgs e)
        {
            PrintService ps = new PrintService();
            ps.PrintDangerousGoodsForm(txtDespatchID.Text.Trim());

            MessageBox.Show("Dangerous Goods Form(s) Print Job Sent.");
        }

        private void btnPrintPackingSlip_Click(object sender, EventArgs e)
        {
            PrintService ps = new PrintService();
            if (ps.PrintPackingSlip(txtDespatchID.Text.Trim()))
                MessageBox.Show("Packing Slip Print Job Sent.");
        }

        private void btnPrinterSettings_Click(object sender, EventArgs e)
        {
            frmPrintSettings ps = new frmPrintSettings();
            ps.ShowDialog();
        }
    }
}
